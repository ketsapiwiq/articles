defmodule ArticleEmbeddings do
  @moduledoc """
  This module converts articles to embeddings using the Universal Sentence Encoder.
  """

  alias Tensorflex.Tensor
  alias Tensorflex.Graph
  alias Tensorflex.Session

  @model_url "https://tfhub.dev/google/universal-sentence-encoder/4"
  @model_path "universal_sentence_encoder"
  @input_tensor "serving_default_input"
  @output_tensor "StatefulPartitionedCall:0"

  def download_model do
    System.cmd("curl", ["-L", @model_url, "-o", "#{@model_path}.tar.gz"])
    System.cmd("tar", ["-xzf", "#{@model_path}.tar.gz", "-C", @model_path])
  end

  def load_model do
    Graph.new("#{@model_path}/saved_model.pb")
  end

  def create_session(graph) do
    Session.new(graph)
  end

  def convert_to_embeddings(articles, graph, session) do
    input = Tensor.string_tensor(articles)
    input_map = %{ @input_tensor => input }
    output_map = %{ @output_tensor => Tensor.new }

    Session.run(session, input_map, output_map)
    |> Map.get(@output_tensor)
    |> Tensor.to_list
  end
end

# Download and load the Universal Sentence Encoder model
ArticleEmbeddings.download_model()
graph = ArticleEmbeddings.load_model()
session = ArticleEmbeddings.create_session(graph)

# Example articles
articles = [
  "This is the first article.",
  "This is the second article.",
  "This is the third article."
]

# Convert articles to embeddings
embeddings = ArticleEmbeddings.convert_to_embeddings(articles, graph, session)
IO.inspect(embeddings)
