---
title: "Religion pour les non religieux – Attends mais Pourquoi"
url: https://namelimitsuck.wordpress.com/2016/07/07/religion-pour-les-non-religieux-attends-mais-pourquoi
keywords: supérieur,vérité,religion,conscience,cest,faire,brouillard,conscient,religieux,vie,marche,quil,attends
---
Religion pour les non religieux

Par Tim Urban – billet originellement publié sur le site Wait But Why et traduit par Pen and Spectacles (avec leur autorisation)

L’esprit… peut faire de l’enfer un paradis, et du paradis un enfer. – John Milton

L’esprit est certainement son propre cosmos. – Alan Lightman

Vous allez à l’école, vous travaillez bien, vous avez un diplôme, et vous êtes contents de vous. Mais est-ce vous êtes plus sages pour autant?

Vous trouvez un travail, vous accomplissez des choses au travail, on vous confie des responsabilités, vous êtes payés plus, vous changez de boîte pour une mieux, on vous confie encore davantage de responsabilités, vous êtes payé encore davantage, vous louez un appartement avec une place de parking, vous arrêtez de faire votre lessive vous-mêmes, et vous achetez une de ces boissons genre “bubble tea à 8 euros”. Mais est-ce que vous êtes plus heureux pour autant?

Vous faites des tas de trucs d’adulte – vous faites les courses, lisez des articles, vous allez chez le coiffeur, mâchez des trucs, achetez une voiture, vous vous lavez les dents, chiez, éternuez, vous vous rasez, vous vous étirez, vous buvez des coups, mettez du sel sur des trucs, faites l’amour avec des gens, chargez votre ordinateur portable, allez courir, videz le lave-vaisselle, sortez le chien, achetez un canapé, fermez les rideaux, boutonnez votre chemise, lavez vos mains, fermez votre sac, réglez votre alarme, arrangez vos cheveux, commandez à déjeuner, vous êtes sympa avec quelqu’un, regardez un film, buvez du jus de pomme, et mettez un nouveau rouleau d’essuie-tout dans porte essuie-tout.

Mais tout en faisant ces choses, jour après jour et année après année, est-ce que vous vous améliorez en tant qu’être humain de façon significative?

Dans le dernier post, j’ai décrit la façon dont ma propre voie m’avait amené à devenir athée – comme j’étais si satisfait de me considérer fièrement comme un non religieux, je n’avais jamais sérieusement pensé à une approche active du côté de l’amélioration personnelle – en freinant du même coup mon évolution personnelle.

Ce n’était pas uniquement ma propre naïveté qui était à l’œuvre. La société en général se focalise sur des choses superficielles, donc elle ne prend pas sérieusement en compte le besoin de grandir. Les institutions principales spécialisées dans le domaine spirituel (les religions) tendent à se focaliser sur la divinité au détriment des gens, en faisant du salut le but final au lien d’en faire du développement personnel. Les industries, qui se focalisent souvent sur la condition humaine (philosophie, psychologie, art, littérature, développement personnel, etc.) se concentrent davantage sur la périphérie, et leur travail est souvent fragmenté les uns par rapport aux autres. Tout cela nous amène à un monde qui rend difficile le fait de traiter sa croissance personnelle plus que comme un loisir, un hobby, une cerise extra-scolaire sur le gâteau de la vie.

Sachant que l’esprit humain est un océan de complexité qui crée chacun des aspect de notre réalité, chercher à comprendre ce qu’il se passe là-dedans semble une priorité qui devrait être prise davantage au sérieux. De la même façon qu’un business en pleine croissance se base sur une mission claire avec une stratégie qui a été bien pensée et des résultats concrets, un humain qui grandit a besoin d’un plan : si on veut s’améliorer significativement, il faut qu’on définisse un but, comprendre comment y arriver, devenir conscient des obstacles sur la route, et avoir une stratégie pour les dépasser.

Quand je me suis plongé dans ce sujet, je pense à ma propre situation et si je m’améliorais ou pas. Des efforts ont été faits (comme on peut le voir dans beaucoup des sujets de ce blogs) mais je n’ai pas de modèle de croissance, pas de vrai plan, pas de mission claire. Juste des sorte de tentatives un peu foireuses, au petit bonheur, de pour m’améliorer dans tel ou tel domaine, juste quand j’en avais envie. Donc j’ai commencé à consolider mes efforts éparpillés, philosophies et stratégies, dans une structure unique : quelque chose de solide auquel me raccrocher à l’avenir, et je vais utiliser ce post pour me plonger dedans.

Alors posez-vous, attrapez un café, sortez votre cerveau de votre tête et sur la table en face de vous : il va vous le falloir à portée de main comme référence pour explorer cet objet étrange et compliqué.

\_\_\_\_\_\_\_\_\_\_\_\_

Le but
======

La Sagesse. On va y revenir.

Comment on arrive au but?
=========================

En étant conscient de la vérité. Quand je dis “la vérité”, je suis pas l’une de ces personnes chiantes qui disent le mot “Vérité” pour signifier quelque chose de mystique et d’informe: je réfère tout simplement aux simple faits de la réalité. La vérité est cette combinaison de ce que nous savons et de ce que nous ne savons pas : et prendre conscience de ces deux aspects de la réalité, et garder cette conscience, est la clé pour devenir sage.

Facile, non? On n’a pas à savoir plus que ce que l’on sait, on a juste à être un peu plus conscient de ce que qu’on est et ce que l’on est pas. La vérité est juste là, sous nos yeux, écrite au tableau : il suffit de le voir et de réfléchir dessus. Mais il y a juste un tout petit problème…

Qu’est ce qui nous bloque?
==========================

Le Brouillard.
==============

Pour comprendre le brouillard, il faut être très clairs. On est pas là:

![evolution](https://namelimitsuck.files.wordpress.com/2016/07/evolution.png?w=660)

On est là:

![evolution-plus](https://namelimitsuck.files.wordpress.com/2016/07/evolution-plus.png?w=660)

Et la situation ce n’est pas ça:

![consciousness-binary](https://namelimitsuck.files.wordpress.com/2016/07/consciousness-binary.png?w=660)

Conscience/ Inconscience/ Un koala

C’est ça:

![consciousness-spectrum](https://namelimitsuck.files.wordpress.com/2016/07/consciousness-spectrum.png?w=660)

Moins conscient – Un koala – Moi – sur le spectre de la Conscience – Plus conscient

C’est vraiment un concept hyper difficile à comprendre pour les humains, mais c’est le point de départ pour avancer. Nous déclarer “conscient” nous permet d’y mettre un point final et d’arrêter d’y penser. J’aime à y penser comme un escalier de la conscience:

![big-staircase2](https://namelimitsuck.files.wordpress.com/2016/07/big-staircase2.png?w=660)

Une fourmi est plus consciente d’une bactérie, un poulet plus qu’une fourmi, un singe plus qu’un poulet, et un humain plus qu’un singe. Mais qu’est-ce qu’il y a au-dessus de nous?

A) Définitivement bien quelque chose et B) Rien que nous ne puissions comprendre mieux que la façon dont un singe peut comprendre notre monde et comment nous pensons.

  

Il n’y a aucune raison de penser que l’escalier ne s’étend pas vers le haut pour toujours. L’extraterrestre rouge quelques marches au-dessus de nous verrait la conscience humaine de la même façon que nous voyons celle d’un [orang-outan](https://namelimitsuck.wordpress.com/2016/07/04/les-primates-awards-attends-mais-pourquoi/) : il pourrait penser que nous sommes assez impressionnants pour un animal, mais que bien entendu on n’est même pas au début du commencement du quart de capter quoi que ce soit. Notre scientifique le plus génial serait dépassé par l’un de leurs maternelles.

 

Pour l’extraterrestre vert plus haut là sur l’escalier, l’extraterrestre rouge semble aussi intelligent et conscient qu’un poulet pour nous. Et quand l’extraterrestre vert nous regarde, il nous voit comme la plus simple des fourmis pré-programmées.

Nous ne pouvons pas concevoir à quoi ressemble la vie plus haut dans l’escalier, mais comprendre le fait que des marches plus hautes existent et d’essayer de nous envisager du point de vue de l’une de ces marches est un point de vue-clé que nous avons besoin d’adopter pour cet exercice de pensée

Pour l’instant, ignorons ce qu’il se passe sur les marches beaucoup plus hautes (sur la marche vert clair). Une espèce sur cette marche pourrait penser de nous que nous sommes comme une enfant de trois ans : émergeant à la conscience à travers un flou de simplicité et de naïveté. Imaginons qu’un représentant de cette espèce aie été envoyée pour observer les humains et de faire un rapport à sa planète sur nous : que penserait-il de la façon dont nous nous comportons et dont nous pensons? Qu’est-ce qui en nous l’impressionnerait? Qu’est-ce qui le ferait tiquer?

Je pense qu’il verrait très rapidement un conflit se déroulant dans l’esprit humain. D’un côté, toutes ces marches en dessous de l’humain sont celle d’où nous venons. Des centaines de millions d’années d’adaptation de l’évolution orientée vers la survie animale dans un monde hostile sont en quelque sorte incrustée dans notre ADN, et les instincts primitifs en nous ont donné naissance à un tas de qualités plutôt basses dans l’échelle : peur, mesquinerie, jalousie, cupidité, gratification immédiate, etc. Ces qualités sont les reliquats de notre passé animal et ont toujours une place prépondérante dans nos cerveaux, créant tout un zoo d’émotions et de motivations de petit cerveau  dans nos têtes:

![normal-animal-brain1](https://namelimitsuck.files.wordpress.com/2016/07/normal-animal-brain1.png?w=660)

Mais au cours des derniers six millions d’années, notre évolution nous a fait gagner exponentiellement en conscience et cette capacité incroyable à raisonner, d’une façon qu’aucune autre espèce sur Terre ne peut le faire. Nous avons franchi un grand pas dans l’escalier de la conscience, très vite : appelons cet élément bourgeonnant de conscience supérieure notre Être Supérieur.

![higher-being](https://namelimitsuck.files.wordpress.com/2016/07/higher-being.png?w=660)

L’Être Supérieur est génial, il voit grand et sur le long terme, et il est totalement rationnel. Mais dans la grande frise de la vie, c’est un nouvel arrivant dans nos têtes, tandis que les animaux primitifs sont là depuis bien longtemps, et leur coexistence avec l’esprit humain en fait un endroit très étrange:

![animal-higher-being1](https://namelimitsuck.files.wordpress.com/2016/07/animal-higher-being1.png?w=660)

Donc ce n’est pas qu’un humain est un Être Supérieur et que l’Être Supérieur a 3 ans : c’est qu’un humain est une combinaison de l’Être Supérieur et des animaux des niveaux inférieurs, et ils se mélangent pour former l’enfant de trois ans que nous sommes. L’Être Supérieur seul serait une espèce bien plus avancée, et les animaux seuls seraient une espèce bien plus primitive, et c’est leur coexistence particulière qui nous rend distinctement humains.

En même temps que les humains ont évolué et que l’Être Supérieur a commencé à se réveiller, il a regardé autour de lui dans notre cerveau et s’est trouvé dans une étrange jungle inconnue, pleine de créatures primitives et puissantes qui ne comprenaient pas qui il était, ce qu’il était. Sa mission était de vous donner de la clarté et une pensée d’un haut niveau, mais avec les animaux qui piétinaient tout autour de son espace de travail, ce n’était pas un boulot facile. Et les choses n’allaient pas tarder à empirer gravement. L’évolution humaine a continué à rendre l’Être Supérieur de plus en plus clairvoyant, jusqu’à ce qu’un jour, il réalise quelque chose de terriblement choquant:

NOUS ALLONS MOURIR
==================

Cela a marqué la première fois qu’une espèce quelconque sur la planète TERRE était assez consciente pour comprendre cet état de fait, et il a jeté tous ces animaux dans notre cerveau (qui n’étaient pas construits pour gérer ce genre d’information) dans un état de panique frénétique, et a plongé l’écosystème tout entier dans le chaos.

![chaotic-brain1](https://namelimitsuck.files.wordpress.com/2016/07/chaotic-brain1.png?w=660)

Les animaux n’avaient jamais ressenti ce genre de peur auparavant, et leur panique à ce sujet (qui continue encore aujourd’hui) était la dernière chose dont l’Être Supérieur avait besoin pour grandir, apprendre et prendre des décisions pour les autres pensionnaires.

Les animaux shootés à l’adrénaline qui chargent en tout sens dans notre cerveau peuvent envahir notre esprit, obscurcir nos pensées, notre bon sens, notre identité et notre compréhension du monde. La force collective des animaux est ce que j’appelle “le brouillard”. Plus les animaux sont aux commandes et nous rendent sourds et aveugles aux pensées et à la perspective de l’Être Supérieur, et plus le brouillard est épais autour de notre tête, et souvent le brouillard est tellement épais que nous ne pouvons pas voir plus loin que le bout de notre nez:

![fog-head](https://namelimitsuck.files.wordpress.com/2016/07/fog-head.png?w=660)

Réfléchissons à nouveau à notre but et à la façon\* d’y arriver : être conscient de la vérité. L’Être Supérieur peut tout à fait voir la vérité, dans à peu près n’importe quelle situation. Mais quand nous sommes dans le brouillard, qui obstrue nos yeux et nos oreilles et brouille notre cerveau, nous n’avons pas accès à l’Être Supérieur et à sa mise en perspective. Voilà pourquoi le fait d’être constamment conscient de la vérité est si difficile : nous sommes trop perdus dans le brouillard pour le voir ou y penser.

Et quand le représentant des extraterrestres aura fini de nous observer et qu’il va retourner dans sa planète, je pense que son bilan de nos problèmes sera le suivant:

La bataille de l’Être Supérieur contre les animaux (d’essayer de voir à travers le brouillard et d’atteindre la clarté)  est le centre de notre lutte en tant qu’être humain.

La lutte dans nos tête se déroule sur plusieurs fronts. Nous avons examiné certains d’entre eux ici: L’Être Supérieur (dans son rôle de Preneur de Décision Rationnelle) se battant contre le Singe de la Récompense Immédiate; l’Etre Supérieur (dans le rôle de la Voix Authentique) qui se bat contre un Mammouth de la Survie Sociale terrifié ; le message de l’Etre Supérieur selon lequel la vie est juste un tas de Aujourd’hui qui se perdent dans la lumière aveuglante de notre aspiration à des lendemains plus beaux. Ils font tous partie d’un conflit plus essentiel qui est celui de notre passé primal et de notre futur évolué.

Le truc le plus naze quand on est dans le brouillard, c’est qu’il obscurcit notre vision, et on ne voit pas qu’on est dans le brouillard. C’est quand le brouillard est le plus épais que l’on est le moins conscient qu’il existe même un brouillard. Il vous rend inconscient. Etre conscient que le brouillard existe et apprendre à le reconnaître est la première étape cruciale pour s’élever au niveau de conscience supérieur et devenir quelqu’un de plus sage.

Nous avons donc établi que notre but est la sagesse, et que pour y arriver nous devons devenir aussi conscient de la vérité que possible, et que ce qui se tient entre nous et ce but, c’est le brouillard. Nous allons zoomer dans le champ de bataille et la raison pour laquelle “être conscient de la vérité” est si important et comment nous pouvons surmonter le brouillard pour y arriver:

Le Champ de Bataille
====================

Peu importe nos tentatives pour y arriver, il serait impossible pour les humains d’accéder à cette marche vert clair au dessus de nous dans l’escalier de la conscience. Notre capacité avancée (l’Être supérieur) n’y est tout simplement pas encore. Peut-être qu’il y arrivera dans un million d’années ou deux. Pour l’instant, le seul endroit où cette bataille peut se dérouler, c’est sur la marche où nous nous trouvons, donc c’est là où nous allons nous focaliser. Nous devons nous concentrer sur le mini spectre de conscience qui est sur notre marche, ce que nous pouvons faire en divisant notre marche en 4 sous-marches:

![substeps](https://namelimitsuck.files.wordpress.com/2016/07/substeps.png?w=660)

La marche de la conscience humaine / Le mini-escalier de la conscience humaine 

L’ascension de ce mini escalier de la conscience est le chemin qui mène à la vérité, la voie de la sagesse, ma mission de développement personnel et tout un tas de sentences clichées que je ne pensais pas que j’allais m’entendre dire un jour. Il faut juste que nous comprenions le jeu et que nous nous mettions à bosser dur pour devenir meilleur.

Jetons un coup d’œil à chaque marche pour essayer de comprendre les défis que nous avons à gérer et comment nous pouvons faire des progrès:

Marche 1 : La vie dans le Brouillard
====================================

La Marche 1 est la marche la plus basse, avec le plus de brouillard, et malheureusement pour la plupart d’entre nous, c’est notre niveau d’existence par défaut. Sur la Marche 1, le brouillard est en plein dans ta face, épais, juste devant nous et il obstrue nos sens, en nous réduisant à passer inconscients à travers la vie. Là bas, les pensées, les valeurs, et les priorités de l’Etre Supérieur sont complètement perdues dans le brouillard aveuglant et le bruit assourdissant des rugissements, roucoulades, glapissement, hurlements, et croassements des animaux dans votre tête.

Cela nous rend 1) étroits d’esprit 2) myopes, et 3) stupides. Discutons de chacun des trois:

1) Sur la Marche 1, vous avez le champ très peu libre parce que ce sont les animaux qui font la loi.
----------------------------------------------------------------------------------------------------

Quand je regarde la grande palette d’émotions motivantes dont les humains font l’expérience, je ne les vois pas comme une constellation dispersée, mais plutôt comme deux boîtes différentes: les émotions élevée, basées sur l’amour, et avancées de l’Être Supérieur et les émotions basses, basées sur la peur et primitives des animaux de notre cerveau.

Et sur l’Etape 1, nous sommes complètement intoxiqués par les émotions animales et ils nous hurlent dessus à travers le brouillard épais.

![animals-in-fog](https://namelimitsuck.files.wordpress.com/2016/07/animals-in-fog.jpg?w=660)

C’est ce qui nous rend mesquins et jaloux, et qui nous fait tant apprécier le malheur des autres. C’est ce qui nous rend effrayés, anxieux, et mal dans notre peau.  Voilà pourquoi nous sommes égoïstes et narcissiques; superficiels et avares; on ne voit pas plus loin que le bout de notre nez, on juge les autres, on est froids, insensibles et même cruels. C’est seulement à sur la Marche 1 que nous ressentons ce tribalisme primitif à base de “c’est eux ou nous” qui nous fait haïr les gens qui sont différents de nous.

\* *

On peut trouver la plupart de ces mêmes émotions dans un clan de

On peut trouver la plupart de ces mêmes émotions dans un clan de [singes capucins](https://namelimitsuck.wordpress.com/2016/07/04/les-primates-awards-attends-mais-pourquoi/) – et elles font sens parce que fondamentalement, ces émotions peuvent être réduites à deux clés fondamentales de l’instinct animal: la préservation de soi et le besoin de se reproduire.

Les émotions de la Marche 1 sont brutales et puissantes, elles vous attrapent au collet et quand elles sont sur vous, l’Être Supérieur et ses émotions élevées et basées sur l’amour sont balancés aux oubliettes*. **  
**  
*

2) Sur la Marche 1, on est myope, parce que le brouillard est à 10 centimètres de notre visage, et il nous empêche d’avoir une vue d’ensemble.
----------------------------------------------------------------------------------------------------------------------------------------------

Le brouillard explique tout un tas de comportements humain complètement illogiques et honteusement myopes de notre part.

Pour quelle autre raison quiconque pourrait-il croire qu’un grand-parent ou un parent sera toujours là quand ils sont encore vivants, et donc ne les voir que de temps en temps, ne s’ouvrir à eux que rarement, et ne pas leur poser de questions… même si, quand ils seront morts, on pensera tout le temps au fait qu’ils étaient géniaux, et au fait qu’on a pas profité de l’opportunité de profiter de notre relation avec eux et de mieux les connaître alors qu’ils étaient en vie?

Pourquoi donc les gens se vantent autant, même s’il est évident que s’il pouvaient avoir une vue d’ensemble, il paraîtrait clair que chacun arrive à trouver des bonnes choses dans la vie dans tous les cas, à un moment – et que l’on se rend davantage service en étant modeste?

Pourquoi donc quelqu’un ferait-il le strict minimum au boulot, pour tirer au flanc lors des projets de taf, et être malhonnêtes sur leurs efforts… quand quiconque qui regarderait les choses sous un angle plus large saurait que dans un environnement de travail, la vérité sur les habitudes de travail de quelqu’un finissent toujours par devenir évidente à la fois aux supérieurs et aux collègues, et que vous ne faites jamais illusion pour qui que ce soit? Pourquoi quiconque insisterait tellement pour être sûr que tout le monde sache qu’il a fait quelque chose d’utile à l’entreprise…quand il devrait être évident que cette attitude est grillée, puisqu’elle montre que vous travaillez dur juste pour vous faire bien voir, alors que vos bonnes actions et que celles-ci soient remarquées font bien plus pour votre réputation à long terme et votre niveau de respect au sein de l’entreprise?

Si ce n’était pas à cause du brouillard épais, pourquoi est-ce que quiconque ferait de minuscules économies sur une note de restaurant ou de garder des comptes de façon rigide et désagréable, sur qui a payé quoi pendant un voyage, quand quiconque qui lit ceci pourrait donner à chacun de leurs amis une note rapide de 1 à 10 sur une échelle de radin à généreux, et que les quelques centaines d’euros que vous avez économisé en étant du côté radin de l’échelle n’en vaut pas vraiment la peine considérant combien il est plus agréable et respectable d’être généreux?

Quelle autre explication pourrait-il y avoir pour la décision complètement inexplicable de tellement d’hommes célèbres, dans des positions de pouvoir, de flinguer la carrière et le mariage qu’il ont passé leur vie à construire en ayant une maîtresse?

Et pourquoi quiconque voudrait faire plier ou perdre leur intégrité pour des résultats extrêmement minimes, quand leur intégrité affecte leur estime de soi à long terme, et que les petits profits qu’ils en ont tiré ne leur apportent rien sur le long terme?

 

Comment pourrait-on expliquer autrement la décision de tellement de gens de laisser la peur de ce que les autres pensent diriger la façon dont ils vivent, quand pourtant ils pourraient se rendre compte s’ils voyaient clairement que A) c’est une raison horrible de faire ou de ne pas faire quelque chose, et B) personne ne pense à vous comme ça… ils sont trop préoccupés par leur propre vie.

Et puis il y a toutes les fois ou les rideaux opaques gardent certaines personnes dans le mauvais travail/relation/ville/appartement/amitiés, etc. pendant des années, parfois des décennies, pour finalement faire enfin un changement et dire “j’arrive pas à croire que j’ai pas fait ça plus tôt”, ou “j’arrive pas à croire que je ne voyais pas à quel point il n’était pas pour moi.” Ils devraient le croire pourtant, parce que ça, c’est le pouvoir du brouillard.

3) Sur la Marche 1, tu es très, très stupide.
---------------------------------------------

L’une des manières dont cette stupidité est visible est que nous continuons à refaire les mêmes erreurs encore et encore.

\*Note: Sans transition, après avoir fini mon plan pour ce billet, j’ai estimé que l’écriture me prendrait environ 10 heures, ce qui est bizarre, parce que cela fait 8 heures que j’écris et je suis toujours à la Marche 1. On pourrait penser qu’après avoir écrit 50 billets au cours de l’année précédente et sous-estimé le temps que chacun d’entre eux me prendrait, je pourrais ne pas avoir été aussi catastrophiquement déconnecté de la réalité. On pourrait.

Si on continuait la réflexion, on pourrait aussi se dire que hier, quand il fallait que j’aille de la 116ème rue au World Trade Center à un événement auquel je ne pouvais absolument pas être en retard (NDT: les deux sont exactement d’un bout à l’autre de Manhattan et le trajet en vélo prend environ une heure), j’aurais pu prendre en compte le fait qu’il me faut environ une heure de trajet avant de partir pour l’événement avec seulement 40 minutes d’avance. On est d’accord que c’est ce qu’on serait en droit de penser. Donc pourquoi est ce que je faisais la course pour arriver à l’heure? Pourquoi est-ce que je me suis retrouvé en sueur pour arriver à l’heure (pour la 9 millionième fois de ma vie) quand cette expérience totalement infamante et désagréable aurait pu être si facilement évitée? Parce que quand je prend ces décisions inexplicablement stupide et vouées à l’échec, je nage en plein brouillard. \*

 

L’exemple le plus évident est la façon dont le brouillard arrive à nous convaincre, encore et encore, que certaines choses vont nous rendre heureux alors qu’en réalité absolument pas. Le brouillard aligne une rangée de carottes, nous dit qu’elles sont la clé du bonheur, et nous dit d’oublier le bonheur d’aujourd’hui au profit de tout le bonheur futur contenu dans ces futures carottes que nous allons obtenir.

Et même si le brouillard nous a prouvé de nombreuses fois qu’il n’a aucune idée de la façon dont le bonheur humain fonctionne : même si nous avons fait tellement de fois l’expérience d’enfin obtenir une carotte et de ressentir tout un tas de bonheur temporaire, juste pour voir ce bonheur revenir à notre niveau de bonheur d’avant… Nous continuons à tomber dans le panneau.

C’est comme d’engager un nutritionniste pour vous aider pour vos problèmes d’épuisement, et qu’il vous dise de boire un expresso à chaque fois que vous êtes fatigué. Et vous allez essayer, et penser que le nutritionniste est un génie jusqu’à ce qu’une heure plus tard ça voue lâche comme une enclume et que vous soyez de nouveau épuisé. Vous retournez chez le nutritionniste, qui vous donne le même conseil, et la même chose arrive. Et après…c’est tout, n’est-ce pas?  Vous allez virer le nutritionniste. N’est-ce pas? Alors pourquoi vous êtes aussi naïfs quand il s’agit des conseils du brouillard sur le bonheur et l’accomplissement de soi?

Le brouillard est aussi beaucoup plus nuisible que le nutritionniste parce qu’il ne se contente pas de nous donner de très mauvais conseils: mais c’est de la faute du brouillard lui-même si nous sommes malheureux. La seule vraie solution à l’épuisement est de dormir, et le seul vrai moyen d’améliorer notre bonheur de façon durable est de faire des progrès dans la lutte contre le brouillard.

Il y a un concept en psychologie qui s’appelle le “Tapis roulant du Bonheur”, qui suggère que les humains ont un niveau de bonheur par défaut et que quand quelque chose de bon ou de mauvais arrive, après un changement initial du niveau de bonheur, nous retournons toujours à ce niveau par défaut. Et sur la Marche 1, c’est tout à fait vrai bien entendu, étant donné qu’essayer de devenir heureux de façon permanente tout en étant dans le brouillard est comme d’essayer de se sécher tout en étant sous une douche en marche.

Mais je refuse de penser que la même espèce qui construit des gratte-ciels, écrit des symphonies, vole jusqu’à la lune, et comprenne ce que c’est qu’un boson de Higgs soit incapable de descendre du tapis roulant et de s’améliorer véritablement.

Je pense que le moyen d’y arriver, c’est d’apprendre à grimper cet escalier de la conscience pour passer plus de temps sur les Marches 2, 3 et 4, et moins de temps perdus et inconscients dans le brouillard.

**Marche 2:  Affiner le brouillard pour révéler le contexte**
-------------------------------------------------------------

Les Humains peuvent faire quelque chose d’extraordinaire qu’aucune autre créature sur terre ne peut faire : ils peuvent imaginer. Si vous montrez un arbre à un animal, il verra un arbre. Seul un humain peut imaginer le gland qui a été enfoncé dans la terre il y a 40 ans, la petite pousse fragile qu’il était à 3 ans, la  force qu’il lui faut pour résister quand c’est l’hiver, et les éventuels arbres morts qui sont couchés autour de lui à ce même endroit.

C’est la magie de l’Être Supérieur dans nos têtes.

D’un autre côté, les animaux dans notre tête, tout comme leurs équivalents dans le monde réel, voient uniquement un arbre, et quand ils en voient un, ils réagissent instantanément sur la base de leurs besoins primaires. Quand vous êtes sur la Marche 1, votre subconscient animal ne se souvient même pas que l’Être Supérieur existe, et ses capacité géniales sont gâchées.

La Marche 2 consiste en priorité à éclaircir le brouillard juste assez pour faire émerger les pensées de l’Être Supérieur et ses capacités à notre conscience, nous permettant de voir derrière et autour des choses qui arrivent dans notre vie. A la Marche 2, il s’agit d’amener du contexte à votre conscience, ce qui révèle une version de la vérité bien plus nuancée et bien plus approfondie.

Il y a plein d’activités ou de projets qui peuvent aider à éclaircir le brouillard. Pour en nommer trois:

### 1) En apprendre plus sur le monde par l’éducation, les voyages et votre expérience de vie –

à mesure que la perspective s’élargit, l’on peut voir une version plus claire et plus exacte de la vérité.

### 2) La réflexion active.

Voilà ce à quoi un journal peut aider, ou une thérapie, qui consiste en gros à examiner votre propre cerveau avec un expert en brouillard. Parfois, une question hypothétique peut être utilisée comme des “lunettes à brouillard”, et vous permet de voir quelque chose de façon claire à travers le brouillard. Des questions comme “Qu’est-ce que je ferais si l’argent n’était pas un problème?” ou “Comment est-ce que je conseillerais quelqu’un d’autre sur cette question?” ou “Est-ce que je vais regretter de ne pas avoir fait cette chose quand j’ai 80 ans?” Ces questions sont un moyen de demander l’opinion de votre Être Supérieur sur quelque chose sans que les animaux se rendent compte de ce que vous faites, donc ils restent calmes et l’Être Supérieur peut vraiment parler : comme quand un parent épelle un mot en face d’un enfant de 4 ans quand ils ne veulent pas que celui-ci comprenne ce dont ils parlent.

\*Note: j’ai une technique bizarre, je fais semblant d’avoir un cadran que je peux régler où je peux régler un certain nombre d’heures de sommeil, j’appuie sur le bouton et instantanément on est tant d’heures plus tard et j’ai dormi le nombre d’heures sur le cadran, et je peux continuer ce que je faisais. Je suis un oiseau de nuit, mais si j’avais ce cadran, j’irais au lit à 23h et je dormirais 8h presque tous les soirs de l’année. Cela m’isole du fait que je ne suis pas un oiseau de nuit, en fait (mon Être Supérieur veut dormir de 11 à 7) c’est juste que j’ai une résistance myope à aller au lit basée sur le brouillard. Qui se résume probablement fâcheusement à la peur de la mort d’une façon ou d’une autre.\*

### 3) Méditation, exercice, yoga, etc.

des activités qui nous aident à calmer le bavardage inconscient du cerveau, c’est-à-dire à permettre au brouillard de se stabiliser.

Pour aller sur la Marche 2, il faut se souvenir de rester conscient du contexte derrière ce que l’on voit et tout autour de ce que l’on voit, de ce que l’on rencontre, et des décisions que l’on prend. C’est tout : rester conscient du brouillard et se souvenir de regarder tout le contexte vous maintient alerte, conscient de la réalité, et comme vous allez le voir, une bien meilleure version de vous même que ce que vous êtes sur la Marche 1. Quelques exemples:

Voilà à quoi ressemble un caissier impoli sur la Marche 1 par rapport à la Marche 2:

![cashier](https://namelimitsuck.files.wordpress.com/2016/07/cashier.jpg?w=660)

Sur la Marche 1: Il été impoli ENVERS MOI. PERSONNE n’est impoli ENVERS MOI. – Naaaaan – NAAAAAN / Sur la Marche 2: Ce type a pas une vie facile. Peut-être que sa journée était naze. Ou son enfance. -Naaaaan – Les choses sont ce qu’elle sont

Voilà à quoi ressemble la gratitude:

![gratitude2](https://namelimitsuck.files.wordpress.com/2016/07/gratitude2.jpg?w=660)

Marche 1: LA vie est trop injuste. Also, c’est bizarre, il y a de la neige par terre. / Choses que je veux – Marche 2 Oh. -Choses que je veux.

Il se passe quelque chose de cool:

![good-thing](https://namelimitsuck.files.wordpress.com/2016/07/good-thing.jpg?w=660)Marche 1: Tout est génial pour toujouuuurs – Marche 2: Ceci fait partie de cela

Marche 1: Tout est génial pour toujouuuurs – Marche 2: Ceci fait partie de cela

Il se passe quelque chose de pas cool:

![bad-thing](https://namelimitsuck.files.wordpress.com/2016/07/bad-thing.jpg?w=660)

Marche 1: La vie ça va être nul à partir de maintenant – Marche 2: Ceci fait partie de cela

Phénomène où tout semble tout d’un coup horrible tard dans la nuit dans son lit:

![late-night](https://namelimitsuck.files.wordpress.com/2016/07/late-night.jpg?w=660)

Marche 1 J’AI PEUR DE TOUT POURQUOI J’AI DIT CE TRUC POURQUOI JE ME FOUT AUTANT LA HONTE – Marche 2 – Parfois tard le soir le cerveau humain pète un plomb et pense J’AI PEUR DE TOUT POURQUOI J’AI DIT CE TRUC POURQUOI JE ME FOUT AUTANT LA HONTE et je sens que mon cerveau fait ça en ce moment-même. Ah… le cerveau! 

Un pneu crevé:

![flat-tire](https://namelimitsuck.files.wordpress.com/2016/07/flat-tire.jpg?w=660)

Marche 1: C’EST BIEN MA VEINE JE SUIS UNE VICTIME – Pneu crevé – Marche 2 : 10 à 15 trucs relous arrivent chaque année, apparemment on s’en débarrasse d’un aujourd’hui. Énervant mais bon ça arrive. – Pneu crevé

Les conséquences à long terme:

![consequences1](https://namelimitsuck.files.wordpress.com/2016/07/consequences1.jpg?w=660)

Marche 1: Ha-Ha! Je m’en suis sorti! Et sans répercussions. Juste ces petites choses bizarres. ACTION

Marche 2: ACTION =&gt; REPERCUSSIONS

Regarder le contexte nous rend conscient de ce que nous savons en vérité de la plupart des situations (et aussi de tout ce que nous ne savons pas, comme par exemple la façon dont s’est passé la journée du caissier jusqu’ici), et il nous rappelle la complexité et la nuance des gens, de la vie et des situations. Quand nous sommes sur la Marche 2, cette perspective plus grande et cette clarté augmentée nous rend plus calmes et moins effrayés par des choses qui ne sont pas effrayantes en réalité, et les animaux (qui puisent de la force dans la peur et puisent leur force de notre inconscience) ont soudain l’air assez ridicules:

![animals-clump](https://namelimitsuck.files.wordpress.com/2016/07/animals-clump.jpg?w=660)

Quand les émotions étroites d’esprit des animaux est moins dans notre face, les émotions plus avancées de l’Etre Supérieur (amour, compassion, humilité, empathie, etc.) commencent à s’éclairer.

La bonne nouvelle, c’est qu’il n’y a pas besoin d’une connaissance quelconque pour être à la Marche 2: notre Être Supérieur connait déjà le contexte autour de toutes ces situations. Il n’y a pas besoin d’un travail acharné, et pas d’information additionnelle ou d’expertise dont nous aurions besoin : vous avez juste besoin de penser consciemment au fait d’être que la Marche 2 au lieu d’être sur la Marche1 et vous y êtes. Vous y êtes probablement rien qu’en lisant ça.

La mauvaise nouvelle, c’est qu’il est extrêmement difficile de rester sur la Marche 2 longtemps. Le cercle vicieux ici, c’est que ce n’est pas facile de rester conscient du brouillard parce que le brouillard rend inconscient.

C’est le premier défi à relever. Vous ne pouvez pas vous débarrasser du brouillard, et vous ne pouvez pas toujours le maintenir au-dessus de votre champ de vision, mais vous pouvez devenir meilleur pour vous rendre compte quand il est épais et développer des stratégies pour l’affaiblir quand vous vous concentrez consciemment dessus. Donc vous arrivez à évoluer, en vieillissant, vous allez passer davantage de temps sur la Marche 2 et de moins en moins de temps sur la Marche 1.

Marche 3: le Choc de la Réalité
===============================

Moi… un univers d’atomes… un atome dans l’univers. – Richard Feynman

\_\_\_\_\_\_\_\_\_

La Marche 3 c’est là où les choses commencent à devenir cheloues. Même dans l’illumination de la Marche 2, on pense en quelque sorte qu’on est là:

![happy-earth-land](https://namelimitsuck.files.wordpress.com/2016/07/happy-earth-land.png?w=660)

Aussi sympa que ça puisse paraître, c’est une illusion totale. Nous vivons nos journées comme si nous étions sur cette terre verte et brune avec notre ciel bleu et nos chiens de prairie et nos chenilles. Mais en fait, voilà ce qu’il se passe:

![little-earth](https://namelimitsuck.files.wordpress.com/2016/07/little-earth.png?w=660)

Et en vrai de vrai, voilà ce qu’il se passe:

![IDL TIFF file](https://namelimitsuck.files.wordpress.com/2016/07/hubble_ultra_deep_field_nicmos.jpg?w=660)

On a aussi tendance à penser qu’il se passe ça

![life-timeline](https://namelimitsuck.files.wordpress.com/2016/07/life-timeline.png?w=660)

Alors qu’en vrai, c’est ça:

![long-timeline](https://namelimitsuck.files.wordpress.com/2016/07/long-timeline.png?w=660)

Frise de votre vie – Une éternité de Néant

Vous devez aussi penser que vous êtes quelque chose, hein?

![thing](https://namelimitsuck.files.wordpress.com/2016/07/thing.png?w=660)

Oui. Je suis Quelque Chose.

Non. Vous êtes une tonne de ça:

![atom](https://namelimitsuck.files.wordpress.com/2016/07/atom.png?w=660)

C’est la nouvelle itération de vérité dans notre petit escalier, et nos cerveaux ne peuvent pas vraiment le gérer. Demander à un humain d’intérioriser l’immensité de l’espace et de l’éternité du temps en même temps que de la petitesse des atomes, c’est comme de demander à un chien de se tenir sur ses pattes arrières : On peut le faire si on se concentre, mais ça demande un effort et on ne peut pas le faire pendant très longtemps.

On peut réfléchir aux faits à n’importe quel moment : le Big Bang c’était il y a 13.8 millions d’années, ce qui est à peu près 130 000 fois plus longtemps que tout l’existence humaine; si le soleil était une balle de ping-pong à New York, l’étoile la plus proche de nous serait une balle de ping-pong à Atlanta (NDT: ou pour un point de vue européen, une balle à Paris et une balle à Rome); la Voie Lactée est si grande que si l’on faisait un modèle réduit à l’échelle de la taille des Etats-Unis, on aurait quand même besoin d’un microscope pour voir le soleil; les atomes sont si petits qu’il y a à peu près autant d’atomes dans un grain de sable qu’il y en a de grains de sable sur toutes les plages de la planète. Mais de temps en temps, quand vous réfléchissez bien à l’un de ces faits, ou quand vous êtes au beau milieu d’une conversation tard le soir avec la bonne personne, ou quand vous observez les étoiles, ou quand vous réfléchissez un peu trop à ce que la mort signifie vraiment : vous avez un moment Whoa.

Un vrai moment Whoa, est difficile à obtenir et encore plus difficile à maintenir pendant très longtemps, comme les difficultés de notre chien sur deux pattes. Penser à ce niveau de réalité c’est comme de regarder une photo géniale du Grand Canyon; un moment Whoa c’est comme être au Grand Canyon : les deux expériences sont similaires mais quelque part immensément différentes. Les faits peuvent être fascinants, mais votre cerveau attrape la réalité seulement pendant un moment Whoa. Pendant un moment Whoa, votre cerveau transcende pendant une seconde ce pour quoi il a été construit pour faire et vous offre un bref aperçu de la réalité stupéfiante de notre existence. Et un moment Whoa est ce que l’on obtient sur la Marche 3.

J’adore les moment Whoa. Ils me font me sentir une combinaison intense de respect mêlé d’admiration, d’allégresse, de tristesse et d’émerveillement. Plus que tout, ces moments me font me sentir ridiculement et profondément humble : et ce niveau d’humilité peut faire des choses étranges à quelqu’un. Dans ces moments-là, tous ces mots que les gens croyants utilisent (respect mêlé de crainte, action de grâce, miracle, connexion éternelle), font tout à fait sens. Je veux me mettre à genoux et me rendre. Voilà ce qu’il se passe quand je me sens spirituel.

Et dans ces moment éphémères, il n’y a plus de brouillard : mon \*\*\*\*

Supérieur est pleinement conscient et peut tout voir avec une clarté parfaite. Le monde normalement compliqué de la moralité est soudain clair comme de l’eau de roche, parce que les seules émotions palpable sur la Marche 3 sont uniquement les plus hautes qui existent. Toute forme de haine ou de mesquinerie est un concept risible sur la Marche 3 : quand ils n’ont plus le brouillard pour les cacher, les animaux sont complètement nus, révélés pour ce qu’ils sont, c’est-à-dire des toutes petites créatures.

![animals-embarrassed](https://namelimitsuck.files.wordpress.com/2016/07/animals-embarrassed.png?w=660)

Sur la Marche 1, j’envoie chier ce caissier malpoli, qui a eu l’audace d’être un con à mon égard. Sur la Marche 2, son manque d’éducation ne me déroute pas, parce que je sais que c’est lui, pas moi, et que je n’ai aucune idée de comment s’est passé sa journée, ou sa vie. Sur la Marche 3, je me vois comme un arrangement miraculeux d’atomes dans un vaste espace qui pour une fraction  de seconde dans l’éternité sans fin s’est rassemblée pour former un moment de conscience qu’est ma vie… et je vois ce caissier comme un autre moment de conscience qui se trouve exister sur la même fraction d’espace et de temps que moi. Et la seule émotion possible que je pourrais avoir à son égard sur la Marche 3 est l’Amour.

.![cashier-2](https://namelimitsuck.files.wordpress.com/2016/07/cashier-2.png?w=660)

Oh Âme infiniment précieuse!

Dans un moment Whoa et son niveau de conscience transcendant, je vois tout: chaque interaction, chaque motivation, chaque nouvelle avec une clarté inhabituelle. Et les décisions difficiles de la vie deviennent bien plus évidentes. Je me sens sage.

Bien sûr, si c’était mon état normal, j’enseignerais à des moines quelque part sur une montagne en Birmanie, à Myanmar, et je n’enseigne pas à des moines nulle part parce que ce n’est pas mon état normal. Les moments Whoa sont rares, et très rapidement après en avoir eu un, je suis à nouveau en bas, à être un humain normal à nouveau. Mais les émotions et la clarté d’esprit de la Marche 3 sont si puissantes, que même après que voue ayez dégringolé de la marche, il y en a un petit peu qui reste. A chaque fois que vous humiliez les animaux, un petit peu de leur futur pouvoir sur vous diminue. Et voilà pourquoi la Marche 3 est si importante: même si personne que je connais n’est capable de vivre de façon permanente sur la Marche 3, des visites régulières vous aident de façon spectaculaire dans la bataille en cours entre la Marche 1 et la Marche 2, et vous rendent meilleurs et plus heureux.

La Marche 3 est aussi la réponse à quiconque accuse les athées d’être amoraux, cyniques ou nihilistes, ou qui se demande comment les athées peuvent trouver un quelconque sens à leur vie sans l’espoir et la motivation d’une vie après la mort. C’est une vision de  Marche 1 1 d’un athée, où la vie sur Terre est prise comme allant de soi et que l’on pense que toute impulsion ou émotion positive doit forcément venir de circonstances en dehors de la vie. Sur la Marche 3, je me sens immensément chanceux d’être en vie et je n’arrive pas à croire à quel point c’est cool que je suis un groupe d’atomes qui peut réfléchir sur les atomes: sur la Marche 3, la vie elle-même est bien plus que suffisante pour me contenter, me rendre enthousiaste, plein d’espoir, aimant, et aimable. Mais la Marche 3 n’est possible que parce que la science a dégagé la voie, voilà pourquoi Carl Sagan a dit que “la science n’est pas seulement compatible avec la spiritualité; c’est une source profonde de spiritualité”. Dans un sens, la science est le “prophète” de cette façon de penser : celle qui nous révèle de nouvelles vérités et qui  nous donne l’opportunité de nous transformer en y ayant accès.

 

Alors pour récapituler : sur la Marche, vous êtes dans une bulle délirante que la Marche 2 fait éclater. Sur la Marche 2, il y a davantage de clarté sur la vie, mais c’est dans une bulle de folie bien plus grande, une bulle que la Marche 3 fait éclater. Mais la Marche 3 est sensée être une clarté totale, entièrement dépourvue de brouillard: alors comment pourrait-il y avoir une autre Etape?

Marche 4: Le Grand Inconnu
==========================

Si nous atteignons un moment ou nous pensons que nous avons totalement compris qui nous sommes et d’où nous venons, alors nous aurons échoué. —Carl Sagan

*\_\_\_\_\_\_\_\_\_*

Le jeu jusqu’ici a été la plupart du temps d’éclaircir le brouillard pour devenir aussi conscient que possible de ce que nous, en tant que peuple et en tant qu’espèce, nous savons de la vérité:

![step-1-3-circles](https://namelimitsuck.files.wordpress.com/2016/07/step-1-3-circles.png?w=660)

Marche 1- Marche 2- Marche 3 CLARTE 

Sur la Marche 4, on se rappelle de la vérité complète, qui est ceci:

![step-4-circle](https://namelimitsuck.files.wordpress.com/2016/07/step-4-circle.png?w=660)

Tout ce qu’on ne sait pas – Tout ce qu’on sait

Le fait est, tout discussion sur notre réalité en entier (sur la vérité de notre univers et de notre existence) est une illusion totale sans prendre en compte ce gros truc violet qui fait pratiquement toute notre réalité.

Mais vous connaissez les humains: ils n’aiment pas du tout du tout ce grand machin violet. Jamais aimé. Le machin effraie et humilie les humains, et nous avons un très riche historique qui consiste à nier son existence-même, qui est un peu pareil que de vivre sur la plage et de prétendre que l’océan n’existe pas. A la place, on tape du pied par terre et on prétend que maintenant on a tout compris. Du côté religieux, on invente des mythes et on les proclame comme vérité : et même un croyant fervent en sa religion qui lirait ceci et qui croit en la vérité de son livre en particulier serait d’accord avec moi sur la fabrication des autres milliers de livres qui existent. Du côté de la science, on a réussi à être toujours crédule sur le fait que “réaliser à quel point on a eu horriblement tort sur la réalité” est un phénomène du passé.

Avoir notre compréhension de la réalité qui est remise en question par une nouvelle découverte extraordinaire est comme un retournement de situation dans ce roman policier épique que l’humanité est en train de lire, et le progrès scientifique est régulièrement émaillé de ces retournements de situation: La Terre qui est ronde, notre système solaire qui est héliocentrique, et non géocentrique, la découverte des particules subatomiques ou de galaxies en dehors de la nôtre, pour en citer quelques unes. Donc, comment est-il possible que, avec la connaissance de tous ces découvertes capitales que Lord Kevin, l’un des scientifiques les plus grands de l’Histoire, a dit en 1900, “Il n’y a rien de nouveau à découvrir en physique à présent. Tout ce qu’il reste, ce sont des mesures de plus en plus précises. c’est à dire que cette fois, tous les retournements de situation sont en fait terminés.” C’est-à-dire que cette fois, les retournements de situation c’est fini.

\*Note: Certains récusent que ce soit Kelvin qui aie dit cela, en arguant que cela a en fait été dit par un autre grand physicien du 19ème siècle, Albert A. Michelson. Quel qu’en soit l’auteur, c’est un grand scientifique qui l’a dit.\*

Bien entendu, Kelvin avait tout autant tort que tous les autres scientifiques arrogants dans l’Histoire : la théorie de la relativité générale et ensuite la théorie de la physique quantique allaient toutes les deux retourner la science  au cours du siècle suivant.

Même si vous prenons conscience aujourd’hui qu’il y aura encore des retournements de situation dans le futur, nous sommes probablement tentés de penser que nous avons capté la plupart des grandes choses et que nous avons une image bien plus complète de la réalité que les gens qui pensaient que la Terre était plate. Ce qui pour moi, a l’air de cette réaction:

![laughing](https://namelimitsuck.files.wordpress.com/2016/07/laughing.jpg?w=660)

Et le truc, c’est que tout ce que je viens de mentionner est toujours dans le champ de notre compréhension. Comme nous l’avons établi plus haut, comparé à un niveau plus élevé de conscience, nous sommes peut-être comme un enfant de trois ans, un singe, ou une fourmi: alors pourquoi est-ce que nous devrions supposer que nous sommes même capable de comprendre tout ce qu’il y a dans le machin violet? Un singe ne peut pas comprendre que la Terre est une planète ronde, sans parler du fait que le système solaire, la galaxie, ou l’univers existe. Vous pouvez essayer d’expliquer ça à un singe pendant des années que ce ne serait pas possible. Alors qu’est-ce que nous sommes complètement incapable de comprendre, même si une espèce plus intelligente faisait tout ce qu’elle pouvait pour nous l’expliquer? Probablement presque tout.

Il y a en réalité deux options quand on réfléchit sur la grande, grande vue l’ensemble: être humble ou être absurde.

Le truc absurde quand on pense aux humains qui font semblant d’atteindre la certitude parce qu’ils ont peur, c’est parce que avant, quand on pensait en apparence que nous étions au centre de toute la création, et que l’incertitude était effrayante parce qu’elle faisait que notre réalité semblait bien plus lugubre que ce que ce que nous pensions : mais maintenant, avec tant de choses qui restent mystérieuses, les choses semblent vraiment très lugubre pour les gens en tant que peuple ou en tant qu’espèce, donc notre peur devraient accueillir l’incertitude. Étant donné que mon point de vue par défaut est que j’ai une petite poignée de décennies qui me restent à vivre puis une éternité de non-existence, le fait que nous avons peut-être totalement tort semble extrêmement encourageant pour moi.  .

De façon ironique, quand ma pensée atteint le haut de son escalier qui est basé sur l’athéisme, la notion que quelque chose qui nous semble divin puisse exister ne me semble plus si ridicule. Je suis toujours totalement athée en ce qui concerne les conceptions humaines d’une force divine supérieure, qui selon moi inclut beaucoup trop de certitude. Mais si une force super-avancée existait? Cela me semble plus que probable. Est-ce que nous pourrions avoir été créé par quelqu’un/quelque chose de plus grand que nous, ou vivre dans une simulation sans s’en rendre compte? Bien sûr! Je suis un enfant de trois ans, qui suis-je pour le nier?

Pour moi, la logique complète et rationnelle me dit d’être un athée pour les religions de la Terre et complètement agnostique à propos de la nature de notre existence ou de la possible existence d’un être supérieur. Je n’arrive pas à cette conclusion via une quelconque forme de foi, juste par la logique.

Je trouve que la Marche 4 est mentalement époustouflant mais je ne suis pas sûr que je vais être jamais capable d’y accéder de façon spirituelle de la même façon que j’accède à la Marche 3 (les moments Whoa de la Marche 4 sont peut-être réservées aux penseurs du niveau d’Einstein) mais même si je ne peux même pas mettre les pieds sur la Marche 4, je sais que ça existe, ce que ça veut dire, et je peux me souvenir de son existence. Alors qu’est-ce que cela me fait en tant qu’humain?

Eh bien vous vous souvenez de cette humilité profonde que j’ai mentionné à la Marche 3? Eh bien multipliez-le par 100. Pour les raisons dont j’ai parlé, cela me rend plein d’espoir. Et cela me laisse résigné positivement sur le fait que je ne comprendrai jamais ce qu’il se passe, ce qui me fait penser que je peux enlever les mains du volant, m’installer relax, et profiter du voyage. De cette façon, je pense que  la Marche 4 peut nous faire vivre davantage dans le présent: si je suis juste une molécule flottant dans un océan que je ne peux pas comprendre, autant m’amuser.

\* *

La Marche 4 peut servir l’humanité en l’aidant à écrabouiller la notion de certitude. La certitude est primitive, mène au tribalisme du “Nous contre Eux” et mène aux guerres. Nous devrions être unis dans l’incertitude, et non pas divisés autour d’une certitude fabriquée. Et plus les humains vont se retourner et regarder ce gros machin violet, et mieux on sera.

Pourquoi la sagesse est le But
==============================

Il n’y a rien qui éclaircit le brouillard comme un lit de mort, ce qui explique pourquoi c’est à ce moment là que les gens peuvent toujours voir avec plus de clarté ce qu’ils ne l’auraient fait différemment: j’aurais aimé passer moins de temps à travailler; j’aurais aimé communiquer davantage avec ma femme; j’aurais aimé voyager plus, etc. Le but de croissance personnelle devrait être de gagner cette clarté d’esprit que l’on a sur notre lit de mort pendant que votre vie est toujours en train de se dérouler et que l’on peut y faire quelque chose.

On fait ça en développant le plus de sagesse possible, et le plus tôt possible. Pour moi, la sagesse est la chose la plus importante à travailler en tant qu’être humain. C’est le grand objectif: c’est le but générique sous lequel tous les autres buts se placent. Je pense que j’ai une seule vie et une seule chance de la vivre, et je veux le faire de la façon la plus épanouissante et profondément satisfaisante possible : c’est la meilleure issue pour moi, et je fais bien plus de bien au monde de cette façon. La Sagesse donne aux gens l’intuition de savoir ce que “épanoui et profondément satisfaisant” veut dire en réalité et le courage de faire les choix qui vont les y mener.

Et pendant que l’expérience de la vie peut contribuer à la sagesse, je pense que la sagesse est la plupart du temps déjà en entier dans nos têtes, c’est tout ce que l’Être Supérieur sait. Quand nous ne sommes pas sages, c’est parce que nous ne nous n’avons pas accès à la sagesse de l’Être Supérieur parce qu’elle est entièrement dans le brouillard. Le brouillard est anti-sagesse, et quand vous grimpez les marches de l’escalier pour arriver à un endroit plus clair, la sagesse est simplement un produit de cette conscience améliorée.

Une chose que j’ai appris à un moment que le fait de vieillir ou de grandir en taille n’est pas la même chose que de grandir. Être un adulte, c’est votre niveau de sagesse et la taille de la portée de votre esprit: et il s’avère que cela n’est pas forcément corrélé avec l’âge. A partir d’un certain âge, grandir c’est surpasser votre brouillard, et c’est une histoire de personne, pas d’âge. Je connais certaines personnes plus âgées qui sont géniales, mais il y a aussi beaucoup de gens de mon âge qui semblent beaucoup plus sages que leur parents au sujet de plein de choses. Quelqu’un qui est sur le chemin de la croissance et dont le brouillard s’éclaircit à mesure qu’ils prennent de l’âge vont devenir plus sage à mesure qu’ils vieillissent, mais je me suis rendu compte qu’il se passe le contraire avec les gens qui ne grandissent pas activement: le brouillard se resserre autour d’eux et ils deviennent vraiment moins conscient, et deviennent encore plus certain à propos de tout, avec l’âge.

Quand je pense aux gens que je connais, je réalise que mon niveau de respect et d’admiration pour quelqu’un est presque toujours dû à la façon dont je pense que cette personne est sage et consciente. Les personnes que je tiens en plus haute estime sont les adultes dans ma vie, et leurs âges varient complètement.

Un autre regard sur la Religion au vu de cette structure:
---------------------------------------------------------

Cette discussion aide à clarifier mes soucis avec la religion organisée traditionnelle. Il y a des tas de gens bien de bonnes idées, de bonnes valeurs, et de bonnes sagesses dans le monde religieux, mais pour moi cela me semble quelque chose qui arrive en dépit de la religion et non pas à grâce à elle. En utilisant la religion comme support de croissance demande une façon innovante de voir les choses, étant donné que fondamentalement, la plupart des religions semblent traiter les gens comme des enfants au lieu de les pousser à grandir. La plupart des religions d’aujourd’hui jouent avec le brouillard des gens à coup de “Crois en ceci ou sinon…” qui font peur, et des livres qui sont souvent un cri de ralliement pour la division “Nous contre Eux”. Ils disent aux gens de regarder des écritures anciennes pour trouver les réponse au lieu des profondeurs de l’esprit, et leur certitude entêtée quand il s’agit du bien et du mal les laisse bien souvent au fond du troupeau quand on parle d’évolution et de problèmes de société. Leur certitudes quand il s’agit de l’Histoire finit par éloigner activement leurs fidèles de la vérité: j’en veux pour preuve les [42% d’Américains](http://www.gallup.com/poll/21814/evolution-creationism-intelligent-design.aspx) qui ont été privés de la vérité de l’Evolution. (un criminel encore pire dans l’escalier, c’est le monde pathétique de la politique américaine, avec une culture qui vit à sur la Marche 1 et où les politiciens en appellent directement aux animaux des gens, [en évitant délibérément](https://www.youtube.com/watch?v=VW188SVN5UU) les Marches 2-4.)

Alors qu’est-ce que je suis?
============================

Oui, je suis un athée, mais l’athéisme n’est pas plus un modèle de croissance que “Je n’aime pas le roller” est une stratégie pour faire du sport.

Donc, je suis en train d’inventer un terme pour ce que je suis: je suis un Véritiste. Dans ma façon de penser, je cherche toujours la vérité, la vérité est ce que j’adore, et apprendre à voir la vérité plus facilement est ce qui me permet de grandir.

Dans le Véritisme, le but est de devenir plus sage au fil du temps, et la sagesse vous tombe dessus quand vous êtes assez conscient pour voir la vérité sur les gens, les situations, le monde, ou l’univers. Le brouillard est ce qui se trouve sur votre route, qui vous rend inconscient, illusionné, et étroit d’esprit, donc la stratégie pour grandir jour après jour, c’est de rester toujours conscient du brouillard et d’entraîner votre esprit à essayer de voir toute la vérité sur chaque situation.

Au fur et à mesure, il faudrait que le \[Temps sur la Marche 2\] / \[Temps sur la Marche 1\] augmente petit à petit chaque année, et aussi devenir meilleur pour déclencher des moments Whoah de la Marche 3, et se souvenir de la Marche 4 du machin violet. Donc si vous faites ces choses, je pense que vous évoluerez de la meilleure façon possible et que cela aura de profond effets sur tous les aspects de votre vie.

C’est tout. C’est le Véritisme.

Est-ce que je suis un bon véritiste? Ça va. Je suis meilleur que je l’ai été et j’ai encore beaucoup à faire. Mais définir le cadre va aider: je sais où mettre ma concentration, de quoi me méfier, et comment évaluer mes progrès, qui vont m’aider à m’assurer que je m’améliore vraiment et ce qui mène à une croissance plus rapide.

Pour m’aider à rester sur ma mission, j’ai fait un logo du Véritisme:

![logo](https://namelimitsuck.files.wordpress.com/2016/07/logo.png?w=660)

C’est mon symbole, mon mandra, mon QFJAMP (NDT: Que Ferait Jésus A Ma Place?) : c’est ce vers quoi je peux regarder quand des choses bonnes ou mauvaises arrivent quand un décision importante arrive, ou pendant une journée normale comme rappel de rester conscient du brouillard et de garder mon attention sur la vue d’ensemble.

Et Vous, Vous Etes Quoi?
========================

Mon défi pour vous c’est de décider d’un terme pour vous-même qui résume précisément votre structure de croissance.

Si le Christianisme c’est votre truc, et que ça vous aide véritablement à grandir, ce terme peut peut être Chrétien. Peut-être que vous avez déjà votre stratégie de développement bien défini, et vous avez juste besoin d’un nom à mettre dessus. Peut-être que le Véritisme vous parle, ressemble à la façon dont vous pensez déjà, et vous voulez vraiment essayer d’être Véritistes avec moi.

Ou peut-être que vous n’avez aucune idée de ce qu’est votre structure de croissance, ou celle que vous utilisez ne marche pas pour vous. C’est soit parce que A) vous ne pensez pas que vous avez évolué de façon positive ces dernières années ou B) vous ne pouvez pas réconcilier vos valeurs et votre philosophie de la vie avec de vraies réflexions qui vous importent, alors vous devez trouver une nouvelle structure.  

Pour ce faire, posez-vous simplement les mêmes questions que je me suis posées moi-même: Quel est le but vers quoi vous tendez (et pourquoi est-ce ça le but), à quoi ressemble le chemin qui vous y mène, qu’est-ce qui se dresse sur votre route, et comment vous surmontez ces obstacles? Quelles sont les pratiques de façon quotidiennes, et à quoi devrait ressembler votre progrès année après année? Après que vous ayez vraiment réfléchi à ça, donnez un nom à la structure et faites un symbole ou un mantra. (Et ensuite partagez votre stratégie dans les commentaires ou envoyez-moi un email à ce propos, parce que le fait de l’articulez aide à le clarifier dans votre tête, et parce que c’est utile et intéressant pour les autres d’entendre parler de votre structure.)

J’espère que je vous ai convaincu d’à quel point c’est important. N’attendez pas d’être sur votre lit de mort pour comprendre le sens de la vie.

Posts Wait But Why reliés, traduits prochainement :

Wait But Why posts each month. We send each post out by email to over 220,000 people—enter your email [**here**](http://waitbutwhy.us7.list-manage1.com/subscribe?u=250cab41702ae3ef7a2c1c965&id=5b568bad0b) and we’ll put you on the list (we only send a few emails each month). You can also follow Wait But Why on [**Facebook**](http://www.facebook.com/waitbutwhy) and [**Twitter**](http://www.twitter.com/waitbutwhy)

Advertisements

### Share this:

-   [](https://namelimitsuck.wordpress.com/2016/07/07/religion-pour-les-non-religieux-attends-mais-pourquoi/?share=twitter "Click to share on Twitter")

    Twitter

-   [](https://namelimitsuck.wordpress.com/2016/07/07/religion-pour-les-non-religieux-attends-mais-pourquoi/?share=facebook "Click to share on Facebook")

    Facebook

-   

### Like this:

Like

Loading...

### Related
