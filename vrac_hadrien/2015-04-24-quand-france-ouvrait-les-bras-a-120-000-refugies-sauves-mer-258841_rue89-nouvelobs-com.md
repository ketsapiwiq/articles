---
title: "Quand la France ouvrait les bras à 120 000 réfugiés sauvés en mer"
url: http://rue89.nouvelobs.com/2015/04/24/quand-france-ouvrait-les-bras-a-120-000-refugies-sauves-mer-258841
keywords: 120,sudest,étrangers,ouvrait,sartre,pays,france,quil,dun,réfugiés,bras,000,mer,sauvés,travail
---
Imaginez l’image : les plus grands intellectuels du pays, de droite comme de gauche, unis pour lancer un appel à sauver les migrants menacés de naufrage en Méditerranée, et à les accueillir à bras ouverts en France !

Impensable aujourd’hui, et pourtant, ça s’est produit. Oui, notre pays a été exemplaire il y a plus de 35 ans, et n’a jamais eu à le regretter.

C’était en 1979, et la crise des boat people d’Indochine dominait le « 20 heures » de nos télés, avec des images de familles entières dans des embarcations de fortune en mer de Chine, menacées par les pirates, les requins, les intempéries... Des Vietnamiens et des Cambodgiens fuyant le communisme et les persécutions ethniques, rackettés pour pouvoir partir, sans savoir où aller.

#### « Des hommes en danger de mort »

Et le miracle se produisit. Jean-Paul Sartre et Raymond Aron, deux intellectuels que tout opposait jusque-là, entourés de toute l’intelligentsia de l’époque, réunis autour d’une même cause. Oubliant un moment leurs divergences politiques à une époque où, pourtant, elles dessinaient des camps autrement plus antagoniques qu’aujourd’hui.

Ecoutez cette interview de Jean-Paul Sartre au « 20 heures » : il explique qu’il se met au service « d’hommes en danger de mort », abandonnant ses « opinions politiques ». Une autre France...

 

Jean-Paul Sartre et les boat people

Quelques mois plus tôt, cette coalition hétéroclite s’était mise en place pour affréter un bateau, avec Médecins sans frontières, pour sillonner la mer de Chine et porter secours et assistance aux boat people en détresse.

 

Bernard Kouchner et Yves Montand

Ce bateau, L’île de lumière, a vu le jour et a effectué sa mission humanitaire en mer de Chine.

Mais dans le contexte particulier de cette fin des années 70, l’élan humanitaire français ne s’arrêta pas là.

Conformément à des décisions internationales, la France prit sa part, et même plus, pour accueillir certains de ces réfugiés qui étaient regroupés, après avoir été sauvés en mer, dans des camps installés dans les pays de la région – Hong Kong, Malaisie, etc.

![Boat people en mer de Chine, sauvs par le bateau humanitaire franais Ile de lumire](https://media.nouvelobs.com/ext/uri/sreferentiel.nouvelobs.com/file/rue89/12c9e8be2c09a7d59f7b6ee914289f9b.jpg)Boat people en mer de Chine, sauvés par le bateau humanitaire français Ile de lumière - FRANCOIS GRANGIE/AFP

#### 128 531 réfugiés admis en France

Si vous n’avez pas la mémoire de ce moment-là, vous ne devinerez jamais combien de ces réfugiés d’Indochine la France a accueilli et a aidé à s’installer et s’intégrer sur son sol. Pas quelques centaines, pas quelques milliers, non : 128 531 Vietnamiens, Cambodgiens et Laotiens entrés légalement en France.

Dans un texte publié en 2006, Karine Meslin, maître de conférence au Centre nantais de sociologie, [relève le paradoxe](http://www.gisti.org/spip.php?article217#nb4) :

« En France, l’immigration de travail vient tout juste d’être suspendue et la crise économique que traverse le pays semble peu propice à l’accueil de nouveaux étrangers. Pourtant, après des mois ou des années d’attente, 128 531 ressortissants de l’ancienne Indochine, dont 47 356 Cambodgiens, entrent légalement sur le territoire français.

Dès leur arrivée, ils bénéficient d’un accueil d’exception accompagné de discours empathiques, que formulent les dirigeants politiques notamment. »

Et elle s’interroge :

« Comment comprendre cette situation pour le moins paradoxale  ? Comment expliquer que des réfugiés aient pu bénéficier d’un tel accueil alors qu’il est admis qu’“en période d’incertitude tout particulièrement, l’étranger est ressenti comme une menace pour le groupe enraciné” \[selon la formule de l’historien de l’immigration Gérard Noiriel, ndlr\] ? »

#### Mobilisation exceptionnelle

La chercheuse rappelle le dispositif exceptionnel que la France de Valéry Giscard d’Estaing avait mis en place pour les accueillir :

« En France, de nouveaux traitements de faveur et de nouvelles dérogations se font jour. L’arrivée des réfugiés de l’ancienne Indochine est d’abord l’occasion d’institutionnaliser une collaboration étroite entre les associations, chargées de l’accueil des réfugiés, et l’Etat, responsable de leur sécurité.

Ainsi, après un court séjour obligatoire dans un des quatre foyers de transit parisiens où ils sont accueillis, les réfugiés peuvent décider de se “débrouiller” par eux-mêmes ou rester sous la tutelle des associations mobilisées pour leur cause.

Dans ce cas, ils sont hébergés dans des centres provisoires d’hébergement (CPH) pour une durée minimum de trois mois au terme desquels, à l’échelle municipale, des comités d’accueil se chargent d’accompagner leurs démarches quotidiennes.

Dans un même temps, de nombreuses mesures facilitent leur mise au travail. Sur le terrain juridique, les conditions d’obtention de leur carte de travail provisoire et renouvelable n’obéissent pas à la procédure habituelle.

Alors que les demandeurs d’asile en sont privés jusqu’à ce que le titre de réfugié politique leur soit délivré, la politique des quotas offre aux réfugiés en question un droit de travail immédiat. De plus, tandis que les migrants – réfugiés ou non – sont, à cette époque, dans l’obligation d’être titulaires d’un contrat de travail d’une durée d’un an pour obtenir une carte de travail (non provisoire), un contrat de trois mois, même à temps partiel, suffit aux réfugiés du Sud-Est de l’Asie.

Des cellules ANPE leur sont également réservées et des mesures incitatives, notamment financières, sont prises à l’égard des employeurs pour favoriser leur embauche. Tous ces dispositifs objectifs s’accompagnent par ailleurs de discours compassionnels et bienveillants, nettement distincts de ceux, plus stigmatisants, qui traitent des autres étrangers à l’heure où l’immigration de travail vient d’être suspendue. »

#### Anticommunisme et mauvaise conscience

Une bonne partie de la communauté asiatique de France, notamment dans le XIIIe arrondissement de Paris, est issue de cette vague d’immigration des boat people fuyant les anciennes colonies françaises d’Indochine.

Certes, le contexte joue énormément. Tant celui de la guerre froide qui fait que des réfugiés fuyant le communisme reçoivent une oreille plus favorable dans les pays occidentaux, que celui spécifique de l’Indochine après la découverte des atrocités commises par les Khmers rouges, dont le monde a tardé à prendre réellement conscience.

L’anticommunisme de la droite et la mauvaise conscience d’une gauche qui avait soutenu les mouvements de libération devenus à leur tour oppresseurs ont permis ce rare consensus au service d’une grande cause humanitaire.

Comme l’écrit Karine Meslin :

« La mobilisation autour des réfugiés cambodgiens, laotiens et vietnamiens est exceptionnelle à plus d’un titre. Non seulement les modalités d’accueil des réfugiés du Sud-Est asiatique ne sont “pas réellement basée(s) sur l’interprétation classique de la Convention de Genève, mais sur la volonté de l’Etat”. Mais de plus, la nébuleuse d’acteurs impliqués dans cet accueil ne fait pas partie des défenseurs traditionnels des étrangers.

Ce premier constat ne doit néanmoins pas dissimuler le second. L’exemple des réfugiés du Sud-Est asiatique permet aussi de montrer qu’une nouvelle fois, le sort des étrangers est étroitement lié aux intérêts nationaux et ce, quels que soient le statut juridique de ces étrangers ou les raisons de leur venue.

Par ailleurs, l’accueil, basé sur la mobilisation gouvernementale, dont ont bénéficié les réfugiés du Sud-Est de l’Asie a eu de nombreuses incidences. Outre qu’il a facilité leurs premiers pas en France, il a participé à modeler le regard porté sur eux et à légitimer leur arrivée en France.

La qualité de cet accueil semble avoir été perçue comme ajustée à la qualité intrinsèque des étrangers auxquels il était destiné. Ce constat rappelle, in fine, l’importance des politiques d’accueil et des discours qui accompagnent l’arrivée des nouveaux migrants. »

C’est ce contexte et ces motivations qui manquent singulièrement aujourd’hui dans la crise de la Méditerranée.

Et pourtant, certains des éléments sont communs. Les hommes et les femmes qui fuient la Syrie en guerre – un ancien territoire géré par la France, on l’oublie trop souvent –, ou l’Erythrée en dictature, ou encore la misère de l’Afrique subsaharienne où les responsabilités historiques françaises sont nombreuses, comme [le rappelait](http://rue89.nouvelobs.com/2015/04/23/aminata-traore-accuse-leurope-infernale-comptabilite-macabre-258819) ici même jeudi l’ancienne ministre malienne Aminata Traoré, ne suscitent pas la même empathie (c’est un euphémisme, à voir certains commentaires sur Rue89 jeudi).

#### Où sont les Sartre et Aron d’aujourd’hui ?

Quand on entend le Conseil européen évoquer l’accueil de 5 000 Syriens à répartir parmi le demi-milliard d’Européens, on croit rêver en pensant aux 123 000 Asiatiques reçus en France trois décennies plus tôt. Ou au million de Syriens, et sans doute plus, réfugiés dans le petit Liban, ou le quasi million de Libyens qui se trouvent en Tunisie...

Il est vrai qu’il n’y a pas, aujourd’hui, de personnages de la stature de Jean-Paul Sartre et Raymond Aron pour symboliser cette union sacrée pour une cause jugée noble. Et que lorsqu’on regarde en permanence derrière son épaule si le Front national n’y est pas, on a tendance à intégrer une partie de sa logique.

Ce qui vaut pour la France vaut pour ses voisins : la palme d’or revenant de ce point de vue à David Cameron, qui a [annoncé](http://www.telegraph.co.uk/news/worldnews/europe/eu/11557074/Mediterranean-migrant-crisis-EU-leaders-meet-for-emergency-summit.html) (à quelques jours d’élections difficiles) qu’il enverrait un bateau de la Royal Navy en Méditerranée, mais que tout migrant sauvé par la marine britannique serait déposé sur les côtes du pays le plus proche, vraisemblablement l’Italie. Elégant.
