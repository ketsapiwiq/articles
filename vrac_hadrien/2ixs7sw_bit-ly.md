---
title: "Sealioning: How to Deal with the Time-Wasting Troll Tactic We’re All Tired Of"
url: http://bit.ly/2iXs7SW
keywords: tactic,number,timewasting,swimming,bree,tired,sea,panel,troll,youve,questions,lion,deal,text,sealioning
---
Have you heard of sealioning? If you’ve encountered trolls online, there’s a good chance that you’ve come across it.

The term *sealioning,* coined by David Malki in [his webcomic Wondermark](http://wondermark.com/1k62/), describes someone who pretends to be clueless about an issue in order to waste your time. It can be frustrating to go around in circles with them – and you’ve got better things to do.

So try these tips the next time a troll tries to sealion into your conversation!  
  
With Love,  
The Editors at Everyday Feminism

![](//everydayfeminism.com/wp-content/plugins/a3-lazy-load/assets/images/lazy_placeholder.gif)![](http://everydayfeminism.com/wp-content/uploads/2017/01/sealioning.jpg)

### Click for the Transcript

The Tail of the Sea Lion: Time-Wasting Trolls Drowning Activists in Notifications

An Everyday Feminism Comic by Alli Kirkham

 

**Panel 1**

*(A sea lion swimming upright)*

**Sea lion**: In 2014 David Malki coined the term “sealioning” in his webcomic, Wondermark. The term is used by activists and educators to describe a type of internet troll who pretends cluelessness about an issue in order to waste the time and attention of activists, frustrate them, and sap their emotional energy.

**Panel 2**

*(The sea lion swimming nose-down)*

**Sea lion**: Sea lions are cool mammals but people who participate in sealioning are really irritating, so here are some techniques for dealing with them.

 

**Panel 3**

**Text at top of panel**: Number one – do not engage. If you don’t want to explain something you don’t have to. It takes a lot longer to refute a ridiculous claim than it does to make such a claim and you are under no obligation to spend your time responding to Fox News talking points.

*(Two merfolks are speaking to one another, ignoring the smirking sea lion swimming next to them; one of the merfolks is a gray-haired black mermaid with a deep magenta tail who appears throughout the rest of the comic, her name is Bree)*

**Sea lion**: Wage gaps are a myth! Otherwise companies would just hire women and minorities so they could pay less. How do you explain that?

**Panel 4**

**Text at top of panel**: Number two – redirect them. Sea lions accuse people of being unwilling to defend their own beliefs or refusing to answer questions. Give them a link to a master-post or well-researched article, tell them how they can learn more about the subject, and ask them to make a good-faith effort to understand the subject before they engage you again.

*(Bree is offering a treasure chest full of gold and pearls to the sea lion, who is reaching for it and frowning)*

**Bree**: Here’s a treasure-trove of information, why don’t you look through it and see if the answer to your question is in there somewhere?

Panel 5

**Text at top of panel**: Number three – ask questions. If the sea lion doesn’t seem like they’re making a real attempt to understand the issue and refuses to answer your questions, seriously engage with your requests for information, or responds to your questions with more questions, the conversation is probably a waste of time.

*(The sea lion is looking at Bree while holding a very large fishhook behind its back. Bree is looking at the sea lion skeptically with her hands on her hips.)*

**Bree**: I feel like this is a well-established fact and you might be trying to bait me into a pointless argument. Could you explain what exactly you find confusing, or what you’ve read that addresses the research I’ve showed you?

**Panel 6**

**Text at top of panel**: Number four – Tag team. Ask your friends who have the energy to chime in to join the conversation; just make sure nobody wastes too much time on the argument. Sea lions often head for calmer waters when met with group solidarity.

*(Bree is swimming contentedly with three other merfolk, gesturing to her friends behind her; one is holding a stack of paper ready to use citations in the discussion. The sea lion is facing them and looks uncertain, holding a fin up in a “wait a minute” gesture)*

**Bree**: Here are my friends who are well-read on this topic. Perhaps they can help you understand.

**Panel 7**

**Text at top of panel**: Number five – troll them right back. If you’re sure the sea lion is a troll and not someone legitimately looking for information respond to their cluelessness with cluelessness of your own. Eventually they’ll float away.

*(Bree is swimming past the sea lion looking cheerful; the sea lion has a frustrated frown on its face and has its back to Bree, starting to swim away)*

**Bree**: I’m not sure where you’re confused here. Should I explain what a control group is again? Or is it empiricism as a whole that you’re having trouble with?

**Text at the bottom of the panel**: After all, if someone is that lost sometimes the answer is to just keep swimming. Perhaps these tips will help to shore up your arguments. And I’d apologize for the puns, but it just seems shellfish to clam up about them.

To learn more about this topic, check out:

![](//everydayfeminism.com/wp-content/plugins/a3-lazy-load/assets/images/lazy_placeholder.gif)![](http://everydayfeminism.com/wp-content/uploads/2015/06/PlsShareArrow5.png)Pin4[+1](https://plus.google.com/share?url=https%3A%2F%2Feverydayfeminism.com%2F2017%2F01%2Fsealioning%2F)

6K Shares

*Alli Kirkham is a Contributing Comic Artist for Everyday Feminism and blogger, cartoonist, and intersectional feminist. Alli earned a BA in English Literature from Cal Poly Pomona in 2011 and uses it as an excuse to* [*blog about books*](http://booksmotherfucker.blogspot.com/) *while swearing a lot. When she isn’t cartooning for Everyday Feminism or cursing at popular fiction, she posts cartoons and other silly things on her* [*Tumblr*](http://ms-demeanor.tumblr.com/)*. Check her out on Twitter* [*@allivanlahr*](https://twitter.com/allivanlahr)*.*

**Found this article helpful?**  
Help us keep publishing more like it by [![\#SaveEF](https://everydayfeminism.com/wp-content/uploads/2018/10/Patreon-After-Article.jpg)](https://everydayfeminism.com/membership/)

Help us keep publishing more like it by [becoming a member!](https://everydayfeminism.com/membership/)
