---
title: "« Tu fais quoi dans la vie"
url: https://trolldejardin.wordpress.com/2016/03/23/tu-fais-quoi-dans-la-vie/
keywords: forcément,cest,faire,quon,soirée,vie,classiste,envie,question,travail
---
\[Mise à jour de septembre : Maintenant, j’ai repris des études. Cependant, je trouve toujours cette question intrusive, classiste, et problématique.\]

Récemment, je suis allé à un évènement. Regroupant des gens bienveillants et essayant d’être inclusifs. Mais rapidement, une personne a posé la question « **et vous, vous faites quoi dans la vie ?**« , et un tour de table a démarré (sur un groupe de 10 personnes). Là, les gens ont enchaîné en disant leurs professions, ou leurs études et leurs projets professionnels. Éventuellement les hobbies aussi, mais surtout la profession. Et moi, je me suis éclipsé.

Parce que je suis actuellement chômeur, avec des petits boulots irréguliers (fréquents dans le temps mais ponctuels), avec espoir de reprise d’études l’an prochain (tout dépend de si on m’accepte). Et j’avais pas du tout envie de dire ça devant plus de dix inconnus, alors que les autres parlaient de leur travail d’ingénieur, de prof, de graphiste ou whatever.

J’aurais pu répondre en prenant la question au pied de la lettre, et en parlant de ce que je fais dans ma vie qui est important pour moi même si ce n’est pas professionnel (comme ce blog, ou d’autres choses). Mais les gens auraient bien vu l’esquive, et surtout, quelqu’un aurait peut-être demandé, « mais **sinon**, tu fais quoi, **en vrai**, dans la vie ? ».

Je trouve que cette question est [classiste](https://convergencedeluttes.wordpress.com/2017/09/11/le-classisme-les-pauvres-en-france-kezako/), psychophobe, validiste, et bref, assez nulle. 

Donc, revenons à « tu fais quoi dans la vie ». Cette question est classiste parce qu’elle oblige les personnes au chômage, ou dans un travail considéré comme peu gratifiant par la société, ou un travail irrégulier, à le dire ou à refuser de répondre ou à biaiser. Devant des personnes ayant une situation mieux considérée. **Demander ça, c’est donc déjà une façon d’exclure ou d’humilier des personnes, en fonction de leur statut social et économique**.

Et puis il y a tellement de clichés négatifs sur les les personnes au chômage et /ou qui touchent des aides, les pauvres en général, que les gens n’ont pas forcément envie d’être renvoyés à ça, ni que tout le monde soit au courant.

**Dire « je suis chômeur », c’est aussi s’exposer à des remarques et questions déplacées**. De gens qui donneront des leçons (« il faut se bouger », « tu devrais faire ci », « tu devrais faire ça »…), avec la **condescendance** qui va bien en général. Et **l’âgisme** en bonus quand on est jeune et que la personne en face est adulte d’âge moyen.

Mais tout ça sur un ton pseudo-bienveillant qui fait qu’on est censés dire merci avec le sourire en plus.

C’est **validiste** et **psychophobe**, aussi. Pourquoi ? Parce que beaucoup de personnes handicapées et/ou neuroatypiques :

-   ne peuvent pas travailler

-   pourraient travailler sur le papier, mais n’arrivent pas à trouver un emploi, à le garder…

-   vivent mal leur emploi (qui n’est pas adapté à leurs besoins et capacités spécifiques, où elles sont exclues, discriminées, subissent des micro-agressions…)

-   galèrent avec les études

Et on n’a pas forcément envie ni d’en parler, ni de se le voir rappeler. 

Même lorsque les conseils sont pertinents, ils correspondent souvent à des choses qu’on sait déjà, et on n’a pas forcément envie de les recevoir avec un ton paternaliste.

De plus, souvent, des gens vont nous parler de notre situation professionnelle (par exemple pendant une soirée ou un repas) de manière très intrusive, **alors qu’on ne leur a pas demandé l’heure** et qu’on a juste envie de passer **une bonne soirée, sans penser encore et toujours à nos problèmes**. On n’a pas forcément envie d’être dans le rôle du chômeur (ou de l’handicapé) 24h/24.

Je ne dis pas ça pour jeter la pierre, disons que c’est de la maladresse. Tout est dans la forme.

-   Manière respectueuse et constructive : demander **discrètement** à la personne si elle est intéressée, parce que « un ami cherche quelqu’un pour \[insérer travail\] », ou « il y a telle formation qui pourrait t’aider », et partager une info **concrète**.

      

    Si c’est dans une soirée où les gens s’amusent, mieux vaut peut-être échanger les mails et faire passer l’info par mail, comme ça, la personne peut lire et répondre au calme chez elle, et **profiter de sa soirée en attendant**.

-   Manière intrusive : dire à la personne sur un ton péremptoire qu’elle devrait faire ceci ou cela alors qu’elle n’a rien demandé, mais sans proposer d’info concrète. Et faire ses remarques et questions déplacées **devant tout le groupe histoire de bien afficher la personne**.

Quand je dis « soirée », ça marche aussi pour un mariage, une réunion de famille, un repas associatif…

**« Tu fais quoi dans la vie », c’est classiste aussi** **quand on le demande sur les sites de rencontre**.   
De plus, beaucoup de gens (plus ou moins consciemment) vont juger la personne en face en fonction de sa profession (ou absence de travail) justement, y compris dans les rencontres amoureuses.

On peut s’intéresser à la vie d’une personne et avoir envie de savoir sa profession. **Mais si elle n’en parle pas spontanément, c’est qu’il y a une raison**. Donc il vaut mieux **ne pas poser la question si la personne n’en parle pas**.

Publicités
