---
title: "Rendre visible le corps et ses douleurs : les technologies de l’endométriose. Journal d’un diagnostic (1/5)."
url: https://femtech.hypotheses.org/132
keywords: visible,lexpérience,dun,journal,maladie,lendométriose,technologies,15,dune,symptômes,douleurs,vie,version,diagnostic,corps
---
**La version complète du texte est disponible en PDF. Vous pouvez télécharger la version lisible sur écran [\[clic\]](http://femtech.hypotheses.org/files/2018/09/A5_WEB.pdf) ainsi que la version mise en page pour impression sous forme de cahier [\[clic\]](http://femtech.hypotheses.org/files/2018/09/A5_HD_brochure.pdf).**

« Toujours avec le sourire, Laetitia Milot a accepté de se confier sur son endométriose ». C’est ainsi que débute [un article de Paris Match](https://www.parismatch.com/People/Television/Laetitia-Milot-evoque-son-endometriose-profonde-946186) relatant « les années d’enfer », les « atroces douleurs » et « les nodules dans le rectum, dans le vagin, dans l’utérus et sur les intestins » qu’évoque la comédienne, marraine de l’association EndoFrance.

L’endométriose est une maladie chronique, évolutive et invalidante. Elle se caractérise par un développement de l’endomètre – la muqueuse qui tapisse l’intérieur de l’utérus – en dehors de la cavité utérine : dans la paroi musculaire de l’utérus, sur les ovaires, les trompes ou la cavité péritonéale. Mais aussi sur les intestins, dans le rectum, la vessie, le diaphragme… Ce tissu est sensible aux hormones : par exemple, au moment des règles, il se désagrège et saigne. Cela provoque d’une part des contractions et des réactions inflammatoires, entraînant de très fortes douleurs (pendant les règles et l’ovulation, mais aussi quotidiennement) et, d’autre part, des lésions apparaissent là où ce tissu s’est installé, comme des kystes, des nodules ou des adhérences. La maladie peut s’accompagner de fatigue chronique, de saignements, de douleurs pelviennes chroniques, d’une lombalgie, d’une cruralgie (douleur irradiant dans la jambe), d’une sciatique, de troubles urinaires ou digestifs, de douleurs pendant les rapports sexuels, ou encore d’infertilité.

« “Ce n’est pas mortel mais enfin j’ai un nom sur la maladie que j’ai et je ne suis pas folle” a-t-elle conclu avec son sourire radieux ». Fin de l’article de Paris Match, qui n’a de cesse de souligner le contraste entre le récit douloureux de Laetitia Milot et « le sourire » de cette comédienne « pétillante », « connue et adorée pour sa joie de vivre ».

Ces dernières années, plusieurs femmes célèbres ont parlé publiquement de leur maladie, dans des interviews que les rédactions choisissent d’illustrer par leur beau visage rayonnant et souriant et leurs corps valides. Étrange paradoxe de cette maladie et de sa représentation médiatique où la mise en récit des lésions, douleurs et handicaps s’accompagne le plus souvent d’une absence de mise en image de ses répercussions sur le corps et dans la vie quotidienne.

![](http://femtech.hypotheses.org/files/2018/09/static1.squarespace.com_.jpg) Un extrait de la série Parks and Recreation, souvent utilisé pour illustrer l’endométriose et ses crises de douleurs.

L’expérience d’une maladie invisible
====================================

À bien des égards, l’endométriose me semble être une maladie « invisible ». Elle fait partie de ces maladies chroniques qui ne s’accompagnent pas de symptômes directement visibles à la surface du corps. De plus, beaucoup d’hypothèses sur ses causes potentielles décrédibilisent les femmes en accusant leur mode de vie ou leurs arbitrages entre carrière et maternité. L’endométriose serait tantôt une maladie de femmes carriéristes et égoïstes, ayant trop longtemps retardé le moment de faire un enfant, tantôt due à un rejet de la féminité, une hostilité vis-à-vis des hommes ou encore une peur de la maternité. Enfin, les douleurs perçues comme « féminines » sont souvent réduites à une dimension psychique, et considérées comme une exagération, voire comme un élément constitutif du « triste sort féminin ». Ainsi, l’invisibilité des symptômes et la disqualification des douleurs participent à des retards ou des absences de diagnostic.

Pour les personnes atteintes d’endométriose, c’est une lutte pour la reconnaissance qui s’engage auprès du corps médical. Dans ce contexte d’invisibilité, matérialiser la maladie et les entraves qu’elle provoque dans la vie quotidienne est un élément central de ce travail de légitimation et de compréhension de l’endométriose : il s’agit tout autant de faire reconnaître ses douleurs que de les comprendre.

Ce qui m’intéresse tout particulièrement dans ce processus, c’est la manière dont sont mobilisées des technologies pour produire des connaissances sur un corps et le mettre en visibilité. Il me semble qu’en retour, ces technologies participent à construire les « événements corporels » et façonnent l’expérience corporelle même de la maladie : elles « donnent corps » à l’endométriose.

![](http://femtech.hypotheses.org/files/2018/09/technos-500x238.jpg)

Journal d’un diagnostic
=======================

Mais tout d’abord : d’où je parle ? Je suis chercheuse en sciences humaines et sociales, et atteinte d’endométriose. Dans mes recherches, je mobilise largement les méthodes ethnographiques. Celles-ci qui me servent aussi d’appui en tant que patiente. Je considère les personnes, les paroles, les actes, les techniques et les environnements comme autant d’éléments constituant « un terrain » qui me permet de comprendre la place que j’y occupe et que l’on m’attribue en tant que patiente, tout en m’offrant un regard, situé, sur la construction de cette maladie et de sa prise en charge.

Je me situe du côté des approches féministes des Science and Technology Studies, qui considèrent technique et genre comme des processus sociaux enchevêtrés, constamment négociés dans les interactions entre les individus et avec les objets. Ces approches cultivent un intérêt tout particulier pour rendre visibles des éléments généralement invisibles, considérés comme « naturels » ou « allant de soi ». À propos du corps justement, les chercheuses Elisabeth Grosz et Elisabeth Wilson notent que celui-ci est souvent étudié comme une surface. L’endométriose offre alors des prises pour saisir ce corps dans toute sa matérialité « charnue, viscérale et organique ».

Pour explorer les technologies de l’endométriose et la manière dont elles donnent corps à cette maladie chronique, je me suis donc intéressée à un corps spécifique, le mien, à l’aide d’une ethnographie de mon expérience de l’endométriose, du parcours médical entamé, mais aussi de la lecture de blogs de patientes, de forums, de sites d’association, ou encore d’usages d’applications smartphone. J’ai choisi de vous raconter mon parcours à travers trois étapes, qui mobilisent des technologies différentes.  

Je commencerai par évoquer les prémices de l’endométriose, ces moments où elle n’est pas connue, parfois même pas suspectée. Il s’agit d’abord de mettre en visibilité la souffrance pour la rendre apparente et légitime aux yeux du corps médical. Dans ce processus, la lecture des blogs, sites internet, documents, publications… est centrale, tout comme le passage à l’écriture, pour « objectiver » les symptômes et les rendre communicables.

Les douleurs sont entendues, l’endométriose est suspectée. On entre cette fois dans une phase de diagnostic avec ses technologies d’imagerie médicale – échographie, IRM – qui produisent une image du corps et des lésions. Comment ce corps-à-corps avec des technologies et des praticien-nes trace-t-il les contours de l’endométriose et de son expérience corporelle ?

L’endométriose est avérée, et après ? Dans cette dernière partie, je reviendrai sur la recherche, puis la fabrication d’outils de quantification et de visualisation des symptômes, qui permettent d’en appréhender le rythme et la chronicité.

Et enfin, une conclusion :

Bien que ce parcours soit loin d’être terminé – et toujours constitué de tâtonnements et de bidouillages – je terminerai par une petite réflexion sur le rôle des technologies dans la production de l’expérience corporelle de l’endométriose, et, plus généralement, dans la production et l’expérience du genre.

– Lire la suite : « [Connaître, faire reconnaître : technologies de partage de l’expérience.](https://femtech.hypotheses.org/185) » –

———————–
