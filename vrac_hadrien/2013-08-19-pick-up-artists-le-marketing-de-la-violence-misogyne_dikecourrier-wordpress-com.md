---
title: "Pick Up Artists : le Marketing de la violence misogyne"
url: http://dikecourrier.wordpress.com/2013/08/19/pick-up-artists-le-marketing-de-la-violence-misogyne/
keywords: viol,site,violence,pick,misogyne,marketing,kamal,lapologie,page,femmes,artists,quil,hommes,sexuelle
---
Trigger Warning : ce texte contient, en citation, des propos intolérables incitant à la violence contre les femmes et notamment au viol.

L’industrie de l’exploitation des femmes a peut-être créé le plus vieux métier du monde (le proxénétisme), mais elle n’en finit pas d’innover. Ainsi, depuis le milieu des années 90, elle s’est alliée avec celle du développement personnel pour accoucher du Pick Up Artist, acteur frénétique des « communautés de la séduction ».

Un **Pick Up Artist** est officiellement un « artiste de la drague », bien qu’il s’agisse plutôt d’une machine marketing bien rodée. Mon but n’est pas de décoder le modèle d’affaires ; [cet article le fait très bien](http://www.slate.fr/story/32839/drague-seduction-communaute-fight-club-the-game "La «Communauté de la séduction», le Fight Club de la drague - Vincent Glad"), avec une critique d’un point de vue masculin.

Pour une critique féministe du Pick Up Artist, mieux vaut se renseigner sur le web anglophone. Ainsi, d’après le [Wiki de GeekFeminism](http://geekfeminism.wikia.com/wiki/Pick_Up_Artists "Pick Up Artist - Geek Feminism Wiki (enanglais)"), le **Pick Up Artist** est un membre d’une sous-culture masculine appelée « la Communauté de la séduction », qui apprend aux hommes des tactiques pour séduire les femmes. Celles-ci sont présentées comme des poupées hypersexualisées, compliquées mais manipulables, infidèles en amour, et interchangeables.

Ce business bien rodé exploite le malaise des hommes qui ne se sentent pas à la hauteur des injonctions patriarcales à la performance sexuelle et à la domination des femmes. Car [les hommes souffrent aussi des contradictions du système patriarcal](https://dikecourrier.wordpress.com/2013/07/02/la-crise-de-la-masculinite/ "La Crise de la masculinité").

Le Pick Up Artist (PUA) crée ou avive le malaise des hommes ayant peu d’expérience sexuelle, **pour mieux leur vendre des solutions à ce prétendu malaise** : coaching, formations, livres…

Problème, cela se fait au détriment des femmes, présentées de manière dégradante par l’accumulation permanente de préjugés misogynes (en plus d’être hétérocentrés), et de photos de [femmes objectivées et hypersexualisées](http://antisexisme.net/2013/08/13/objectivation-1-2/ "L’objectivation sexuelle des femmes : un puissant outil du patriarcat - Antisexisme").

Très concrètement, les femmes fréquentées ou ciblées par les clients de PUA sont directement mise en danger, car **les techniques de « séduction » promues encouragent les comportements prédateurs**.

Voici un exemple précis [dénoncé depuis octobre 2012 sur un forum féministe](http://feminisme.fr-bb.com/t746-les-pick-up-artists-nous-apprennent-a-bien-violer "Les pick-up artists nous apprennent à bien violer !! - Forum Féministe") puis sur [le blog de l’Elfe](http://lesquestionscomposent.fr/les-violeurs/ "Les violeurs - Les Questions Composent") :

Kamal Kay est l’un des PUA qui s’attaquent au marché francophone, voire français. Derrière le personnage fabriqué d’ancien loser timide devenu un séducteur charismatique se cache en fait **SBK Coaching**, une société opportunément enregistrée en Pologne, même si ses partenaires sont au Canada et en France.

Sur leur site **Seduction By Kamal**, [cette page](http://www.seductionbykamal.com/comment-bien-baiser/ "Incitation au crime de viol et à la violence masculine misogyne") intitulée « Comment bien baiser : les 3 secrets du hard SEXE » constitue **une apologie du viol et une incitation à la violence contre les femmes**. Faut-il rappeler que le viol est un crime ?

Cette page est restée en ligne depuis mai 2012, soit 16 mois, malgré des signalements dans les commentaires sous l’article (publiés pour certains mais dénigrés ou ignorés). \[EDIT : cet article a finalement été retiré et remplacé par un article soft le 6 septembre 2013, suite à [notre mobilisation](https://dikecourrier.wordpress.com/2013/09/05/appel-citoyen-contre-incitation-viol-internet/ "Appel citoyen contre l’incitation au viol sur Internet") [médiatisée](http://www.rue89.com/rue69/2013/09/05/conseils-bonne-baise-hard-incitation-viol-245422 "Conseils pour « une bonne baise hard » : l’incitation au viol dénoncée - Rue69"). Une sauvegarde de l’article datant du 19 août est conservée [**ici**](https://dikecourrier.files.wordpress.com/2013/08/comment-bien-violer-une-femme-par-seduction-by-kamal-kay-et-jb-marsille1.pdf "Sauvegarde de la page "Comment Bien Baiser" de Seduction By Kamal - Apologie du viol")\]

Comme le dit Antisexisme, il s’agit de conseils pour violer une femme, et même d’une **méthode pour violer une femme**.

Extraits violents. Passons le laïus sur la puissance sexuelle et la domination naturelle de l’homme, et concentrons-nous sur les passages incitant explicitement à la violence physique contre les femmes, notamment au viol, et à l’humiliation :

« Montrez-lui qu’**elle n’a pas vraiment le choix**« 

« **Attaquez** sa poitrine »

« créer rapidement une image du mec qui sait ce qu’il veut et **qui l’obtient quand il veut**« .

« **vous décidez** \[…\] **tout est entre vos mains** (ou vos cuisses devrais-je dire) »

« **perdre tout contrôle de la situation** est un « turn on » majeur pour les femmes ». (Alerte : mythe sur le viol)

« appliquez-vous à aller en profondeur et à **ne stopper la cadence que quand VOUS le décidez** ! **Elle se plaint ? Pas pour longtemps !** C’est un phénomène naturel de rejet de l’autorité, mais **une fois cette barrière franchie, elle s’abandonnera à vous et vous demandera de la défoncer ».**

En utilisant les termes « rejet » et « barrière« , l’auteur reconnait explicitement que le non-consentement d’une femme doit stopper le rapport sexuel, mais invite ses lecteurs à n’en pas tenir compte, en prétendant qu’un NON se transformera en OUI enthousiaste. **Se passer du consentement d’une femme, « c’est ça en fait la véritable notion du fameux BIEN BAISER ».**

« **Imposez** votre puissance ».

« **Donnez des ordres et soyez inflexible**. **Ne lui demandez pas gentiment** si, éventuellement, vous pourriez avoir une fellation et éjaculer dans sa bouche… **La décision est prise**, retirez-vous et **faites la descendre** vers votre sexe afin d’affirmer votre posture. »

« Si seulement vous saviez combien de femmes rêvent de **se faire démonter** par un inconnu au chibre géant ». (Réponse : ZERO femme – 2ème alerte).

« Cette **méthode** est relativement efficace quand on rencontre une inconnue qui nous ramène chez elle. **Si elle en arrive là, c’est sans doute parce qu’au fond, ce qu’elle veut, c’est tirer un coup.** » (3ème alerte)

« **Ne lui demandez pas si vous pouvez la pénétrer** comme un animal sauvage, faites-le ! »

« il vous suffit \[…\] de laisser parler vos envies, **sans vous restreindre**. **Prenez le contrôle du rapport sexuel** et pensez que votre masculinité passe par des **coups de boutoir infligés**. »

« **ne vous refusez rien**« .

Une page insoutenable, et pourtant les commentaires sont globalement enthousiastes.

**Cette méthode de viol met en danger l’intégrité de toutes les femmes** qui vont être prises pour cibles (ou l’ont déjà été) par des lecteurs influençables du site.

Sachant que ce site vise explicitement des **adolescents** parmi leurs segments de clientèle, **des filles mineures sont directement menacées**.

Si cette page vous répugne, je vous invite à la signaler :

-   au Gouvernement français : <http://www.internet-signalement.gouv.fr>. Il n’y a pas d’option « incitation à la haine misogyne », j’ai donc choisi l’option « menaces ou incitation à la violence »

    \[EDIT : ce n’est plus nécessaire comme [indiqué par @Alex\_Le\_Rouge](https://twitter.com/Alex_Le_Rouge/status/369882017279647744)

    « 

    le contenu que vous avez mentionné nous a été récemment signalé à de très nombreuses reprises

    « 

    . Mais vous pouvez continuer à signaler la page aux autres organismes ci-dessous.\]

-   à la Centrale Canadienne de signalement des cas d’exploitation sexuelle des enfants sur Internet : <https://www.cybertip.ca/app/fr/report>. Option « *Rendre accessible à un enfant du matériel sexuellement explicite*« .

Si vous connaissez d’autres sites pertinents, pouvez-vous les indiquer en commentaires ?

Sollicités depuis octobre 2012, **l’auteur de l’article, Jean-Baptiste Marsille alias JB, et le patron Kamal Kay ont refusé de retirer ou modifier cette page**, persistant dans l’apologie du viol.

Or le succès affiché de leur affaire ne repose pas sur le charisme d’un ou deux PUA, mais plutôt **sur une organisation très bien structurée d’entreprises partenaires**. Ces entreprises se répartissent les rôles (et les profits) en fonction de leurs compétences.

Il est important de montrer à toutes ces entités économiques, impliquées dans un projet de collaboration commerciale commun, que la recherche du profit ne peut justifier une contribution même indirecte à la violence contre les femmes.

L’hébergeur du site est **Mavenhosting**, une entreprise canadienne. Compte tenu de son service strictement technique, il ne peut légalement être tenu pour responsable des contenus illicites qu’il héberge. Du moins tant qu’il n’en a pas connaissance. N’hésitez donc pas à **porter à la connaissance de Mavenhosting le caractère violent et illicite d**e la **page qu’ils hébergent**, par ce formulaire de contact : <https://www.mavenhosting.com/support-hosting.html>.

En revanche, l’**Agence CSV** fournit un service de web marketing et de référencement sur Internet. **Elle ne peut donc pas ignorer le contenu des pages ni la stratégie du site ou la cible visée**. Cette agence basée en France appartient à Alexandre Chombeau. Lui aussi tire profit (en termes de revenus et de notoriété) de ce business gynocide qu’il est chargé de promouvoir. Voici d’ailleurs une interview de Kamal par Alexandre Chombeau : <http://www.agence-csv.com/blog/kamal-le-seducteur-monetiser-blog-ebooks>. Une interview complaisante qui expose le positionnement officiel de Seduction By Kamal, mais pas son réel business-model, fondé sur l’exploitation sexuelle des femmes et la manipulation psychologique des hommes en mal de virilité.

Le 16 août, j’ai donc pris contact en privé avec Alexandre Chombeau de l’Agence CSV pour me renseigner sur **ses critères éthiques en matière d’apologie de la violence**. Pour cela, je lui ai proposé de m’aider à promouvoir un site de promotion de la misandrie et l’apologie de la castration des hommes. Je n’ai pas eu de réponse jusqu’à la publication de ce billet…

Je vous invite donc aussi à écrire [un courrier à l’Agence CSV](http://www.agence-csv.com/contact/ "Web marketing pour promouvoir l'apologie de la violence"), pour lui demander de soutenir mon projet (ironiquement, bien entendu) ou **de refuser ses services à tout site qui fait l’apologie de la violence contre les femmes**.

Voici ma lettre satirique à Alexandre Chombeau : [Entreprendre sur le marché du sexisme](https://dikecourrier.wordpress.com/2013/08/19/entreprendre-sur-le-marche-du-sexisme "Entreprendre sur le marché du sexisme").

Voici sa réponse, ainsi que celle de l’auteur de la page Jean-Baptiste, que j’ai gardée en souvenir, même s’ils l’ont effacée au bout de quelques heures : c’est [un modèle de misogynie et de bingo féministe !](https://dikecourrier.files.wordpress.com/2013/08/argumentation-anti-fc3a9ministe-ce-quil-ne-faut-pas-faire.pdf "Argumentation anti-féministe : ce qu'il ne faut PAS faire !")  
———

à lire sur **la culture du viol** :

– Le viol est une pratique banalisée et encouragée dans notre société qui entretient [la culture du viol](http://www.crepegeorgette.com/2013/03/20/comprendre-la-culture-du-viol/ "Comprendre la culture du viol - Crêpe Georgette"). Ce crime reste largement impuni grâce à l’intimidation des victimes, le sentiment d’impunité des agresseurs, et aussi un faisceau de [défaillances de notre système judiciaire](http://lacorrectionnalisationduviol.wordpress.com/ "La correctionnalisation du viol, la négation d'un crime").

– L’apologie du viol sur Internet n’est que le reflet de ce que pensent [les violeurs](http://lesquestionscomposent.fr/les-violeurs/ "Les violeurs - Les Questions Composent"), et de leurs stratégies de viol, plus facilement assumées grâce à l’anonymat. Le fait qu’on les laisse s’exprimer sans retenue sur Internet est le corollaire du fait qu’on les laisse agresser les femmes hors d’Internet.

– Pour en savoir plus, deux séries d’articles très documentés d’Antisexisme, réalisés à partir d’études sociologiques : [Les Mythes sur le viol](http://antisexisme.net/2011/12/04/mythes-sur-les-viols-partie-1-quels-sont-ces-mythes-qui-y-adhere/ "Les mythes autour du viol et leurs conséquences - Antisexisme"), et [Les Cultures enclines au viol](http://antisexisme.net/2013/01/09/cultures-du-viol-1/ "Les cultures enclines au viol et les cultures sans viol").

– Et enfin des témoignages de survivantes de viols perpétrés dans le cadre de relations amoureuses ou de séduction : [La Parade des violeurs est le Silence des violées](https://dikecourrier.wordpress.com/2013/08/24/la-parade-des-violeurs-est-le-silence-des-violees/ "La Parade des violeurs est le Silence des violées"). Un témoignage plus développé ici aussi : [J’ai subi la « bonne baise » (viol) selon Seduction By Kamal](https://dikecourrier.wordpress.com/2013/08/24/mon-violeur-est-un-jeune-homme-de-26-ans/ "J’ai subi la « bonne baise» (viol) selon SeductionByKamal").  
———

à lire sur cette polémique (Seduction By Kamal, apologie du viol, réactions) :

– Résumé de cette polémique par ChaCha : [L’incitation au viol, le sexisme, l’éthique et le SEO](http://riensurchacha.wordpress.com/2013/08/21/lincitation-au-viol-le-sexisme-lethique-et-le-seo/ " L’incitation au viol, le sexisme, l’éthique et le SEO");  et par Sophie Gourion : [PUA : quand un site de drague incite au viol](http://www.toutalego.com/2013/08/pua-quand-un-site-de-drague-incite-au.html "PUA : quand un site de drague incite au viol - Tout à l'ego").

– Si on dénonce l’apologie du viol, vous nous collez une étiquette de « puritaines » : le [Prude Shaming](http://les-hysteriques.overblog.com/le-prude-shaming "Le Prude Shaming - Les Hystériques... ou pas") n’est qu’un instrument de contrôle social des femmes, de leur parole et de leur sexualité.

– Mais si vous nous attaquez ainsi, [nous contre-attaquons](http://lesquestionscomposent.fr/toi-aussi-encourage-le-viol-comme-kamal/ "Toi aussi encourage le viol comme Kamal - Les Questions Composent") : ne sous-estimez pas les féministes ! \#****ToiAussiSéduisCommeKamal****…

———

**Captures d’écran ci-dessous (34 photos) :**

-   copie de l’article faisant l’apologie du viol
-   commentaires enthousiastes de lecteurs sous l’article
-   quelques commentaires critiques ont été ignorés ou dénigrés
-   la seule réaction obtenue a été une lettre d’injures misogynes

**Cliquez sur la première photo pour visualiser le diaporama en grand :**

Publicités
