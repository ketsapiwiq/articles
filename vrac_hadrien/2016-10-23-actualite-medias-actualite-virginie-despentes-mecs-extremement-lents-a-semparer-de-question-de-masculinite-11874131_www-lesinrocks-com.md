---
title: "Virginie Despentes : "Les mecs sont extrêmement lents à s'emparer de la question de la masculinité""
url: http://www.lesinrocks.com/2016/10/23/actualite/medias-actualite/virginie-despentes-mecs-extremement-lents-a-semparer-de-question-de-masculinite-11874131/
keywords: genre,lents,trouve,despentes,virginie,mecs,extrêmement,semparer,virilité,masculinité,question,rapport
---
L’écrivain et cinéaste était l’invitée de « Dans le genre de » sur Radio Nova. Elle explique entre autres, le rapport qu’elle entretient avec son genre, sa féminité, sa masculinité. Elle parle aussi longuement des hommes…

Quelle relation entretient chacun d'entre nous avec son genre, sa féminité, sa virilité?  Tel est le point de départ de [Dans le genre de](http://www.novaplanet.com/radionova/65694/episode-dans-le-genre-de-virginie-despentes), une des nouvelles émissions de la grille de Radio Nova. En une heure, une fois par mois, Géraldine Sarratia, journaliste aux Inrocks, part à la rencontre d’une personnalité qu'elle interroge sur son rapport au genre et à l'identité.

Se trouve-t-elle féminine, virile ? Se sent-elle à l'aise avec les codes de la masculinité ? Comment définit-elle le masculin et le féminin ? Par le biais de quelles identifications, culturelles (acteurs, musiciens, personnages de fictions) mais aussi plus personnelles (familiales, amicales, etc.) s'est-elle construite ?

Première invitée de l'émission, Virginie Despentes. A 47 ans, cette écrivaine et cinéaste est l'une des voix les plus singulières de la littérature française. Depuis la sortie de Baise-moi, son premier roman sorti en 1993, elle bouscule depuis la société française avec ses textes acérés, sa langue orale, brute, qui n'a pas peur de recevoir des coups ou d'en donner. Dans son viseur, la domination masculine, les inégalités sociales ou encore la radicalisation de notre monde contemporain.

À son actif, neuf romans, dont le culte King Kong Theorie et deux longs-métrages. Virginie Despentes achève actuellement l'écriture du troisième tome de Vernon Subutex, un roman-feuilleton qui raconte les aventures de Vernon, disquaire tombé dans la précarité, et radiographie, avec une acuité sidérante – et parfois beaucoup d'humour– , la société contemporaine.

Dans cette émission d'une heure, Despentes qui dit ne pas se trouver féminine revient sur tout son parcours, de l'enfance à Nancy à la sortie de Baise-moi, en passant par l'adolescence punk et la découverte d'icônes telles que Marilyn Monroe ou Lydia Lunch. Elle parle aussi longuement de la fluidité des genres et de son rapport aux hommes. Des hommes qu'elle trouve particulièrement lents à remettre en question les codes et normes qui régissent la masculinité depuis des siècles :

> «Moi j'ai l'impression que les mecs sont vachement lents sur des trucs extrêmement simples : ils sont extrêmement lents à porter des jupes, extrêmement lents à se maquiller, extrêmement lents à se vernir les ongles, extrêmement lents quand ils sont beaux à se servir de leur corps, exception faite des milieux queer (...). Je les trouve extrêmement lents à s'emparer de sujets qui les concernent directement et qui pourraient les concerner exclusivement, comme le viol. Comme quand il y a Nuit debout et qu'on commence à entendre que beaucoup de jeunes filles qui restent la nuit se plaignent de mains au cul (...), ça me surprend que le lendemain les mecs n'éprouvent pas le besoin de se rassembler immédiatement pour dire : qu'est ce qu'on fait ? (...) Je trouve les mecs extrêmement lents à s'emparer de la question de la masculinité (...). A chaque fois qu'un mec viole, ça les concerne tous, au sens ou c'est leur virilité qui s'assoit là-dessus. Quand ils se trimbalent en ville en maîtres du monde, c'est sur le travail des violeurs qu'il s'appuient.»

L'intégralité de l'émission est à écouter [ici](http://www.novaplanet.com/radionova/65694/episode-dans-le-genre-de-virginie-despentes) :
