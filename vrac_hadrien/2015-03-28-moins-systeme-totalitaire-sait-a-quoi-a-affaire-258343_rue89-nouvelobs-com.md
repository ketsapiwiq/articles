---
title: "« Au moins, dans un système totalitaire, on sait à quoi on a affaire »"
url: http://rue89.nouvelobs.com/2015/03/28/moins-systeme-totalitaire-sait-a-quoi-a-affaire-258343
keywords: nest,quil,cest,faire,quon,données,projet,loi,exemple,système,totalitaire,affaire,algorithmes
---
![](https://media.nouvelobs.com/ext/uri/sreferentiel.nouvelobs.com/file/rue89/245ad4a4bcfb3e78963b43e56b98d8c0.jpg)  

« Big data is watching you » - [Jeremy Keith/Flickr/CC](https://www.flickr.com/photos/adactio/9276962702/in/photolist-f8LRNN-qbGfqn-peTfNh-qbP8rQ-qbP51G-q9A6S9-q9A8cy-qbP6Cj-5ZdiB3-5teXvc-9X6egv-8R7PAt-kidXen-bHqqg8-jDJVX5-jDG1jB-jDJaWh-jDJcbw-jDJbgq-jDJWZW-jDJZvh-jDG4mi-jDJe99-7xsGhC-kwvy4z-kidP5g-jDJV8E-hypmYn-oeuJh4-8Mpcd5-eZELhp-kwvpGe-7aBLP5-c2nrdA-kaZNV8-nMY5MY-nzPuMj-7e2XvC-7icyVt-kwxwoA-kwvVcV-kw6bC6-kwxuNS-kwvN7x-ao5gXp-kwxuE5-kwvKtX-kwvVNz-kwxBw1-k2SrWp)

[Antoinette Rouvroy](http://directory.unamur.be/staff/arouvroy) est l’une des premières intellectuelles à s’être penchée sur la question du pouvoir et des données.

Elaborant le concept de [« gouvernementalité algorithmique »](http://blogs.mediapart.fr/blog/antoinette-rouvroy/270812/mise-en-nombres-de-la-vie-meme-face-la-gouvernementalite-algorit), cette chercheuse en droit de l’université de Namur s’interroge sur les changements amorcés par l’adhésion de la société à un monde de chiffres bruts.

Soit l’illusion d’une réalité plus certaine, plus objective, qui préside à l’instauration d’un régime où la logique sécuritaire ne passe non pas par le contrôle des corps et la discipline, mais par la sensation de liberté.

Nous avons voulu avoir son avis suite à l’annonce, en France, [d’un grand projet de loi sur le renseignement](http://rue89.nouvelobs.com/2015/03/17/logiciels-espions-cameras-lecture-mails-prevoit-loi-renseignement-258238?%3F%3F=), qui s’appuie en partie sur [« des boîtes noires »](http://rue89.nouvelobs.com/2015/03/19/antiterrorisme-actes-numeriques-fliques-boites-noires-258270) où tourneront des algorithmes.

**Rue89 : Le gouvernement français a présenté le 19 mars un [projet de loi sur le renseignement](http://rue89.nouvelobs.com/2015/03/17/logiciels-espions-cameras-lecture-mails-prevoit-loi-renseignement-258238). L’un des volets de ce projet de loi est d’installer sur le réseau français des équipements de traitement de données, dans lesquels des algorithmes vont tourner pour détecter des comportements, le but étant d’identifier des terroristes potentiels. Ça vous inspire quoi ?**

**Antoinette Rouvroy :** Ça m’inspire beaucoup de choses. Pas toutes très positives.

Tout d’abord, c’est l’idée que grâce au calcul, grâce à l’analyse des données en quantité massive, grâce au big data, on va pouvoir vivre dans un monde non-dangereux, dans le sens où ce monde aurait été expurgé de trois sources principales de l’incertitude radicale.

D’une part, la **subjectivité**, que l’on éradique avec l’automaticité. On substitue une sorte d’objectivité machinique à la subjectivité humaine du surveillant, du contrôleur, du policier, de l’agent de sécurité, qui est toujours trop biaisée, trop partiale, et qui est liée au fait que l’observateur humain, lui, a un corps, placé dans une certaine perspective, qui est sous le joug de la représentation humaine. L’idée est que grâce au big data, on pourra se passer de cette représentation et accéder au réel directement. C’est le réel qui va parler de lui-même : les terroristes vont se trahir par leurs propres données, sans qu’on ait vraiment à traduire leurs motivations, les causes de leurs actions...

Ensuite, la **sélectivité**. L’une des causes de notre impression d’incertitude est que l’on a toujours une perception sélective des événements : on ne peut avoir qu’une vision partielle des choses. Or, cette sorte d’idéologie du big data promet une prétendue absolue non-sélectivité qui permettrait d’épuiser tous les possibles.

C’est la définition même de la préemption. Qui n’a rien à voir ni avec la prédiction, ni avec la prévention. « Pré-dire », c’est dire par avance ce qu’il va se passer. La préemption, c’est agir par avance sur tous les possibles, sans se soucier des causes. Pas de prévention. On éradique directement. Ce sont par exemple les frappes des drones en Afghanistan.

La troisième cause d’incertitude radicale qu’on voudrait éradiquer, c’est précisément cette **virtualité**. Le fait que le présent tremble toujours d’un devenir, qui est bien réel sans être actuel. L’idée de la préemption, c’est précisément d’actualiser ce virtuel. De faire comme si le virtuel était déjà réel.

Les gens qui font du big data sérieusement vous diront que tout cela, c’est faux ; que les données brutes sont fabriquées, et qu’il y a de nombreux biais.

Ce projet de loi est donc exemplaire de ce fantasme de maîtrise de la potentialité. Alors que cette potentialité, cette part d’incertitude radicale inhérente à l’existence, autrefois, on la gérait autrement : par exemple, par des dispositifs de solidarité, de mutualisation des risques, par des calculs de probabilité qu’on abandonne aussi. Ce que prévoit ce projet de loi, c’est tout autre chose que la probabilité.

Du coup, forcément, l’idée même de proportionnalité de la récolte des données par rapport à l’objectif n’est plus posée. L’objectif, c’est de réaliser la logique absolue de la sécurité, par avance. Et cette logique absolue ne peut pas être mise en balance avec quoi que ce soit d’autre.

Le terrorisme, c’est le cataclysme et le cataclysme, c’est incommensurable.

Alors même qu’il y a eu des jugements, notamment de la Cour de justice des communautés européennes, qui dit justement que collecter les données de tout le monde, y compris en vue de faire respecter la loi, c’est tout à fait excessif.

**Le projet de loi encadre un peu l’utilisation de ces équipements, ne la permettant qu’à des fins de prévention du terrorisme. Le problème, c’est que techniquement, il n’y a rien qui permette de s’assurer que ces « boîtes noires » ne seront pas utilisées pour autre chose...**

Oui, c’est bien le problème dès qu’on utilise des algorithmes.

D’abord, il faut savoir quels types d’algorithmes sont concernés : des algorithmes supervisés ? Ou auto-apprenants ? A partir du moment où ils sont auto-apprenants, il devient très difficile de superviser ce qu’ils font puisque leur objectif est précisément de générer eux-mêmes les critères selon lesquels ils vont opérer les classifications.

On ne sait donc pas les critères de comportement normal et de comportement suspect qui vont émerger. Tout cela se décide en cours de route.

C’est une sorte d’objet normatif qui évolue en fonction des comportements des individus. Et c’est une normativité qui semble inhérente à la vie même. Qui perd tout contenu politique.

D’une certaine manière, ça semble très émancipateur : ce sont les comportements humains qui génèrent eux-mêmes leurs propres normes. Mais en même temps, ces normes deviennent incontestables. Et imperceptibles.

Selon moi, l’un des critères fondamentaux de légitimité de toute norme, c’est qu’elle reste contestable. Ce qui n’est plus le cas avec ce type d’algorithmes.

**On a l’impression qu’il n’y a déjà plus de place à la critique. Ce projet de loi, mis à part du côté de quelques défenseurs des libertés sur Internet, ne provoque aucune émotion. Et on a du mal à voir où la critique peut émerger, car c’est incontestable : c’est le chiffre, la donnée brute...**

Exactement. C’est un peu ce que disait [Castoriadis](http://fr.wikipedia.org/wiki/Cornelius_Castoriadis) : une norme qui est parfaitement adaptée au comportement de chacun et qui lui colle au corps comme une seconde peau, c’est ce qu’il y a de pire. Parce qu’on ne peut pas prendre de distance vis-à-vis de cette norme : on l’incarne en quelque sorte.

Par ailleurs, il est difficile de voir ce que font ces algorithmes et de les critiquer.

Il y a une littérature qui devient abondante sur l’auditing des algorithmes – le fait de les examiner –, mais ça reste assez compliqué. Et j’imagine en plus que ces algorithmes seront couverts par le secret défense – doublé éventuellement du secret industriel si l’on fait intervenir des acteurs privés.

**Pourquoi ça émeut si peu de gens ?**

Ça émeut assez peu de gens parce que finalement, ces données sont massivement anonymes. On n’a pas besoin de données à caractère personnel pour pouvoir catégoriser les personnes dans des classes de potentiels terroristes.

Grégoire Chamayou \[auteur de « La Théorie du drone » (La Fabrique, 2013), ndlr\] l’explique très bien dans son livre : les frappes ne visent pas des personnes identifiées, elles visent des comportements suspects.

Et puis, ça vise l’avenir. L’avenir, ce n’est personne d’identifié. Et c’est tout à fait démocratique vu qu’on enregistre tout le monde !

La difficulté, c’est que c’est une démocratie particulière où il y a une asymétrie totale entre gouvernés et gouvernants. Ces derniers sont totalement opaques, du fait des algorithmes.

Le risque aussi, c’est la production de comportements différents.

**C’est-à-dire ?**

L’idée qu’il y a des trajectoires normales sur Internet et d’autres, anormales. C’est ce qu’on appelle le « nominalisme dynamique » : les individus, à partir du moment où ils savent qu’ils sont classifiés, même s’ils ne savent pas quels sont les critères de classification, vont adapter leur comportement à ce qu’ils pensent qu’on attend d’eux.

Il y a aussi, peut-être, la fin de la curiosité. Saine ou malsaine. Or, la curiosité malsaine n’est pas quelque chose d’illégal : on peut très bien avoir la curiosité malsaine d’aller voir ce qu’est un site djihadiste, sans pour autant se préparer à commettre un acte terroriste ou adhérer à des actes commis par d’autres…

**Et ces vidéos de l’Etat islamique…**

Oui, on a tous un fond voyeuriste et ce n’est pas illégal. Or, avec ce type de mesure, on crée une nouvelle catégorie d’illégalités non codifiées dans le texte, des illégalités de fait et pas de droit. Et ça, c’est particulier.

Ça me pose question sur les droits et libertés fondamentaux. Parce qu’il me semble qu’ils ne sont pas là pour protéger les formes de vie, d’expression, de comportements, banales, normales, standardisées… Au contraire, ils sont là pour protéger les prises de position qui, sans être jugées illégales, sont jugées déviantes, malsaines, voyeuristes…

Les droits de l’homme jouent le rôle d’auto-subversion de la norme juridique par elle-même.

J’ai un peu peur que ce type de projet de loi érode progressivement, sans qu’on s’en rende compte, cette signification fondamentalement anti-totalitaire des droits et libertés fondamentaux.

C’est un risque d’autant plus grand que c’est quelque chose de non directement perceptible. Que les comportements risquent progressivement de s’adapter, sans douleur, à cette forme de bien-pensance, de bien-dire, d’auto-censure de nos propos et de nos trajectoires sur Internet.

A moins que – et ce serait un geste assez sain, mais il faudrait que ce soit collectif – il y ait une mobilisation massive des internautes : que tous aillent voir des sites un peu sensibles de manière à brouiller les pistes. De manière aussi à montrer que c’est inefficace !

J’entendais [Slavoj Zizek](http://en.wikipedia.org/wiki/Slavoj_%C5%BDi%C5%BEek) \[un philosophe slovène, ndlr\] dire : « J’en ai rien à faire de la surveillance, parce que les algorithmes sont stupides. » Je suis d’accord avec ça. Pour lui, c’est comme faire lire « L’Ethique » d’Hegel à une vache. Lui ne se sent pas menacé par la surveillance car ce qu’il est réellement se trouve dans sa tête et, pour le moment, cela n’est pas transposable dans une trace numérique. Et ça j’y crois aussi.

On tend à nous faire croire, dans cette idéologie du big data, que tout est numérisable. Y compris nos intentions non encore formulées. Je pense qu’une forme de récalcitrance est vraiment de rendre compte du fait que tout n’est pas dans la machine. Et qu’il y a bien encore une distinction radicale entre le monde et le monde numérique.

**Mais il y a tout de même un risque d’auto-réalisation. Si, à partir de ce que je fais, un algorithme me considère comme suspect, je vais moi-même me rendre compte que je suis perçu comme suspect aux yeux de la société. Cela risque de cadrer, de servir de référence, à ce que je vais devenir à l’avenir.**

Tout à fait. C’est pour ça qu’il faut une réflexion sur les décisions qui ont un impact sur les individus tout en étant fondées sur le traitement automatisé des données.

Des dispositions spécifiques existent, notamment dans le régime européen de protection des données à caractère personnel. Ce sont des choses intéressantes qui prévoient que l’individu ne peut faire l’objet de décisions qui ont un impact substantiel sur sa vie si ces décisions sont prises sur la seule base du traitement automatisé de données à caractère personnel.

Le problème, c’est que l’effectivité de ce régime européen n’est pas forcément assurée. Et que ces décisions qui ne sont pas fondées sur la seule base du traitement automatisé vont devenir minoritaires. Pour le simple fait que, quand on a une recommandation automatisée et qu’on s’en écarte, on prend seul la responsabilité de cet écart, et des éventuelles conséquences négatives de cet écart.

Il y aura peu d’agents par exemple, dans le cadre d’une surveillance assistée par ordinateur, qui vont s’écarter de la recommandation automatique.

C’est la question de la liberté de choix qui nous reste. Alors même que nous sommes formellement libres de nous écarter de ces recommandations automatisées.

C’est ce que Zizek décrit comme « la liberté dans des situations de choix forcés ». Vous pouvez choisir ce que vous voulez à condition que vous choisissiez la bonne solution ! Sous Lénine, on pouvait formellement dire tout ce qu’on voulait, tant qu’on disait que Lénine était grand, beau et fort ! On est exactement dans la même situation. On peut transposer cette théorie du choix forcé dans cette situation.

**Si on accepte ce type de projet de loi sans résister, vers quelle forme de pouvoir va-t-on ?**

C’est un changement de rationalité. On entrerait dans une substitution du réel numérique à la réalité vécue existentiellement par les personnes. Une image quantitative se substituant à la réalité qu’elle est censée penser. Ce qui pose une multitude de problèmes.

Par exemple, si on étendait cela dans d’autres domaines comme la justice, ça donnerait l’impression d’une justice objective. Or, qui dit objectivité totale, dit injustice totale.

On voit cela se développer dans de nombreux contextes : l’obsession du temps réel, du coût réel, mêlé à une hyper-individualisation. L’idée que si on peut, en temps réel, mesurer ce que coûte un individu à la société en termes de risques qu’il fait peser sur les autres, de comportements plus ou moins déviants… Cette idée d’hyper-individualisation qui est à l’opposé d’une notion de justice distributive.

On aurait l’impression qu’on pourrait arriver à ce qu’il n’y ait plus de discrimination. Puisque les catégories algorithmiques ne correspondent plus à des catégories visuelles ou éprouvées – on ne peut plus discriminer les Noirs parce qu’ils sont noirs, par exemple –, on risque d’avoir une naturalisation des inégalités économiques.

Je m’explique : on croit avoir accès à des catégories non-biaisées, mais on ne se soucie plus du tout des causes. Un algorithme qui aide à la décision d’embauche, par exemple, pourrait évincer automatiquement des candidats venant d’une certaine partie d’une ville, parce que statistiquement, ces gens-là restent moins longtemps embauchés que les autres… Ça paraît parfaitement objectif.

Sauf qu’il se peut que les raisons pour lesquelles ces individus restent moins longtemps embauchés tiennent à des préjugés raciaux de la part des employeurs, et que ces individus sont en majorité d’origine nord-africaine. Donc il y a une sorte de masquage des réalités socio-économiques et culturelles par le chiffre.

Un autre exemple donné par [Kate Crawford](http://thenewinquiry.com/essays/the-anxieties-of-big-data/) \[chercheuse américaine, ndlr\] : la détection des trous dans les trottoirs de Boston. La ville a mis en place une application mobile pour les détecter : à chaque fois que les gens en voient un, ils envoient une photo et une équipe arrive pour les réparer. Sauf que dans les zones les moins riches, il y a moins de smartphones et donc les trottoirs sont moins réparés. C’est une fausse objectivité.

Un autre exemple : permettre aux assureurs de prendre en compte les forums sur lesquels des personnes victimes de violence conjugale vont chercher des soutiens. L’objectivité voudrait qu’on prenne en compte ce genre de données : faire payer plus cher ou exclure les hommes et les femmes victimes de violences qui risquent de mourir prématurément.

**C’est en train d’être intégré : [avec les bracelets connectés aux Etats-Unis](http://www.slate.fr/story/91411/compagnies-assurance-objets-connectes-apple), les employés sont incités par leurs assurances à faire du sport.**

Dans les temps idéaux de l’Etat-providence, la mauvaise santé était perçue comme un coup du sort, qui se distribuait de façon moralement neutre et devait donc être compensée par la solidarité et la collectivité. Désormais, avec tous ces systèmes de « quantified-self », on hyper-responsabilise les individus de ce point de vue.

Tout ça me fait évidemment penser à la gouvernementalité algorithmique. Et de façon très égoïste, je suis très heureuse que ça se passe comme ça : je peux joyeusement m’horrifier !

Pour mon boulot, ça me donne des exemples concrets qui me manquaient il y a quelques années, quand on me disait que mes histoires étaient de l’ordre du fantasme.

Finalement, ce n’est que l’intensification du néolibéralisme et du capitalisme : c’est l’explosion de la notion même de personne en une multitude de données. Ce qui compte, ce n’est plus vous, vos intentions, le récit que vous faites, mais vos trajectoires, sur Internet, dans la « smart city »…

Cette fragmentation des existences atteint le sujet lui-même et instaure une compétition radicale entre individus à un niveau moléculaire, avec le quantified-self. Une compétition entre fragments impersonnels auxquels les personnes sont temporairement associées. Une compétition sans sujet…

**On entend la critique selon laquelle ce projet de loi fait de la France une sorte de Chine. Est-ce qu’on a raison de prendre comme comparaison des régimes totalitaires ? De faire référence à « 1984 » ?**

Ah non, pas du tout. C’est exactement l’inverse. C’est un gouvernement par les libertés. Pour que ces données puissent être interprétées de manière intéressante, il faut nécessairement que vous vous sentiez libre de les émettre.

Ce sera ressenti d’autant plus comme ça que la norme est sentie comme immanente, comme collant à vos comportements mêmes. Ne pas vouloir être profilé, c’est presque ne pas se vouloir soi-même.

C’est très différent de « 1984 » aussi : il n’y a pas de Big Brother, c’est très diffus, peu centralisé, on a besoin d’acteurs privés… Et la définition des critères va être sous-traitée aux machines, diffusée dans le réseau.

Ce serait presque plus facile si c’était un système totalitaire : au moins, dans un tel système, on sait à quoi on a affaire.

**L’auteur Alain Damasio, dans une interview sur l’après-Snowden, [parle](http://www.lesinrocks.com/2013/11/25/medias/lauteur-sf-alain-damasio-disseque-societe-controle-cest-pas-big-brother-cest-big-mother-11447605/) non pas de Big Brother mais de « Big Mother », parce que l’individu embrasse cette surveillance. La logique sécuritaire est donc plus insidieuse qu’un totalitarisme car on ne se rend même plus compte.**

Oui, c’est très amniotique. Très fluide aussi. On ne se cogne à rien dans cet univers. C’est tout à fait spectral et tout à fait rassurant aussi. C’est difficile de qualifier ce pouvoir-là autrement que par la négative : ce n’est pas un pouvoir totalitaire, ce n’est pas vraiment de la normalisation, ce n’est pas de la disciplinarisation…

En même temps, ce mode de pouvoir gouverne en creux : faire en sorte que certaines choses n’arrivent pas. On est dans une sorte de vide du pouvoir. Et c’est précisément ce que font ces genres de pouvoirs : se dispenser de gouverner.

**Comment expliquez-vous que les hommes politiques, eux, ne voient absolument pas le problème ? Alors même que cela les vide de leurs compétences et change le paradigme de l’exercice du pouvoir ?**

Je pense qu’ils n’ont pas trop envie de comprendre parce que ça leur sert aussi. C’est très généralisateur, mais j’ai l’impression que beaucoup d’hommes politiques ont très envie de la fonction, mais pas vraiment de l’exercer réellement : de décider et de prendre en charge la responsabilité politique.

Il est plus facile de s’en remettre à des indicateurs quantifiés. Et ça dure depuis quelques années déjà : « Les statistiques nous disent que… », « C’est l’Europe qui nous oblige à… » On est dans un régime d’irresponsabilité croissante dans laquelle on a une inflation incroyable du langage politique qui ne veut absolument plus rien dire, puisqu’il n’est que dans la réaction aux stimuli.

Ils sont, comme nous tous, soumis à cet empire numérique.

**On a l’impression que ce régime, difficile à définir, flou, que l’on ne peut presque pas nier si on ne veut pas se nier en tant qu’individu, est impossible à combattre. Qu’il n’y a pas d’alternatives.**

Je pense qu’il y a des alternatives. Mais elles demanderaient un certain nombre de sacrifices. Parce que tout ça, c’est très confortable aussi. Cette automatisation, cette préemption, le fait de ne pas avoir à décider...

Mais les alternatives pourraient par exemple passer par l’exigence de justification : toute décision qui doit être prise, et qui a des impacts sur autrui, doit être justifiée par celui qui prend la décision. Que cette dernière lui soit dictée par des algorithmes ou pas.

Ça résoudrait beaucoup de soucis. On dit toujours qu’il est difficile d’auditer des algorithmes parce qu’ils sont protégés par le secret et qu’ils sont compliqués… Mais quand on met à charge la personne qui utilise les algorithmes, c’est à elle qu’incombe la charge de la preuve pour montrer que sa décision n’est pas discriminatoire, par exemple.

Cela risque d’être assez dissuasif : par exemple, les employeurs qui s’appuient sur un algorithme pour embaucher vont y réfléchir à deux fois. Ils vont devoir faire appel à des ingénieurs, et ces derniers devront ouvrir la boîte noire pour l’expliquer ; bref, un surcroît de travail dont les algorithmes étaient censés les dispenser ! C’est une mesure stratégique de récalcitrance qui passe par la loi.

L’autre façon de faire serait d’immuniser toute une série de domaines de la décision algorithmique. Par exemple ,dire que la justice, ça n’a rien à voir avec les données, avec l’objectivité, que ça a à voir avec la prudence du juge. De la même manière qu’on a interdit aux assurances, ici en Belgique, de prendre en compte les informations génétiques.

Le droit est une machine assez magique qui peut tout faire. Donc on pourrait l’utiliser de façon plus stratégique que ce qu’on fait aujourd’hui. Je ne pense pas que l’on fasse face à une situation inéluctable. C’est une question de volonté politique.
