---
title: "You're not going to believe what I'm about to tell you"
url: http://bit.ly/2qwYB6z
keywords: backfire,im,effect,wonderful,believe,youusc,creativity,brain,going,youre,theresea,tell,thank,podcast
---
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/2_1.png)  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/2_2.png)  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/2_3.png)  
![Sources](https://s3.amazonaws.com/theoatmeal-img/comics/believe/sources.png) [![Source 2](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source1.png)](http://www.nbcnews.com/id/6875436/) [![Source 2](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source2.png)](http://www.mountvernon.org/george-washington/the-man-the-myth/the-trouble-with-teeth/) [![Source 3](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source3.png)](http://www.history.com/news/ask-history/did-george-washington-have-wooden-teeth)  
  
  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/2_4.png)  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/2_5.png)  
![Sources](https://s3.amazonaws.com/theoatmeal-img/comics/believe/sources.png) [![Source 2](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source1.png)](https://www.nytimes.com/2014/04/29/upshot/george-washingtons-weakness-his-teeth.html?_r=0) [![Source 2](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source2.png)](http://gwpapers.virginia.edu/george-washingtons-false-teeth-come-slaves-look-evidence-responses-evidence-limitations-history/) [![Source 3](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source3.png)](http://www.snopes.com/george-washington-wooden-teeth/)  
  
  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/2_6.png)  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/2_7.png)  
![Sources](https://s3.amazonaws.com/theoatmeal-img/comics/believe/sources.png) [![Source 1](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source1.png)](https://books.google.com/books?id=Q7CkHF7xTuYC&pg=PT116#v=onepage&q&f=false) [![Source 2](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source2.png)](http://www.cbsnews.com/news/forget-napoleon-height-rules/)  
  
  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/2_8.png)  
![Sources](https://s3.amazonaws.com/theoatmeal-img/comics/believe/sources.png) [![Source 1](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source1.png)](http://www.snopes.com/business/names/crapper.asp) [![Source 2](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source2.png)](http://content.time.com/time/specials/packages/article/0,28804,2016258_2016259_2016274,00.html)  
  
  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/2_9.png)  
![Sources](https://s3.amazonaws.com/theoatmeal-img/comics/believe/sources.png) [![Source 1](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source1.png)](https://web.archive.org/web/20130301174011/http://www.newton.dep.anl.gov/natbltn/400-499/nb453.htm) [![Source 2](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source2.png)](https://en.wikipedia.org/wiki/Housefly#Life_cycle)  
  
  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/2_10.png)  
![Sources](https://s3.amazonaws.com/theoatmeal-img/comics/believe/sources.png) [![Source 1](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source1.png)](http://www.webcitation.org/68Aef3glC?url=http%3A%2F%2Fimagine.gsfc.nasa.gov%2Fdocs%2Fask_astro%2Fanswers%2F970603.html) [![Source 2](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source2.png)](http://www.abc.net.au/science/articles/2005/04/07/1320013.htm)  
  
  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/2_11.png)  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/2_12.png)  
![Sources](https://s3.amazonaws.com/theoatmeal-img/comics/believe/sources.png) [![Source 2](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source1.png)](https://www.ucg.org/the-good-news/biblical-evidence-shows-jesus-christ-wasnt-born-on-dec-25) [![Source 2](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source2.png)](http://www.livescience.com/42976-when-was-jesus-born.html) [![Source 3](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source3.png)](https://en.wikipedia.org/wiki/Date_of_birth_of_Jesus)  
  
  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/2_13.png)  
![Sources](https://s3.amazonaws.com/theoatmeal-img/comics/believe/sources.png) [![Source 2](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source1.png)](http://www.ushistory.org/documents/pledge.htm) [![Source 2](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source2.png)](https://en.wikipedia.org/wiki/Francis_Bellamy) [![Source 3](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source3.png)](http://boingboing.net/2016/09/10/a-socialist-wrote-the-pledge-o.html)  
  
  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/2_14.png)  
, ![Sources](https://s3.amazonaws.com/theoatmeal-img/comics/believe/sources.png) [![Source 2](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source1.png)](http://www.heraldcourier.com/news/roe-v-wade-approved-by-republican-appointees/article_4fa2fb82-d738-5a11-95b6-650037c19451.html) [![Source 2](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source2.png)](https://en.wikipedia.org/wiki/Roe_v._Wade#Supreme_Court_decision) [![Source 3](https://s3.amazonaws.com/theoatmeal-img/comics/believe/source3.png)](http://www.u-s-history.com/pages/h2603.html)  
  
  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/2_15.png)  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/3_1.png)  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/3_2.png)  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/3_3.png)  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/3_4.png)  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/3_5.png)  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/3_6.png)  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/4_1.png)  
![](https://s3.amazonaws.com/theoatmeal-img/comics/believe/4_2.png)  

Inspiration
===========

This comic was inspired by this three-part series on the [backfire effect](https://youarenotsosmart.com/2017/01/13/yanss-093-the-neuroscience-of-changing-your-mind/) from the [You Are Not So Smart Podcast](https://youarenotsosmart.com/).  
  
If you want to learn more about the backfire effect and other related behaviors (confirmation bias, deductive reasoning, etc), I **highly** recommend listening to the whole thing:  
[Podcast Part 1](https://youarenotsosmart.com/2017/01/13/yanss-093-the-neuroscience-of-changing-your-mind/) - [Podcast Part 2](https://youarenotsosmart.com/2017/01/30/yanss-094-how-motivated-skepticism-strengthens-incorrect-beliefs/) - [Podcast Part 3](https://youarenotsosmart.com/2017/02/11/yanss-095-how-to-fight-back-against-the-backfire-effect/)  
  
I would also like to thank my wonderful girlfriend [Theresea](https://www.instagram.com/idomakelove/) for calling my attention to the backfire effect in the first place. The past year has been rough for a lot of people, and she pointed me in a direction that could actually help people. Theresea: you're amazing, I love you, thank you.

USC Creativity and Brain Institute
----------------------------------

[Neural correlates of maintaining oneâ€™s political beliefs in the face of counterevidence](https://www.nature.com/articles/srep39589)  
By Sarah Gimbel and Sam Harris.  
  
[Official website](https://dornsife.usc.edu/bci/)

Other fun reading
-----------------

[Reddit - Change My View](https://www.reddit.com/r/changemyview/)  
[Wikipedia's list of common misconceptions](https://en.wikipedia.org/wiki/List_of_common_misconceptions)

  

Sources
-------

You Are Not So Smart  
[Website](https://youarenotsosmart.com/) [Podcast](https://youarenotsosmart.com/podcast)  
  
USC Creativity and Brain institute  
<https://dornsife.usc.edu/bci/>  
  
Wooden teeth  
<http://www.nbcnews.com/id/6875436>  
<http://www.mountvernon.org/george-washington/the-man-the-myth/the-trouble-with-teeth/>  
<http://www.history.com/news/ask-history/did-george-washington-have-wooden-teeth>  
  
Slave teeth  
<https://www.nytimes.com/2014/04/29/upshot/george-washingtons-weakness-his-teeth.html?_r=0>  
[http://gwpapers.virginia.edu/george-washingtons...](http://gwpapers.virginia.edu/george-washingtons-false-teeth-come-slaves-look-evidence-responses-evidence-limitations-history/)  
<http://www.snopes.com/george-washington-wooden-teeth/>  
  
Napoleon  
[https://books.google.com/books...](https://books.google.com/books?id=Q7CkHF7xTuYC&pg=PT116#v=onepage&q&f=false)  
<http://www.cbsnews.com/news/forget-napoleon-height-rules/>  
  
Thomas Crapper  
<http://www.snopes.com/business/names/crapper.asp>  
[http://content.time.com/...](http://content.time.com/time/specials/packages/article/0,28804,2016258_2016259_2016274,00.html)  
  
Houseflies  
[https://web.archive.org/...](https://web.archive.org/web/20130301174011/http://www.newton.dep.anl.gov/natbltn/400-499/nb453.htm)  
<https://en.wikipedia.org/wiki/Housefly#Life_cycle>  
  
Exploding in a vacuum  
[http://www.webcitation.org...](http://www.webcitation.org/68Aef3glC?url=http%3A%2F%2Fimagine.gsfc.nasa.gov%2Fdocs%2Fask_astro%2Fanswers%2F970603.html)  
[http://www.abc.net.au/science/...](http://www.abc.net.au/science/articles/2005/04/07/1320013.htm)  
  
Jesus's birthday [https://www.ucg.org/the-good-news/biblical...](https://www.ucg.org/the-good-news/biblical-evidence-shows-jesus-christ-wasnt-born-on-dec-25)  
[ttp://www.livescience.com/42976-when-was-jesus-born.html](http://www.livescience.com/42976-when-was-jesus-born.html)  
<https://en.wikipedia.org/wiki/Date_of_birth_of_Jesus>  
  
The Pledge of Allegiance  
<http://www.ushistory.org/documents/pledge.htm>  
<https://en.wikipedia.org/wiki/Francis_Bellamy>  
<http://boingboing.net/2016/09/10/a-socialist-wrote-the-pledge-o.html>  
  
Roe v. Wade  
<http://www.heraldcourier.com/news/roe-v-wade-approved-by-republican-appointees/article_4fa2fb82-d738-5a11-95b6-650037c19451.html>  
<https://en.wikipedia.org/wiki/Roe_v._Wade#Supreme_Court_decision>  
<http://www.u-s-history.com/pages/h2603.html>  
