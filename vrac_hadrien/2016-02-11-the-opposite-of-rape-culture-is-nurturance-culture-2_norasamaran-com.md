---
title: "The Opposite of Rape Culture is Nurturance Culture"
url: https://norasamaran.com/2016/02/11/the-opposite-of-rape-culture-is-nurturance-culture-2/
keywords: culture,secure,rape,needs,style,know,attachment,men,women,nurturance,need,opposite
---
The opposite of masculine rape culture is masculine nurturance culture: men\* increasing their capacity to nurture, and becoming whole.

The Ghomeshi trial is back in the news, and it brings violent sexual assault back into people’s minds and daily conversations. Of course violence is wrong, even when the court system for handling it is a disaster. That part seems evident. Triggering, but evident.

But there is a bigger picture here. I am struggling to see the full shape emerging in the pencil rubbing, when only parts are visible at a time.

A meme going around says ‘Rape is about violence, not sex. If someone were to hit you with a spade, you wouldn’t call it gardening.’ And this is true. But it is just the surface of the truth. The depths say something more, something about violence.

Violence is nurturance turned backwards.

These things are connected, they must be connected. Violence and nurturance are two sides of the same coin. I struggle to understand this even as I write it.

Compassion for self and compassion for others grow together and are connected; this means that men finding and recuperating the lost parts of themselves will heal everyone. If a lot of men grow up learning not to love their true selves, learning that their own healthy attachment needs (emotional safety, nurturance, connection, love, trust) are weak and wrong – that anyone’s attachment, or emotional safety, needs are weak and wrong – this can lead to two things.

1. They may be less able to experience women as whole people with intelligible needs and feelings (for autonomy, for emotional safety, for attunement, for trust).

2. They may be less able to make sense of their own needs for connection, transmuting them instead into distorted but more socially mirrored forms.

To heal rape culture, then, men build masculine nurturance skills: nurturance and recuperation of their true selves, and nurturance of the people of all genders around them.

I am discovering a secret, slowly: the men I know who are exceptionally nurturing lovers, fathers, coworkers, close friends to their friends, who know how to make people feel safe, have almost no outlets through which to learn or share this hardwon skill with other men. They may have had a role model at home, if they are lucky, in the form of an exceptionally nurturing father, but if they do not have this model they have had to figure everything out through trial and error, alone, or by learning with women rather than men. This knowledge shapes everything: assumptions about the significance of needs, how one ought to respond to them, what closeness feels like, how to love your own soul, and what kind of nurturance is actually meant to happen in intimate space.

Meanwhile, the men I know who are kind, goodhearted people, but who are earlier on in growing into their own models for self-love and learning how to comfort and nurture others, have no men to ask. Growing entails growing pains, certainly, but the way can be smoothed when one does not have to learn everything alone.

Men do not talk to one another about nurturance skills: doing so feels too intimate, or the codes of masculinity make doing so too frightening. If they can’t ask and teach each other – if they can’t even find out which other men in their lives would welcome these conversations – then how do they learn?

Men have capacities to heal that are particularly masculine and particularly healing. They often are not fully aware of this deep gift and how helpful it can be for those close to them, whether family or close friends.

To completely transform this culture of misogyny, then, men must do more than ‘not assault.’ We must call on masculinity to become whole and nurturing of self and others, to recognize that [attachment needs are healthy and normal and not ‘female,’ and thus to expect of men to heal themselves and others](https://norasamaran.com/2016/07/21/for-men-who-desperately-need-autonomy-make-it-dont-take-it/) the same way we expect women to ‘be nurturers.’ It is time men recognize and nurture their own healing gifts.

In Ursula K. Leguin’s book Gifts, an entire culture lives by the rule of what they call ‘gifts’ – powers to do harm – possessed by certain of its members. Some families possess gifts of [Unmaking](https://norasamaran.com/2016/08/28/overt-and-covert-boundary-crossings/), where they can turn a farmer’s field into a blackened waste or a puppy into a sack of dissolved flesh. Some possess the ability to create a wasting illness, or blindness, or the gift of calling animals to the hunt.

By the book’s end, the child at its centre has struggled, against all signs in his culture, to realize something profound and fundamental. The gift they call [Unmaking](https://norasamaran.com/2016/08/28/overt-and-covert-boundary-crossings/) is actually a gift of Making, turned backwards upon itself and rendered unthinkingly into a weapon. The gift of calling animals is turned into a way to hunt them, when it is meant to let humans understand animals and live in balance with them. The wasting disease is the backwards use of a gift of healing illness and old age. He finally asks his sister and closest confidant: what if we are using our gifts backwards? To harm instead of to help? What if they were meant to be used the other way around?

Nothing in the boy’s culture would tell him this is so. His entire society has been built around fear of these gifts used as weapons. Yet he has seen his father use the gift of Unmaking ‘in reverse’ to gently undo a knot or mend a creaking gate. His best friend’s gift of calling animals also gives her an aversion to hunting them, an aversion she must overrule in herself to meet her culture’s expectations. These images knock on the door of his mind until he makes sense of them; he has to struggle to see the truth without a single signpost or mentor to help him find this knowledge. Nothing in his world reflects this reality back to him, and yet it is real. He at first can hardly believe it or understand it.

Something odd happens when you google ‘man comforting a woman.’ Many of the top hits are about women comforting men. (try it.) The ‘suggested search’ terms too: ‘how to comfort a guy, how to comfort a man when he’s stressed, how to comfort a guy when he’s upset.’ Apparently lots and lots of people on planet earth are googling how to comfort men… and fewer are googling how to comfort women. Strange, isn’t it, since this culture views women as ‘the emotional ones’ and men as the strong ones. Perhaps something is a bit backwards here.

I tried to find an image that would capture the way men have actually comforted me, which for me is the most intimate image of holding me in their arms, skin on skin like a young baby, rocking or singing, letting me be at my most vulnerable, held safe. There when needed, when it matters. I could find only one image that looked remotely like the real thing.

Could it be that a lot of men have no models for how to nurture, comfort, soothe, and thus strengthen people they care about? If you happen to not have a highly nurturing model at home, where would you learn how to nurture? A top search hit is a bewildered humour piece about how utterly terrifying and confusing it is when a woman cries and about how men have no idea what to do. Could it be that the things that come naturally to many of us – [hold the person, look at them with loving, accepting eyes](https://norasamaran.com/2016/07/21/for-men-who-desperately-need-autonomy-make-it-dont-take-it/), bring them food, hot tea, or medicine – that these are unfamiliar terrain for some, can’t even be imagined, let alone acted on consistently?

These things seem connected to me. And here is where my friend Rebekah, a drama therapist, comes in, who one day handed me the books Hold Me Tight and A General Theory of Love, and blew my mind. This is where attachment theory comes in. Bear with me, as this takes a little background knowledge – a quick summary of these books – before I can go on.

Attachment theory: cutting edge neuroscience

According to Hold Me Tight and A General Theory of Love, current advancements in neuroscience have completely transformed understandings of human relationships, from birth to death. What used to be called Freud’s ‘unconscious’ is actually located in the body, in a knowable place. Specific understandings of how the limbic brain work have replaced old ideas about love as a ‘mystery.’

Apparently about 50 percent of the population, people of all genders, have a secure attachment style: they were raised by responsive, attuned parents, who recognized their need to go out and explore as well as their need to come back and be comforted, and responded in a timely, attuned way to both. According to A General Theory of Love, this experience of attunement – having all their developmental needs met by attuned parents – literally shapes their limbic brain.

These folks as adults find closeness comfortable and enjoyable, they easily desire intimacy, and they know how to create a secure attachment bond in which [autonomy naturally emerges](https://norasamaran.com/2016/07/21/for-men-who-desperately-need-autonomy-make-it-dont-take-it/) and [daily nurturance is the norm](https://norasamaran.com/2016/05/19/send-yours-nurturance-culture-in-mass-media/). This shapes the brain in material, physiological ways. This is how you build secure attachment: through daily [attunement](https://norasamaran.com/2016/05/19/send-yours-nurturance-culture-in-mass-media/) to the subtle cues of other people, and lavishing love and care while letting them come and go as needed. In this kind of connection, you know your home base is always there for you, so [you feel comfortable going out into the world, taking risks, trying new or scary things, because you can return to safe arms when you need to](https://norasamaran.com/2016/07/21/for-men-who-desperately-need-autonomy-make-it-dont-take-it/).

Securely attached people know how to comfort and be there for one another when they need each other, and so they naturally know how to create healthy autonomy and healthy intimacy, which emerge in balance as they get comfortable with one another and create trust. Securely attached people are comfortable being vulnerable; they have had positive experiences of trust. There can be no joy of trust without the risk of vulnerability, letting your true self show and experiencing others catching you, mirroring you, liking you, and letting you go, when you are all there, visible, open.

Just like the first time you walk on ice or sit on a new chair, at first your muscles are clenched, waiting to see if the ground under you is secure or about to fall away. If the ice has always been solid, or you have never had a chair break under your weight, you may assume that you can relax quickly into your seat, or head out onto the ice and skate. You have no reason to think otherwise. If, however, you have had a chair break under you, you may think hard about sitting down again, and may take longer to relax into the secure base. If the chair has never been there for you at all, you may decide you simply don’t need chairs and prefer to stand. These are insecure attachment styles.

Secure, Anxious, Avoidant 

Attachment science also has learned that about 50% of the population has an insecure attachment style; this breaks down into about 23% anxious and 25% avoidant styles, which are apparently both physiologically insecure styles, but look and feel different on the surface. The avoidant style breaks down further, into anxious-avoidant and dismissive-avoidant styles. A very small percent of the population, around 3%, has a style called ‘disorganized‘ which is a mix of the other styles.

People with an anxious attachment style actively seek closeness and are afraid of losing it, and have a harder time trusting and knowing their partner will be there for them. The chair may have broken for them many times, or in a formative early relationship that was significant. Their limbic brains and entire autonomic nervous system is built differently than those with secure styles. They need extra reassurance and comfort to get secure and enjoy lots of closeness, especially with a new trust figure – though they have the same need for autonomy as anyone else, and it emerges as they become secure. They engage in ‘protest behaviour,’ i.e become upset, to try to seek closeness if they cannot receive it by asking directly. However, once they are secure and feel safe, they become exceptionally loyal and loving nurturers and feel immense gratitude and loyalty to those who give them this safety.

People with a preoccupied-avoidant style crave closeness but are afraid to show it, and will show it instead through sulking or silence, hoping their partner will guess. They can come to name their needs with a secure loving partner, but will struggle to do so.

People with a dismissive-avoidant attachment style also have a need for intimacy – every mammal has this need hardwired in our limbic brains – but at a very early age they complete a transition to a belief that they are autonomous and do not feel their need for intimacy. They decide if the chair isn’t going to be there, they will just stand, thank you very much. They can come to open up and become secure as they come to recognize their distorted beliefs about intimacy, but they need lots of time, space, and compassion about how difficult this is for them.

Having thoroughly repressed their attachment needs, these folks may have learned to act ‘fine’ at a very young age in order to keep a dismissive attachment figure close, or may have learned to create constant nonverbal barriers in order to keep an unattuned, invasive or dismissive attachment figure at arm’s length. They may feel suffocated or trapped when people get too close, and will unconsciously and involuntarily use ‘deactivating strategies’ – body language and facial expressions – to tell even their most intimate people to ‘back up’ even in the most intimate moments.

In other words, [the nonverbal cues that other people use with strangers on the subway to maintain distance are the daily communication that dismissive-avoidant attachers use with their closest family members](https://norasamaran.com/2016/05/19/send-yours-nurturance-culture-in-mass-media/), often without even understanding they are doing it, which may feel very confusing both to them and to those close to them. They may feel that no matter how hard they try, those who depend on them never get reassured. They may blame this on the other person and call them ‘needy’ without ever realizing the nonverbal distancing cues preventing secure attachment that are leading to the signs of ‘neediness’ in the other person.

Nurturance, the literature teaches us, recognizes and responds appropriately, in an alive, moving dance, to the other person’s need for intimacy and need for space, learning how to engage in nonverbal limbic communication that comforts, reassures, and breathes. In addition to talking openly and honestly, the quality of care that creates a feeling of safety happens in a moment-by-moment way through mainly nonverbal cues. The limbic brain does not use language but reads the small muscles around the eyes, the set of shoulders, the breathing, the posture, of other people.

‘Earned Secure’ attachment: where nurturance creates growth

It is possible to change your attachment style by creating an ‘earned secure’ attachment as an adult. It is possible to create an ‘earned secure’ attachment between two insecure attachers, but it takes a lot more time, effort, and compassion: both have to recognize nurturance is entirely good and expected.

Of course, nothing can replace talking things over and calibrating with people you are close to. No one should be a mind reader. But it takes more than talking to change these patterns. The avoider has to risk opening up and letting their true self show in order to give and receive nurturance, and the anxious attacher has to trust and let go more, knowing the avoider will be back. Both of these changes are difficult; limbic responses happen very, very fast, below the conscious level and often outside of language.

The easiest way to form an ‘earned secure’ attachment is by being in a relationship with a secure attacher, and learning healthy intimacy from them, in which needs are responded to as they arise. However, secure attachers usually date a few people, then pick one and settle down early. They know how to create a big warm home bond. Avoidant attachers tend to prefer anxious attachers, and anxious attachers tend to be drawn to avoidant attachers, because each reinforces the early ‘rules’ about ‘reality’ – actually just haphazard chance, what happened to be going on between them and their caregivers at the time – laid down in their limbic brains before the age of three.

Shame and guilt over which kind of attachment style you have are completely not appropriate or called for, as one’s attachment style is wired in from an age when we are much too young to choose. It is no one’s fault. However, shame and guilt can be quite convincing even when completely uneccessary, as is the nature of shame. It can be incredibly convincing to the person experiencing it even when it is completely absurd.

What does all this have to do with assault?

That summary – above – is what the books say. But like the boy in Gifts, many of us are fumbling into an even bigger picture, trying to see a pattern that is just coming clear. Our culture does not give us many signposts. I’m trying to put things together.

Fundamentally, a healthy, secure attachment style is what lets people effectively protect and care for the wellbeing of others. It allows for the skill of attunement: recognizing when someone wants to come close and when they want space, not only by asking but also by reading subtle nonverbal cues.

Attachment styles can land in any gender, of course, and people can combine in any combination.

However, when attachment styles land in particularly gendered ways, we see certain patterns emerge that are all part of the bigger pattern, and, maybe, they can be understood as part of the ‘answer’ to the question of violence.

People with secure attachment styles are better at recognizing and being comfortable with this dance of approach-and-retreat, better at supporting others while letting others do what they need to do. They know deep down they are loved and loveable, and thus are more likely to be loving and nurturing towards others, both to be there for them when needed as sources of strength and solace, and to be able to recognize and honour when someone does or does not want to be touched. Shame prevents this skill from emerging.

We misunderstand shame

Attachment science tells us that human beings need mirroring and containers in others. Whatever is in us that does not get mirrored, or held in a larger container of acceptance by others, becomes a source of shame, simply for not being accepted. And if you have shamed something in yourself – like a normal need for intimacy – so early and so completely that you don’t even notice you are doing it, you will interpret that same need as shameful when you see it in others. Shame is entirely subjective, in this sense. This is all happening in the body, below the conscious level, not in a vague ‘unconscious’ but in a recognizable region of the brain: the limbic brain, which does not have language.

Shame and guilt unhealed and unaddressed remain powerful and, like a volcano, rise up in surprising ways. For instance, shame can lead men to shut down and run or blame women or act defensive instead of offering comfort and nurturance when someone they care about needs them. It can, alternately, lead men to ignore signs that someone does not want them close.

These are two sides of the same system, and must be understood together, because in a culture that does not expect men to show up for their own emotions, women get blamed for unaddressed male shame. 

In other words, it seems possible that shame and guilt, left subterranean, interrupt attunement, and can lead to an inability or unwillingness to properly respond to the needs of others, whether for nurturance or for space. I mean the really deep, structural kind of shame, that is so old and convincing, it doesn’t even appear as anything in particular. It just appears as ‘the way the world is,’ laid down in patterns in the limbic brain. This kind of shame hides, appears as nothing in particular, until questioned with compassion and curiosity, deeply, in safe company.

Anxious attachment styles and the mystery of human relating

In a patriarchal, misogynist culture, both of these imbalances (which are common to all humans), when they appear in men, are laid in women’s laps as blame and misogyny when men do not do their own emotional healing.

I am making sense of this, bit by bit, seeing the pattern emerge. For instance: men with anxious attachment styles may feel distress when an attachment figure seeks to back up a little, or a lot, and may not develop a healthy capacity to recognize and respond appropriately to someone’s nonverbal cues communicating the need for space.

They may come closer or become upset as the other person signals their need to disengage. If a man who happens to have an anxious attachment style does not know how to understand and accept his own needs for nurturance, he may attack a woman for rejecting him. The typical ‘hello, cutie,’ on the street followed almost instantly with ‘fine, be that way, bitch’ is an example many of us will be familiar with.

They may not notice or register or in extreme cases be concerned that someone they want to touch has frozen up, is giving off signals of paralysis or distress. Thus we sometimes find men who don’t think of themselves as ‘bad men’ who nonetheless rape and assault: their partners, girlfriends, wives, or women on a first or second date. (This is how the majority of assaults happen, of course: the ‘man jumping out of the bushes’ while more spectacular is much more rare.) They may resort to seeking power-over and dominance, because normal intimacy needs, when distorted and denied, come out in distorted ways. They are caught up in their own pain and can’t name it, or find appropriate avenues for it, and given the larger social norms that centre men’s experiences, this imbalance doesn’t get addressed as an imbalance but instead gets projected out into the world. A society that actively, financially, politically, socially, privileges traits it deems ‘masculine’ – nonemotionality, strength, independence – and actively disparages traits it deems ‘feminine’ – interdependence, nurturance – has few ways for these patterns to be openly loved, addressed, and changed.

In another example, those with a preocuppied-avoidant style – who feel the need for closeness but have a hard time asking and do not expect others to be there for them – may sulk if they feel rejected, putting silent pressure on women they are with to meet their demands. Perhaps the sulking partner who turns away in anger when sexual desires aren’t met may be having a limbic attachment experience that needs to be addressed as such, in a mature way, a way that takes ownership of the experience and works to heal it rather than project it outwards onto women.

Avoidant attachment styles: holding trust

Those with a dismissive-avoidant style may simply need to develop attunement in order to hold the trust they are given. They may want women to get close to them at first, and begin to build trust, but not actually know how to maintain trust once it begins, which can create destabilizing and confusing experiences for everyone involved.

When men happen to have a dismissive-avoidant attachment style, they may simply not know how nurturance and comfort looks and feels. They may have a very difficult time recognizing and loving their own deepest selves, and not even be aware of what they have lost. Thus they may blame women for being ‘too needy’ out of not recognizing their own needs for closeness and nurturance of self and others, having learned early that closeness is suffocating and that needs are to be denied.

They may not recognize their own body’s needs for comfort and connection, which result in elevated heart rate and changes in neurochemicals just as it does for anxious attachers, but in a way the avoidant attacher does not understand or recognize as they learned early on to repress these needs completely in themselves and others. They may not know how to meet their own and other people’s needs simultaneously, a highly developed nurturance capacity.

Even if they do not act in invasive ways, their style may inadvertently interrupt the creation of deep, honest, nurturing relationships, in which women they sleep with or get close to can feel emotionally safe with them.

In striving to be good people they may make ‘rules’ (like ‘a good man doesn’t touch,’) and have a very logical approach to checking if a woman wants to be touched, but have a harder time responding to her nonverbal cues or even sometimes responding to verbal cues for comfort and reassurance, creating an odd gap feeling.

The attachment needs are still there, but they may transmute into other more recognizeable things: instead of giving and receiving nurturance they may seek sexual connections while feeling utterly bewildered about how physical love relates to intimate or consummate love. They may experience immense, paralyzing guilt and shame when someone needs them to be comforting, and lash out, freeze up, or run. They may hurt people they care about by having sex with them in a strangely cold or distant way, without even knowing why they are doing it.

If a man with an avoidant attachment style experiences internal distress when someone he cares about expresses nurturance needs (such as the need for trust, reliability, availability, closeness, responsiveness, attunement) he may blame the woman for ‘being too needy’ instead of dealing with those intensely confusing feelings of shame.

Men with avoidant attachment styles may not notice the confusing nonverbal signalling they are actively doing very early on that prevents safety from happening with women they want to nurture and support, who may become more and more imbalanced towards them in response.

Since ‘absence of nurturance’ is just an absence, it can be hard to recognize early. When early avoidant responses to requests for closeness are not noticed as such, attachment science teaches us, ‘protest behaviour’ – the distress when needs aren’t met – may get louder over time, in ways both people are contributing to and neither understand. It becomes all too easy in a patriarchal culture that values rugged individualism over interdependence to call an anxiously-attached woman ‘crazy’ without noticing the parallel avoidant responses that are contributing, that are ‘crazymaking’. In other words, it takes two to enter into the avoidant-anxious trap, but patriarchal culture normalizes an avoidant style and stigmatizes an anxious style, wherever it appears.

None of this is worthy of shame; fundamentally, all of the insecure styles are based in an unquestioned belief that people will not be there for them and that nurturance is somehow a problem rather than wholly desireable and good. Avoidant attachers ‘know’ from an early age that the ice will break, the chair will collapse, best not to try. Insecure attachment styles are not chosen, are not conscious or intentional, and it is an understatement to say they are not easy to change. They deserve understanding, compassion, and empathy.

And yet living without loving, secure attachment bonds is the loneliest experience in the human repertoire.

Community care and cultural transformation

The solution to this is not to pile on more shame and guilt. This is really tricky, because insecure attachers have limbic brains [structured by shame and guilt and may hear accusations where there are none](https://norasamaran.com/2016/07/30/cognitive-distortions/). The solution is not to shame people for feeling shame. Instead, the solution is a complete transformation of social relations to allow wholeness back into our world. Yes, models of healthy interdependence exist if we know where to find them and how to recognize them. But no one stands in a shining circle of light and no one lives in the dark abyss; it is time we finally abandon these Eurocentric, western dichotomies.

What we need is a model for slow self-love that brings the shame up into the light, and reality checks with others who accept you unconditionally, hold you accountable, and aren’t going anywhere. We need a model of justice that recognizes the lived reality of interdependence and learns to do it well, not a justice of shame that frightens us all out of looking at our shadow sides or weakest selves in a world in which most men are expected to cut off parts of themselves from the time they are quite young.

The solution, in tangible terms, is community care and a great deal of awareness of how most of us did not get our needs met at key developmental stages, which means we did not move out of those stages and must do so now. Collective healing is possible. We can heal when we can finally be our whole, unguarded selves, in human community, without shields or guards, and be liked, accepted, seen, held. This is systemic change, spiritual change, at the core levels of our culture, lived each day.

Once shame can be reduced to more manageable levels, both personally and culturally, people can become more able to openly expose their raw spots trusting they will be accepted, and can [respond to the needs of others](https://norasamaran.com/2016/10/11/psychological-violence-is-physical-violence/) rather than freeze and become defensive, invasive, or paralyzed.

Turning the gifts around: masculine nurturance culture

The answer to all of these difficulties is to openly discuss nurturance: how it looks, how it feels, how men can learn to practice it from the men who already know how in addition to communicating through women or fumbling around for years learning by trial and error.

Simplistic answers gleaned through this fumbling do not help: for instance, some men may actually avoid nurturing or protecting women out of fear of ‘white knighting.’ But ‘white knighting’ isn’t synonymous with ‘all forms of protection.’ White knighting means acting ‘protective’ in ways that aren’t attuned. Paternalistically telling her what she needs instead of listening to what she says is white knighting. To stop white knighting, don’t stop protecting; just protect while you also listen and believe. Protect her, actively, in the ways she actually wants protecting, and not in the ways she does not. Protecting people you care about – in ways that are attuned and responsive to their actual needs – is a normal, needed, and healthy part of nurturance. Only in the wasteland of guessing and fumbling alone would this confusion even be possible.

Why is there no high-profile institute for men teaching nurturance skills to men?

Men need to do this work with other men – not alone, not instead of doing it with women, but in addition, in accountable relationship with and to women. In other words, keep learning in the ways learning is happening now – but then share that learning with one another. Our institutions need to count this work as valuable, rewardable labour: fund it, give it high prestige, give it speaking tours and jobs in teaching nurturance. Read that line a few times. It sounds so impossible, doesn’t it?

The absurdity of that line suggests it may be a long time before a nurturing masculinity is recognized and rewarded socially the same way an abstract intellectual masculinity currently is.

In the meantime, men need to do this healing work every day, behind the scenes, reaping the rewards of having women and people of all genders feel safe with them, and of growing their own self-love and love of one another.

The wonderful reward of creating safe bonds is that in these places of trust, a warm glow of meaning and purpose emerges.  An inner circle of trust and vulnerability allows movement and rest: it lets the bees come and go from the hive. It creates shelters of chosen family and beloved community from which action, challenges to racism, sexism, institutional violence, can arise, a safety net to catch each other’s bodies and souls, the foundation that allows risk.

The opposite of masculine rape culture is masculine nurturance culture. This is men’s work to do, and yet it is needed by people of all genders who have men in their lives. The rewards are waiting.

Are you a nurturing man? Do the women in your life – partner, daughter, sister, friend, coworker, parent – tell you or show you that you make them feel unusually safe and close and cared for? If so, how did you learn? How do you open up spaces for men who want these conversations to begin to have them?

Every single man I asked this of said, “both men would need to want it.” Fear of closeness, masculine codes of interaction, the lower-level lizard-brain signals that men send one another, are real and are part of the picture. But many men are struggling with these questions, locked alone in their own little boxes.

Men have to do this with other men, despite the difficulties in doing so, for three reasons. For one, men understand what it is like to be a man much better than women do, and they can teach one another while understanding what it actually feels like and having compassion for one another. Men must also do this with other men because, frankly, women cannot be responsible for healing men while they also protect themselves from male violence and neglect, which is still endemic and thus a daily part of women’s lives. Finally, one of the great distortions of the human spirit in our culture is that each man lives in solitary confinement, thinking they can and should solve problems alone, that they shouldn’t need others. Jumping the barriers that keep men from talking about emotions with other men is itself a fundamental change, one that reduces shame and confusion.

So how do you know when men around you – the friend you just met for drinks, the colleague you have collaborated with on projects for years, the hockey buddy – may actually be quietly confused and thirsty for this kind of learning?

How can you signal your availability, to let men in your life know you are doing this yourself, so that those men who want to learn about nurturance can find each other? It’s as simple as starting a men’s discussion group based on this article.

It can be as simple as sharing this piece, and asking, “does this ever come up for you?”

It can be as simple as sending someone you know this piece, and saying “I’m available.”

It can be as simple as posting this piece, and saying “I’m here.”

—–

Hey, good news: this has become a book! It’s coming out in June with my favourite little indie publisher, AK press. You can already pre-order a copy here: <https://norasamaran.com/2018/10/25/book-release-date-announced-turn-this-world-inside-out-the-emergence-of-nurturance-culture/>

 

Next: [Own, Apologize, Repair: Coming Back to Integrity](https://norasamaran.com/2016/08/28/variations-on-not-all-men/)

Resources:  
Hold me Tight, Sue Johnson  
[Wired for Love](https://www.goodreads.com/book/show/13225387-wired-for-love), Stan Tatkin  
A General Theory of Love, Thomas Lewis, Fari Amini and Richard Lannon  
Attached, Amir Levine and Rachel S.F. Heller  
Bell Hooks, [The Will to Change: Men, Masculinity, and Love](http://www.goodreads.com/book/show/17601.The_Will_to_Change)

[Become a patron! Link to Nurturance Culture Patreon](https://www.patreon.com/NoraSamaran?utm_medium=social&utm_source=facebook&utm_campaign=creatorshare2&fbclid=IwAR2Qbj8bGkhI2j97_MTsiljck-cxm--sndobRLnttYMVHa6hhR56p_W0vz8)

I love hearing from readers! Reach the author at nora.samaran@gmail.com.

I apologize that I may not be able to respond to each request individually, but I read every email that comes in. 🙂

 

Please share widely!

I prefer people share by linking directly to this page. Please do not copy content to a different page without the express permission of the author, because bringing readers to this writing space is a big part of what helps make the work meaningful. Thank you!

I love this Bay Area Transformative Justice pod mapping worksheet so much that big, dramatic, hyperbole feels called for. ie I wanna shout it from the rooftops and say it again and again: if you consider yourself a feminist man, or you allow others around you to let you walk around with this identity and you enjoy having that reputation, or if you find you get laid or get dates or partners because of this reputation, and **if you have not yet mapped out your pod of people who you would want to call you on it when you act in abusive ways**, then do this right now. like today. like right away. Because it is everything, it is wonderful: <https://batjc.wordpress.com/pods-and-pod-mapping-worksheet/>

For a world in which everyone can feel safer, including those who harm and those who cause harm. Thank you.

This is an incredibly on point and insightful piece from Everyday Feminism I highly recommend you read and act on right away:  [Abusive ‘Feminist’ Men Exist — Here Are 6 Things Men Can Do to Stop Them](http://everydayfeminism.com/2014/12/abusive-feminist-men/?utm_content=buffer49ce8&utm_medium=social&utm_source=facebook.com&utm_campaign=buffer)

\*men: I want to be clear here that I am using this term in a trans-inclusive way, referring to masculine-identified people. I have chosen not to write ‘men and trans men’ etc in the piece above because I’ve been told and understand trans men do not need their own separate signifier as that suggests they aren’t already part of the main signifier. I recognize there are different opinions on how to do this well; as a ciswoman I’m no expert, am open to feedback so let me know if this works. For now until I hear otherwise, I’m going with the approach that made the most ethical sense to me when I heard it.

Puung image used with permission by the artist. See more here: <http://www.grafolio.com/puuung1/illustration.grfl>

Also feel free to join the [Nurturance Culture and Masculinity Discussion Space](https://www.facebook.com/groups/nurturanceculture/) online to connect with other men (and people of all genders) doing this work.[  
](http://www.mediacoop.ca/blog/264)**  
Do you love speculative fiction and social justice? I am working on a speculative fiction project that deals with the transformations our planet is undergoing, and the undoing of cultures of domination. Cipher is currently seeking collaborators, advisors, an agent, and a publisher. Learn more about [Cipher](https://norasamaran.com/2016/03/26/966/) here.  
**

Men’s discussion groups all over the world have gotten in touch to say they’re discussing this piece. If you start a men’s Nurturance Culture discussion group please let me know. 🙂

Dating Tips for the Feminist Man was originally housed at the Media Coop, and is archived [here](http://www.mediacoop.ca/blog/264)

Find out your attachment style with this [quiz](http://www.attachedthebook.com/compatibility-quiz/).

Did you like this post? Please share it using the buttons below

Follow Nora on [Twitter](https://twitter.com/NoraSamaran) and [Facebook](https://www.facebook.com/nora.samaran)

![tipjar](https://datingtipsforthefeministman.files.wordpress.com/2016/02/tipjar.png?w=100&h=107)

[Tip Jar! help keep the writing going – donate with Paypal by clicking here](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ZDX7GPSLDXJ9J)

Advertisements

### If you like this post, please help out, share widely!:

-   [](https://norasamaran.com/2016/02/11/the-opposite-of-rape-culture-is-nurturance-culture-2/?share=email "Click to email this to a friend")

    Email

-   [](https://norasamaran.com/2016/02/11/the-opposite-of-rape-culture-is-nurturance-culture-2/?share=twitter "Click to share on Twitter")

    Twitter

-   [](https://norasamaran.com/2016/02/11/the-opposite-of-rape-culture-is-nurturance-culture-2/?share=facebook "Click to share on Facebook")

    Facebook

-   [](https://norasamaran.com/2016/02/11/the-opposite-of-rape-culture-is-nurturance-culture-2/?share=tumblr "Click to share on Tumblr")

    Tumblr

-   [](https://norasamaran.com/2016/02/11/the-opposite-of-rape-culture-is-nurturance-culture-2/?share=pinterest "Click to share on Pinterest")

    Pinterest

-   [](https://norasamaran.com/2016/02/11/the-opposite-of-rape-culture-is-nurturance-culture-2/?share=reddit "Click to share on Reddit")

    Reddit

-   [](https://norasamaran.com/2016/02/11/the-opposite-of-rape-culture-is-nurturance-culture-2/?share=linkedin "Click to share on LinkedIn")

    LinkedIn

-   

### Like this:

Like

Loading...

### Related
