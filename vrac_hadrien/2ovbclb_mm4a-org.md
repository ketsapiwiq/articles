---
title: "MOAB Bombing Shows CNN Is Actually More Obsessed Than Fox News With Bomb Videos"
url: http://mm4a.org/2ovBclb
keywords: bombing,coverage,cnn,airing,minutes,footage,fox,msnbc,test,videos,2003,shows,moab,actually,obsessed,bomb
---
After the U.S. military dropped the most powerful conventional weapon in its arsenal, nicknamed the “mother of all bombs,” on an Islamic State complex in eastern Afghanistan, cable news networks responded with almost continuous coverage of the event, but the visuals in the networks’ coverage varied widely. Fox News spent about 21 minutes airing video footage from a 2003 test of the bomb, MSNBC barely used video footage at all, and CNN played and replayed the bomb test footage for a staggering almost 54 minutes in just six hours. In their coverage, Fox News and MSNBC both mentioned the potential for civilian casualties as a result of the bombing eight times, while CNN mentioned civilians 15 times.

US Drops Massive Bomb Developed In 2003 For First Time Ever
-----------------------------------------------------------

**CNN: U.S. Drops Most Powerful Non-Nuclear Bomb On ISIS Target In Afghanistan.** CNN reported that the Massive Ordnance Air Blast (MOAB) bomb, also known as the “mother of all bombs,” was dropped on an “ISIS cave and tunnel complex and personnel” in Nangarhar province in eastern Afghanistan. The weapon was developed during the Iraq War and was “mainly conceived as a weapon employed for ‘psychological operations.’” President Donald Trump, who “declined to say whether he personally signed off on the strike,” called it “‘another successful job.’” \[CNN, [4/13/17](http://www.cnn.com/2017/04/13/politics/afghanistan-isis-moab-bomb/)\]

In Six Hours Of Coverage, CNN Aired Almost An Hour Of Bomb Test Footage And MSNBC Aired Almost None
---------------------------------------------------------------------------------------------------

![](HTTPS://CLOUDFRONT.MEDIAMATTERS.ORG/static/uploader/image/2017/04/14/1_MOAB-Footage_Cable.png)

**CNN Spent Nearly An Hour Airing Bomb Test Footage From 2003.** In six hours of coverage of the April 13 U.S. bombing in Afghanistan, CNN repeatedly aired footage of the 2003 bomb test that was conducted by the U.S. Air Force, for a total of nearly 54 minutes.

**Fox News Spent 21 Minutes Airing The Bomb Test Footage.** During Fox News’ coverage of the U.S. bombing in Afghanistan, the network spent approximately 21 minutes airing video footage of the 2003 bomb test.

**MSNBC Relied Mostly On Still Images, Airing Only Five Seconds Of Bomb Test Footage.** During MSNBC’s coverage of the bombing, the network aired only five seconds of video footage of the 2003 bomb test.

![](HTTPS://CLOUDFRONT.MEDIAMATTERS.ORG/static/uploader/image/2017/04/14/1_Afghanistan-Consequences_Cable.png)

**CNN Made 15 Mentions Of Potential Civilian Casualties, While Fox News And MSNBC Both Made Eight Mentions.** Despite airing the bomb test footage for nearly 54 minutes in six hours, CNN mentioned possible civilian casualties only 15 times in the six-hour period. Fox News mentioned the topic eight times, and MSNBC, despite airing only five seconds of bomb test footage, also mentioned possible civilian casualties eight times.

**Methodology:** Media Matters searched Snapstream for MOAB bomb testing footage aired by CNN, MSNBC, and Fox News between noon. and 6 p.m. on April 13, 2017. Time count began when bomb test footage appeared on the screen and ended when the footage ended or cut to still images of the test. To identify discussions of the bombing’s impact on civilians or the potential for collateral damage, Media Matters searched for mentions of “civilian OR collateral” in Snapstream transcripts for CNN, MSNBC, and Fox News shows from noon. to 6 p.m. on April 13, 2017.

Additional research provided by Nick Fernandez, Jared Holt, and Madeline Peltz
