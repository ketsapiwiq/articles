---
title: "Les femmes de droite, par Andrea Dworkin"
url: http://antisexisme.net/2012/12/20/les-femmes-de-droite/
keywords: modèle,femme,andrea,cest,quil,sexe,femmes,masculine,dworkin,hommes,sexuelle
---
Les femmes de droite, par Andrea Dworkin
========================================

![les femmes de droite](https://antisexisme.files.wordpress.com/2012/12/les-femmes-de-droite.gif?w=660)

Les femmes de droite est un ouvrage écrit par Andrea Dworkin, féministe radicale, en 1983.  Cette dernière nous propose une

![andrea dworkin](https://antisexisme.files.wordpress.com/2012/12/andrea_dworkin.jpg?w=216&h=163)

explication à l’allégeance de certaines femmes  à la droite  américaine dure. Comment expliquer que ces femmes adhèrent à des idées opposées à leurs intérêts ? Comment des femmes peuvent-elles être anti-avortement, anti-contraception ou encore rêver d’être femmes au foyer ? Andrea Dworkin nous donne une réponse convaincante dans cette œuvre.

Ce livre a été traduit en français par  Martin Dufresne et Michele Briand. Il est préfacé par Christine Delphy. Martin Dufresne m’a très gentiment envoyé le manuscrit quasi-finalisé. Je vous propose un compte-rendu de ma lecture fort passionnante.

Le livre arrivera en France au mois de mars. Il sera diffusé par la **Librairie du Québec**, à Paris, à qui les libraires et les individus pourront le commander directement.  
Leurs coordonnées sont : 30 Rue Gay-Lussac 75005 Paris, France +33 1 43 54 49 02

Chapitre 1 : la promesse de l’extrême droite
--------------------------------------------

Les femmes vivent dans un milieu dangereux (viol, violence conjugale, etc.) et cherchent avant tout à survivre. Elles obéissent donc aux règles des hommes et pensent qu’elles seront ainsi protégées contre la violence masculine. **La droite propose aux femmes une certaine sécurité si elles acceptent d’obéir aux règles**. Cela explique pourquoi elles ont tendance à être conservatrices.  
Dworkin estime que c’est un suicide.

> De la maison du père à la maison du mari et jusqu’à la tombe qui risque encore de ne pas être la sienne, une femme acquiesce à l’autorité masculine, dans l’espoir d’une certaine protection contre la violence masculine. Elle se conforme, pour se mettre à l’abri dans la mesure du possible. C’est parfois une conformité léthargique, en quel cas les exigences masculines la circonviennent progressivement, comme une enterrée vive dans un conte d’Edgar Allan Poe. Et c’est parfois une conformité militante. Elle sauvera sa peau en se démontrant loyale, obéissante, utile et même fanatique au service des hommes qui l’entourent. \[…\]. Quelles que soient les valeurs ambiantes, elle les incarnera avec une fidélité sans faille.  
> Les hommes respectent rarement leur part du marché tel qu’elle l’entend : la protéger contre la violence masculine.

Chapitre 2 : la politique de l’intelligence
-------------------------------------------

L’intelligence est refusée aux femmes. L’intelligence est une forme d’énergie qui modifie le monde, et qui a besoin de le connaître pour se développer : le récurage des WC ne permet pas le développement de son intelligence. Deux tiers des analphabètes sont des femmes. L’intelligence qui influence le monde – l’intelligence créatrice-, constitue l’inverse de la féminité. Même chez les femmes des classes supérieures, instruites, l’intelligence sert avant tout à décorer.  
Cela ne signifie pas que les femmes ne sont pas intelligentes, mais que l’on nie leur intelligence, et que celle-ci doit rester confinée à une fonction décorative, au lieu de servir à modifier le monde.

Dworkin parle également d’une **intelligence sexuelle, consistant à réussir à respecter son intégrité, à pouvoir posséder son propre corps et à n’avoir des rapports que s’ils sont désirés**. L’intelligence sexuelle ne se mesure pas en nombre d’orgasmes ou de partenaires, ou d’enfants portés. Ce n’est pas montrer des fesses, mais poser des questions et proposer des théories.

Par ailleurs, les femmes sont sous-payées, et dépendent donc économiquement des hommes. Elles doivent donc exercer un labeur sexuel pour de l’argent, que ce soit dans la prostitution ou le mariage.

Les femmes de droite se disent qu’il veut mieux être une épouse qu’une prostituée : on appartient à un seul hommes. De plus, elles se disent que celles qui ont un emploi sont dupées : en plus d’un travail sous-payé, elles doivent au final quand même exercer le labeur sexuel.

> Les femmes de droite ont examiné le monde ; elles trouvent que c’est un endroit dangereux. Elles voient que le travail les expose à davantage de danger de la part de plus d’hommes ; il accroît le risque d’exploitation sexuelle.\[…\] Elles voient que le mariage traditionnel signifie se vendre à un homme, plutôt qu’à des centaines : c’est le marché le plus avantageux. \[…\]. Elles savent également que la gauche n’a rien de mieux à offrir : les hommes de gauche veulent eux aussi des épouses et des putains ; les hommes de gauche estiment trop les putains et pas assez les épouses. Les femmes de droite n’ont pas tort. Elles craignent que la gauche, qui élève le sexe impersonnel et la promiscuité au rang de valeurs, les rendra plus vulnérables à l’agression sexuelle masculine, et qu’elles seront méprisées de ne pas aimer ça. Elles n’ont pas tort. Les femmes de droite voient que, dans le système où elles vivent, si elles ne peuvent s’approprier leur corps, elles peu-vent consentir à devenir une propriété masculine privatisée : s’en tenir à un contre un, en quelque sorte.

Chapitre 3 : l’avortement
-------------------------

![foetus](https://antisexisme.files.wordpress.com/2012/12/foetus.jpg?w=150&h=129)

Les femmes qui ont avorté à l’époque où c’était encore un crime, étaient très souvent mariées et bonnes mères de famille ; elles ont gardé le silence pour éviter le honte. On leur a appris que toute vie a plus de valeur que la leur. Les hommes refusent l’avortement car ils s’identifient au fœtus. Ils pensent que ce fœtus, ça aurait pu être eux.  
La loi, par le mariage, remet une femme à un homme pour qu’il puisse la baiser à volonté (à l’époque où Dworkin écrit son livre, le viol conjugal est encore très souvent non condamné par la loi aux USA, et même quand il l’est, il est en réalité très peu puni). La grossesse est une conséquence de ce coït. Les femmes ne contrôlent pas la venue de la grossesse, car elle ne contrôle pas quand a lieu le coït.

> Les femmes sont tenues de se soumettre au coït, et elles peuvent ensuite être tenues de se soumettre à la grossesse.Les femmes sont tenues de se soumettre à l’homme, et elles peuvent ensuite être tenues de se soumettre au fœtus.

Le sexe forcé maintient le coït comme élément central dans la sexualité. Dworkin note que la force masculine est considérée comme « sexy » et sert de mesure au désir masculin. **Plus il y a de force dans l’acte sexuel, plus celui-ci semble sexuel.**  
Sans la force, les hommes ne pourraient amener les femmes à la « baise ». Elle énumère plusieurs types de forces :

> Le premier type de force est la violence physique : omniprésente dans le viol, la violence conjugale, l’agression.  
> Le deuxième type de force est la différence de pouvoir entre les hommes et les femmes, qui fait d’emblée de tout acte sexuel un acte de force – par exemple, l’agression sexuelle des filles dans la famille.  
> Le troisième type de force est économique : le fait de maintenir les femmes dans la pauvreté pour les garder sexuellement accessibles et sexuellement soumises.  
> Le quatrième type de force est culturel, sur une grande échelle : une propagande misogyne qui transforme les femmes en cibles sexuelles légitimes et désirables

Elle précise que les femmes sont exploitées en tant que classe de sexe, et que donc on ne peut se référer à leur sexualité sans tenir compte du contexte de sexe forcé. Chaque femme vit dans ce régime de sexe forcé. Il y a des croyances comme quoi les hommes utilisent la force car ils sont hommes, et que les femmes aiment la force.

Or le coït transgresse les limites du corps. Ni la procréation ni le plaisir n’exige d’en faire l’acte sexuel central. Mais **le coït est le symbole de la condition subordonnée des femmes et c’est pour cette raison qu’il est si répandu.**

Ensuite, Dworkin évoque le mouvement hippie des années 1960. Les hommes avaient les cheveux longs et des chemises colorés. Ils ressemblaient à des filles, et les filles ont donc cru qu’ils pouvaient être des alliés, car ils n’avaient pas peur d’être considérés comme efféminés. Ils leur ont promis pleins de choses, notamment l’égalité hommes-femmes.  
En réalité, ils ont surtout mis en place des harems : les femmes ont été échangées, jetées, partagées. Si elles refusaient, elles étaient considérées comme coincées.

> Les filles des années soixante vivaient ce que les marxistes appellent – mais ne reconnaissent pas dans ce cas-ci – une « contradiction ». C’est précisément en tentant d’éroder les frontières du genre par une pratique apparemment neutre de libération sexuelle que les filles investirent de plus en plus l’acte le plus réificateur du genre : la baise.  
> \[…\]  
> En termes empiriques, la libération sexuelle fut pratiquée à une vaste échelle par les femmes durant les années soixante, et elle échoua : c’est-à-dire qu’elle ne les libéra pas. Son but –découvrit-on – était de libérer les hommes afin qu’ils puissent utiliser les femmes hors des contraintes bourgeoises, et en cela elle a réussi. Une de ses conséquences pour les femmes fut d’intensifier l’expérience d’être sexuellement typées comme femmes – précisément le contraire de ce que ces filles idéalistes avaient envisagé comme avenir.

La grossesse était un frein à la baise pour ces hommes de gauche. Une femme enceinte pouvait plus facilement la refuser. Par

![Hippies](https://antisexisme.files.wordpress.com/2012/12/hippies.jpg?w=300&h=188)

ailleurs une femme avec un enfant, ça réduit aussi la baise : elle doit s’en occuper et ne peut pas être toujours disponible. Donc les hommes de gauche s’investirent dans la lutte pour l’IVG et la contraception.

Entre temps, les femmes de gauche se rendirent compte qu’elles avaient été baisées. Elle quittèrent les mouvements des hommes, la contre-culture et firent un mouvement bien à elles : **le mouvement féministe**.  
Elles se rendirent compte qu’elles avaient vécu de nombreuses violences sexuelles sous prétexte de « libération sexuelle ». Elle considérèrent donc que **la liberté sexuelle pour une femme passe d’abord par la maîtrise de son corps dans le champs de la sexualité et de la reproduction.**

Les hommes de gauche n’apprécièrent pas cela :

> mais pour les hommes, \[le féminisme\] fut une impasse – la plupart d’entre eux ne virent jamais le féminisme autrement que sous l’angle de leur privation sexuelle ; les féministes leur enlevaient la baise facile.

Soudain, les hommes de gauche délaissèrent le combat de l’IVG : si c’était pour baiser, ils étaient d’accord, mais ils s’y opposèrent si le but était que les femmes contrôlent leur fertilité. Ils laissent ce combat de côté en espérant que le droit à l’IVG soit banni. Ainsi, les femmes reviendront à eux, bien à leur place, les jambes écartées, pour les supplier de faire quelque chose. Ainsi, les femmes auront l’IVG, mais aux conditions des hommes.

Les femmes de droite pensent que l’avortement est lié à l’avilissement sexuel des femmes. Elles croient que l’explosion de la pornographie est une conséquence de la légalisation de l’IVG. Elles savent que le contrôle de son corps est juste un prétexte pour l’IVG : ce que veulent vraiment les hommes, c’est de la baise.  
Elles savent que la droite et la gauche réduisent les femmes à la baise. Mais elle trouve que la droite est un peu plus généreuse : on ne se fait baisée que par un homme dans le cadre du mariage. Un homme a moins de « force » que dix.

Elle savent que la grossesse permet de responsabiliser un peu les hommes sur les conséquences du coït. Par ailleurs, la grossesse est aussi une bonne excuse pour refuser le coït.

Si elles doivent avorter, elles avorteront illégalement, dans le silence, en priant et en espérant échapper à la mort. S’opposer à l’avortement est une folie, selon Dworkin. Car légaliser l’avortement est le seul moyen d’éviter la boucherie.

Chapitre 4 : Juifs et homosexuels
---------------------------------

La droite chrétienne étatsunienne est très hostile aux homosexuels et aux juifs.

Dworkin évoque le racisme et précise que les hommes du groupe racial méprisé sont sujets à deux stéréotypes sexuels. Ils sont décrits :

-   soit comme des violeurs, à la virilité intense et au membre énorme.
-   soit comme des castrés, des efféminés, ou des homosexuels.

La droite joue de ces stéréotypes pour justifier et maquiller l’exercice du pouvoir contre les êtres humains des catégories dominées.

**Exemples** :

-   les hommes allemands ont été dévirilisés par l’issue de la 1ère guerre mondiale. Ils ont alors avili un groupe d’hommes perçus comme plus masculins (les juifs), comme pour retrouver une virilité perdue..
-   Aux USA, les hommes noirs ont été perçus comme des violeurs seulement après l’ère esclavagiste. Avant, ils étaient considérés comme efféminés, semblables à des bêtes de somme, propriétés de leur maître. Après l’abolition de l’esclavage, les hommes blancs se sont sentis castrés. Ils créèrent alors l’image du violeur comme image-mirroir de ce qu’ils avaient perdu : le droit au viol systématique des femmes de l’autre race.

L’Ancien Testament est beaucoup moins hostile aux homosexuels que le Nouveau Testament. Les règles du Lévitique avaient pour objectif d’assurer la domination masculine, et donc d’éviter les conflits entre hommes, que peut générer l’homosexualité masculine. Il était en fait considéré comme répugnant d’utiliser les hommes comme des femmes, comme des objets sexuels bons à être baiser, car cela risquait de fragiliser le patriarcat. Dworkin évoque à ce sujet la légende de Lot, qui montre le viol des femmes comme acceptable, alors que celui d’un homme est odieux.

![lot sodome](https://antisexisme.files.wordpress.com/2012/12/lot-sodome.jpg?w=660)

C’est Saint Paul qui a introduit la haine des homosexuels, une haine que continue à ressentir la droite fondamentaliste américaine. Saint Paul a par ailleurs associé homosexualité et judaïsme. Pour les juifs, l’image de la faiblesse associée à l’homosexualité est toujours une menace, dans un monde qui les a presque exterminés. Ceci explique pourquoi Israël est un état militariste : on ne pourra plus accuser les juifs d’être des mous.

Dworkin explique alors pourquoi les femmes de droite rejettent tellement l’homosexualité. Les femmes sont jetables et interchangeables en tant qu’objets sexuels ; elles le sont un peu moins en tant que mères. Elles savent que les hommes n’ont qu’une raison de garder les femmes en vie : elles peuvent porter les enfant.

> Seul le fait d’avoir des enfants modère l’utilisation sexuelle que les hommes font des femmes : les user jusqu’à la corde et les jeter, les baiser à mort, les tuer à petit feu. Si l’on n’a pas besoin de femmes pour gouverner le pays ou écrire les livres ou faire de la musique ou cultiver la terre ou bâtir des ponts ou extraire le charbon ou réparer la plomberie ou guérir les malades ou jouer au basketball, pour quoi a-t-on besoin d’elles ? Si l’absence des femmes de tous ces domaines, de tous les domaines, n’est pas perçue comme une perte, un vide, un appauvrissement, à quoi servent les femmes ?
>
> Les femmes de droite ont affronté la réponse. Les femmes servent à la baise et à faire des enfants. La baise mène à la mort, à moins d’avoir aussi des enfants. L’homosexualité – sa visibilité grandissante, les tentatives de la légitimer ou de la protéger, l’impression qu’il y a là une option attrayante et dynamique, qui gagne non seulement des appuis mais des adeptes – a pour effet de rendre les femmes jetables : la seule chose que peut faire une femme pour être valorisée perdra sa valeur, elle ne pourra plus servir d’assise à la valeur des femmes.
>
> C’est aussi vrai pour l’homosexualité masculine que pour le lesbianisme en ce que l’un et l’autre nient la valeur reproductive des femmes aux yeux des hommes ; mais l’homosexualité masculine est d’autant plus terrifiante qu’elle laisse entrevoir un monde sans femmes – un monde où elles sont vouées à l’extinction

Dworkin dit que les femmes de droite se rendent compte que l’humanité des femmes n’est réduite qu’à leur rôle maternel. Or **l’homosexualité les menace d’être privée même de cela.** Certaines femmes ont mis au monde des enfants car c’était pour elles la seule façon d’avoir un peu une vie de valeur. Elles n’ont compté que là-dessus, et ne veulent pas perdre cela.

Cependant, l’homophobie est un suicide pour les femmes, car elle encourage la haine de tout ce qui est perçu comme féminin.

Chapitre 5 : le gynocide annoncé
--------------------------------

> Lorsque les enfants cessent d’être entièrement désirables, les femmes cessent d’être entièrement nécessaires.

C’est sur cette phrase, énoncé par un journaliste du début du XXième siècle et qui avait déjà attiré l’intention de Virigina Woolf, que Dworkin va bâtir son argumentaire.

**La valeur des femmes ne réside que dans leur valeur reproductive.** Si leurs enfants sont indésirables, les femmes sont considérées comme inutiles ; si leurs enfants sont désirables, les femmes ont alors plus de valeurs. Ainsi :

-   les femmes pauvres et non blanches, dont les enfants sont indésirables, sont stérilisées ou soumises à des programmes de contrôle des naissances.
-   les femmes blanches sont poussées à se reproduire (médailles, etc.)

C’est les deux faces de la même médaille.

Le statut social des personnes âgées s’est fortement dégradé au cours du XXième siècle. Personne ne fait le lien avec le fait qu’avant les vieux étaient surtout des hommes, alors que maintenant les vieux sont surtout des vieilles. Les femmes âgées n’ont aucune valeur, car incapables de se reproduire. Ces dernières – majoritairement blanches, car les noires meurent avant – sont reléguées dans des hospices, une fois devenues inutiles. Là elles connaissent la crasse, l’avilissement et le sadisme. Les jeunes femmes blanches sont empêchées de savoir ce qui les attend, du fait justement que ces personnes âgées soient tenues à l’écart.

![médicaments](https://antisexisme.files.wordpress.com/2012/12/mc3a9dicaments.jpg?w=150&h=100)

Dworkin évoque ensuite la surmédicamentation des femmes (tranquillisant, somnifères, amphétamine…). Les femmes sont perçues comme irrationnelles et émotives. Si elles vont mal, ce n’est pas à cause des conditions objectives de leur existence, mais parce que ce sont des femmes. Les médicaments permettent de les maintenir dans leur rôle social, dans la passivité et le silence. Ce dopage massif montre aussi le peu d’importance qu’on accorde à l’intelligence et à la personnalité des femmes.

Passons aux programmes d’aide sociale. Dworkin évoque la règle de la « mère apte au travail » : on impose un travail aux femmes assistées, si on considère qu’il s’agit pour elles d’un « travail convenable ». Si ces femmes refusent ce « travail convenable », elles sont exclues du régime d’aide sociale. Souvent ces femmes sont incitées à se prostituer. Au Nevada, où la prostitution est légale, la prostitution a déjà été proposée comme « travail convenable ».  
Les assistées sociales n’ont pas droit à une vie sexuelle privée, car l’Etat fouille dedans pour savoir avec qui elles couchent. Il est donc logique de les orienter vers la prostitution.  
Au final, la mission de l’aide sociale est de punir les femmes d’avoir eu des rapports sexuels et des enfants hors mariage. La souffrance des assistées sociales n’est que ce qu’elles méritent. L’aide sociale a au final deux grands rôles :

-   elle crée et maintient un bassin de main d’œuvre disponible à faible prix.
-   elle incite aussi les femmes pauvres à ne pas avoir d’enfants.  
    En bref elle permet un contrôle des femmes et endigue la reproduction des superflues (femmes noires et hispaniques)

La question que pose ensuite Dowrkin est : **qu’arrivera-t-il aux femmes quand les hommes pourront contrôler la reproduction, non seulement socialement, mais aussi biologiquement ?** Qu’arrivera t-il aux femmes qui ne sont plus vraiment nécessaires, soit parce qu’elles ne peuvent plus se reproduire (femmes âgées), soit parce que leurs enfants sont indésirables (pauvres et minorités ethniques) ?

Dworkin dit ensuite qu’il existe deux modèles qui décrivent la façon dont les femmes sont socialement contrôlées et sexuellement utilisées : **le modèle du bordel et le modèle de la ferme**.

> Le modèle du bordel est lié à la prostitution, au sens strict ; des femmes rassemblées aux fins d’être utilisées pour le sexe par des hommes ; des femmes dont la fonction est explicitement non reproductive, presque antireproductive ; des animaux sexuels en rut ou qui feignent de l’être, s’affichant pour le sexe, qui se pavanent et posent pour le sexe.
>
> Le modèle de la ferme est lié à la maternité, aux femmes en tant que classe ensemencées par le mâle et moissonnées ; des femmes utilisées pour les fruits qu’elles portent, comme des arbres ; des femmes allant de la vache primée à la chienne pelée, de la jument pur-sang à la triste bête de somme.

Ces deux pôles semblent opposés, mais ils ne le sont qu’en apparence. Une femme peut connaître dans sa vie ces deux conditions.  
Le modèle du bordel est accepté seulement parce qu’il s’agit de femmes. Il s’agit d’une sorte de prison où les femmes sont exhibées comme des animaux sexuels dans un enclos. C’est un endroit où les hommes aiment avoir des femmes à disposition, parquées, parmi lesquelles ils peuvent avoir du choix. La prostitution du rue suit aussi ce modèle.  
Dworkin dit que la prostitution n’est pas un choix : c’est l’Etat qui crée les conditions qui font que les femmes tombent dans la prostitution. En général, personne ne s’intéresse à la volonté des femmes, sauf dans ces débat, où l’on met en avant le prétendu choix des femmes à se prostituer. Il en va de même pour les débats sur la maternité de substitution.  
**Alors que, traditionnellement, la reproduction entre dans le cadre du modèle de la ferme, la maternité par substitution la placerait dans le modèle du bordel**.

Le prohibitionnisme de la prostitution n’est pas compatible avec la vision féministe. Les prohibitionnistes considèrent que les prostituées sont responsables de leur situation ; les féministes pensent que c’est l’Etat qui crée la conjecture sociale et économique dans laquelle la vente de sa sexualité et de sa capacité reproductive devienne l’un des rares moyens de subsidence pour les femmes.

![mere-porteuse](https://antisexisme.files.wordpress.com/2012/12/mere-porteuse.jpg?w=300&h=197)

Dworkin fait ensuite une comparaison entre le modèle de la ferme et le modèle du bordel.  
Selon elle, **le modèle de la ferme, qui sert au sexe et à la reproduction, est peu efficace.** Il implique une relation particulière entre le fermier et sa terre. Le fermier peut ressentir certains sentiments, une certaine tendresse et compassion pour sa terre, même s’il l’exploite. De plus, il y a une certaine valorisation de la maternité.  
A l’inverse, **le modèle du bordel, qui sert seulement au sexe, est très efficace.** Le joug est trop lourd. Les femmes n’arrivent pas à se rebeller collectivement.  
Le modèle de la ferme a connu quelques rebellions. Le fait qu’il y ait eu les luttes féministes, formées par des femmes issues de ce modèle, démontrent son inefficacité.

Dworkin pense que **les technologies reproductives vont permettre l’application du modèle du bordel à la reproduction. La reproduction va devenir une marchandise comme l’est déjà le sexe.**

> Ces nouveaux moyens permettront – enfin – aux hommes de vraiment posséder des femmes pour le sexe et des femmes pour la reproduction, toutes contrôlées avec la même précision sadique par des hommes.  
> Et se produira un nouveau genre d’Holocauste, aussi inimaginable aujourd’hui que ne l’était la version nazie avant son avènement ; une chose dont personne ne croit « l’humanité » capable. La technologie reproductive déjà ou bientôt disponible, liée à des programmes racistes de stérilisation imposée, offrira enfin aux hommes les moyens de créer et de contrôler le genre de femmes qu’ils veulent : le genre de femmes qu’ils ont toujours voulu. Pour paraphraser la Ninotchka d’Ernst Lubitsch justifiant les purges de Staline, il y aura moins de femmes, mais des femmes meilleures. Il y aura des domestiques, des prostituées sexuelles et des prostituées reproductives.

Les femmes ne veulent pas mourir et qu’elles ont trouvé deux solutions très différentes pour survivre dans ce monde d’hommes :

-   la solution des femmes de droite est de se plier aux impératifs sexuels et reproductifs des hommes, afin d’obtenir des miettes de dignité.
-   l’autre solution, celle des féministes, est de voir en chaque femme un être humain.

Avec le développement des technologies de reproduction, il sera de plus en plus dur pour les femmes de s’affirmer en tant que personnes de valeur. De plus en plus de femmes croiront être protégées par les valeurs religieuses qui vénèrent la maternité. Les femmes utiliseront les valeurs religieuses face aux scientifiques mâles. En réalité les hommes scientifiques et les hommes religieux s’allieront.

Chapitre 6 : l’antiféminisme
----------------------------

Le féminise est détesté car les femmes sont haïes. **L’antiféminisme est l’expression de la misogynie, c’est son argumentaire politique**.  
L’antiféminisme soutient la conviction que l’avilissement massif des femmes n’est pas une violation de leur nature. L’utilisation des femmes par les hommes est conforme à leurs natures respectives d’homme et de femme.  
Bloquer les efforts des femmes pour avancer vers la liberté est l’indépendance est une démonstration de haine envers les femmes.

L’antiféminisme se développe selon trois modèle

-   le modèle séparés-mais-égaux
-   le modèle de la supériorité féminine
-   le modèle de la domination masculine

Le modèle séparés-mais-égaux a été appliqué à la race dans le cadre de l’apartheid aux Etats-Unis. La séparation est bien réelle, mais l’égalité est une chimère. Ce modèle se fonde sur des prétendus critères biologiques. Selon cette idéologie, les femmes ne sont inférieures que parce qu’elles ont intégré une sphère qui n’est pas la leur : la sphère masculine, où elles n’ont pas leur place. Il y a promesse d’égalité dans le fait de présenter deux sphères séparées et d’affirmer qu’elles sont égales.

Le modèle de la supériorité féminine affirme que les femmes sont supérieures moralement et qu’elles n’ont pas désir sexuel. Derrière son aspect apparemment sympathique pour les femmes, ce modèle sert en réalité à les rabaisser car il considère que leur nature morale ne devrait pas être contaminés par les vulgaires activités et responsabilités des hommes. Parce qu’elles sont bonnes, les femmes ne peuvent pas être des sujets à part entière. Il existe une version pornographique de ce modèle qui prétend que les femmes détiennent le pouvoir car elle provoque du désir sexuel chez les hommes. C’est un pouvoir que Dworkin compare à celui du cadavre qui attire les vautours.

Enfin, le modèle de la domination masculine s’appuie sur la biologie ou la religion et prétend que les hommes sont supérieurs aux femmes, car c’est comme ça. Ce modèle fonctionne en faisant passer l’hostilité pour de l’amour. C’est un modèle qui soutient aussi que ce sont les hommes qui doivent gouverner et dominer l’économie, la politique ou la culture.

Ces trois modèles ne sont pas antagonistes qu’en apparence : ils se combinent à merveilles pour subordonner les femmes.  En effet, les argumentaires antiféministes puisent dans ces trois modèles simultanément, sans égard à aucune logique.

L’antiféminisme propose deux normes en ce qui concerne les droits et les responsabilités, deux normes déterminées par le sexe. **Le féminisme au contraire soutient qu’il existe un seul critère unique de liberté humaine et un seul critère de dignité humaine.** S’il refuse de se fonder sur un principe de dignité universel, le féminisme se transforme en son pire ennemi : l’antiféminisme

**La libération des femmes ne se fera qu’en démantelant le système de classe de sexe.** Pour cela il faut prendre acte de ce système et ne pas faire comme s’il n’existait pas. Les femmes doivent avoir conscience de leur situation pour pouvoir la changer. Les antiféministes considèrent femmes et hommes comme ayant déjà atteint l’égalité, et prétendent que l’on doit éviter toute analyse sexuée des phénomènes sociaux dans l’état actuel des choses. Ils prétendent qu’on victimise les femmes quand on analyse le système de classe de femmes, et qu’on les dégrade.

Deux éléments structurent ainsi le féminisme :

-   affronter le système de classes de sexe
-   l’exigence d’un critère unique de dignité humaine

Les femmes constituent une classe et partagent une condition commune. Cela signifie que le sort de chaque femme est lié au sort de l’ensemble des femmes. Cette condition commune est celle d’être subordonnée aux hommes.

Quatre crimes balisent cette condition :

-   le viol
-   la violence conjugale
-   l’exploitation économique
-   l’exploitation reproductive

**Le cercle de ces crime définit la condition des femmes.**

![mur de la prostitution](https://antisexisme.files.wordpress.com/2012/12/mur.jpg?w=150&h=112)

**Au cœur de la condition des femmes se trouve la pornographie, l’idéologie qui définit ce que sont les femmes.** C’est la justification des crimes. **La prostitution est le mur de cette condition, qui les enferme dans leur classe de sexe.** La pornographie et la prostitution servent à signifier que les femmes méritent les crimes qui définissent leur condition et qu’elles subissent.

Dworkin en déduit :

> Pour les féministes, le sens de cette description de la subordination des femmes, de la façon dont elles y sont maintenues et dont elle leur est appliquée systématiquement, est très simple : nous devons briser ce cercle, abattre ce mur, annihiler le cœur de ce système. Pour les antiféministes, le message est également simple : tout ce qui renforce ou nourrit n’importe quel aspect de ce modèle est d’une grande utilité pratique pour maintenir les femmes en état de subordination.

L’antiféminisme sévit également lorsqu’il propose de **sacrifier un groupe particulier de femmes** (généralement pauvres, noires,

![coeur](https://antisexisme.files.wordpress.com/2012/12/coeur.jpg?w=150&h=111)

etc.). C’est une promesse politique qui est faite – et tenue : certaines femmes s’acquitteront des pires tâches et les autres n’auront pas à le faire. C’est une stratégie qui séduit certaines femmes prêtes à sacrifier d’autres femmes aux bordels et aux fermes, pour se protéger. Dworkin rappelle qu’il n’existe qu’une seule protection pour n’importe quelle femme : la liberté de toutes les femmes.

Les femmes de droite croient que le monde actuel ne peut pas être changé. Leur point de vue est alors assez logique. Elles pensent que :

-   le mariage va les protéger du viol et de la violence conjugale
-   le statut de femme au foyer va les protéger de l’exploitation économique
-   le reproduction va leur apporter de la valeur – même si cela les rend plus vulnérables à l’exploitation reproductive

Mais leur raisonnement est faux, car il y a quelque chose qu’elles ignorent : le foyer est l’endroit le plus dangereux pour les femmes.  Une information qu’elles n’ont pas, à cause du silence des mères – et de la société – sur la violence des conjoints et des pères.

Par ailleurs, les femmes de droites voient les féministes comme des femmes, c’est-à-dire des êtres pornographiés et captifs. Elles ressentent de la répulsion et préfèrent miser sur des personnes de pouvoir. De plus les féministes peuvent leur être nuisibles car elles ruinent les marchés qu’elles cherchent à conclure avec le pouvoir masculin.

Une fois que l’on a compris l’ampleur du pouvoir masculin, il y a deux stratégies :

-   tenter de le détruire, comme le proposent les féministes
-   s’y plier, comme le proposent les femmes de droite

Y a t-il un moyen de détruire ce système ? Dworkin finit son livre sur ces réflexions :

> Faudra-t-il cent poings, mille poings, un million de poings lancés contre le cercle de crimes sexuels pour le détruire, ou les femmes de droite ont-elles essentiellement raison de le croire indestructible ? Le mur de la prostitution peut-il être escaladé ? Peut-on faire obstacle à ce qui constitue le cœur de l’oppression de sexe : l’utilisation des femmes comme pornographie, la pornographie comme étant ce que les femmes sont ? Si l’antiféminisme triomphe du mouvement de libération des femmes – maintenant, encore, toujours –, il faut admettre que quiconque possède le pouvoir politique ou représente l’ordre social ou impose son autorité tient les femmes pour de bon – quel que soit le nom que l’on ou qu’il donne à sa ligne politique ; la droite, au sens large, tient les femmes pour de bon. Le statisme et la cruauté auront triomphé de la liberté. La liberté des femmes face à l’oppression de sexe a de l’importance ou elle n’en a pas ; soit elle est essentielle, soit elle ne l’est pas. Décidez une fois de plus.

Publicités

### Évaluez ceci :

### Partagez :

-   [](https://antisexisme.net/2012/12/20/les-femmes-de-droite/#print "Cliquer pour imprimer")

    Imprimer

-   [](https://antisexisme.net/2012/12/20/les-femmes-de-droite/?share=email "Cliquez pour envoyer par email à un ami")

    Email

-   [](https://antisexisme.net/2012/12/20/les-femmes-de-droite/?share=facebook "Cliquer pour partager sur Facebook")

    Facebook

-   [](https://antisexisme.net/2012/12/20/les-femmes-de-droite/?share=twitter "Partager sur Twitter")

    Twitter

-   [](https://antisexisme.net/2012/12/20/les-femmes-de-droite/?share=reddit "Partager sur Reddit")

    Reddit

-   [](https://antisexisme.net/2012/12/20/les-femmes-de-droite/?share=skype "Cliquez pour partager sur Skype")

    Skype

-   [](https://antisexisme.net/2012/12/20/les-femmes-de-droite/?share=linkedin "Cliquez pour partager sur LinkedIn")

    LinkedIn

-   [](https://antisexisme.net/2012/12/20/les-femmes-de-droite/?share=tumblr "Cliquer pour partager sur Tumblr")

    Tumblr

-   [](https://antisexisme.net/2012/12/20/les-femmes-de-droite/?share=pinterest "Cliquez pour partager sur Pinterest")

    Pinterest

-   [](https://antisexisme.net/2012/12/20/les-femmes-de-droite/?share=pocket "Cliquez pour partager sur Pocket")

    Pocket

-   [](https://antisexisme.net/2012/12/20/les-femmes-de-droite/?share=telegram "Cliquez pour partager sur Telegram")

    Telegram

-   [](https://antisexisme.net/2012/12/20/les-femmes-de-droite/?share=jetpack-whatsapp "Cliquez pour partager sur WhatsApp")

    WhatsApp

-   

### WordPress:

J'aime

chargement…

### Sur le même thème
