---
title: "Pourquoi je suis féministe et j’aime le rap"
url: http://www.madamerap.com/2016/07/14/pourquoi-jaime-le-rap-et-je-suis-feministe/
keywords: jai,violence,sexisme,cest,rap,hip,femmes,stupide,féministe,jaime,violences
---
« Ah bon, tu es féministe et tu aimes le rap ? Mais comment c’est possible ? » Si vous saviez le nombre de fois que j’ai entendu cette phrase !

Depuis toujours, le hip hop est décrié pour son sexisme. Sur fond de racisme latent, de mépris ou d’ignorance de cette culture, la société nous apprend que le rap est la pire des musiques pour les femmes. Pourtant, je suis féministe et j’aime le hip hop. Dilemme.

![banniere booba](https://madamerap.files.wordpress.com/2016/07/banniere-booba.jpg)

Ironiquement, c’est par le rap que j’ai découvert le féminisme. Entre autres. A la fin des années 1990, au lycée, je faisais de la danse hip hop à un niveau intensif et j’écoutais beaucoup de rappeuses américaines : **Queen Latifah**, **Missy Elliott**, **Salt N Pepa**, **MC Lyte**, **EVE**, **Lauryn Hill**, **Da Brat**, **Lady of Rage**, **Bahamadia**, **Rah Digga**, **Lil’ Kim**, **Foxy Brown**…. J’étais fascinée par leur liberté, leur impertinence et leur manière frontale de parler de certaines thématiques, comme la sexualité et l’indépendance financière des femmes, l’avortement, les violences physiques et sexuelles et bien sûr le clitoris, dont j’ai découvert l’existence grâce à Lil’ Kim et son titre [Not Tonight](https://www.youtube.com/watch?v=8k5hSHTt52Q) ! Des sujets dont je n’avais jamais entendu parler ailleurs.

J’ai ensuite creusé le sujet à la fac. Dans le cadre de mes études d’anglais, je me suis spécialisée dans le féminisme africain-américain et les mouvements des droits civiques aux États-Unis. J’ai découvert le livre [When Chickenheads Come Home to Roost](https://www.amazon.com/When-Chickenheads-Come-Home-Roost/dp/068486861X)  de l’écrivaine et journaliste américaine **Joan Morgan **dans lequel elle parle de « hip hop feminism ». Je me suis parfaitement reconnue dans ce terme. Pour moi, le lien entre les deux était évident.

Mais très vite, on m’a fait comprendre que hip hop et féminisme étaient incompatibles et qu’il me fallait choisir mon camp. Si je voulais être crédible en tant que défenseuse des droits des femmes, je devais tirer à boulet rouge sur le rap. Et si j’en écoutais quand même en cachette, comme mon sale petit secret de complice occulte du patriarcat, j’étais priée de brûler tous mes vinyles, de revendre mes CDS sur Amazon et de trasher ma bibliothèque iTunes, pour les remplacer par des musiques convenables. Les **Spice Girls**, **Beyonce** ou **Patti Smith** feraient l’affaire.

![ana-duvernay](https://madamerap.files.wordpress.com/2016/07/ana-duvernay1.png)

Alors que les amatrices/teurs de rap ne m’ont jamais reproché d’être féministe, \#lesgens me serinent souvent avec l’idée que toute bonne militante qui se respecte ne peut se retrouver dans ces paradoxes. Je me suis alors demandé d’où venait ma passion schizophrène, que la réalisatrice américaine **Ava DuVernay** résume en un tweet : « Etre une femme et aimer le hip hop revient à être amoureuse de son agresseur. Parce que c’est bien ce qu’était et ce qu’est cette musique. Et pourtant, cette culture nous appartient. »

Serait-ce une envie inconsciente et irrépressible de posséder un pénis ? Du déni ? Du sexisme intériorisé ? Et surtout, pourquoi les filles qui ont grandi en écoutant John Lennon, en regardant des films de Roman Polanski et en lisant Charles Bukowski seraient-elles de « meilleures » féministes que moi ?

![rick-ross](https://madamerap.files.wordpress.com/2016/07/rick-ross.jpg?w=1024)

Alors oui, le hip hop est un milieu largement masculin, sexiste et LGBTphobe. Il ne s’agit pas de prétendre que tout va bien. [Entre 22 % et 37 % des paroles de rap sont misogynes](http://www.albany.edu/scj/jcjpc/vol8is2/armstrong.html) et [67 % objectivent sexuellement les femmes](http://www.academia.edu/2769580/Misogyny_in_Rap_Music_A_Content_Analysis_of_Prevalence_and_Meanings).

D’innombrables textes de rap, reliques du gangsta rap, banalisent la culture du viol ou glamourisent les violences de genre. Dans les années 1980-1990, le groupe **NWA** a largement participé à la glorification de cette imagerie caricaturale et de la vie de « thug » avec grosses caisses tunées, filles à poil asservies, culte de l’argent, ego trip et hyper-virilité exacerbée. « Si une salope essaie de me manquer de respect quand je suis bourré, je la défonce et tue le n\*\*\*\* qui est avec elle » rappait **Dr Dre**. Mais en même temps, le trio de Compton dénonçait les violences policières, le racisme systémique, la misère sociale, les foyers brisés et la dureté du quotidien dans les quartiers défavorisés américains.

[En faisant l’apologie du viol](http://www.youtube.com/watch?v=25r_A76xqno), **Rick Ross** s’est aussi attiré les foudres des féministes. « J’ai mis de l’ecstasy dans son champagne / Elle ne le sait même pas / Je l’ai ramenée à la maison et j’en ai profité / Elle ne le sait même pas. » 

En France, **Booba** se fait régulièrement épingler pour [ses rimes](http://www.youtube.com/watch?v=6i1wDLivanc) : « j’ai de quoi te siliconer si jamais tu vieillis mal / Gangster et gentleman, c’est dans le mille que je tire / Je fais mal mais je fais jouir si tu vois ce que j’veux dire. »

Idem il y a quelques années pour [**Busta Flex**](https://www.youtube.com/watch?v=MBWkwWsUMrU) : « Une fois montée en selle / Tu détrônes Julia Chanel / Ne t’attends pas à un roulage de pelle / T’as beau mettre du rimmel / C’est criminel mais c’est ton cul qui m’interpelle. »

[**La Fouine**](http://www.youtube.com/watch?v=Vaogx0nJ-ik) : « Pétasse suis-moi dans mon hôtel / Pour une volontaire agression sexuelle / Faites monter les mineurs j’suis pire que R. Kelly / 1 pour le sexe 2 pour la money. »

[**Black M**](http://www.youtube.com/watch?v=IMDTr8d8ZVE) : « Ta gueule ! Parce que t’es stupide, matérialiste, cupide, stupide, stupide, stupide, stupide / Et tu te crois super intelligente et mature / Hélas, la seule raison pour laquelle on t’écoute sont tes obus / Sinon t’as pas un 06 ? J’crois que j’ai l’coup de foudre / Euh non ! Bon OK va te faire foutre. »

Ou encore **Orelsan** : « J’respecte les schnecks avec un QI en déficit / Celles qui encaissent jusqu’à finir handicapée physique » et « Ferme ta gueule, ou tu vas t’faire Marie–Trintigner », finalement [relaxé](http://www.lemonde.fr/police-justice/article/2016/02/18/le-rappeur-orelsan-relaxe-pour-ses-textes-violents-envers-les-femmes_4867808_1653578.html) en février 2016 de poursuites pour provocation à la discrimination, à la haine ou à la violence envers les femmes.

Dans les années 1990, c’était **Doc Gynéco** avec [Ma salope à moi](http://www.youtube.com/watch?v=upMv1ZbYaQk) ou **NTM**, dont le simple nom « Nique Ta Mère » suscitait l’ire de certain.e.s.

Pourtant, ces violences verbales ne tombent pas du ciel et proviennent directement de la manière dont les femmes sont traitées dans la société. L’écrivaine et militante américaine **bell hooks** [l’explique parfaitement](https://blackexcellencex.wordpress.com/2012/11/16/hip-hop-is-killing-us/) : « Sans aucun doute les hommes noirs, jeunes et vieux, doivent être tenus responsables de leur sexisme. Cependant, cette critique doit toujours être contextualisée ou nous risquons de donner l’impression que ces comportements – le viol, la violence des hommes envers les femmes, etc. – est un truc d’homme noir. Et c’est ce qui se passe. Les jeunes hommes noirs sont forcés d’en ‘prendre pour leur grade’ pour encourager, via leur musique, la haine et la violence envers les femmes, qui est un élément central du patriarcat. »

En outre, tout le hip hop n’est pas misogyne. Certains artistes, comme **Young Thug**, **Kendrick Lamar**, **Shad**, **Talib Kweli**, **Lupe Fiasco** ou **Common** aux États-Unis, **Médine, D’ de Kabal, Oxmo Puccico, Gaël Faye, Hyacinthe, Ismaël Métis** ou **Georgio** en France, proposent d’autres discours ou d’autres performances de genre, moins conformes aux codes d’une masculinité hégémonique.

Aux États-Unis, certaines MCs portent un message ouvertement féministe et émancipatoire : **Queen Latifah** avec son hymne planétaire U.N.I.T.Y,  **Missy Elliott**, **EVE**, **Angel Haze**, **Lauryn Hill,** **Amber Rose**, **Cardi B, Nicki Minaj, Princess Nokia, MC Lyte**, pour n’en citer que quelques-unes.

Cette dernière a même qualifié **Fetty Wap** de féministe pour son titre [Trap Queen](http://www.youtube.com/watch?v=i_kF4zLNKio) : « C’est très courageux par rapport à ce qu’il représente dans sa musique parce que [ce n’est vraiment pas la norme](http://www.billboard.com/articles/columns/the-juice/6776930/mc-lyte-fetty-wap-hip-hop-feminist-j-cole). »

Mais le message de certains rappeurs n’est pas toujours si facile à décrypter. Ainsi, **2Pac **rend hommage aux femmes dans Wonda Why They Call U Bitch, Keep Ya Head Up et Never Call U Bitch Again mais a été reconnu coupable du viol d’une femme en 1993. Compliqué de continuer à l’écouter en ayant cette information.

**Drake**, quant à lui, se prévaut de promouvoir une image valorisante des femmes, mais [lâche](https://www.youtube.com/watch?v=bx7Waxi6DbY) quand même un « je déteste appeler les femmes ‘salopes’ mais les salopes adorent ça. »

Par ailleurs, le rap n’est pas plus misogyne que d’autres courants musicaux, il use juste de codes différents, sans détours et sans fioritures, ce qui rend le problème plus visible. Les autres styles musicaux produisent un sexisme plus mainstream et pernicieux, presque indécelable et surtout beaucoup mieux accepté.

Parce que quand on fouille dans la culture populaire, il n’y a pas vraiment de quoi pavoiser. Entre les Murder Ballads de **[Nick Cave](https://www.youtube.com/watch?v=rXA-9mWGCXk)** et **[Johnny Cash](https://www.youtube.com/watch?v=Y1iKEPzF1Js)**, qui nous racontent des errances meurtrières avec zigouillage de femmes, **[Pink Floyd](https://www.youtube.com/watch?v=K7-aDHsWuhA)** qui veut « passer (une femme) à tabac un samedi soir » , **[Tom Jones](https://www.youtube.com/watch?v=VBD-thfgX8o)** qui « a senti le couteau dans (sa) main et elle a arrêté de rire »  et les **[Misfits](https://www.youtube.com/watch?v=YrugYB7dPOA)** qui menacent « si tu ne fermes pas ta gueule, tu vas mordre la poussière », on est servi.

De son côté, **[John Lennon](https://www.youtube.com/watch?v=wRczG47AkJc)**, spécialiste en violences conjugales, prévient : « je préfèrerais te voir morte, petite fille, plutôt qu’avec un autre homme » et les **Rolling Stones** annoncent la couleur « Sous mon joug, elle est le plus adorable animal de compagnie au monde / Ça dépend de moi, la manière dans elle parle quand on lui adresse la parole. »

Pas mieux quand on sonde les classiques de la chanson française, truffés de misogynie. Parmi eux,  feu **[Michel Delpech](https://www.youtube.com/watch?v=2n6pgyeWyk0) **: « Que c’est bon de choisir une minette / Dans ces filles à vedette qui ne sont venues que pour ça / C’est bon de serrer dans ses bras une groupie, une groupie (…) / C’est un joli parasite qui s’accroche et que l’on quitte / Quand on en connaît un meilleur, ça ne reste pas dans le cœur. »

Feu **[Georges Brassens](https://www.youtube.com/watch?v=plhQ00rlnuI)** : « Le comble enfin, misérable salope / Comme il n’restait plus rien dans le garde-manger / T’as couru sans vergogne, et pour une escalope, / Te jeter dans le lit du boucher. »

[**Julien Clerc**](https://www.youtube.com/watch?v=nT2rfs8G76I) avec une bonne couche de colonialisme en sus : « Sous la soie de sa jupe fendue en zoom en gros plan / Tout un tas d’individus filment noirs et blancs / Mélissa, métisse d’Ibiza, a des seins tous pointus. »

Ou encore le champion toutes catégories **Michel Sardou** avec son [sexisme à papa](https://www.youtube.com/watch?v=oPMccS3I5MI) : « Si tu sais te servir de ta beauté, ma belle / Et pour lui faire plaisir t’encombrer de dentelles (…) / Si tu n’écoutes pas la voix des mal-aimées qui voudraient à tout prix te citer comme témoin au procès du tyran qui caresse ta main… » et son magnifique « J’ai envie de violer des femmes, de les forcer à m’admirer / Envie de boire toutes leurs larmes et de disparaître en fumée. »

Alors pourquoi s’offusque-t-on plus des paroles de rap que de ces horreurs ? Sans doute, parce que le rap est toujours méprisé et regardé par un prisme raciste et classiste. Tant qu’il s’agit d’hommes blancs, « présentables », à la masculinité acceptable, on les érige en références populaires. On applaudit tous ces messieurs de la variété qui parlent de leur désir pour des femmes, souvent objectivées et dont on ignore le degré de consentement, car le tout est enrobé dans du prétendu romantisme et de la chanson d’amour.

Résultat, on nous colle des stéréotypes désastreux dans le crâne et on nous apprend que ces chanteurs normés sont des gentlemen, tandis que les rappeurs sont des brutes bornées, des racailles misogynes, des sauvages capitalistes ou des délinquants illettrés. Quand **Marc Lavoine** [déclare](http://vincent-presse.com/dossiers/LE_POISSON_BELGE_SITE/revuedepresse/LE%20POISSON%20BELGE%20Parisien%20Magazine.pdf) « Une femme qui boxe ça reste une femme avec des petits seins, un petit sac à mains et du bordel dedans« , personne n’est là pour le relever.

![marc lavoine](https://madamerap.files.wordpress.com/2016/07/marc-lavoine.png?w=1024)

Désolée, mais en tant que féministe, je me reconnais davantage dans l’énergie, le discours et le langage de **Angel Haze**, **Keny Arkana**, **Ice Cube**, **Booba** et **PNL** que dans **Benjamin Biolay**, **Zazie** ou **BB Brunes. ** En tant que militante, je suis plus sensible à cette musique ancrée dans le réel et empreinte d’empowerment. Etre féministe, c’est aussi choisir. Choisir ses combats, sa musique, sa consommation et ses contradictions.

En tant que féministe, j’ai aussi pu trouver dans le rap des rôles modèles diversifiés et des images de femmes plurielles. Ou d’autre peut-on voir des femmes de toutes origines, âges, classes sociales, religions, orientations sexuelles, identités de genre et morphologies parler de leur plaisir sexuel, de body positivisme, de violence conjugale, d’inégalités de genre, de politique, de violence policière, de racisme, de sexisme, de LBTphobies ou de PMA ?

Dans le rock ? La pop ? La variété ? En France, nulle part ailleurs. Le rap est le seul espace artistique qui donne aux femmes cette liberté de parole. Et c’est notamment pour cette raison que je continuerai à être féministe et à aimer le rap.

[Éloïse Bouton](http://eloisebouton.org/)

Une version courte de cet article a été publiée dans Le Huffington Post [ici](http://www.huffingtonpost.fr/eloise-bouton/pourquoi-je-suis-feministe-et-jaime-le-rap_b_10925046.html).
