---
title: "Robot Hugs"
url: http://www.robot-hugs.com/definition/
keywords: trying,white,capture,dictionary,language,view,robot,dictionaries,think,hugs,academic,groups
---
[↓ Transcript]()

  
  
Well, the dictionary definition of racism is -   
  
Look, I'm going to have you pause here for a second, because I think you're trying to make this dictionary definition about a socially complex word into an argument and it's not going to work the way you think it will.   
  
What's it to you?  
  
This is a large part of my academic research and my current career. I know a lot about this stuff, and it's very important to me!   
  
(linguist and information specialist)  
  
Dictionaries are these man-made documents that are supposed to somehow be an authority on how we speak and write, but that is practically impossible! No document could possibly capture the variations and nuances of a language.   
  
ENGLISH: Volume 1  
  
People seem to view dictionaries as being objective, like they don't have a stake in what they're describing, but that isn't the case.  Dictionaries express a point of view, an opinion, nothing more. People insist on pointing to dictionary definitions in arguments, but they don’t consider whose opinion they're pointing to!  
  
Dictionaries as books describing language are historically prescriptive - that means they were meant to tell people how they should speak.  The movement to begin writing dictionaries from a descriptivist point of view - that is, to capture how people actually speak (language behaviour), was controversial and highly contentious.   
  
"...We have seen the propped wide open in enthusiastic hospitality to miscellaneous corruptions and confusions. In fine, the anxiously awaited work that was to have crowned cisatlantic linguistic scholarship with a particular glory turns out to be a scandal and a disaster"  
January 1962 Atlantic Monthly somewhat hyperbolic review of Webster's Third Dictionary, a pointedly descriptivist body of work.   
  
More like dick-tionary, amirate?  
  
How crass - an offensive and uncultured colophony of degenerate speech.  
  
Gasp! A neologism and an elision, in situ! I must record and preserve this precious language-butterfly.  
  
The prescriptivist vs. descriptivist divide continues to this day, and most modern dictionaries for mass audiences try to straddle the two camps.   
  
The writers and editors of popular and academic dictionaries have been overwhelmingly white, academic, straight men, and that has influenced the kind of language they have allowed in their dictionaries, and how that language is described.  
  
Hey! We did our best!  
It was (mostly) solid work!  
  
Herbert Coleridge  
Noah Webster  
Frederick James Furnivall  
  
So the language in dictionaries tends to reflect the opinions of white, straight, academic men on what is acceptable English language, including the exclusion and denigration of different dialects and usages.   
  
For example, the struggle for the recognition of the grammatical legitimacy of African American Vernacular English has been hampered by a historical resistance by English dictionaries to include AAVE as acceptable usage.   
  
Ahem! I think you meant to say ‘She is here’, not ’She be here’.  
  
No, I meant what I said.  
  
This bias within dictionaries continues to this day - while online dictionaries are more fluid and can more easily include changes and variations in language, dictionaries in general are still fairly static, and the authorities in charge of administering and editing dictionaries are still almost always white academics.  
  
Even though many modern dictionaries are trying to capture different ways of speaking, new terms, and shifting definitions, they are still dragging behind them the weight of 'proper usage' and academic prescriptivism.    
  
I’m better than this!  
  
I'm not saying that dictionaries are useless - they're really useful when I want to look up how to spell something, or the definition of an obscure word, or win points in scrabble.   
  
Aha! See? “Norites” is totally a word!   
FINE.   
  
Smaller speciality dictionaries and language descriptions that are written around certain groups, professions, or environments are especially useful as they are more likely to accurately capture actual use and, most importantly, are more likely to be written and edited by the actual language users in that environment.   
  
Trans\* and Gender Glossary  
A guide to appropriate Aboriginal terminology  
Paleontology Terms  
  
What I'm saying is that when you look through a dictionary, especially general language dictionaries, you have to remember that it is not an objective document. It is a document with a history and a bias that is rooted in privilege, exclusion, elitism, and gatekeeping. This is why dictionary definitions are a particularly poor resource when trying to win arguments on topics like racism and other kinds of social discrimination.   
  
The language describing systemic institutions of oppression, like racism, ableism, and transmisogyny is of more value when it is interpreted and defined by the groups it affects, not the groups in power.   
  
Dictionaries often fail to capture how certain languages and terms have been used or excluded in ways that perpetuate oppression and violence against groups of people.  
  
  
Language is spoken and signed and sung and yelled and whispered and typed; it's invented and created and transformed and disposed.  Language doesn't live anywhere, certainly not in one book.   
￼  
  
  
I mean, quoting a book that is grounded in a history of a bunch of white men deciding what is appropriate, correct, what’s true, and what isn't, that feels about as convincing as...  
  
'Well, the bible says...."  
  
oh please.   
  
