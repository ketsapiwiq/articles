---
title: "Why Even the Hyperloop Probably Wouldn’t Change Your Commute Time"
url: https://www.nytimes.com/2017/08/10/upshot/why-even-the-hyperloop-probably-wouldnt-change-your-commute-time.html
keywords: workers,halfhour,similar,commute,commutes,minutes,average,change,probably,american,work,hyperloop,york,wouldnt,whisk
---
The hyperloop, Elon Musk has boasted, could whisk you from New York to Washington [in 29 minutes](https://www.nytimes.com/2017/07/20/business/elon-musk-hyperloop.html?module=inline). Other maglev boosters sell similar dreams: [San Francisco to Los Angeles](https://www.usatoday.com/story/tech/news/2016/05/09/la-sf-30-min-hyperloop-wars/84137224/) in under 30. [Dallas to Houston](http://www.khou.com/travel/houston-to-dallas-in-30-minutes/430201408), [Portland to Seattle](https://www.pdxmonthly.com/articles/2017/5/15/portland-to-seattle-in-17-minutes-the-hyperloop-could-make-it-happen), [Orlando to Miami](http://www.miamiherald.com/news/business/article143362219.html) in the same.

The half-hour trip is something of a mystical notion in transportation. These visions of the future sound seductive in part because half an hour is, in fact, roughly how long many of us spend getting to work. The typical American commutes 26.4 minutes, one way, according to the American Community Survey. Even in metro New York, with nearly the longest commutes in the country, that average is 36 minutes.

Of course plenty of workers trek less or [much more](https://www.nytimes.com/2017/07/21/realestate/extreme-commuting.html?module=inline), but average American commute times have budged only modestly over the last 35 years, since the census began asking about them. International studies have shown similar half-hour patterns. History even hints that the Romans traveled about the same, when most people went everywhere on foot.
