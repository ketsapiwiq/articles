---
title: "Orthographe : le point de vue d’une professionnelle"
url: http://lachaisepliante.canalblog.com/archives/2016/01/29/33287865.html
keywords: point,écrire,vue,quil,cest,faire,quon,professionnelle,dune,père,loi,orthographe,lorthographe,langue,français
---
[![12540611\_1715082148715353\_8642786088775446925\_n](https://p5.storage.canalblog.com/52/08/984222/108810031.jpg)](https://p5.storage.canalblog.com/52/08/984222/108810031_o.jpg)

**Avertissement :** il y a probablement des fautes dans ce texte. Je n’aime pas me relire parce que je passe déjà mes journées à relire les trucs des autres. Merci de votre clémence.  

 

 

Quelle idée. Après avoir parlé de révolution, je parle d’orthographe. Mais ne croyez pas que le thème est anodin. Notre usage de l’orthographe, notre attitude face à l’usage de l’orthographe, sont, je pense, révélateurs de nos statuts sociaux, de nos conceptions philosophiques et de nos idées politiques. L’orthographe revient régulièrement sur le tapis, notamment sur facebook où, à coup de perles du bac, de corrections doctorales et autres dîners de cons modernes, elle occasionne sourires narquois, honte populaire et revendications rebelles. Qui n’a pas vu un débat politique ponctué de « apprenez d’abord à écrire le français, appeler prend deux p et un l ». Avec souvent, en dessous, une correction du premier correcteur et cette surenchère du mépris : « c’est celui qui dit qui y est, c’est pas toi qui me méprises, c’est moi qui te méprise ! » … À défaut d’arguments, l’orthographe fait office d’outil de jugement ultime sur la valeur d’une personne, sur son intelligence, sur sa légitimité à débattre… voire même à siéger dans les rangs de la société française : ne pointe-t-on pas perpétuellement du doigt ces immigrés qui refuseraient de s’intégrer parce qu’ils ne parlent pas un français châtié ou ces soi-disant « jeunes dégénérés » d’aujourd’hui lobotomisés par leurs iPhone et Nabilla, avec leur « langage sms » ? « Et dire que ça va voter… », soupirait souvent en nous regardant ma prof de français de troisième.

[via GIPHY](http://giphy.com/gifs/movie-80s-ferris-buellers-day-off-bkIgPwdV3Afzq)

 

Parce que j’ai bac+5 en Lettres, parce que j’ai toujours adoré la linguistique, et surtout parce que je pratique aujourd’hui le métier ultime de correctrice, j’ai un statut privilégié par rapport à tout ça : on me prend souvent à témoin de la « bêtise » de ceux qui écrivent ou parlent mal le français. Ou, au contraire, on bredouille des excuses quand on apprend mon métier parce qu’on est nul en orthographe et qu’on a peur que je sois personnellement offensée par un oubli de « s » au pluriel.

Mais ce que les gens savent peu c’est que non seulement **je me foutais déjà complètement de l’orthographe** à l’époque où je m’amusais à faire les dictées de Pivot au collège et je pratique aujourd’hui ce métier sans passion aucune, bien au contraire. Mais aussi, comme un astrophysicien qui se sent de plus en plus petit à mesure qu’il comprend l’immensité et la complexité de son sujet d’étude, je me fous de plus en plus de l’orthographe à mesure que j’avance dans l’étude de la langue française.

Voici donc quelques réflexions philosophiques sur le langage, en tant que « professionnelle de l’orthographe » :

-   **Personne n’est vraiment bon en orthographe.**

    C’est ce que j’ai appris en apprenant le métier de correcteur. Ce qui m’a rendue apte à faire ce métier n’est pas que je sois infaillible en orthographe : personne ne l’est ! Ce qui m’a rendue apte à faire ce métier c’est ma passion pour la lecture. Le fait d’avoir lu pendant des années un livre par jour m’a donné un atout : la capacité à douter de l’orthographe d’un mot de manière photographique... « tiens, yavait pas qu’un seul p à laper … ? »  Car être correcteur, c’est entrer dans l’ère du soupçon. On vérifie tout. Absolument TOUT. Certains jours je suis dans un tel état de paranoïa que je vérifie s’il y a un t à « chat ». Et il y a des choses que je ne sais toujours pas, et que je vérifie tous les jours, depuis 5 ans. C’est plus fort que moi, je ne réussis pas à l’enregistrer. Et puis je n’ai jamais été excellente en orthographe. J’écris toujours pardi avec un s comme paradis, je n’arrive jamais à savoir quelle est la tâche qui a un accent circonflexe, je suis incapable de deviner où il y a un seul m et où il y en a deux, je suis en générale une vraie queue avec les lettres doublées, entre autres. Mais en devenant correctrice, j’ai appris que personne n’était excellent en orthographe. Pourquoi ? Parce que la première chose qu’on fait quand vous avez corrigé un texte, c’est de le faire voir par un second correcteur, qui y trouve encore des fautes. Puis un troisième, un quatrième… tous trouvent encore et toujours des fautes que vous n’aviez pas vues, ni celui avant ni celui après vous. Jusqu’au lecteur, qui vous écrit pour vous faire part d’une ou deux fautes d’orthographe dans sa revue préférée : « inadmissible, ça se perd le respect de la langue française »… Ce qu’on apprend vraiment quand on devient correcteur ? À arrêter de péter plus haut que son cul et de se croire infaillible. Je ne saurais que trop conseiller l’exercice à nos profs d’orthographe en herbe sur facebook.

 

-   **Le français est une suite d’erreurs et de décisions arbitraires.**

    Ceux qui ont étudié l’ancien français le savent. Exemple parlant s’il en est : un cheval, des chevaux. Mais pourquoiiiiiii ?

[via GIPHY](http://giphy.com/gifs/reactiongifs-ePeHKwWSed0Ag)

Et bien parce qu’en fait, à la base, on disait un cheval, des chevals. Sisi ! Sauf qu’en parlant vite, avec le s derrière (car à l’époque on prononçait toutes les lettres comme en anglais, ou plutôt on écrivait que ce qui se prononçait, puisqu’on commençait tout juste à écrire cette langue « vulgaire » autre que le latin) le l s’est moins dit, on a fait un l vocalisé un peu à la manière du Brasiouuu du carnaval de Rio. Essayez de dire très vite « chevalsse », vous verrez, vous finirez par dire « chevaosse », voire « chevausse ». Alors donc on s’est mis à dire « chevaous » ou « chevaus ». Mais me direz-vous, d’où sort ce maudit x ? Et bien c’est là le plus drôle tenez-vous bien : les scribes de l’époque, pas loin du langage sms, se trouvaient des ptis tricks pour écrire plus vite. Dans le genre, « x » était une abréviation pour « us ». Donc on s’est mis à écrire « chevax ». Et ça ça claque, si c’était que moi on dirait tous « chevax » parce que ça a un côté klingon hyper eighties que j’adore.

[![12604\_600](https://p0.storage.canalblog.com/03/44/984222/108809959.jpg)](https://p0.storage.canalblog.com/03/44/984222/108809959_o.jpg)

Bref, ya un ou deux scribes qui se sont gourés. Genre au lieu d’écrire chevax, ils ont écrit chevaux. Et on y est. Une simple faute de frappe en langage sms, voilà l’histoire de « un cheval, des chevaux ». Après, ya des momies qui se prenaient pour les maîtres du monde et qui s’appelaient les académiciens qui ont décidé tous seuls comme des grands de comment qu’on devait écrire le français correc’. Alors ils ont décidé plein de trucs comme qu’on dirait « soleil » comme le roi et pas « souleil » comme 90% de la population. Et puis que l’orthographe serait compliquée parce qu’on devait pas écrire comme ça se prononce mais comme les mots grecs ou latins d’où que ça vient. Et puis comme on était dans la première et belle logique nationaliste de Louis XIV et qu’il fallait créer une nation française, cette réforme des bases de la langue a gracieusement accompagné la répression systématique de tous les régionalismes culturels, linguistiques ou politiques.

 

-   **Une langue vivante n’appartient à personne.** **Une langue vivante est insaisissable.**

[via GIPHY](http://giphy.com/gifs/crazy-woman-horse-jSJDt0ujXzeFy)

 

Ce qui m’amène au plan politique : je disais que notre usage de l’orthographe, notre attitude face à l’usage de l’orthographe, sont, je pense, révélateurs de nos statuts sociaux, de nos conceptions philosophiques et de nos idées politiques. J’en veux pour preuves ces sempiternelles disputes sur le sujet entre mon père et moi. Comment à travers, notamment, l’idée insignifiante de réforme de l’orthographe peut se jouer en fait l’éternel affrontement d’un Républicain nationaliste qui a fait l’école militaire et d’une anarchiste allergique à l’autorité. Pourtant, aucun de nous deux n’est particulièrement attaché à la belle orthographe classique et tordue de notre langue française. Au contraire : mon père pense qu’il faut à tout prix appliquer la réforme qui enlève tout un tas de ces subtilités inutiles, tout un tas d’accents circonflexes et de lettres inaudibles qui rendent le français impossible et élitiste. Quant à moi, la grandeur de notre langue nationale à la Bernard Pivot, en bonne anarchiste que je suis, je me torche le cul avec. Alors pourquoi n’est-on jamais d’accord ? Parce que mon père pense que « la loi c’est la loi », et que vu que les mêmes académiciens qui nous avaient dit qu’il fallait écrire des « chevaux » ont soudainement décidé, ou plutôt gracieusement concédé au petit peuple qui s’obstine à faire des fautes, qu’on allait simplifier la chose, alors tout le monde doit se soumettre et « appliquer la réforme ».

Or, ce que j’essaye de faire comprendre à mon père sans jamais y parvenir, c’est qu’une langue vivante ne se soumet pas à une réforme. Qu’une langue vivante est comme son nom l’indique « vivante » et se contrefout subséquemment des momies, académiques ou non. Qu’il aura beau tempêter contre les anglicismes et autres mots-nouveaux-inutiles-parce qu’il-existe-l’équivalent-en-français, les formules absurdes car redondantes ou injustes grammaticalement comme « au jour d’aujourd’hui », les tics de langage journalistiques… il n’y changera rien. Les jeunes, dieu soit loué, inventeront toujours des formules neuves qui, si elles semblent étranges ou alambiquées pour les vieilles oreilles, suivent une loi millénaire, une des rares lois qui régissent une langue vivante : la SIMPLICITÉ.

 

Imaginez deux routes pour aller d’un point A à un point B. L’une permet d’être à B en une heure mais elle est interdite. L’autre, autorisée, fait 2 jours de marche. Vous ne pourrez jamais empêcher les nouvelles générations d’emprunter la voie numéro 1, que vous le vouliez ou non. Vous pourrez décourager quelques individus, quelques générations en les punissant. Mais la génération suivante retentera, encore et encore, jusqu’à ce que vous ayez légalisé la voie la plus simple. Il en est de même du langage. Le dictionnaire peut tenir bon sur ses acquis, il finit toujours par intégrer les anglicismes qu’il condamnait quelques années avant. Parce que sinon, il devient un dictionnaire d’une langue étrangère, d’une langue morte. C’est là le fondement de ma dispute avec mon père : mon père pense que le peuple doit s’adapter à la réforme, à la loi académique. Je pense que la loi académique doit s’adapter au peuple. Je pense que la langue est un bien populaire, un de nos biens les plus précieux, un vrai trésor qu’on ne peut mettre en coffre. Que la langue est sauvage, indomptable, et jamais laide, jamais pauvre. Et c’est un avis très politique. Mon père pense que le peuple doit se conformer à la loi, servir la loi. Je pense que la loi doit se conformer au peuple, servir le peuple. Et que la désobéissance est synonyme de créativité, de vie et de démocratie.

 

 

 

Tout à l’heure je regardais un article sur les tics journalistiques. Il commençait en ces termes : « Oui à la liberté d’expression… mais avec une réserve : pour s’exprimer, encore faut-il savoir le faire. La multiplication des médias (télévisions, radios, Internet, presse gratuite) permet à tout le monde d’avoir un avis sur tout. » Pourquoi permettre à tout le monde d’avoir un avis sur tout serait mal ? **Depuis quand** et surtout **de qui** « tout le monde » devrait-il attendre une permission pour avoir un avis « sur tout » ? Cette citation est assez parlante : permettre à tout le monde d’avoir un avis sur tout, c’est abandonner l’aristocratie des savants, la domination de ceux qui ont, entre autres, une bonne connaissance de la langue française… là où mon père était simplement un républicain orthodoxe, l’auteur de cet article est carrément un oligarque du savoir…

Alors voilà où la « professionnelle de l’orthographe » en est : continuons à parler et à écrire sans complexes, sans honte, en transgressant les règles, que ce soit volontaire ou non. Personne n’est plus apte qu’un autre à vous dire comment parler ou écrire votre langue. Votre faute d'aujourd’hui sera peut-être la règle de demain. Je ne dis pas qu’il faut absolument faire des fautes ou parler chacun une langue incompréhensible pour d’autres. Mais une faute d’orthographe récurrente est toujours significative d’une évolution nécessaire de la langue. Une expression nouvelle quelle qu’elle soit est toujours un enrichissement linguistique, même si c’est de l’anglais, de l’arabe ou du manouche. Réjouissons-nous de ce marasme d’orthographes et de langages qui constituent notre jolie langue métissée. Et n’ayons plus peur des tenants du savoir : la langue française est à nous autant qu’à eux !

Plus je lis des réflexions sur l’orthographe sur facebook, plus j’en entends au quotidien, et plus je me dis : dingue ce que le gens sont capables d’envoyer comme flux de mépris (c’en est presque odorant) quand il s’agit de respect de l’orthographe.  Le respect de l’orthographe. Me fait marrer ce terme. Et si on commençait à respecter les personnes plutôt que l’orthographe ?

 

 

** **
