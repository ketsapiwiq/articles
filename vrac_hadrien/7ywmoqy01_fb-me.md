---
title: "The Urban Poor You Haven’t Noticed: Millennials Who're Broke, Hungry, But On Trend"
url: http://fb.me/7YWmOqY01
keywords: urban,person,trend,broke,work,coffee,jobs,hungry,need,poor,money,buy,whore,millennials,havent,noticed,spent,pay,spend
---
![](https://img.buzzfeed.com/buzzfeed-static/static/2016-05/5/7/enhanced/webdr13/longform-original-2902-1462447627-3.jpg?downsize=700%3A%2A&output-quality=auto&output-format=auto&output-quality=auto&output-format=auto&downsize=360:*)

Rebecca Hendin / BuzzFeed

  

There’s an underground dance bar in Santacruz West where I saw a former national-level beauty pageant contestant perform. According to the person who took me there, she began working there when she was looking for a Bollywood break. To land roles, she needed to be seen on red carpets and at parties, for which she needed heels and dresses. While acting gig after acting gig fell through, the dance bar turned out to be so lucrative, it became her primary vocation.

I know a young marketing executive who bought a car with her first salary and now sleeps in it. Between rent and loan repayments, she was starting to starve. I won't tell you where she parks, but thank god Mumbai is still safe.

Then there’s my junior journalist friend. For a period, she was coming into work less often. And she was growing thinner. She insisted it was because she was jogging every evening. When she started to disappear at lunch time, or nurse a cold coffee all day, I knew. (I didn’t miss the signs, because I've done it too.)

I WhatsApped her. It was the only way to be discreet.

“Do you have enough money for a meal?”

She didn’t.

She explained that when she did, she’d wait to go to Le Pain Quotidien and pay ₹200 for a sandwich. After 6pm, the day’s stock is discounted.

The office canteen offered meals all day that she could afford, but eating was a lower priority than keeping up the appearance that she could, when she chose to, do it at Le Pain Quotidien.

These are the urban poor. Objectively and relative to a vast majority of Indians, they aren’t “poor” at all. But they’re certainly hungry and broke a lot. These are the metro-dwelling twentysomethings who've internalised the pressures surrounding them, and spend a majority of their salaries on keeping up the lifestyles and appearances that they believe are essential to earning those salaries.

Objectively and relative to a vast majority of Indians, they aren’t “poor” at all. But they’re certainly hungry and broke a lot.  

  

The expenses that rack up are notionally non-negotiable: the clothes and the grooming, the bar nights and office dinners, the Olas and Ubers you have to take because you’re networking until 1am, the Starbucks coffee you have to buy because that’s where your job interview is. The heels and the dresses.

As the bank balance crashes past zero by the 22nd of the month, they concede that the math may not work today, but they hold on to hope that it will work out in the end; when that increment comes, when the promotion arrives, when Dad sends a little extra one month.

Their influences are not difficult to spot. Their startup economy’s success stories are of entrepreneurs who spent VC money to create their own wealth, who spent every paisa immediately to multiply each into a rupee. The stories they hear are of Mukesh Ambani, who inherited an empire and built a very expensive home, instead of Dhirubhai, who lived in a very small home and built a very big empire. They read about Katrina Kaif’s hair costing ₹50 lakh to dye correctly. They internalise the lesson that to earn any money, you’ve got to spend a lot of it.

For admission to good colleges, we spend uninhibitedly on tuitions. For job placements, we throw savings at GMATs and MBAs. For promotions, we spend on suits and drinks.

We dress for the jobs we want, forgetting that most salaries are tailored to afford dressing for the jobs we have. 

We dress for the jobs we want, forgetting that most salaries are tailored to afford dressing for the jobs we have.

Every newspaper and media house has it in neon lights: how you need to eat, look, and dress to be successful. Where you need to vacay, what you need to smell like, what car you should probably drive. But they don't tell you how to pay for any of it.

What we’re left with is a flood of twentysomethings running hard to leave behind roti-sabzi for a perception of burger-coke. From there, they sprint with equal abandon toward the cheese-champagne.

![](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

  

When I first moved out on my own 15 years ago, my salary was ₹10,000. My rent was ₹4,000, my creche fee was ₹4,000, and I spent the remaining ₹2,000 on my commute and electricity. I used my credit card for groceries. And, because I was 25 and my son was 1 and sometimes you need ice cream, or a movie, or to be able to laugh at life, I used my credit card to do those things too. By the time I moved to a higher-paying job, I had a maxed-out credit card to pay off. I had spent all the money I was about to earn.

I quickly learned that with each salary hike, the price of earning it goes up. While in my first job I’d gotten away with rotating three tops with one pair of jeans, more advanced roles brought the need for better clothes. I was asked to “grow up”. Then a lunch here, a happy hour there, a meeting at a high-end coffee shop.

I worked hard to defy the circumstances conspiring to push young professionals into bankruptcy. I did the mental math of each outing before committing to it. I got only one beer and drank it slowly all night.

Now, at any table, I can easily spot the person verging on broke: the vegetarian who didn't eat any starters, the teetotaller who drank only water, the junior who pretended she already ate dinner, no thanks. And when, after all that, someone else casually suggests divvying up the bill equal parts, you recognise theirs as the faces that fall.

You don't say no because not only might you cry, you’d also look cheap.

I’ve been there. You don't say no because not only might you cry, you’d also look cheap. So, regardless of whether you can really afford the drinks and appetisers you intentionally didn’t have, you sometimes suck it up and pay for them.

Later, you count coins. You pull ₹1 out of the sofa corner. You wait until everyone’s out of sight and then you board a bus home.

Now, I make it a point to stop my younger colleagues and ask: Have you eaten? Can I buy you a coffee? Are you walking home? Need a lift? Sometimes, they stay strong and pass on the offer. Other times, their facade crumbles and they nod.

Their parents, subscribers to a new-age refusal to openly discuss finances, taught them that no expense is too much for their happiness and mobility. Now, in phone calls, when Dad asks if he should send more money, they say it’s fine, everything's under control. Yes, eating well. Yes, all good at work. Raised by parents who sacrificed everything for their comforts, a whole generation is nonetheless learning discomfort quietly.

People who survive this stuff get called “strong” all the time. Strong is just a quiet hunger and a stifled sob. Most days, I think I’ve put that time behind me.

Recently, I was at an interview when the person I was speaking to stopped me in the middle of my question.

“Babe, my driver has a better phone than you,” she laughed. “Buy an iPhone, for Chrissakes!”

I’m better dressed now. I own my home. I have an actual bank balance. But the humiliation rushed back like the last 10 years never happened.

![](data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)

  

Last month, I began tweeting about this particular brand of urban poverty, and watched an outpouring of “me too”s.

One person confessed that for three years in Germany, he ate only tomatoes, saving money so he could buy his family chocolates when he went home. Someone else said “everything’s fine!” on long-distance phone calls to justify his mother having sold her bangles for his move abroad.

Someone sleeps on a single mattress and stashes sneakers under his desk so he can walk home 8km from work every night.

I got stories about marketing guys who starve all day to buy one coffee at a five-star hotel.

About a father who hasn't taken vacation days in 13 years to be able to pay for an international education for his child.

We’d rather spend a lot to appear full than spend a little bit to buy food.

Someone survived on water all day and hitched rides on trucks to get through university.

Someone got called a miser for not eating out.

In a country where genuine hunger is ubiquitous, this brand of it comes via lifestyle choices. Somehow, we’ve built a culture that places such immense value in appearances that we’d rather spend a lot to *appear* full than spend a little bit to buy food.

The hunger has touched different people differently – briefly or permanently, lightly or severely, maybe once or maybe over and over again. But once you’ve felt it, it’s indelible, marking you forever as a member of a tribe that understands what’s going on when someone starts bringing their own lunch to work one day, starts losing weight, starts spending nights at the office to avoid paying for the commute. If you’ve felt that hunger, even briefly, even a long time ago, you see it everywhere you look.
