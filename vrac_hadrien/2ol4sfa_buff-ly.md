---
title: "Everyone Is Miserable: Here’s What We Can Do About It"
url: https://buff.ly/2ol4sfA
keywords: values,depressed,heres,world,writes,work,hari,miserable,need,social,feel,depression
---
Johann Hari took his first antidepressants at age 18, and the experience, he says, was like a “chemical kiss.” The burden was lifted immediately from his whirring brain. He kept on taking the pills for 13 years, at higher and higher doses–until, at one point, the drugs didn’t work anymore. He was still depressed.

advertisement

advertisement

In his early 30s, Hari, a journalist, started to question the prevailing wisdom about depression. Was his desperation and anxiety really connected, as he had been told by a succession of doctors, to a chemical imbalance in the brain? Was it genetic, as other scientists claimed? Or were the reasons why so many people are depressed these days really more social? Is the [depression epidemic](https://www.theguardian.com/commentisfree/2017/jul/03/drugs-alone-wont-cure-epidemic-depression-we-need-strategy) connected to how we’ve chosen to construct the world around us?

“For the first 18 years of my life, I had thought of it as ‘all in my head’–meaning it was not real, imaginary, fake, an indulgence, an embarrassment, a weakness. Then, for the next 13 years, I believed it was ‘all in my head’ in a very different way–it was due to a malfunctioning brain,” Hari writes in his new book, [Lost Connections: Uncovering the Real Causes of Depression–and the Unexpected Solutions](https://www.amazon.com/Lost-Connections-Uncovering-Depression-Unexpected/dp/163286830X). 

“The primary cause of all this rising depression and anxiety is not in our heads. It is, I discovered, largely in the world, and the way we are living it. I learned there are at least nine proven causes of depression and anxiety . . . and many of them are rising all around us–causing us to feel radically worse.”

Of course, people who are merely unhappy are not the same as people like Hari who are diagnosed as severely depressed and anxious. We tend to view the latter group as having a disease, and the first as, well, having a bad day. But Hari argues that these traditional distinctions aren’t as useful as we’ve been taught to think. Unhappiness and depression are on a continuum, he argues, rather than being separate planets. They are caused, to an extent, by the same thing: disconnection from the things we need to be happy.

“The forces that are making some of us depressed and severely anxious are, at the same time, making even more people unhappy,” he writes.

Lost Connections is a fascinating look at what causes people to be depressed and asks what we can do aside from simply throwing more pills at the problem. Already one in five American adults are taking a drug for a psychiatric problem, including almost a quarter of middle-age women. And these days the U.S. is far from the only Prozac Nation; France, for example, has as many psychotropic drug takers.

advertisement

Arguably, however, the unhappiness plague is larger than the actual medicated population. When you consider a wider group of unhappy people–those who don’t rate as depressed, but are nonetheless sad and miserable–we’re probably talking about many, many millions around the world.

What We Need To Be Happy
------------------------

Hari interviews dozens of social scientists around the world who’ve studied various aspects of depression and unhappiness. His concludes that what causes these conditions most of all is a lack of what we need to be happy, including the need to belong in a group, the need to be valued by other people, the need to feel like we’re good at something, and the need to feel like our future is secure.

Hari talks, for example, to [Michael Marmot](https://en.wikipedia.org/wiki/Michael_Marmot), who carried out a famous study of British civil servants in the 1980s. We assume that people with more responsibility in their jobs are more stressed out and liable to be depressed. After all, the clerk at the bottom of the pay scale gets to go home on time and be with their kids. In fact, Marmot found something like the opposite when he talked to thousands of civil servants in the U.K. Those lower down the highly hierarchical bureaucracy were more anxious and unhappy.

Marmot concluded that monotonous, boring, and soul-destroying work is the most stressful kind. It’s not a matter of responsibility level; what matters is whether work is meaningful, whether we feel like we have control over our jobs, and whether we feel that our hard work will have some equal reward. Senior people are more likely to enjoy these perks than juniors, even if the former’s decisions are more nerve-racking.

Finding Reconnections
---------------------

In the second half of the book, Hari gives some suggestions for how we can all be less unhappy–what he calls “reconnections” with the things we need. He came to realize we need to think less about ourselves and more about others. We need to ditch spending so much time alone with ourselves; it’s more natural to be in the flow of other people. “Nature is connection,” leading expert on loneliness [John Cacioppo](http://www.johncacioppo.com/) tells Hari.

And Hari learns that it’s better to lose oneself in the crowd. “The real path to happiness, \[the researchers\] were telling me, comes from dismantling our ego walls–from letting yourself flow into other people’s stories and letting their stories flow into yours; from pooling your identity, from realizing that you were never you–alone, heroic, sad–all along,” Hari writes. Now, when he feels depressed, Hari doesn’t do something for himself, like buying a new shirt, or renting a favorite movie. He tries to do something for someone. He feels better for it.

advertisement

“Meaningful values” are another source of improved contentment. To be happy, we should avoid materialist values and the mental pollution that is advertising. “When they talk among themselves, advertising people have been admitting since the 1920s that their job is to make people feel inadequate–and then offer their product as the solution to the sense of inadequacy they have created, ” Hari writes. “Ads are the ultimate frenemy–they’re always saying: Oh babe, I want you to look/smell/feel great; it makes me so sad that at the moment you’re ugly/stinking/miserable . . . ”

Hari now avoids social media. It is a comparison engine that makes lots of people feel inadequate; the idealized images of their friends make us feel worse. He only watches subscription TV, not the old stuff interrupted by truck and drug ads. More cities could follow the lead taken by São Paulo, Brazil, he says–it has banned public display advertising (the law is called the Clean City Law)–or Sweden and Greece, which have banned advertising to children.

And following the advice of Tim Kasser, a professor of psychology at Knox College, in Illinois, Hari suggests that we live by our intrinsic values as opposed to extrinsic values. That means values that are important in themselves, like loving our friends and family and following our interests, as opposed to caring how others view us and trying to fill the hole in our hearts with more possessions. These don’t, ultimately, make us happier, even if buying something has a momentary thrill.

Hari also lays out some compelling research about the importance of nature. For example, the University of Essex has shown in large-scale studies that people who move to the countryside from cities, as opposed to the other way round, have higher levels of mental health. Likewise, people who live near green spaces within cities are happier than those who live [adjacent to asphalt and tall buildings](http://www.bbc.com/news/science-environment-25682368).

Lost Connections imagines that any number of social interventions might make us feel better about ourselves. In a final chapter, Hari interviews the Dutch historian [Rutger Bregman](https://www.fastcompany.com/40412384/venture-capital-for-the-people-making-the-case-for-a-basic-income), a leading advocate for a [basic income](https://www.fastcompany.com/3040832/a-universal-basic-income-is-the-bipartisan-solution-to-poverty-weve-bee). By giving people money to meet their everyday needs, Bregman argues, we can improve their well-being, free them from pointless jobs, and allow everyone to engage in meaningful activity again. Research, though limited, seems to back up this idea, including a large basic income trial in Manitoba, Canada, in the 1970s.

Some [have taken issue with Hari’s book](https://www.theguardian.com/science/brain-flapping/2018/jan/08/is-everything-johann-hari-knows-about-depression-wrong-lost-connections), saying that he unfairly paints psychiatrists as pill pushers. They say Hari’s social science insights aren’t new or particularly revelatory, which may be true–to academics and professional therapists. The public clearly mostly thinks depression is a matter of serotonin levels and genetics, and that the divide between depression and mere unhappiness is absolute. Hari did and he lived with depression for more than 20 years. Here, Hari makes the subject more humane and social. In his telling, it’s something we can fix and work on, not something we must acquiesce to and medicate.

advertisement

advertisement
