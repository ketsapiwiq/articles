---
title: "Et si le futur de l’Afrique était cyberpunk ?"
url: http://usbeketrica.com/article/et-si-le-futur-de-l-afrique-etait-cyberpunk
keywords: lavenir,johannesburg,continent,zoo,lauren,africains,roman,cyberpunk,futur,africaine,sciencefiction,lafrique
---
Non, la science-fiction n’est pas la chasse gardée d’écrivains geeks et occidentaux. Inspirés par leur quotidien à Accra ou à Johannesburg, des artistes africains prophétisent des villes magiques, hyperconnectées et chaotiques. Voyage dans l’imaginaire de la science-fiction africaine.

C’est un quartier poussiéreux de Johannesburg, un ghetto pour criminels. Là-bas, les hommes s’entassent dans des appartements minables avec leurs animaux de compagnie. Une fouine, un chien, un paresseux. Quiconque commet un crime se retrouve avec une bestiole à protéger. Impossible de s’en défaire, c’est une nouvelle loi de la nature. Si l’animal meurt, son propriétaire aussi. Les cages d’escalier sont maculées de poils, de plumes et de sang séché. Des bêtes à tous les étages. Bienvenue à Zoo City.

Aucune carte de Johannesburg ne mentionne cet endroit, aucun guide ne pourra vous y emmener. Zoo City n’existe pas. Cette enclave bestiale, inspirée du quartier de Hillbrow, a été imaginée par Lauren Beukes. Son dernier roman (Zoo City, Eclipse, 2011) montre un visage inédit – et ravagé – de la cité la plus riche d’Afrique du Sud. « Ceux qui attendaient un livre sur les animaux de la savane, les bébés contaminés par le sida et les enfants soldats ont dû être surpris, reconnaît l’écrivain sud-africaine. C’est l’envers des brochures touristiques ou des titres d’actualité racoleurs. » L’ouvrage a décroché le prix Arthur C. Clarke 2011, qui couronne chaque année le meilleur roman de science-fiction. Une première pour un auteur africain. Il était temps.

« L’avenir est là-bas »
-----------------------

Afrique et science-fiction. L’association est aussi séduisante que surprenante. Le genre apparaît souvent comme la chasse gardée de l’hémisphère Nord. Il semblait éternellement promis aux descendants d’Aldous Huxley et James Cameron, jusqu’à ce que des œuvres singulières, parfois fauchées mais ambitieuses, viennent remettre les pendules à l’heure. En matière d’imaginaire futuriste, il faudra compter avec l’Afrique. Des films, des livres et des disques explorent l’avenir du continent. Des auteurs ghanéens, des réalisateurs kényans, des musiciens sud-africains, des hommes et des femmes, noirs, blancs ou métis, racontent l’Afrique de demain.

Dans le genre, l’Afrique du Sud excelle. Il y a les beats futuristes et flippants du rappeur Spoek Mathambo. Il y a les townships de [District 9](http://www.allocine.fr/video/player_gen_cmedia=18908098&cfilm=143026.html), le blockbuster de Neill Blomkamp, un gars de Johannesburg lui aussi. Il y a évidemment les villes de Lauren Beukes qui, avant [Zoo City](http://www.pressesdelacite.com/livre/science-fiction/zoo-city-lauren-beukes), emmenait ses lecteurs à [Moxyland](http://www.pressesdelacite.com/livre/science-fiction/moxyland-lauren-beukes), une version high-tech du Cap. La créativité du continent impressionne. Jetez-vous sur YouTube pour regarder Pumzi, court-métrage kényan présenté à Cannes, éblouissant de maîtrise formelle.

 Réfugiée dans un bunker après une guerre de l’eau dévastatrice, une scientifique lutte pour récupérer chaque goutte d’or bleu. Allez feuilleter  [AfroSF](https://www.amazon.fr/AfroSF-Science-Fiction-African-Writers-ebook/dp/B00AEUH112), un recueil de nouvelles édité par Ivor W. Hartmann, un Zimbabwéen installé en Afrique du Sud. Impossible d’être exhaustif, surtout si on se plonge dans le passé. Car la science-fiction africaine a ses pères fondateurs, tels Amos Tutuola, l’auteur nigérian de Ma vie dans la brousse des fantômes, un conte fantastique publié en 1954, ou encore Mohammed Dib, qui envisage l’avenir d’une Algérie déchirée dans Qui se souvient de la mer (1962).

> L’afro-futurisme: des musiciens afro-américains, s’imaginent projetés dans l’espace, en explorateurs intergalactiques.

Les racines de la science-fiction africaine sont aussi à chercher aux États-Unis. La diaspora afro-américaine s’empare du genre dans les années 1960-1970, donnant naissance à un courant puissant et déluré dont les secousses agitent toujours la culture pop mondiale : l’afro-futurisme. À l’époque, des musiciens afro-américains, héritiers symboliques de l’esclavage, s’imaginent projetés dans l’espace, changés en divinités ou en explorateurs intergalactiques. Ces déracinés se cherchent une planète. Sur scène, leurs performances sont déchaînées, proches de la transe. Le chant rejoint souvent le slogan : « One Nation Under a Groove », scande le groupe Funkadelic, icône de l’afro-futurisme, sur une pochette d’album montrant des cosmonautes à la conquête de la Terre avec un drapeau « R&B ».

![](https://static.usbeketrica.com/images/upload/57a32b799da85.jpg) Pochette de l'album One Nation Under a Groove de Funkadelic (1978)

 Le mouvement pour les droits civiques a pris fin quelques années plus tôt. L’émancipation est désormais à portée de boogie. Recyclé par Janelle Monáe ou Keziah Jones, l’afro-futurisme est revenu à la mode. Les messages politiques ont disparu, ne reste que l’esthétique. Au Sud, de façon plus discrète, les créateurs africains sortent progressivement de l’anonymat. Familiers des réseaux sociaux, souvent auteurs de blogs, ils ont fait d’Internet leur éditeur, leur galerie d’art et leur salle de projo, tandis qu’à l’étranger l’image d’une Afrique figée, hors du temps, se fissure lentement. « Le regard occidental a changé », confirme Kapwani Kiwanga, artiste canadienne d’origine tanzanienne et auteur d’Afrogalactica, lecture-performance sur un voyage spatial organisé par les États-Unis d’Afrique. « Le marché africain s’ouvre, la population est jeune... En Occident, les gens se disent que l’avenir est là-bas. »

Geeks et sorciers
-----------------

Bien sûr, l’existence d’une science-fiction « africaine » est une utopie en soi, un rêve panafricain. Rassembler dans un même mouvement artistique une multitude de pays, de cultures et de langues, voilà un projet fou, comparable aux mythiques États-Unis d’Afrique... La nouvelle vague futuriste est à l’image d’un continent immense : un kaléidoscope. L’œil attentif notera pourtant des rémanences. Il verra que la science-fiction africaine mêle souvent, très naturellement, la magie et les rituels aux technologies les plus modernes. Au contraire, dans la science-fiction occidentale, plus cartésienne, le progrès technique entraîne souvent la disparition des coutumes anciennes. « La magie et la mythologie sont enracinées dans la littérature africaine, parce qu’elles font toujours partie de la vie quotidienne en de nombreux endroits, interprète Lauren Beukes, l’auteur de Zoo City. Le culte des ancêtres et la médecine traditionnelle sont banals pour beaucoup de gens et cohabitent joyeusement avec les mobiles et Facebook. C’est un mélange de premier et de tiers-monde, de technologie et de magie. »

> « L’Afrique est la dernière frontière »

Les sorciers africains sont des geeks, adeptes de la divination sur Skype ou Twitter. Chez Lauren Beukes, un sangoma convoque les esprits grâce à un téléphone mobile. « L’âme est trop surchargée », justifie-t-il. Sa méthode est un signe des temps. Alors que le téléphone fixe n’est jamais parvenu à s’imposer en Afrique, le mobile organise la vie quotidienne (650 millions d’appareils sur le continent) : échanges bancaires, navigation Internet, communication avec les esprits... D’immenses déchetteries électroniques recueillent ensuite les cadavres du progrès technique au Nigeria et au Ghana. Les « brouteurs » d’Abidjan ou d’ailleurs, escrocs en ligne doués pour le chantage à la webcam et les mails tire-larmes, font partie du paysage.

Cette frénésie technologique irrigue toute la science-fiction africaine. Elle donne naissance à des objets, des lieux et des comportements qui sont autant de sources d’inspiration pour les artistes, comme le raconte Jonathan Dotse, jeune écrivain ghanéen et promoteur de la science-fiction africaine sur son blog, [afrocyberpunk.com](http://www.afrocyberpunk.com/). Chez lui, à Accra, les prémices du futur affleurent de tous côtés : « J’observe ma ville et je vois les traces subtiles de la société de l’information, écrit-il, des centres-villes qui grouillent de smartphones, des communications qui monopolisent les ondes avec un nombre croissant de données, une cybercriminalité qui prospère tranquillement à chaque coin de rue. Je réalise qu’une sorte de futur cyberpunk s’est matérialisé ici sans que je m’en aperçoive – en fait, sans que personne ne s’en aperçoive. »

Le constat vaut à Accra comme à Lagos ou à Johannesburg. Les cités tentaculaires s’étendent et se divisent, fracassées par endroits, aériennes parfois. Le Ghana et la Côte d’Ivoire affichent des taux de croissance auxquels la vieille Europe n’ose plus rêver. Les richesses minières, pétrolières et halieutiques aiguisent les appétits chinois, coréens, turcs ou brésiliens. Des entrepreneurs africains tentent leur chance dans ces pays. Ils reviendront habiter les mégapoles du continent, futures grandes places de l’économie mondiale. « L’Afrique est la dernière frontière », assure Jonathan Dotse qui travaille à son premier roman, une aventure cyberpunk située à Accra en 2057.

Les fantômes de l’Histoire
--------------------------

Urbanisme anarchique, inégalités, nature polluée... La « dernière frontière » est aussi celle de toutes les peurs. La science-fiction africaine ne promet rien de bon, qu’il s’agisse du néocolonialisme chinois dans une nouvelle d’AfroSF où le continent tombe sous la coupe du mandarin et du yuan, de corruption politique dans le long-métrage franco-camerounais Les Saignantes, ou de xénophobie (District 9) et de ségrégation ([Elysium](http://www.elysium-lefilm.fr/)) dans les films de Neill Blomkamp.

Bande Annonce Les Saignantes  (2009)

 

 

Pour Lauren Beukes, l’intérêt de la science-fiction africaine réside justement dans ces métaphores socio-politiques : « Elle s’intéresse aux véritables problèmes, elle observe comment la violence surgit, comment l’Histoire nous façonne et les fantômes qu’elle laisse dans son sillage. C’est une résistance aux stéréotypes sur l’Afrique, même si elle permet aussi de les détourner. »

Les représentations de l’Afrique, notamment à Hollywood, font effectivement peine à voir. La guerre ou l’âge de pierre, en résumé. On compte bien quelques héros noirs, mais rarement africains. L’avenir s’écrit toujours à New York, Los Angeles ou Tokyo. L’ignorance domine. Auteur d’un roman postapocalyptique situé au Soudan, Qui a peur de la mort ?, Nnedi Okorafor estime que la science-fiction africaine vient combler un manque : « Les lecteurs africains, comme tous les autres, apprécient de se retrouver dans les histoires et d’y voir leur propre point de vue, leur culture, leur passé et leur environnement. » L’écrivain américaine d’origine nigériane aime leur glisser des clins d’œil : « Mon roman Lagoon en est rempli. C’est une histoire d’invasion extraterrestre à Lagos, au Nigeria. Celle-ci est pleine de références à des événements que les Nigérians reconnaîtront. Il y a même des paragraphes entiers rédigés en pidgin anglo-nigérian. » N’en déplaise aux fans de Star Trek : dans le futur, on ne parlera pas forcément klingon.

 

Illustration de une : Pochette de l'album "The ArchAndroid" de Janelle Monáe

**Article paru dans le numéro 12 d'Usbek & Rica. **
