---
title: "A l’ère de la post-virilité"
url: http://bit.ly/2mip1d7
keywords: gourarier,domination,pouvoir,lère,mélanie,raewyn,lire,femmes,postvirilité,masculinités,masculinité,hommes
---
C’est une pub assez sexiste des années 80 comme on les concevait à l’époque et qui marqua de ce fait les esprits. Un homme, entre balbutiements et mots heurtés en définitive jamais prononcés, n’arrive pas à exprimer tout son amour pour «elle» qui s’avère être… un parfum. «Darling, ce sont les hommes qui en parlent le mieux», claque le slogan au nez de cet homme amputé de sa capacité à dire ses émotions. Dans une inversion ironique de l’histoire, on pourrait dire aujourd’hui que «les hommes, ce sont les femmes qui en parlent le mieux».

Deux jeunes chercheuses viennent de publier des essais où le principe de l’égalité n’est pas abordé du côté du féminin, mais en explorant ce continent encore inexploré qu’est le masculin. Une façon d’éclairer d’un nouveau jour les enjeux de pouvoir entre les sexes et comprendre les mécanismes complexes qui se jouent dans la domination. Un renversement de la question féministe en somme.

Pour s’attaquer à cet autre versant de la domination masculine, l’anthropologue Mélanie Gourarier est allée explorer le monde de la séduction où œuvrent des coachs en masculinité. En crise, les hommes ? Non pas vraiment, répond Mélanie Gourarier dans son essai Alpha mâle qui paraît ce jeudi au Seuil. Dans le concept valise de «crise de masculinité», récurrent au cours des siècles, la chercheuse voit plutôt une stratégie pour maintenir un pouvoir, l’adapter aux exigences de la modernité ([lire ci-contre](https://www.liberation.fr/debats/2017/03/01/la-masculinite-contemporaine-c-est-se-gouverner-soi-meme-pour-mieux-gouverner-les-autres_1552580)).

De son côté, la politologue Réjane Sénac interroge un autre impensé : le mot «fraternité» de la devise républicaine. Le propos des Non-Frères au pays de l’égalité (Presses de Sciences-Po) se ramasse en une exclamation. «Qu’entendrait-on si on proclamait "Liberté", "Egalité", "Sororité" ! avance la politologue du Centre de recherches de Sciences-Po (Cevipof). Il est étonnant de voir que la notion de fraternité ne fasse pas plus question alors que cette devise produit une forte identification politique.» ([lire page 24](https://www.liberation.fr/debats/2017/03/01/une-fraternite-tres-masculine_1552578)). La persistance du concept de fraternité est révélatrice selon elle du poids de cette histoire aveugle aux inégalités.

### L’angle mort

Longtemps, la masculinité est restée le continent noir des études de genre, l’angle mort de la domination. Bien sûr, il y a des pionnières comme les féministes Nicole Claude-Mathieu et Christine Delphy ou l’anthropologue Françoise Héritier plus tard. Il y a aussi des hommes qui se sont intéressés à la condition de leurs semblables : les sociologues Pierre Bourdieu, François de Singly, l’anthropologue Maurice Godelier ou le chercheur Daniel Welzer-Lang. Mais globalement l’homme, implicitement universel, échappe à l’étude de la singularité - alors que les femmes y sont assignées. La démarche est bien «d’aborder la norme universelle dans sa particularité, et la révéler ainsi comme point aveugle d’une approche de la domination», explique le sociologue Eric Fassin de l’université Paris-VIII (1). Car la norme est forcément invisible… habituellement, la masculinité va sans dire alors qu’elle signifie toujours d’autres rapports de pouvoir.

Depuis quelques années, une nouvelle génération de jeunes chercheurs comme Alban Jacquemart de l’université Paris-Dauphine, les anthropologues Mélanie Gourarier et Gianfranco Rebucini et le sociologue Florian Vörös se sont spécialisés dans l’étude des masculinités. Les trois derniers ont animé un séminaire à l’Ecole des hautes études en sciences sociales (EHESS) autour de ce thème. Objectif : appréhender les masculinités dans leur multiplicité mais aussi à partir des rapports de pouvoir qui les constituent et les hiérarchisent. Ce champ d’études est particulièrement marqué par les travaux de la sociologue australienne Raewyn Connell qui a forgé le concept majeur de «masculinité hégémonique». Un peu à la manière de Gramsci, cette masculinité n’est pas tant une injonction coercitive, directe, immuable, mais bien «une emprise idéologique». C’est donc moins par la force que par le consentement à leurs valeurs que les dominants obtiennent l’adhésion des dominés, explique l’anthropologue Mélanie Gourarier. La masculinité hégémonique change et s’adapte aux évolutions de la société : elle est relationnelle, contextuelle, critiquable, insiste Raewyn Connell, elle n’est pas essentialiste. Loin d’un «éternel masculin» invariant et anhistorique, elle assoit sa légitimité en opposant et hiérarchisant les autres masculinités dites subalternes, subordonnées, marginalisées. Un pouvoir qui sans cesse se régénère et se nourrit du monde contemporain. Dans le sillage du travail de l’historien Patrick Boucheron, il s’agit bien, rappelle Mélanie Gourarier d’«avoir conscience de l’historicité de la notion». On ne naît pas femme, ni homme non plus…

Masculinités. Enjeux sociaux de l’hégémonie, le livre de Raewyn Connell, écrit en 1995, a été traduit en France en 2014 par les éditions Amsterdam. «L’étude des masculinités a pour but d’interroger à nouveaux frais les rapports sociaux de classe, de genre ou encore les enjeux liés à la globalisation et à la colonisation», est-il écrit dans l’introduction. Montrer ainsi que les dominés ne sont pas que des femmes, mais aussi des hommes à la masculinité dévalorisée - de la figure du «looser» à l’homme de la banlieue - les intersexes ou les non-Blancs.

### Un spectre large

Cette nouvelle géographie de la domination ringardise de ce fait la «guerre des sexes» qui oppose inlassablement les hommes aux femmes comme dans un énième film de mœurs. La domination œuvre sur un spectre bien plus large, la population des «subalternes» se révèle bien plus nombreuse.

«Si l’hégémonie peut être lue comme une reformulation de la violence symbolique, ce concept permet de penser l’efficacité de la domination non seulement sur les femmes, mais aussi sur les hommes qui n’y consentent pas moins», résume Eric Fassin dans la postface de la traduction française du livre de Raewyn Connell.

Cette nouvelle description de la domination s’accompagne d’un nouveau vocabulaire. Réjane Sénac parle des «non-frères», tous ceux exclus de la fraternité politique initiale, Mélanie Gourarier des «subalternes», d’une «classe de vulnérabilité». Pour cette génération de chercheurs, l’enjeu est de mener un travail sur la persistance et l’aggravation des inégalités, sans se restreindre au seul domaine du genre, mais en intégrant aussi les dimensions sociales, économiques et d’origine. Une façon d’éviter une hiérarchisation des inégalités qui entamerait la cohésion d’une société déjà éprouvée.

(1) In Masculinités. Enjeux sociaux de l’hégémonie, Raewyn Connell, éditions Amsterdam, 2014.

A lire aussi

### Mélanie Gourarier : «La masculinité contemporaine, c'est se gouverner soi-même pour mieux gouverner les autres».

**L’anthropologue Mélanie Gourarier s’est invitée dans l’entre-soi des hommes. Observant leur stratégie de reconquête d’un pouvoir qu’ils n’ont jamais perdu et ne cessent de déractualiser.**

Speed dating, coaching en séduction : pour sa thèse de doctorat, la jeune anthropologue Mélanie Gourarier a arpenté le terrain intime des recompositions masculines. Autour du mal-être et du déclin supposé du premier sexe, des consultants et coachs ont investi le champ de la séduction dispensant techniques de drague et recettes pour ego meurtri. La séduction, comme arme de reconquête du masculin ? Loin d’être anecdotique, ce coaching pour séducteurs montre que la «maison des hommes», cet entre-soi masculin qui entretient et renforce les hiérarchies sexuelles et de pouvoir, est loin d’avoir disparu. En crise, le masculin ?

[Lire l'intégralité de l'article](https://www.liberation.fr/debats/2017/03/01/la-masculinite-contemporaine-c-est-se-gouverner-soi-meme-pour-mieux-gouverner-les-autres_1552580)

A lire aussi  

### Une fraternité très masculine

**La politologue Réjane Sénac met au jour un système républicain inégalitaire, dont les femmes ne sont pas les seules perdantes.**

Jeune chercheuse à Sciences-Po, Réjane Sénac n’a pas peur de s’attaquer à un totem républicain et d’égratigner au passage quelques commandeurs du savoir académique. L’égalité à la française est un conte de fées, affirme-t-elle dans son dernier essai les Non-Frères au pays de l’égalité (éditions Sciences-Po) sorti fin janvier.

[Lire l'intégralité de l'article](https://www.liberation.fr/debats/2017/03/01/une-fraternite-tres-masculine_1552578)

![Image extraite de la «Série V» d'Ingrid Berthon-Moine.](https://medias.liberation.fr/photo/999333-v3.jpg?modified_at=1488446410&width=750)

A lire aussi

### **L'égalité doit-elle être rentable ?**

**De plus en plus de politiques égalitaires sont justifiées en terme de rentabilité. Un dévoiement du principe.**

C’est devenu un argument choc des politiques d’égalité : intégrer plus de femmes ou des profils divers dans les rouages supérieurs de l’économie serait gage d’efficacité. Une valeur ajoutée. Le mouvement est parti du monde de l’entreprise où parité et diversité sont vendues depuis des années comme facteur de performance.

[Lire l'intégralité de l'article](https://www.liberation.fr/debats/2017/03/01/l-egalite-doit-elle-etre-rentable_1552579)

 
