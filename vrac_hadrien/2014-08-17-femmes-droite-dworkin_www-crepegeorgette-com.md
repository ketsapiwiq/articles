---
title: "Résumé de : Les femmes de droite d’Andrea Dworkin"
url: http://www.crepegeorgette.com/2014/08/17/femmes-droite-dworkin/
keywords: évoque,viol,violence,femme,mariage,résumé,dandrea,sexualité,femmes,dworkin,livg,hommes
---
Je vais vous résumer [Les femmes de droite d'Andrea Dworkin](http://www.editions-rm.ca/livre.php?id=1436). Le livre date d'il y a trente ans ce qui explique par exemple qu'elle évoque le viol conjugal en soulignant qu'il est autorisé. Je résume ce livre en réaction aux nombreux textes réagissant au tumblr des femmes anti féministes.

Dans la préface, Christine Delphy souligne qu'à part Dworkin peu de féministes ont évoqué la sexualité hétérosexuelle dans une société patriarcale. On a revendiqué le droit des femmes à se prémunir des conséquences de cette sexualité via la contraception et l'IVG.  
Dans la vision féministe comme dans la vision patriarcale, le viol, l'inceste sont vues comme des transgressions à la sexualité comme les violences conjugales sont vues comme des transgressions à la définition du mariage.  
Pourtant s'ils sont aussi banalisés c'est qu'ils sont tolérés sinon encouragés et que la violence est partie intégrante de la sexualité hétérosexuelle patriarcale comme le pense Dworkin.  
Dans ce livre Dworkin parle des femmes de droite qu'elle ne condamne pas mais dont elle regrette les choix. Elle estime qu'elles ont affaire à un pouvoir trop vaste et qu'elles se sont aménagées l'espace qu'elles pouvaient.  
La question se pose de savoir sir les gains du mouvement féministe ne peuvent être saisis par les hommes et utilisé contre les femmes. Ainsi elle rappelle que la libération sexuelles des années 60 a enjoint les femmes à être disponibles envers les hommes sinon elles étaient considérées comme non libérées.  
Delphy estime que les féministes ont échoué à définir la sexualité hétérosexuelle ; cela se définit toujours par un rapport sexuel qu'avant les femmes n'étaient pas censées aimer et que, maintenant elles doivent aimer.  
Dworkin dit que la violence de l'acte sexuel ne réside pas dans l'anatomie masculine mais dans l'interprétation qui en est faite.  
La sexualité hétérosexuelle devient un acte où la femme doit jouir de sa propre destruction, pour se conformer à l'archétype du masochisme féminin.  
Delphy critique le féminisme queer qui réduit le genre aux rôles dans la sexualité qu'on pourrait performer alors que les discriminations persistent, elles, bel et bien.

Résumé de Dworkin.  
Dworkin montre que la majeure partie des travaux faits par les femmes sont sous-payés, stéréotypés et stagnants.  Pour elle, les femmes de droite ont  fait un deal. Comme au foyer leur valeur est davantage reconnue qu'au travail, entre autres parce qu'elles sont mères, alors elles défendent le rôle de femme au foyer.  
Le travail ne rend pas les femmes autonomes puisqu'elles sont en général mal et peu payées. Les féministes ont fait le pari qu'un salaire rendrait les femmes indépendantes sauf que cela n'arrivera pas tant qu'on sera dans une société patriarcale. Il y a un intérêt à maintenir les salaires des femmes bas ainsi elles vendront du sexe (dans le mariage ou la prostitution) pour survivre.  
Pour Dworkin, le salaire égal pour un travail égal n'est donc pas une réforme mais une révolution.  
De plus le marché du travail est devenu un autre lieu de coercition sexuelle pour les femmes.  
Les femmes de droite ont donc fait le choix de rester au foyer en espérant que cela sera plus vivable que dehors.

Dworkin aborde ensuite le sujet de l'IVG. Le livre date des années 80, l'IVG est autorisé depuis dix ans et dans de nombreux états, le viol conjugal n'existe pas.  
L'IVG peut rendre la femme coupable de ne pas mettre la maternité au coeur de sa vie alors qu'elle serait censée le faire.  
Si le viol conjugal est autorisé, comment distinguer le viol dans le mariage du coït dans le mariage ?  
Si un homme marié viole une femme comment pourrait-il voir cela différemment de ce qui se passe dans son couple où il a l'autorisation de l'état de violer sa femme ?  
Si la grossesse doit être menée à terme dans le mariage où le viol est légitime alors pourquoi la grossesse issue d'un viol par un inconnu serait illégitime ?  
Dans les années 60, les femmes doivent baiser (le mot est employé par Dworkin "fuck") sinon elles sont accusées de ne pas être libérées.  
Or la possibilité d'une grossesse, ou le souvenir d'une IVG clandestine, ne leur donne ps envie de le faire.  
Beaucoup d'hommes de gauche se sont alors engagés dans un combat pour l'IVG.  
Robin Morgan dit en 1970 "*Il est douloureux de comprendre qu'à Woodstock ou à Altamont, une femme pouvait être qualifiée de "coincé" ou "vieux jeu" si elle refusait de se laisser violer*".  
Lorsque les mouvements féministe émergent, ils font réaliser à beaucoup de femmes qu'elles ont été sexuellement utilisées. Elles vont alors lutter pour le contrôle de leur fertilité ; les mouvements de gauche vont alors se détourner des droits des femmes et de l'IVG puisqu'il ne s'agit plus de seulement pouvoir baiser des femmes.

Les femmes de droite dénoncent l'IVG car pour elles, cela a participé à l'avilissement sexuel des femmes. Elles ont vu que quand l'IVG a été légalisée, les hommes ont revendiqué l'accès au corps des femmes.  Pour elles, l'IVG rend les femmes baisables par les hommes sans qu'ils en paient plus aucun conséquence comme un enfant à nourrir.  
De leur point de vue, la grossesse est la conséquence du sexe qui oblige les hommes à rendre des comptes. Sans IVG, l'homme est obligée d'entretenir la femmes et les enfants qui ne pourrait s'en sortir seule puisque les salaires versés aux femmes sont trop faibles.

Dworkin évoque ensuite la peur de l'homosexualité masculine qui constitue un outil de contrôle des hommes. Elle dresse des groupes d'hommes les uns contre les autres dans une quête éperdue de la masculinité.  
Dans le racisme, l'homme du groupe racisé, subit des stéréotypes sexuels ; soit il est violeur, soit il est dévirilisé. Les noirs dans les Etats-Unis esclavagistes sont dévirilisés et vus comme des animaux. Lorsque l'esclavage est aboli les hommes blancs se sentent dévirilisés, privés de leur masculinité, ils font des noirs des violeurs. Le viol par les hommes noirs (qui est une construction raciste) est avant tout vu comme un vol, non pas des femmes blanches mais de la masculinité des hommes blancs.

Pour Dworkin, les femmes de droite haïssent les homosexuels car pour elles, ils se passent des femmes. Sans la reproduction, les femmes en tant que classe n'ont rien ; les homosexuels font donc croire aux femmes que les hommes n'ont plus besoin d'elles.

Dworkin évoque le programme démographique d'état qu'elle qualifie de "gynecide programmé".  
Elle paele des femmes noirs et hispaniques stérilisées contre leur gré. Elle évoque des contraceptifs testé à Puerto Rico chez les femmes hispaniques.  
Elle souligne que les femmes sans valeur reproductive, les pauvres, les malades, les droguées, les prostituées, les malades mentales sont enfermées. La majorité des personnes pauvres et âgées aux USA sont des femmes. Dans les hospices, 72% des patients sont des femmes.  
Les noirs (hommes comme femmes) meurent plus jeunes à cause du racisme systémique qui les privent par exemple de soins. Il n'y a que 9% de non blancs chez les personnes âgées et 5% dans les hospices.

Les femmes sont surmédicalisées tout au long de leur vie, surtout avec des anti dépresseurs, barbituriques etc.

On verse peu de prestations sociales aux femmes ainsi elles restent obligées d'accepter des emplois peu qualifiés. C'est encore plus vrai pour les femmes noires ; il faut que leurs prestations sociales soient très basses pour qu'elles continuent à faire les boulots les plus difficiles.  
Les aides sociales maintiennent les femmes en état de dépendance. Elles n'auront de toutes façons que des emplois mal payés et où elles seront exploitées. Elles punissent les femmes d'avoir eu des enfants hors mariage, crée une main d'œuvre disponible à bas prix et permet le contrôle reproductif des femmes qui n'ont pas à se reproduire comme les noires et les hispaniques. Selon Dworkin, les femmes n'existent que si elles sont une valeur reproductive.

Dworkin craint que le modèle du bordel (là où des femmes sont disponibles sexuellement pour les hommes) s'étende à la reproduction, qu'elle appelle "modèle de la ferme" . Elle pense que les technologies reproductive vont permettre de créer des fermes où la reproduction sera une marchandise.

Elle évoque ensuite l'anti féminisme qui se développe selon 3 axes :

\-  le modèle "séparés mais égaux" qui consiste à dire qu'il existe un principe de nature masculine, un principe de nature féminine qui sont si différents qu'ils entraînent une séparation sociale absolue et des choix de vie complètement différents. On déclare les hommes et les femmes égaux car chacun fait également ce qui est propre à sa classe de sexe. On promet l'égalité si chacun reste  à sa place.

\- le modèle de la supériorité féminine. Les femmes sont vénérées de façon abstraite, selon des modèles inatteignables comme la vierge Marie. Les femmes ont un tel pouvoir sur les hommes qu'elles les forcent à faire des choses comme les conquérir, les forcer.  
- le modèle de la supériorité masculine : dieu et la biologie ont déclaré que les hommes étaient supérieurs.

Les femmes sont exploitées par le viol, la violence conjugale, l'exploitation économique, l'exploitation reproductive. Elles forment une classe subordonnée aux hommes, colonisée sexuellement; privée de droits, traitées comme une possession.

La pornographie et la prostitution condensent toutes ces exploitations. Il y a antiféminisme lorsqu'on propose de sacrifier une partie de la population féminine pour épargner les autres.

Pour les femmes de droite, le mariage est censé les protéger du viol.  
En étant entretenu au foyer, elles sont censées être protégées de l'exploitation économique.  
La reproduction leur accorde un peu de respect et de valeur.  
Un mariage religieux les garantit contre la violence conjugale.  
Bien sûr elles se trompent. Elles risquent davantage d'être violée par leur mari que par un inconnu, sans argent elles sont sous la dépendance économique de leur mari. Le foyer reste le lieu le plus dangereux pour une femme.

La postface de Frederick Gagnon parler de l'Amérique post 2008 pour voir si les choses ont changé.  
85%des victimes de violence conjugale sont des femmes.  
20%des femmes ont déjà subi un viol.  
Le salaire hebdomadaire d'une femme est de 669 dollars contre 824 pour un homme.  
Pendant les élections de 2008, il y a eu énormément de misogynie envers Clinton et Palin. L'une était bête, l'autre trop émotive. Clinton a tenté de contrer ces attaques en se comportant comme un homme. Palin a adopté l'attitude de la femme parfaite en misant sur son look, elle a été qualifiée de babe et de MILF quand Clinton était qualifiée de bitch et de cunt.  
Le nombre de femmes de droite n'a pas diminué ; Palin se revendique féministe, ne veut pas voir les femmes comme des victimes mais comme des femmes capables de tout y compris de mener à terme une grossesse non désirée. Elle se définit comme la "mama grizzly", pro-carrière, pro-famille, pro-maternité  et pro-vie. Elles reproduisent ce que Dworkin avait dénoncé.  
L'antiféminisme est toujours très populaire aux Etats-Unis et les combats contre l'IVG, la contraception le viol sont persistants.
