---
title: "La France préfère payer (deux fois) pour les articles de ses chercheurs"
url: http://rue89.nouvelobs.com/2014/11/10/france-prefere-payer-deux-fois-les-articles-chercheurs-255964
keywords: articles,marché,millions,scientifiques,france,payer,chercheurs,préfère,libre,revues,publications,lire,recherche,accès,elsevier
---
![Des fioles (Erlenmeyer), dans une classe de science](https://media.nouvelobs.com/ext/uri/sreferentiel.nouvelobs.com/file/rue89/d9e301e04b30468db7d13519bbd46bef.jpg)  

Des fioles (Erlenmeyer), dans une classe de science - [Lokesh Dhakar/Flickr/CC](https://www.flickr.com/photos/lokesh/6767422267)

La France n’a plus d’argent pour ses universités. Mais elle en a pour les éditeurs.

Tandis que les présidents d’université [apprennent](http://www.liberation.fr/societe/2014/11/06/l-etat-tranche-dans-les-dotations-aux-universites_1138001) que leur dotation est amputée de 400 millions d’euros, le ministère de la Recherche [s’engage](http://scoms.hypotheses.org/293), dans le plus grand secret, à payer 172 millions d’euros au leader mondial de l’édition scientifique, [Elsevier](http://fr.elsevier.com/).

Rue89 dévoile en exclusivité le contenu de cet accord \[[lire le document PDF](http://bo.rue89.nouvelobs.com/sites/news/files/assets/document/2014/11/marche_elsevier.pdf)\]. Son objectif : racheter pendant cinq ans des publications déjà payées par le contribuable pour les rendre accessibles... à leurs auteurs.

#### Le travail de bénévoles

Le marché de l’édition scientifique est un secteur peu commun : ceux qui créent de la valeur ne sont jamais rémunérés ; au contraire, ils paient souvent pour voir leurs productions publiées. Les auteurs ne touchent rien sur leurs articles ; l’évaluation par les pairs est réalisée bénévolement.

Ce travail considérable est indirectement financé par l’argent public. L’écriture d’articles et la participation à des comités de lecture font partie des activités attendues des chercheurs et donnent lieu à des crédits de recherche, financés par le contribuable.

La publication scientifique est organisée autour de quelques maisons d’édition privées. Elles tiennent les journaux où les résultats des recherches sont publiés. Chaque journal a un comité éditorial, recevant les potentielles contributions  ; celles-ci sont ensuite envoyées à des scientifiques bénévoles qui effectuent la revue par les pairs. C’est sur la base de leurs commentaires et retours que l’on décide si un article sera finalement publié ou rejeté et renvoyé à ses auteurs. En cas d’acceptation de l’article, ses auteurs doivent souvent s’acquitter d’une somme donnée afin que la publication se fasse.

#### Des marges énormes

Ces revues sont rarement accessibles gratuitement. Les éditeurs vendent l’accès à ces mêmes articles aux bibliothèques universitaires et aux laboratoires de recherche. Les ressources financières pour la publication proviennent des crédits de recherche accordés aux laboratoires  ; les accès aux revues sont achetés au niveau de l’institution. Dans les deux cas, les subventions sont publiques.

Les principaux acteurs de l’édition scientifique dégagent des bénéfices considérables. Le secteur est en effet dominé par un oligopole. Quatre «  grands  » se partagent l’essentiel du gâteau mondial :

-   le néerlandais Elsevier,
-   l’allemand [Springer](http://www.springer.com/fr/),
-   l’américain [Wiley](http://onlinelibrary.wiley.com/browse/publications?type=journal),
-   l’anglais [Informa](http://www.informa-ls.com/).

Ils en tirent des marges énormes : de 30% à 40% de bénéfice annuel net pour Elsevier ou Springer.

![Bnfices des principaux diteurs scientifiques en 2011, en euros](https://media.nouvelobs.com/ext/uri/sreferentiel.nouvelobs.com/file/rue89/4431f4517d8d515834b07d85acb042b3.jpg)  

Bénéfices des principaux éditeurs scientifiques en 2011, en euros - [Chiffres de Heather Morrison, mis en forme par Of Storks and Germs](http://ofstorksandgerms.wordpress.com/2014/02/21/le-savoir-cest-comme-la-confiture-ca-se-vend-et-ca-rapporte-des-thunes/)

Ces quatre grands revendent donc chèrement aux universités un contenu qu’elles ont elles-mêmes produit.

Dans ce marché entièrement cloisonné, la libre concurrence n’existe pas et l’entente préalable est la règle  : les prix des abonnements ne cessent de s’envoler depuis trente ans, alors que les frais de production, à l’ère de l’édition électronique, n’ont jamais été si bas. Par exemple, l’abonnement annuel à la revue [Brain Research](http://store.elsevier.com/product.jsp?issn=00068993) d’Elsevier coûte la bagatelle de 15 000 euros.

#### Une politique assumée

L’accord entre la France et Elsevier s’élève à 171 697 159,27 euros HT pour un périmètre de 476 universités et établissements hospitaliers.

Le premier versement (d’environ 34 millions euros d’argent public) a été entièrement déposé en septembre 2014. En contrepartie, 476 établissements publics pourront accéder à un corpus d’environ 2 000 revues.

Aussi bien les recherches publiées que ce droit de lire sont essentiellement financées sur fonds publics. Ces derniers auront donc été versés à Elsevier deux fois : une première fois pour publier, une deuxième fois pour lire.

Ce n’est pas un accident de parcours. L’accord avec Elsevier répond à une politique totalement assumée du gouvernement. En mars 2014, Geneviève Fioraso, alors ministre de l’Enseignement supérieur et de la Recherche, spécifie à l’Académie des sciences les principaux axes de sa politique. Deux d’entre eux supposent les interactions privilégiés avec l’éditeur Elsevier. La négociation du droit de lire de centaines d’établissements publics d’enseignement et de recherche est ainsi gérée au niveau national, une première dans le domaine.

#### La «  négociation  » décidée d’avance

On pourrait arguer de la bienveillance du ministère vis-à-vis des établissements publics, dans la mesure où il prend en charge cet engagement vital pour la recherche. Ce serait ne pas voir, entre autres problèmes, l’absence totale de transparence quant au choix du fournisseur (pourquoi Elsevier en particulier ?) et la non-mise en concurrence du marché entre plusieurs acteurs (pour un tel montant, une ouverture de marché public est obligatoire).

Prisonniers d’un jeu aux règles fixées d’avance, les négociateurs (le consortium [Couperin](http://www.couperin.org/) et l’Agence bibliographique de l’enseignement supérieur ou [Abes](http://fr.wikipedia.org/wiki/Agence_bibliographique_de_l%27enseignement_sup%C3%A9rieur)) n’ont pas pu négocier grand-chose. Comme nous l’évoquions plus haut, la libre concurrence n’existe pas. L’article 4 de l’accord est explicite  :

« Marché de prestations de services sans publicité et sans mise en concurrence préalables, négocié avec un opérateur économique déterminé pour des raisons tenant à la protection de droits d’exclusivité de distribution. »

Dans cette perspective, un curieux montage est mis en place pour que Elsevier conserve ses anciens clients à ses conditions. Les institutions ayant déjà un contrat avec l’éditeur ne peuvent rejoindre la licence nationale qu’en moyennant une majoration de la participation (de l’ordre de 2,5% à 3,5%). Celles n’ayant pas de contrats ne sont pas concernées (art. 10.1).

#### Combien d’accords de ce type ?

Pour faire grossir la note, Elsevier cède des corpus de revues (ses «  revues phares  »)  : « Aucun titre considéré comme “ Revue phare ” (liste en annexe 5 du présent cahier) ne pourra être retiré du périmètre documentaire auxquels accèdent les abonnés.  » (art. 6.2). Elles ne sont pas de qualité égale et, surtout, ne sont pas toutes également intéressantes.

Le prix final a été abaissé par rapport à la somme initialement évoquée en février, et [sur Rue89](http://blogs.rue89.nouvelobs.com/les-coulisses-de-wikipedia/2014/02/17/amis-chercheurs-vous-vous-faites-arnaquer-trois-fois-merci-elsevier-232350) : on passerait de 188 millions à 172 millions d’euros. Seulement, de nombreuses institutions se sont retirées de l’accord  : on passe de 642 à 476 institutions associées.

Il va sans dire que, si la situation est scandaleuse, il ne s’agit que d’un accord avec un seul éditeur. Un récent rapport de l’Académie des sciences \[[PDF](http://www.academie-sciences.fr/presse/communique/rads_241014.pdf)\] évoquait une dépense de 105 millions d’euros par an pour l’acquisition des publications scientifiques. Ce chiffre apparaît largement en-dessous de la réalité  : pour un accord couvrant seulement une partie des universités et établissements de recherche français, Elsevier préempte 33 millions à 35 millions par an. Les coûts réels représentent probablement 200 à 300 millions.

Une alternative est tout à fait possible.

#### Ailleurs en Europe, pourtant...

Depuis une quinzaine d’années, un grand mouvement international s’est développé en faveur du libre accès aux publications. Il s’agit de les rendre accessibles et réutilisables à tous, non seulement aux chercheurs, mais à tout le monde.

Les chercheurs n’ont en effet aucun intérêt au maintien du système actuel. Ne donnant pas lieu à rétribution, le droit d’auteur n’est qu’une fiction pour perpétuer les droits de l’éditeur. Non seulement cette captation limite l’accès aux publications scientifiques  ; elle empêche même le chercheur de réutiliser son propre travail, car il a cédé ses droits lors de la signature du contrat de publication.

Le principal blocage vient de l’Etat. Rien n’est fait pour que la recherche soit libérée de l’emprise des grands groupes éditoriaux. L’évaluation publique se focalise sur les [revues dites qualifiantes](http://www.aeres-evaluation.fr/Publications/Methodologie-de-l-evaluation/Listes-de-revues-SHS-de-l-AERES) (soit surtout des revues détenues par les grands éditeurs)  : certaines sections universitaires vont jusqu’à [considérer](http://www.cpcnu.fr/web/section-60/conseils-generaux) que les publications en libre accès ne sont, par défaut, «  pas scientifiques  ».

D’autres pays européens montrent la voie.

-   l’**Allemagne** a édicté l’année dernière [une loi](http://openaccess.inist.fr/?Point-sur-le-Libre-Acces-en) limitant les droits d’exclusivité de l’éditeur à un an  : passé ce délai, la publication peut être rediffusée en libre accès ;

&nbsp;

-   les **Pays-Bas** viennent [d’interrompre](http://www.vsnu.nl/news/newsitem/11-negotiations-between-elsevier-and-universities-failed.html) avec fracas une négociation avec Elsevier. Le gouvernement néerlandais soutient en effet pleinement les revendications des chercheurs et des bibliothécaires, qui appellent à diffuser la totalité des productions scientifiques en libre accès.

C’est peut-être finalement la conséquence la plus grave de l’accord Elsevier  : geler pour cinq ans toute politique de libre accès ambitieuse. Les citoyens français continueront de payer deux fois une recherche qu’ils ne pourront pas lire. Et leur gouvernement portera à bout de bras un système cloisonné et obsolète dont l’étendard est la barrière au droit de lire.
