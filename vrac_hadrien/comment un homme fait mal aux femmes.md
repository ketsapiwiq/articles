eluniversal.com.mx
Comment un homme bon fait le mal aux femmes
5-6 minutes

Il n'a jamais frappé une femme auparavant. Sans parler d'en tuer un. Il est loin de ce genre de sauvage.

-Au contraire, je respecte la cause des femmes", dit le héros de notre histoire, en se gonflant la poitrine. -Leur lutte pour l'égalité me semble juste.

Mais il respecte cette cause de loin. Comme si cela se passait sur une planète voisine. Sur Vénus.

Enfant, son père lui a expliqué la différence entre les femmes et les hommes. Il l'a mis devant les portes des toilettes. "Par la porte où il y a une photo d'un danseuse de ballet, n'entrez jamais", a-t-il dit. "Jamais. Par la porte où il y a une moustache, oui".

C'est pourquoi, en 55 ans de vie, notre héros n'a pas lu un seul livre sur le féminisme : il ne s'est pas impliqué dans cette question des femmes. Il n'a pas non plus lu d'article sur le sujet. Lorsqu'il en voit un dans le journal, il tourne respectueusement son attention vers un autre article.

-Je sais que je ne devrais pas y aller.

C'est vrai, il a des amis qui sont violents envers les femmes. Avec leurs partenaires ou leurs filles ou leurs employés. Ils les ont fait taire brusquement. Ils les traitent d'imbéciles. Ou inepte. Ou font allusion à leur corps. Ils les appellent laids. Les sorcières. Ou bien ils font des blagues sur "les vieilles dames". Il n'entre pas là-dedans. Il obéit à l'humiliation et détourne respectueusement son regard.

Son cousin Ezekiel, par contre, ne se contente pas d'humilier sa femme, il la bat un dimanche sur deux.

-Il est une sorte de sport avec cet âne fou", s'indigne notre héros.

Mais il ne transgresse pas son respect pour le sexe opposé, et lorsque la femme d'Ezekiel arrive aux repas du dimanche avec un œil au beurre noir, il ne fait aucun commentaire : il la salue et l'embrasse sur la joue, à côté de l'œil au beurre noir.

-Je suis impressionné", a déclaré notre héros à propos de cette agaçante affaire, "mais je ne m'en mêle pas.

Il est fier d'appartenir à une institution qui compte 75 professeurs et 5 femmes enseignantes.

-Mais ce n'est pas vrai que nous sommes misogynes", proteste notre héros, très sérieusement. -Ce qui se passe, c'est qu'il y a une mystérieuse inertie dans cette activité d'embauche des femmes.

Concrètement, ni lui ni aucun des professeurs n'ont de contact avec les femmes universitaires : ils ne travaillent pas avec elles, ni ne les lisent : ils les respectent, oui, mais à distance, cela aussi. C'est le mystérieux mécanisme qui explique que son institution ne compte pas de professeures.

Et parfois, très occasionnellement, notre héros consomme de la pornographie. Il va à la danse de table. Pour boire et regarder des femmes nues grimper sur un tube d'acier. Ou bien il regarde des vidéos brûlantes sur son ordinateur. Il aime particulièrement que les femmes nues soient pénétrées par derrière.

-Ils m'excitent, dit-il en rougissant, ils me donnent, ahem, des érections, ahem, très respectueuses.

De plus, bien que ce ne soit que très rarement, lorsque sa femme quitte la ville et qu'il se sent célibataire, libre et un peu triste, il engage une prostituée. La belle, très jeune, aux yeux verts, Elena.

-Un adolescent exquis, -souriez notre héros.

Il sait que la moitié de ce qu'Elena gagne est donnée à son mac et l'autre moitié est versée au mac pour la triste chambre d'hôtel où il vit.

-Mais je la respecte", dit-il, "et je n'interfère pas avec ses conditions de travail. Je lui parle, quand je lui parle de quelque chose, de la poésie. Je lui ai donné les Versets du capitaine de Pablo Neruda.

Ce 9 mars, sa femme et sa fille vont se joindre à la grève nationale des femmes. "Il tombera, il tombera, le patriarcat tombera", lui a chanté sa fille en le regardant droit dans les yeux et avec une insistance mystérieuse qu'il ne comprend pas.

-Ce sont des femmes privilégiées, car elles ont besoin de moi pour s'occuper d'elles", dit-il, et il lève le menton avec une fierté familiale paternelle. -Et je leur ai dit que je pensais que la grève était stupide et que je pouvais leur donner de meilleures idées. Mais quoi qu'il en soit, je leur ai donné ma permission de participer.

Il ne sait pas, notre héros, qu'il est le patriarcat. La partie structurelle du patriarcat. La jambe droite sur laquelle repose le patriarcat lorsqu'il donne des coups de pied aux femmes avec la gauche.

De son indifférence, de son abstention, de son recul par rapport à la question des femmes ; de son adhésion verbale et de sa totale ignorance de la cause féministe ; de sa participation aux cérémonies d'exclusion des femmes sans séparer ses lèvres ; de son privilège d'être né homme sans cligner des yeux : de là naissent la prostituée esclave, la traite des blanches, la femme battue, les dix assassinés chaque jour dans le pays.

Si elle faisait quelques pas dans la question des femmes, si elle lisait quelques livres sur l'expérience féministe, si, lorsqu'elle voyait un œil au beurre noir sur une femme, elle frappait son mari, si elle suspendait son omertá - son pacte de silence - avec les hommes violents, si elle donnait une bourse à la prostituée au lieu de la pénétrer : wow, si elle incluait les femmes dans le cercle de ses frères humains : le patriarcat tomberait, tomberait et lui briserait la tête sur le sol.


Traduit avec www.DeepL.com/Translator (version gratuite)