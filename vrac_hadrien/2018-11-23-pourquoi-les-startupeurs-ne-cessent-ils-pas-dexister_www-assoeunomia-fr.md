---
title: "Pourquoi les startupeurs ne cessent-ils pas d’exister ?"
url: http://www.assoeunomia.fr/2018/11/23/pourquoi-les-startupeurs-ne-cessent-ils-pas-dexister/
keywords: dexister,sociaux,émancipateur,collectif,startup,peutêtre,vivatech,répandre,imaginaire,réussi,startupeurs,rêve,cessentils
---
![](http://www.assoeunomia.fr/wp-content/uploads/2019/02/Equipe-HEC-Lausanne-Team.jpg)

Il n’y a pas d’alternative à l’esprit start-up
----------------------------------------------

Il y a 2 ans, quand Take Eat Easy a fait faillite, ce sont 400 000 euros qui n’ont pas été payés aux cyclistes. Aujourd’hui, ses co-fondateurs Adrien Roose et Karim Slaoui lèvent 10 millions pour un nouveau concept de start-up écrit sur un coin de table (lire [« La mémoire courte des golden-cowboys »](https://plus.lesoir.be/187230/article/2018-10-29/la-memoire-courte-des-golden-cowboys)).

Il y a 2 mois, Julien Foussard, golden boy en vogue encensé à VivaTech (le grand salon français “consacré à l’innovation technologique et aux start-up”) pour sa startup disruptive (Oyst, permettant d’acheter en un clic), était inquiété par la justice pour [avoir extorqué environ 100 millions d’euros](https://www.lemonde.fr/societe/article/2018/09/17/julien-foussard-le-sulfureux-golden-boy-des-demarches-administratives-en-ligne_5356213_3224.html) à des particuliers français avec une arnaque aux faux sites administratifs, un “service”, pourtant gratuit via le site officiel, qui finissait par vous coûter 15 euros par mois.

Pour pas mal de monde, le délire collectif autour de cette Start-up Nation™ et ses success stories est incompréhensible et totalement détaché du réel, des vrais chantiers à entreprendre en société.

Chaque société a besoin d’un rêve collectif, et nous n’avons pas réussi à imaginer un futur “émancipateur” (par exemple des liens sociaux plus riches, grâce à une économie et un urbanisme alternatifs, une solidarité réduisant la souffrance…).

Mais nos liens sociaux sont détruits. Le bonheur ne peut être désormais qu’individuel, jamais collectif et tissé d’amitiés fortes, d’amours, de famille(s) (au sens large de ce que pourrait être une “famille”), voire même (!) de sentiment d’appartenance à une communauté, car immédiatement “communautariste”.

Du coup nous sommes bloqué·es avec ce qu’il reste : un imaginaire “par défaut”, où on ne bouillonne pas de rage face aux injustices que l’on observe et que l’on ressent. Ainsi, il nous raconte que le monde est peut-être un endroit injuste, cruel et froid, qui ne fera certainement qu’empirer jusqu’à son effondrement, mais où au moins, peut-être, un jour, “au moins nous”, “au moins moi et mes proches”, on réussira à triompher, des autres, de ses voisin·es, de ses difficultés personnelles : la preuve, d’autres auraient pourtant réussi “à partir de rien” à “monter un business” et enfin vivre la belle vie.  
Enfin ne plus avoir à se soucier de sa misère et de celle des autres. “Et si je n’y arrive pas, je n’ai qu’à m’en prendre à ma stupidité ou mon manque de débrouillardise.”

![](http://www.assoeunomia.fr/wp-content/uploads/2019/02/innov_award_winners_cover_news-927x510.jpg)Deuxième à gauche, Julien Foussard à VivaTech donc (Bernard Arnault à sa droite)

Culpabiliser sans distinction toutes les personnes pensant ainsi serait un peu à côté de la plaque : c’est un lieu commun de dire que les gens qui vivent plutôt bien la situation socio-économique actuelle ont un intérêt intrinsèque, si ce n’est à répandre activement, au moins à laisser se répandre cette pensée individualiste.  
A cela s’ajoute le mécanisme de dissonance cognitive : il est tellement coûteux émotionnellement de s’avouer à soi-même (et d’en tirer les conséquences dans sa vie personnelle) que la réussite individuelle de tou-tes est une supercherie, et que ce “rêve” permet à une violence terrible de perdurer. Paradoxalement, il est peut-être plus “naturel”, en tout cas compréhensible, de se morfondre tout·e seul·e sur ses propres échecs “individuels”, quand leur cause n’est pas si individuelle que ça.

Quand on parle de créer un nouvel imaginaire émancipateur, il faut donc prendre la tâche au sérieux, ce qui signifie aussi ne pas fabriquer cet imaginaire à seule destination des milieux où les gens ont déjà réveillé leur soif de nouveaux imaginaires. Si quelqu’un aujourd’hui passif (et déprimé) face à ce grand récit de la start-up salvatrice se voyait suggérer un autre imaginaire collectif, appropriable et investissable construit avec des références culturelles et des mots qui lui parlent, pas sûr que cette personne resterait à se morfondre avec l’autre imaginaire, qui de toute façon ne lui a jamais été favorable.
