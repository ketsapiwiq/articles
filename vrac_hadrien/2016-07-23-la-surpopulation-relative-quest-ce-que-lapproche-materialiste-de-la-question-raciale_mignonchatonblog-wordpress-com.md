---
title: "La surpopulation relative – Qu’est ce que l’approche matérialiste de la question raciale ?"
url: https://mignonchatonblog.wordpress.com/2016/07/23/la-surpopulation-relative-quest-ce-que-lapproche-materialiste-de-la-question-raciale/
keywords: surpopulation,sagit,cest,réserve,quest,question,dune,discours,raciale,quil,lapproche,social,matérialiste,travail
---
![5a8c6b99a50dc742c8581716cae57c62](https://mignonchatonblog.files.wordpress.com/2016/07/5a8c6b99a50dc742c8581716cae57c62.jpg?w=325&h=432)

Dans ce blog nous allons notamment proposer d’analyser le racisme dans son intrication avec la question de classe, c’est à dire que nous allons étudier la racialisation de la force de travail. Cela ne veut pas dire que le racisme se résume à cela, mais que l’analyse du racisme doit réserver à ce champ d’études une place spécifique.  
Il y a plusieurs façons d’étudier le lien entre la racialisation et le monde du travail, qui feront l’objet d’articles particuliers, notamment du point de vue sociologique. Du point de vue de la critique de l’économie politique il existe une approche qui part de la question de la surpopulation relative. C’est cette approche que nous nous proposons de survoler aujourd’hui.  
Pour aborder cette question, donnons donc déjà une brève définition de ce que l’on appelle la surpopulation relative avant d’aborder son rapport avec la racialisation.

Très simplement, la surpopulation relative \[1\] désigne la fraction du prolétariat qui n’occupe pas effectivement un emploi à un moment donné.

On l’appelle surpopulation parce qu’elle apparaît comme étant en trop, et relative parce qu’elle apparaît en trop relativement aux besoins de force de travail par le capitalisme à un moment donné, et non pas dans l’absolu comme effet inévitable du progrès.

On divise la surpopulation relative en plusieurs sous- catégories.

L’armée industrielle de réserve : elle est formée de l’ensemble des travailleurs qui constituent une réserve de force de travail qu’il est nécessaire de maintenir à disposition pour permettre une flexibilité dans l’organisation globale de la force de travail.

Cette armée de réserve se divise elle- même en trois sous-catégories : les précaires qui oscillent constamment entre période d’emploi (CDD, intérim) et chômage mais qui sont le plus souvent en période de travail (on appelle cette sous catégories la surpopulation flottante) ; les chômeurs de plus ou moins longue durée qui peuvent immédiatement être employés par le capital dès que le besoin s’en fait sentir (on appelle cette sous-catégorie la surpopulation latente) ; et enfin les travailleurs qui n’ont quasiment plus de chance de trouver un emploi (âge, manque de qualification, maladie, handicap etc.) et qui vivent dans un état de grande pauvreté (on appelle cette sous-catégorie la surpopulation stagnante).

Pour résumer : précaires, chômeurs de courte durée et chômeurs de longue durée sont donc les trois sous catégories (flottante, latente et stagnante) de l’armée industrielle de réserve.

Reste encore une dernière catégorie aux marges de l’armée industrielle de réserve que l’on appelle les lumpen ou encore les exclus de nos jours, et qui ne font presque même plus partie de l’armée industrielle de réserve, et sont donc quasiment exclus de l’emploi salarié.

Il s’agit des malades chroniques, infirmes et estropiés graves, trop déqualifiés, trop âges, casier judiciaire trop chargé, psychiatrisés graves, marginaux condamnés à l’économie parallèle etc.

On voit que toutes ces catégories ne sont pas étanches entre elles, elles sont poreuses et forment un continuum.  
L’idée globale est qu’il s’agit d’une graduation progressive des différents sans- emplois condamnés de la précarité temporaires à la marginalité pure et simple en passant par le chômage de plus ou moins longue durée.

L’hypothèse de la dite analyse matérialiste de la question raciale \[2\] est de lier la racialisation avec la surpopulation relative, c’est à dire qu’il s’agit d’affirmer que ce sont majoritairement les prolétaires racisés qui composent le gros de la surpopulation relative, et ce d’autant plus que l’on descend dans les couches paupérisées du prolétariat.  
Il s’agit d’une hypothèse née d’une observation empirique assez facile à réaliser, de très nombreuses études de sociologie, fastidieuses à toutes énumérer (ce sera sans doute fait au fur et à mesure sur ce blog) existent sur le sujet.

En outre quiconque se promène dans un quartier pauvre peut en constater la flagrante coloration ethnique. L’observation et l’hypothèse sont donc facilement réalisables. Ce qui manque encore cependant c’est que l’on en fasse une théorisation scientifique à proprement parler.  
On parlera donc d’hypothèse pour le moment, hypothèse qui ouvre un champ d’études et d’analyses encore à faire.  
Pour en finir, passons maintenant à une revue très brève des fonctions a priori intéressantes dans la racialisation des classes populaires, qui expliquent que la question des surnuméraires soit à prendre en considération.

Avant de passer à la suite il faut être bien clair sur un point : une domination structurelle ne résulte pas d’un complot des classes dominantes, ou d’une stratégie consciemment organisée. Il s’agit simplement d’un processus historique par lequel une forme de stratégie globale fonctionne et l’emporte par rapport à d’autres parce que c’est celle qui suscite le moins de résistances.

Cela posé, le marquage racial d’une sous- catégorie particulièrement paupérisée du prolétariat a notamment trois fonctions intéressantes pour le capitalisme.

1 – La ségrégation dans le monde du travail \[3\], notamment par la discrimination à l’embauche, permet de reproduire facilement, sur un critère simple, cette fraction spécifique du prolétariat que sont les surnuméraires.

2 – La ségrégation urbaine permet de parquer cette fraction de la population dans des zones de regroupement spécifiques et de les livrer à des formes de contrôle social elles aussi spécifiques, facilitées par le marquage racial au faciès.

3 – Les discours racistes et sécuritaires, quant à eux, apportent la justification idéologique à ces formes de contrôle social, tout en déplaçant le cadre du débat hors de l’analyse critique en termes sociologiques et économiques. On passe ainsi à des faux débats mobilisant des catégories identitaires nationales, raciales ou pénale.

Puisque nous en parlons ici, arrêtons- nous un instant sur cette question : on peut distinguer trois grands types de faux débats sur le sujet :

1 – Le discours humaniste ou social démocrate, qui pense la question de l’exclusion sociale de façon non- structurelle, c’est-à-dire comme un accident plutôt que comme une production. Sauf que l’exclusion n’est pas un accident social qui se produirait, comme par miracle, à grande échelle et sur une grande durée de temps. Si le chômage est **de masse** et qu’il dure en présentant des avantages objectifs aux capitalistes on peut dès lors former l’hypothèse qu’il s’agit d’une forme spécifique d’organisation globale de la force de travail dans les centres historiques d’accumulation du capital depuis la restructuration capitaliste des années 70.

2 – Le discours sécuritaire \[4\], commun à la droite et à la gauche, qui transforme tout prolétaire, et surtout s’il est racisé, en délinquant potentiel pour les raisons évoquées plus haut.

3 – Le discours raciste pur et simple, qui s’attache au marquage raciale de fait de cette partie du prolétariat pour ramener le débat à une question raciale, d’immigration, d’intégration, de culture, de religion etc.

Ces trois types de discours ne s’excluent pas mutuellement, et ne sont pas non plus étanches entre eux, encore une fois il s’agit d’un continuum, continuum qui va du social au racial en passant par le pénal.

Pour aller plus loin sur le sujet nous renvoyons aux ouvrages suivants :

**Mathieu Rigouste**. L’ennemi intérieur. (Editions La Découverte)

**Mathieu Rigouste**. La domination policière. (Editions La Fabrique)

[FICHIER PDF](http://pdf.lu/F22i)

**———-**

**NOTES**

———-

\[1\] **Karl Marx**. Le Capital. Livre I. Chapitre XXIII. Section 3.

\[2\] http://www.vacarme.org/article2778.html

\[3\] **Lire :**  
<http://www.inegalites.fr/spip.php?page=article&id_article=1099>  
<http://www.inegalites.fr/spip.php?page=comprendre_analyses&id_article=824&id_rubrique=110&id_mot=25&id_groupe=17>  
<http://www.inegalites.fr/spip.php?article670>  
**Séries d’articles sur le même sujet :  
**<http://www.inegalites.fr/spip.php?page=rubrique&id_groupe=11&id_rubrique=3>

\[4\] **Mathieu Rigouste**. Les marchands de peur. (Editions Libertalia). 2013.

Publicités

### Partager :

-   [](https://mignonchatonblog.wordpress.com/2016/07/23/la-surpopulation-relative-quest-ce-que-lapproche-materialiste-de-la-question-raciale/?share=twitter "Partager sur Twitter")

    Twitter

-   [](https://mignonchatonblog.wordpress.com/2016/07/23/la-surpopulation-relative-quest-ce-que-lapproche-materialiste-de-la-question-raciale/?share=facebook "Cliquer pour partager sur Facebook")

    Facebook

-   

### WordPress:

J'aime

chargement…

### Sur le même thème
