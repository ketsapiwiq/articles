---
title: "The Case Against the Media, by the Media"
url: http://nym.ag/29WqGPU
keywords: case,really,trump,say,coverage,media,thats,think,theres,going,interview
---
The Case Against the Media.

By the Media.

For decades, the pollsters at Gallup have been asking Americans if they trust their media. In 1974, the year Woodward and Bernstein brought an end to Richard Nixon’s presidency*,* 69 percent of them did. In a poll released last year, that number was at a historic low. Today, the only institutions Americans have less faith in than television news (21 percent) and newspapers (20 percent) are Congress and “big business.” That’s pretty damn low — humiliatingly low, especially for a group of people who fancy themselves members of “the Fourth Estate.” (For those of you who have never worked in the media, which is basically the only place anyone would still use that 18th-century term: It refers to the power of the press to balance that of the three estates — clergy, aristocracy, and the well-to-do — in the British Parliament.) The other three estates don’t really exist in 21st-century America, but the fourth’s high opinion of its role in the body politic has been pretty constant.

The same is also true of the public’s disdain, the Watergate era being something of an exception (“The press tyrannizes over publick men, letters, the arts, the stage, and even over private life,” James Fenimore Cooper wrote in 1838). People love to shoot the messenger, and these days especially, in an era of proverbial cable-news shoutfests and clickbait journalism, the messenger probably hasn’t been doing itself many favors. But the presidential candidacy of Donald Trump has seemed to usher us into a whole new season of media loathing. And self-loathing: A candidate who built [his campaign](http://nymag.com/daily/intelligencer/2016/04/inside-the-donald-trump-presidential-campaign.html) in part on attacking, mocking, belittling, and dismissing the press has inspired a massive wave of journalistic self-recrimination. On the one hand, the media (and let’s just pause to acknowledge that throughout this project we’re committing a cardinal media sin of conflating for convenience, since the “media” we’re describing lumps in the New York *Times,* TMZ, reporters, pundits, 24-hour cable news, and ourselves — vastly different entities, but ones seen, or described, by the public as a monolith) have been accused of underestimating Trump’s popularity; on the other, of fueling his rise by showering him with attention in a cheap ploy to boost ratings — while also failing to scrutinize his record and misstatements and standing at the center of a rigged system now bent on bringing him down. That last criticism is probably Trump’s personal favorite — the most mendacious presidential candidate in history constantly accusing the media of not telling the truth about him. Even [Melania Trump’s plagiarized speech](http://nymag.com/daily/intelligencer/2016/07/melania-plagiarized-obama.html) was spun by some in the Trump camp as an example of biased-media overkill (the subject of stolen words being of inherent interest to journalists). Here at *New York* Magazine we’re guilty, too, of course. Our audience has (always, actually) had an enormous appetite for Trump, which we’ve diligently fed, while not always taking him seriously. In October, we passed on the chance to publish Steven Brill’s investigation into Trump University, before *[Time](http://time.com/4101290/what-the-legal-battle-over-trump-university-reveals-about-its-founder/)* published it. Why? Because we thought, wrongly, that Trump was fading and that the story had been told. Also, we were, frankly, a little bored with him. We miscalculated that our readers, and the country, would soon be, too.

But it’s not just Trump; dire signs for media were everywhere this year. The story line around [the Gawker trial](http://nymag.com/selectall/2016/03/how-media-reacted-to-gawkers-115-million-hit.html), in which the web empire was sued for publishing a Hulk Hogan sex tape — a suit later revealed to have been [funded by billionaire Peter Thiel](http://nymag.com/selectall/2016/05/how-peter-thiel-was-unmasked-as-hulk-hogans-secret-backer.html), seeking revenge over his own coverage — turned the site from monster to martyr but made the press look terrible from every angle. [Jennifer Aniston blasted the celebrity media](http://nymag.com/thecut/2016/07/huffington-post-blogger-jen-aniston-is-fed-up.html) for essentially shaming her for not having children. The press was castigated by the left for various racist and sexist offenses, and by the right for being too far to the left. And with every successive police shooting of a black man, the question was raised — why hadn’t the media been paying proper attention to the underlying problems before social media forced its hand? (And how much of that inattention had to do with how overwhelmingly white so many media organizations — including ours — still are?)

In fact, social media was forcing our hand on all of these points at once, making journalists confront, out in the open, the possibility that their work might not be any of the things they imagined it was — objective, rigorous, informative. Instead, we found we often looked partisan, mendacious, lazy, sloppy, and shrill. Conservative commentators slammed Facebook, which has, rather suddenly, become the single biggest force in media, for liberal bias, while the media itself had more existential worries, like the way Facebook was swallowing it whole. Among other things, Facebook drove a stake through the contradiction that had always been at the heart of most media: The news pretended (and yearned) to be a public trust when it was really just a consumer good, now more nakedly exposed to the marketplace.

As the media anxiety around media anxiety began to crescendo this spring, we at *New York* decided to turn our journalistic operation in on itself to investigate just how bad the media really is. We were less interested in bad actors â€” the Jayson Blairs and such â€” than in the structural dilemmas of the media trade: What keeps media people up at night when theyâ€™re thinking about what they do for a living? We began by asking ourselves and our peers what they think the mediaâ€™s greatest faults are. The response was overwhelming but probably shouldnâ€™t have been too surprising (the media loves to criticize the media). In interviews with more than 40 journalists and media figures and in [a survey of 113 of our peers](http://nymag.com/daily/intelligencer/2016/07/media-survey.html), we heard much about deals cut with anonymous sources, the pressure for speed and easy hits that squeezes the nuance out of complicated stories, editors who knowingly simplified stories past the point of accuracy and publishers who spent resources on subjects they believed were trivial rather than those they felt were important. At times, the surveyâ€™s answers read like the minutes from an anonymous group-therapy session.

The interviews that follow make the media look pretty bad â€” really bad, in fact. But as it happens, weâ€™ve engaged in our own form of media distortion in this project. Weâ€™ve dwelled mostly on the negative, because thatâ€™s what we considered the â€œnews.â€? Yet many we spoke to gave reasons for optimism, too. Some hailed the rise of the internet and social media for bringing new voices to the fore and for connecting established ones to new audiences. Several observed that the Trump effect was salutary for media credibility, as journalists learned to become more aggressive in fact-checking falsehoods mid-quote or mid-chyron. Some pointed to the rise of nonprofit investigative shops like [ProPublica](https://www.propublica.org/) or [the Intercept](https://theintercept.com/) as happy developments, and others highlighted a new generation of benevolent media owners, [like Jeff Bezos](http://nymag.com/daily/intelligencer/2016/06/washington-post-jeff-bezos-donald-trump.html), as a sign that good journalism will still be produced. Weâ€™ve included links below to longer transcripts of our interviews; thatâ€™s so you can see for yourselves how weâ€™ve cherry-picked the quotes â€” and also because reading each of the commentators at length makes for almost an entirely different (and much deeper) exploration of the subject.

Most strikingly, the journalists we spoke to responded to the biggest criticism by embracing it: Yes, the media imposes what might be called its own bias, but thatâ€™s its job. Itâ€™s supposed to function as a filter, making complex events intelligible, and maybe even supposed to entertain, too. And itâ€™s also true that while itâ€™s easy for people to loathe the media as an abstraction, â€œif you ask them how they feel about their own particular media niche, you often find out that they are very loyal to it,â€? pointed out Bill Keller, who used to run the New York *Times* and now runs the excellent [Marshall Project](https://www.themarshallproject.org/). â€œThe people who read the *Times* might be the same people who would tell a pollster that the media is unreliable, but theyâ€™re deeply devoted to the New York *Times*.â€?

Itâ€™s not just the *Times.* The same seems true across media â€” or true-ish, in an era of shrinking TV ratings and rabid competition among print outlets. In the media market, like any market, that loyalty is earned, one presumes, by giving an audience what it wants. But what does it want? Is the media there to excite or to inform? To challenge or to reassure? To be objective or partisan? To try to parse the media-hate, as we have over the last few months, is to sense that the media still does at least one job very well: reflecting the contradictory desires of the audience it serves.

1.

News is an entertainment business, even if it pretends otherwise.

“You can’t have drama without conflict. And you can’t have melodrama without good guys and bad guys. The problem for journalism is: Our actual problems are bigger, more complicated, more sprawling and complex, than good guys and bad guys. I don’t take any issue with the press attending to conflict. That’s Job One, actually. But the simplicity of the narrative is incredibly debilitating. News organizations chase simple narratives, and if they are prize-hunting, they look for an evil actor. Such folks can be found, to be sure, and a scandal is a scandal. But it is mortifying to realize that much of the press thinks exposing the overt scandal is the equivalent of examining, assessing, and arguing for systemic solutions to systemic problems.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/david-simon-problem-with-media.html)\]

2.

So it doesnâ€™t know how to handle serious issues.

“When the only thing that’s consumed is the one-day tweet and the listicle and the ‘Seven Awesome Slams From the Obergefell Dissent,’ then there really isn’t time to do the piece that we need to do, which is, ‘Hey, I read all 80 pages of the opinion, and here’s what it means, and here’s why it matters, and here’s what the district court has to do going forward’ — all of that disappears. I had colleagues who, a day after reading *King* v. *Burwell,* the big Affordable Care Act case, when they had time to file a really deep-dive, thoughtful story, they were told, ‘It’s all played out. We had this conversation.’ ”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/dahlia-lithwick-problem-with-media.html)\]

“Can I add, ‘Blah, blah, fucking blah’? I like Dahlia Lithwick, I’m not going to rag on her, but you hear that a lot from reporters — ‘There’s so much meat out there, and we only cover the applesauce.’ It’s like, ‘Okay, but isn’t your job to explain clearly and in a compelling manner what’s important?’ It does seem like that’s inherent in the job description. This is the lecture I give to my reporters: Nobody has time to read. People barely *can* read. So you need to give them an overwhelming reason to read your piece. You need to grab them by the face and pull them in, and cover them in the hot stinky garlic breath of journalism until they fuckin’ wake up.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/tucker-carlson-problem-with-media.html)\]

3.

Gets addicted to conflict.

"I think one of the problems of the media, both on the left and on the right, is that it tends to want staged battles. I mean, it really is like professional wrestling. If a media figure, a writer for example, goes on television or radio, there’s a tendency for producers to expect that person to present a party line, and to present a contrast with other guests that will be best for — again, a staged fight, a sort of professional wrestling match. And not actually have views that will be orthogonal to what’s expected, and create a more interesting and perhaps more informative discussion. You saw this during the Iraq War, for example. There was a tendency for conservatives who were actually very critical of the war for conservative reasons, but who were often expected to just be defending the George W. Bush line. That’s a stark example, but this doesn’t just apply to things like the Iraq War.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/daniel-mccarthy-problem-with-media.html)\]

“Talking heads, screaming, fighting, and some of the *faux*-journalism practiced by people like O’Reilly — there’s a cynicism to that kind of coverage. CNN’s hiring Donald Trump’s former campaign manager as a commentator looks cynical to the viewer or the reader. It looks not serious, and it fuzzies up the definition of journalism. And that has done some damage to all of us.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/dean-baquet-problem-with-media.html)\]

“You want to hear the worst thing I ever did on TV? During the Clinton impeachment, I was running Court TV. They asked me to be on *Crossfire* to talk about whether the deliberations in the Senate should be open to cameras. So I decided, ‘Well, obviously it should be open to cameras.’ In the car going from my office over to the CNN studio, I’m thinking about it a little bit —Â *You know, I’m not so sure about this.* I get on the show, and opposite me is Arlen Specter. Specter’s arguing that the deliberations should be closed to cameras. And he goes first. I’m listening to him, and they turn to me, and whoever it was says, ‘Well, Mr. Brill, what do you think?’ And I said, ‘You know, I think he’s right.’ The producer’s screaming in my ear, ‘You can’t say that! You can’t do that!’ ”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/steven-brill-problem-with-media.html)\]

4.

Loves simple heroes.

“I think people like these ‘Genius comes in to save the day’ narratives, as if it’s that simple. And then when it doesn’t turn out, they’re like ‘Genius didn’t save the day, she’s a goat.’ Look at Theranos: There was one profile after another of Elizabeth Holmes that were just like, ‘Isn’t she fantastic?’ Nobody looked at the actual product. It took just one really great health-care reporter to say, ‘Hey, does this product work?’ ”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/kara-swisher-problem-with-media.html)\]

5.

And simple villains.

“Look at the Syria conflict. For several years, it was all Assad, Assad, Assad. ‘He’s a demon, he’s the devil, he’s committing genocide. He’s bombing — we should intervene.’ And then all of a sudden two Americans get their heads cut off on the internet and it switches. It’s ISIS is the bad guy. ISIS is the one we need to attack. It becomes a totally different thing. It’s no wonder people are confused. And then there’s a refugee crisis, and everyone’s thinking, ‘Well, these people are fleeing ISIS.’ Well, no, they’re not, they’re fleeing the Assad regime, which was our old enemy. It’s whiplash. But I can sympathize. When it comes to the Assad regime, that’s a devil we know — Assad is a dictator, he’s trying to preserve his power, and he’s killing a lot of innocent people to do so. That’s terrible, and that’s worthy of our coverage. But that’s not something that the world hasn’t seen before. ISIS is a different animal. They’re almost cartoonishly evil. There’s a reason people are still reading books about the Nazis. They were freaks, and they were so good at being bad that people want to hear about them. Among my editors, there was an enormous appetite for any story that would bring us inside ISIS. They’re a spectacle — it’s a circus show. You can’t take your eyes away. So of course the press propagates their message for free.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/matt-bradley-problem-with-media.html)\]

6.

Reduces complexity to comfortable narratives.

“When I went to Moscow, there was some resistance to the idea that Gorbachev should be taken seriously. The narrative about the Soviet Union was it’s an unchangeable monolith and anybody who came to power in the Soviet Union, professing to want to change things, was either faking it or delusional. Because it couldn’t change. I remember in the early days sometimes having trouble persuading editors that, yes, something is really happening here. So that’s not a novelty of the digital age.

The irony of the Gorbachev years, once people bought the idea that he was for real, was he became this celebrity magician. He came to the U.S. and people lined up in the streets to watch him. For a lot of people, the wisdom about Gorbachev moved from incredulous to overly credulous.

That experience definitely shaped me when I was an editor. We tried, for example, to be very careful about celebrating the Arab Spring as the true arrival of democracy, because the reporters who were out there were, yes, caught up in the euphoria of the crowds, but they were also saying, “It’s not that simple … don’t break out the Champagne and the party hats yet.”

But reporters aren’t immune either. There’s something genuinely inspirational about seeing people who’ve never had a chance to vote for anything line up for miles to cast their first vote. It’s powerfully moving stuff. But in neither Russia nor South Africa did it mean that the sun was coming out.Â  There was a lot of stuff written about how the fall of Qaddafi was the end of a problem. And potentially the birth of a new, more benign time. It turned out to be not at all true.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/bill-keller-problem-with-media.html)\]

7.

And is desperate to be respected, which produces blindness.

“I think that a lot of coverage decisions that get made are often made subconsciously — most journalists think they don’t actually make decisions about what’s newsworthy and what isn’t, that their media outlets cover anything that’s newsworthy. And this is plainly not the case — there’s huge numbers of obviously newsworthy stories that are routinely, systematically ignored by large media outlets. One major pattern is that the political media in particular views everything through a partisan lens. Journalists want to be respected by their colleagues and they want to be mainstream, and in American political discourse, the mainstream figures are the Establishment heads of both political parties. So if there’s some sort of dispute between the two parties, where the Democrats think one thing and the Republicans think another, that tends to get covered, because that’s viewed as an important political debate. But on the issues where there’s bipartisan consensus, where the two parties essentially agree, which is far more common than disagreeing, those tend to get completely ignored. So you look at U.S. support for Israel, or the idea that the U.S. should have the largest military in the world, or that we should continue with our state of mass incarceration, or just the general neoliberal economic policies that both parties believe in and support, those tend to be completely excluded from any kind of media discussion or coverage. Another example is the NSA stuff. For years before we were able to get the [Snowden](http://nymag.com/daily/intelligencer/2016/06/edward-snowden-life-as-a-robot.html) archive, there was a lot of indication that the U.S. government was engaged in mass surveillance. But because there was no disagreement between Democrats and Republicans on surveillance, the NSA got this huge boost after 9/11 under Bush, and then expanded even further under Obama. It was only because we got this huge mother lode of documents that it finally becomes something that people spoke about.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/glenn-greenwald-problem-with-media.html)\]

8.

Journalists are easily bored â€¦

“You’re seeing almost no stories about the Garland vacancy. I do think the sort of depressing id answer as to why is that it’s just not interesting to people. If Merrick Garland had been an African-American, out lesbian, whatever, this would have been a story, but we have a 63-year-old, eminently qualified white guy. \[Mitch\] McConnell and company are saying, ‘Eh, you could have eight justices or nine justices. It doesn’t really matter.’ And we’re kind of reporting as if that’s true. That plus the fact that the work of the court is so mystified and invisible that people can’t figure out why it would matter if the court was shorthanded — all of this dovetails so neatly into the narrative that this can’t be *that* big a deal because nobody’s writing about it, and nobody’s writing about it, so it doesn’t seem like that big a deal.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/dahlia-lithwick-problem-with-media.html)\]

“You wake up in the middle of the night and you think about the mom who wanted you to write about the little baby with cancer and your editor said, ‘Well, that’s not really news, because lots of kids have cancer. Is this a really rare cancer?’ ‘No.’ ‘Well, it’s not a story then.’ ”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/sheila-hagar-problem-with-media.html)\]

9.

â€¦ Especially by good news.

“Barack Obama was elected with 53 percent of the vote in 2008, then reelected with 51 percent four years later, and yet for the vast majority of his time in office, his approval rate has stood below that level. Why? Not because he failed to deliver the change he promised. On the contrary, Obama has carried out his campaign promises to reform health care, education, phase in green energy, wind down wars in Iraq and Afghanistan, and redirect attention to crushing Al Qaeda.

But news operates on very short time horizons, while Obama obsesses over long ones. There are two kinds of major news stories involving presidents. One is political conflict, the other is disaster and scandal. What both these kinds of stories have in common is that, almost by definition, they are news only if they’re bad for the president. The story is a conflict or a problem. If a bill gets passed into law, or the oil spill gets patched, or it turns out the IRS was not targeting Republicans at all, then the resolution is a one-day story, and it’s on to the next thing, which will also be bad. The opposite of a bad story isn’t a good story, it’s a nonstory.

There is one kind of story that allows a president to escape the dynamic: if the disaster or conflict pits the United States against an external threat. But here, too, the effect paradoxically punishes success. In the wake of 9/11, Americans rallied around George W. Bush, and his approval ratings stayed high for two years after. Obama’s numbers surged in the wake of killing Osama bin Laden too, but for a much briefer period — precisely because the threat was eliminated. In Obama’s final year, his approval ratings have finally floated back up into the 50s, mostly because the presidential campaign has displaced him from the center of conflict. The same dynamic had once made Hillary Clinton wildly popular during her term as secretary of State — avoiding the daily grind of political conflict is the functional media equivalent of achieving world-historical greatness. Political news in polarized America is like Global Thermonuclear War in *WarGames.* The only way to win the game is not to play.” Â 

### On Facebook, users really love to hate what they read.

We asked [Naytev](http://www.naytev.com), a leading content optimization platform and [Facebook partner](https://www.facebook.com/business/success/funny-or-die), to test two different versions of our headline with Facebook users.

clicked the positive headline:

â€œThe Media Really Is Fantastic.â€?

clicked the negative headline:

â€œThe Media Really Is Terrible.â€?

10.

Unfortunately, so are readers, who are hard-wired for panic.

“There’s a famous scene from *South Park* that aired shortly after 9/11 in which Stan’s mom, Sharon, is lying fully horizontal under a blue blanket on her couch, exhausted, thoroughly incapacitated, powerless to stop watching cable-news coverage of the attacks’ aftermath. ‘Another high-alert status for terrorist activity this weekend,’ the CNN anchor reads. ‘The government said bad things are likely to happen.’ Sharon is so far gone into her own terror she can’t respond to the doorbell, to her family, to anything, other than by mumbling helplessly. Her eyes are locked wide open in terror.

We’ve all been Sharon Marsh on the couch. And there’s a reason that whatever the world’s actual trajectory, there will always be disproportionate coverage of threats to health and well being. We are wired to be finely attuned to negativity. Throughout our evolutionary history, it’s been beneficial to sense danger quickly. There’s a degree of artifice and oversimplification to any evolutionary-psychology argument, but to an important approximation, it was those who sensed the saber-toothed lion rustling in the bushes first who got a jump on surviving its attack; it was those most adept at discerning the social tide of the clan turning against them who would escape the stoning. These nervous Nellies got to survive, reproduce, and pass on their worrying gene, while the blithe were more likely to live fast and die young.

This can partially explain why we will always be more likely to share the story online about the earthquake that killed a few hundred people in a distant corner of the world than the one about a small decrease in infant mortality somewhere that reflects a much larger quantity of saved lives. Millennia of evolution, of vigilantly looking out for threatening pieces of information and passing it on to our kin and our friends, sometimes in life-and-death situations, are responsible for this obsession.

You want it, in other words. You’re trapped wanting it, cowering under that blanket as footage of the wreckage loops for the hundredth time, but you want it nonetheless.”Â 

11.

Which editors, producers, and publishers know.

â€œThereâ€™s no more powerful force in human history than tribalism, and thereâ€™s nothing that gets people to band together more than a perceived common enemy. If you can generate a story that someone will say, â€˜This is an outrage, I must tell my friends about this,â€™ then youâ€™re going to do well. Okay, whatâ€™s an outrage story? I know what that is, letâ€™s find someone whoâ€™s done something bad and skewer him. The opposite of outrage is hate, where the target becomes not the person being written about, but the actual author themselves. People share stuff that they actively loathe by saying, â€˜Can you believe this shit?â€™ When the media says, Why are we getting so much criticism and abuse? Well, it's because you are constantly kicking the hornetâ€™s nest to get clicks. Youâ€™re publishing stuff purely for the sake of provoking your readers.â€?

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/david-auerbach-problem-with-media.html)\]

12.

Journalists are deluded â€¦

“It’s a progress narrative. We want to imagine a kind of triumph of reason. If you’re in the business of finding out facts and reporting on things, you still have, especially in the American press, a kind of pseudo-religious commitment to this as the best way of knowing.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/jeff-sharlet-problem-with-the-media.html)\]

13.

More cynical than their readers â€¦

“If you see how the sausage is made in whatever business you are in — whether working at a restaurant or a car dealership or a hospital or a school or a church or wherever — you inevitably become cynical. You know there’s a gap between the public image of your profession and what really goes on when no one is looking. Political journalists see how the sausage is made not only in their own business but in the institutions they cover. It’s a trait that serves them well, because the whole point of political journalism is to ask questions, rude questions in particular, of those in power. It’s always been thus, and if you don’t believe me, go see the revival of *The Front Page* on Broadway this fall. That scabrous 1928 comedy, written by the one-time Chicago newspaper reporters Ben Hecht and Charles MacArthur, remains a potent reminder that sentimentality and charity have never been of any use if you are a news hound who wants to be first with the lowdown on miscreants in high places.”Â 

14.

Rush their work â€¦

“The major feature of the media landscape today is the acceleration of everything. Probably the most troublesome tension is the one between the need to file immediately, because a thousand other people are filing immediately, and the time it takes to do real reporting, to reflect on what you’ve got and then to write it in a way that’s fair and clear but doesn’t gloss over the complications. And that tension just seems to get more intense. I think one of the greatest casualties of the high metabolism of the news business is complexity. That’s a big loss.”

Â \[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/bill-keller-problem-with-media.html)\]

15.

Believe popular opinion is all that matters â€¦

“The willingness to defer to the mob’s opinion on the part of American reporters is really striking. Every time I talk to somebody about Putin, it’s like, ‘But isn’t he vastly popular?’ Is that really the most important question?”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/masha-gessen-problem-with-media.html)\]

“We’re now in a media environment where they will take a poll — any of the news organizations or various organizations that do this — and say, ‘What do you think now? Of the candidates or the controversies? Who do you trust? Who are you going to vote for? Who do you favor?’ And what we’re not saying to people is, ‘We’ve provided you with lots of information, but maybe we haven’t provided you with enough.’ ”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/bob-woodward-problem-with-media.html)\]

16.

â€¦ And are completely comfortable cutting deals.

“Anybody who has been in the position of receiving complaints about coverage at any news organization will tell you this is the No.â€¯1 issue — anonymous sources. Readers believe that anonymous sources are one of two things — it’s either something that’s been made up or someone that’s manipulating the reporter. And certainly the latter is often the case.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/daniel-okrent-problem-with-media.html)\]

“The default presumption has moved increasingly toward a lack of accountability. There’s this notion that everybody’s sort of in a club, where they start off speaking freely, and then they decide whether they will allow any of that to be used. There can be a very good reason why people want to be off the record — sometimes it’s totally legit. But when it’s a spokesperson, they’re paid to defend their people, and they refuse to do it on the record, there’s a reason there, too.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/erik-wemple-problem-with-the-media.html)\]

“\[Hillary Clinton spokesperson\] Philippe Reines and I were on the phone. He was reading me portions of her speech. I said, ‘It sounds pretty muscular.’ And he liked that adjective. He said, ‘You’ve gotta use that.’ And I said, ‘Well, we’ll see.’ And then there was the email follow-up. He was like, ‘Fine, I’ll give you the speech on three conditions, one of which is that you call it muscular.’ Which is a word that I had called it before.

We maintain these sort of popular fictions about how journalism works, and then there’s the reality of how journalism works. And the reality is, often a story is made better by showing sources parts of the story before it goes to publication and saying, ‘Is this right?’ And the source might say, ‘No, that’s not right.’ And then you would say, ‘Okay, what’s not right here?’ And the source will talk more, and give another fact, or give two facts. It’s up to you as to whether or not to include that in the story or not. The practice is ubiquitous among really good journalists. You’re not supposed to explain this stuff either, because journalism, like every other profession, is a fraternity. We like to pretend that every story we write is akin to the movie version of *All the President’s Men.*”

\[[Read the Interview Transcript](marc-ambinder-problem-with-media.html)\]

17.

And itâ€™s not just in politics.

“There is a coziness in the business, and you can’t help it. Some reporter wrote about me — ‘Russell Crowe, on the way into the Oscars, gave Jeanne Wolf a kiss! Is she a professional or what?!’ Russell Crowe is not my best friend. I’ve interviewed him a lot of times, and he said to me later, ‘I had to give you a kiss for good luck.’ If that makes me a bad reporter, too bad. I do not lose my credentials because I’ve known people for a long time, and I’m happy when things are good for them and sad when they either die or are sick.

And I do know a lot of people, and so there’s a mutual appreciation, or a kind of friendship. People say, ‘Be careful, those stars are never your best friends.’ It’s a *kind* of friendship. But people who are new, who haven’t learned how to ask a question or handle a situation, they can feel confused or left out. As a reporter, you have to decide, number one, I’d better make sure it’s true. Who’s whispering to me, and why are they doing it? Promotion — it’s how you get to people. It’s sort of a trade-off. A deal’s got to be good for everybody. If I do a good interview with this person, it may be that it helps them sell movie tickets or get their next part. It’s a trade-off: I need them, they need me.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/jeanne-wolf-problem-with-media.html)\]

18.

Often journalists think they know the story before they report it.

“When I started producing, you would start reporting from a position of ‘I don’t know.’ You would love someone who told you the untold story — the unknown story. That’s a been a big shift. Now, it’s ‘We’re doing a story on deadbeat dads, so here’s what I need you to say. Are you going to say this? And guess what, if you’re not going to say this, then I’m going to move on and find someone else who will say this.’ ”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/soledad-obrien-problem-with-media.html)\]

19.

And are no longer protected from market forces.

“Lurid headlines, fighting, always attracts more attention. The question is, How fortified is the press to be responsible? And it’s just in a macro way increasingly less fortified. When CBS, ABC, and NBC were an oligopoly doing the news at 6:30 at night and had 91 percent of all the televisions in the country between them, there was no way that someone was going to say, ‘Look at these ratings for the missing airplane, let’s do the first 15 minutes with the missing airplane.’ It didn’t have to; they were subsidized by the fact that the network was rich because it was an oligopoly. And so if NBC beat them by a little bit, or lost by a little bit, it didn’t matter.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/steven-brill-problem-with-media.html)\]

“Ratings matter even more now in a far more competitive television marketplace, and you don’t get ratings if your programming is not entertaining. As Don Hewitt, the creator of *60 Minutes,* was fond of saying in his later years, his show would have been killed in its infancy by any contemporary television network because its Nielsen numbers were lackluster for several seasons before it finally caught on with the public. Once *60 Minutes* did become a hit, Hewitt was assiduous in preserving its longevity in part by amping up show-business and celebrity pieces along with the harder-hitting segments.

At this late date, to rail against the merging of entertainment and news on television is a waste of time. It’s too late to rejigger that DNA now. And there may not be any reason to, given that the form itself is on a path to extinction. In 1963, America was glued to the three broadcast networks’ coverage of the Kennedy assassination — it was the first time television covered a news event 24/7 for days on end, and became the template for the cable news format that was still years in the future. But when all hell broke out again in Dallas in 2016, most Americans did not turn to the anchors of CNN, MSNBC and Fox News for the latest, no matter how entertaining their coverage may or may not have been. Those who wanted the latest from Dallas turned to their phones.”

20.

The media is also clueless about its audience (and country).

“The real problem with journalism is groupthink. My father was a journalist — he never graduated from high school, he joined the Marines as a 17-year-old and then went to work at the L.A. *Times.* It was not a profession; it was a trade, and you had a whole diverse field of people entering it. Now, for a bunch of reasons — and this is the problem with American society more broadly, in my view — it’s just a masturbatorium, filled with people who think exactly the same, who are from the same backgrounds, who have the same assumptions about everything. And you get a much less interesting product when you have that. And you also get a lot of fearful people. A lot of people who are too dumb to go into finance, so they went into journalism instead. And they get older and they realize, ‘I’ve got tuitions, and this is actually a pretty shaky business model on which to build a career,’ and they just become unwilling to take any risk at all. When was the last time you saw anybody in the press — except the fringe press — really write a piece that challenged the assumptions of their neighbors? That would actually make their friends in Brooklyn avert their gaze?”Â 

Â \[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/tucker-carlson-problem-with-media.html)\]

“Nobody recognizes their bias, ultimately. I remember my very first job in Boston. We’re doing something on somebody who’s on welfare, which means you’re going to be in the inner city somewhere, you’re not going to go out to Appalachia. Something like 70-some-odd percent of people on food stamps are white. The percentage is something like 25 percent for blacks. So if you were actually going to try to match the actual math and statistics, you could look them up pretty easily and say, ‘Gosh, the face of the person on food stamps is a white woman living in rural America with a job. Usually, it’s somebody who’s working, and a couple of kids.” But no, what we do is we run out and find that thing that we believe in our heads to be true. We see it all the time. That is why most of the time, when local media does something on someone on food stamps, it’s a black woman, and she’s got a bunch of kids, and she’s obese. That is not accidental. You’re just living out your own stereotypes. This is the bias that reporters bring to their job.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/soledad-obrien-problem-with-media.html)\]

“Think about how few African-Americans and other people of color there are in today’s traditional print newsrooms. There’s a demand there from black readerships and other readerships of color: saying, ‘Hey! We need our stories reflected: not just through white reporters. We need them reflected through *our* lenses too. We need *our* perspectives, *our* lives reflected as they truly are. And the reasons people give for being reluctant about media diversity — it just strikes me as antithetical to what journalism is. We’re basically being paid for our curiosity.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/jamil-smith-problem-with-media.html)\]

“The same unconscious bias that exists in police departments exists inside of newsrooms. And the proof is very clear: If those persons who make decisions about the staffing in our newsrooms had the same respect for the dignity and the humanity and the sanctity of black life — that is to say, if they thought more of black people and the gifts and the talents that they bring to the table — then there would be more of us sitting at the table. And I say to my friends all the time that if you’re not at the table, then most likely you’re on the menu.

I go back to the Trayvon Martin case. The reality is that that story would never have made it to the front pages were it not for black media. The mainstream media — I’m talking about the Washington *Post,* the New York *Times,* everybody, *Time, NewsweekÂ *— the mainstream media was late getting to that story. And that’s not really surprising. Oftentimes the mainstream media — particularly where people of color are concerned—is on the late freight. The irony is the corporate media end up being the ones that make the money off the story!”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/tavis-smiley-problem-with-media.html)\]

“I went to an event where the editor of *Crain’s* said all of the Chicago black banks weren’t on their radar. So the audience, which was black, reacted. And here’s why. He was comparing apples to oranges. You cannot compare Chase, Bank of America, Northern Trust, whatever, to the small black banks in the community. However, that small black bank is still large enough to be the main provider of loans to black business start-ups. That’s an important story!”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/kai-el-zabar-problem-with-media.html)\]

“I think it really does come down to an eastern elite problem.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/michael-hirsh-problem-with-media.html)\]

21.

Which brings us to Donald Trump.

“He has a message that a lot of people like. That’s a real thing, and that’s not a media creation. But he is fundamentally a television character — a character created on *The Apprentice* by Jeff Zucker, and which is now sort of a property of CNN. That has obviously been central to his rise. Trump is deeply interwoven with television, and he’s also arisen at a moment of weakness in the television industry in a way that has certainly played to his advantage. Political reporting is part of the fabric of politics and political campaigns are themselves media organizations — they are both your subject and your competitor for people’s attention. And they only exist to the degree that you’re writing about them. It’s very compromised and complicated and always has been.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/ben-smith-problem-with-media.html)\]

“I think it’s useful to see Trump as very much like an independent television production company that has a hit show called *Trump.* Or *Trump for President.* Media ownership, let’s say TV executives, want that show really badly. And this gives him more power than the network’s own people, or their own journalists sometimes — it’s almost like they would rather go for an outside source for their programming than to their own people. So because he produces this show, *Trump for President,* that has amazing ratings, he was able to command what was called in the trade “free media” — that’s a fascinating phrase in itself. Trump, the independent producer with the hit show, was kind of irresistible.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/jay-rosen-problem-with-the-media.html)\]Â 

“Pundits and poll analysts famously screwed up by consistently underestimating his chances, in some conspicuous cases nearly up the point when he locked up the nomination. But I don’t buy the widespread notion that the news media has given Trump a free ride by bestowing $2 billion worth of airtime on his primary campaign or by failing to vet him seriously. Yes, he got a ton of television coverage but he did so by creating news — outrageous and infuriating news, perhaps, but news nonetheless. By contrast, most of his Republican opponents favored scripted public appearances that generated little if any news. Hillary Clinton suffered a media shortfall for the same reason: She often goes out of her way to avoid committing news, and, as of July, she had not given a single press conference in 2016.”

“The media did not create Trump. I don’t think the media missed a story of Trump, or failed to scrutinize Trump — that’s a ridiculous criticism. The criticism that might be valid is whether the media understood the circumstances that caused so many Americans to vote for Donald Trump. It’s always hard to have your finger on the pulse of the country. It’s one of those things that we’re always beating ourselves up for. We probably didn’t quite understand the deep economic fallout after the financial crisis a few years ago. There were fewer national correspondents out in the country, and we’re one of the last institutions to have a big national staff. That’s probably part of it. Some of the anger was quiet, and Donald Trump came along and turned the volume up. The anger hadn’t quite showed up in ways that were obvious until he came along. But our job is to be out in the country, trying to understand the country, and to reflect the country back to itself in some way.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/dean-baquet-problem-with-media.html)\]

22.

His campaign was catnip to the partisan outrage machine.

“Anytime Donald Trump speaks right now, you have to believe that anyone on the progressive side may be saying, ‘I can’t believe he said that,’ but what they’re thinking is, ‘Aww, this is going to be great!’ ”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/kurt-bardella-problem-with-media.html)\]

“I’ve spent much of the past year blogging about American politics; which is to say, I’ve spent full weeks of my life cataloguing the refuse that pours from Donald Trump’s gaffe hole. As a human being, I’ve developed a bone-deep loathing of the demagogue. Â But, as a high-volume news blogger (i.e. a subhuman), my feelings about the man are more complicated. When you need to produce your first post by 9 a.m., it’s hard not to feel some affection for a presidential candidate who starts most of his days by calling in to a cable show and saying something insane. When I woke up one morning in December to find the Republican front-runner had called for the mass murder of terrorists’ families, I felt more gratitude than outrage: Trump was only making my news items great again.”Â 

23.

And everybody was transfixed by the spectacle.

“I was on the campaign trail with Marco Rubio for the duration of his campaign. I don’t think he ever gave us a gaggle where he wasn’t at least asked about Donald Trump once. It was usually, ‘What do you have to say about the latest thing that Donald Trump said?’ Ted Cruz, Jeb Bush, faced the same questions. It became a struggle for them to break through it and to actually talk about the campaigns that they were running. It was a kind of chicken-and-egg scenario, where now that he’s responded to Donald Trump, now we have to write that. The story has become ‘Marco Rubio responds to Donald Trump.’ And it just keeps going from there. There was this moment where Donald Trump had talked about Muslims celebrating in New Jersey on 9/11. Someone was asking Marco Rubio about those comments, and he just said, ‘Guys, I can’t be responding to everything he says. Otherwise I won’t be able to run my campaign.’ At what point do you decide that every single statement needs this level of coverage? At what point do you need to say, ‘Okay, he knows exactly how to dominate the news cycle. It’s a ploy’? Essentially, any day that he felt like he was losing traction, he would say something, and lo and behold, he dominated the day and the week again.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/sabrina-siddiqui-problem-with-media.html)\]

24.

And obsessed with the incredible horse race.

“All the intellectual action in campaign coverage is over before the campaign begins. The basic story is: ‘Who’s gonna win?’ And then there is a sidebar story called ‘Issues.’ ”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/jay-rosen-problem-with-the-media.html)\]

“It seems very interesting in the moment to write about the latest poll, who’s ahead, who scored points in the debate. And all those things are fine. But when you’re overdoing it, you may neglect some of the things that are more substantive, and that would actually be more useful to people. I did a little analysis at the *Times*, maybe a couple months ago, of horse-race stories versus issues stories. The percentage of them that could be described as horse-race stories was extremely high. Those horse-race stories were very, very popular with readers. So it’s hard to not keep doing them.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/margaret-sullivan-problem-with-media.html)\]

“The fact of the matter is that the horse race is much more interesting than a bunch of horses standing around in a field. Races are interesting! It’s unavoidable.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/daniel-okrent-problem-with-media.html)\]

25.

Including the public.

“When you realize that it makes good copy, it’s a great story, it wins in the ratings, *and* it wins in the one master narrative that dominates all others in campaign coverage, which is Who’s Ahead — because he *was* ahead — then there’s no defense. There’s nothing to be done. It’s entirely predictable that he would dominate the coverage. Lots of people said that they gave him too much coverage, and I sort of agree that it was overboard, but when you ask yourself, ‘What would be the limiting principle to give Trump less coverage?’ It’s really hard to come up with one, because the horse-race narrative is so central, so basic to how the press approaches a campaign, that there almost *is* nothing else to limit that.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/jay-rosen-problem-with-the-media.html)\]

26.

But gorging on Trump coverage didnâ€™t actually mean coming to terms with him.

“We all underestimated him at first. Â He was, by every analysis, a ridiculous premise as a presidential candidate. Â He remains so. Â But Trump has been caught up on any number of idiocies, offenses and affronts and it just doesn't matter. Like Huey Long or Father Coughlin, his outrages — even when carefully reported — serve only to excite some of those who are having their deep anger at the status quo fingered by Trump. The press has been fine at holding Trump to his shit-headedness, and while there is some criticism that can be directed at the media for over-reporting him in the run up to New Hampshire, since then he has been the leader in the GOP field. Â He gets legitimate attention for being such. I think the press has been exceptionally unimportant at assessing the American status quo for the last twenty years, for losing sight of the trends that were producing the anger that has led to Trump, and in a healthier sense, to Sanders. Â The marginalization of labor, the purchase of government, the rampant drug war, mass incarceration, the demeaning of the working class and the brutalities of globalization — we told those stories too late or not at all. Â That was our complacency. Â This is just one more election cycle.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/david-simon-problem-with-media.html)\]

“The ability to report on mainstream politics — that’s what the machine is for. The machine was not made to understand things like Occupy Wall Street or Trump. The machine was not made to understand the sort of Bernie Sanders world. It’s making a very strong effort at it, but even so, you see the machine stumbling.

Of course the media failed. But they’ve failed with conservatism all along. Now it’s reached a full boil, where suddenly you can’t ignore the failure. You can’t keep ignoring this thing that you call the fringe. We talk about a 10 percent factor of American society; that’s not a fringe, it’s 30 million people. But beyond all that, it’s the idea of the journalist measuring a story’s importance by the scale of the stage on which it occurs, rather than by the depth of meaning it reveals. You go back to the Scopes ‘monkey’ trial — the famous evolution trial. Every paper in America declared fundamentalism dead. Good call on that one. In fact, you can find that declaration about every five years. And the reason they keep getting it wrong is that they’re guided by the idea of trends. If creationists’ candidates are not in office, then they don’t stop existing. They’re still there, and they’ll have kids, and they’ll raise their kids to believe these things. \[The media failed to see this\] not because the press is so left, but because the press is so relentlessly center.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/jeff-sharlet-problem-with-the-media.html)\]Â 

27.

Later, the journalists who tried harder to call out Trump (by fact-checking him, for instance) werenâ€™t very effective.

â€œHereâ€™s a guy who bragged in 2013 that he knew how to get free media, then went ahead and did it. The last six or seven months have been really just a display of absolute mastery by a guy who has spent a couple of decades perfecting his exploitation of the media. We in the media did our best to cover him in a tough way, in a fair way, but he did a better job of exploiting us than we did of exposing him.  Glenn Kessler, the Washington *Post* fact-checker, wrote [a piece](https://www.washingtonpost.com/politics/few-stand-in-trumps-way-as-he-piles-up-the-four-pinocchio-whoppers/2016/05/07/8cf5e16a-12ff-11e6-8967-7ac733c56f12_story.html) fulminating over why the media doesnâ€™t do a better job of going after Trump for his obvious lies, and heâ€™s been awarded all these Pinocchios. And I think thereâ€™s very little self-awareness in the media that the Pinocchios donâ€™t matter. Everyone here on the eastern seaboard reads the New York *Times* and the Washington *Post* and they do pay attention to how many Pinnochios heâ€™s awarded. But letâ€™s face it, thatâ€™s a pretty small sliver of the US population.

We could have tried to do a better job challenging him, perhaps taking him seriously earlier rather than treating him as nothing more than a clown. There was the spectacle of months and months going by where people in the media kept delivering judgments that we’d reached ‘peak Trump,’ and that the inevitable decline would start. It never happened. It was just a massive misreading. I think the failure of the mainstream media to take him seriously probably only heightened his appeal among Republican base types who despise the mainstream media.

I see this in some ways as the tail end of a long arc of history. We started with a few authorized outlets that really spoke authoritatively; and thatâ€™s largely gone with the wind. The *Times* and the *Post* are still around, but thereâ€™s a question of how much they really dictate the agenda in the way that they used to. And in the meantime 10,000 weeds have bloomed on the internet, sites where like-minded people gather, where they get a lot of their information, much of it just garbage. Thereâ€™s no real anchor. And of course a guy like Trump comes in and exploits that to the hilt. He ran roughshod over a traditional media that still sees itself as the platform for authoritative news, but which is actually used less and less for that. If Trump is elected president, that will underline just how irrelevant weâ€™ve become.â€?

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/michael-hirsh-problem-with-media.html)\]

“I think the media has been too afraid to just say, ‘You’re a racist.’ I think if we don’t stand up and call Trump a racist when he calls Mexican immigrants rapists, or he questions a judge because his parents are from Mexico, or sexist when he calls women ‘pigs’ — how do we retain the trust of our readers who fundamentally go, ‘That shit’s racist!’? If we’re not going to be willing to say, ‘That shit’s racist,’ what are we doing?”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/alexis-madrigal-problem-with-media.html)\]

“For the mainstream media, it’s no longer plausible to say, ‘Oh, we’re just going to be objective.’ How could you not have an opinion at this point, when your reporter is sitting there and Donald Trump is calling him an idiot? It’s not believable. I think that, at some point, the mainstream media — both rightfully and strategically — needs to shed that cloak of objectivity and say, point blank, ‘This is not a good person, this is not who should lead our country.’ They’re handicapping themselves by playing by a set of rules that no one else is playing by anymore. And if they continue to cede that ground, their influence will continue to be diminished.”

\[[Read the Interview Transcript](kurt-bardella-problem-with-media.html)\]

“The very aggressive stance the *Times* and the *Post* have taken in calling his lies out as they occur, that’s really new. Is it biased? I don’t know. I can’t tell whether it’s biased, because they are lies, they should be called out, but in the act of calling something out, you’re staking out a position. And that’s what you’re seeing in the Trump coverage — a movement away from false balance, toward saying what’s true and not true. That’s good, but it’s also destabilizing to the way we were raised to eat our news.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/daniel-okrent-problem-with-media.html)\]

â€œThe genius of Donald Trump is that he knows how to change the subject. â€˜Oh, they want to talk about taxes? Well, Iâ€™m going to insult someone so we wonâ€™t be talking about taxes, weâ€™ll be talking about how attractive my wife is.â€™ He understands the business of journalism. He understands journalists can be like a school of fish, they all turn in formation really fast, chasing the bright shiny lure.â€?

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/bill-keller-problem-with-media.html)\]

28.

Not that that is really anything new.

“If you ask lots of people, lots of voters, I think a large number would say, ‘We didn’t know enough about Bill Clinton when he became president. We didn’t know enough about George W. Bush when he became president. We didn’t know enough about Barack Obama when he became president.’

With Bill Clinton’s womanizing, there were intimations of it, there were names. And then he gets in the White House. I remember when I first heard about the Monica Lewinsky relationship. I couldn’t believe it! I thought, ‘Is this possible?’ It was inconceivable that a sitting president would have an affair with an intern.

In the CIA they have a process called ‘walking the cat back.’ If something happens that is a surprise, like a country shockingly testing a nuclear weapon, as they did, years ago, then the CIA will go through all their intelligence. They say, ‘Okay. We know this happened on this date. When could we have — when should we have — seen it earlier?’ ”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/bob-woodward-problem-with-media.html)\]

29.

Whoever the subject, the press can be cruel.

“In June 2014, I went to Montana to profile Brian Schweitzer, the wildly entertaining and politically unorthodox former governor who was then considering challenging Hillary Clinton for the Democratic nomination in 2016. I spent nearly a week in Montana, visiting Schweitzer’s ranch, riding snowmobiles, and driving through snow-covered mountains — but mostly just listening to the governor hold forth on everything from America’s misbegotten foreign-policy adventures to his own unusual plan for health-care reform to the daily prices of certain precious metals (Schweitzer is chairman of the board of a mining company).

Profile writing requires earning the trust of subjects in a way that inspires them to be their most honest and interesting selves — and then telling the truth about that person. I found Schweitzer — a gun-toting progressive isolationist — at once familiar to the types of liberals I’d known growing up in rural Pennsylvania and totally fascinating. By the end of the process, I was rooting for him to run, if just to make the race more interesting.

But Schweitzer said some things to me that were very … let’s just call them colorful. Like, for example, that Dianne Feinstein ‘was the woman who was standing under the streetlight with her dress pulled all the way up over her knees, and now she says, “I’m a nun,” when it comes to this spying!’ (Feinstein had recently criticized the NSA for spying on foreign allies.) Schweitzer then pivoted: ‘I mean, maybe that’s the wrong metaphor — but she was all in!’

The night former House majority leader Eric Cantor was booted from office by a tea-party primary challenger, I called Schweitzer back. ‘Don’t hold this against me, but I’m going to blurt it out. How do I say this … men in the South, they are a little effeminate,’ he said. ‘They just have effeminate mannerisms. If you were just a regular person, you turned on the TV, and you saw Eric Cantor talking, I would say — and I’m fine with gay people, that’s all right — but my gaydar is 60 to 70 percent. But he’s not, I think, so I don’t know. Again, I couldn’t care less. I’m accepting.’

Michael Kinsley recently [asked](http://www.vanityfair.com/news/2016/02/politics-media-gaffes-lies): â€˜Why shouldnâ€™t the words that a candidate has carefully crafted be considered a more accurate reflection of actual opinion and character than the words uttered by accident?â€™ And heâ€™s right. How many otherwise good people have been driven away from the political process by the mediaâ€™s obsession with scandal and gaffes? Itâ€™s a difficult question to answer. But the ethical quandaries that arise from reporting on politics are, in some ways, much easier to answer. The relationship between the press and the powerful people they cover *should* be adversarial. Itâ€™s the stories about private citizens that are even more difficult. Journalism is a profession characterized by the overly macho shibboleths of its practitioners â€” the types of people who like to say things like â€˜if youâ€™re not making people angry, youâ€™re not doing it right.â€™ But it is sometimes extremely difficult to be a good journalist and also a good person.

I included both quotes in my [otherwise positive profile](https://www.nationaljournal.com/magazine/2014/06/18/gonzo-option) because they were splashy, funny, impolitic, and a little bit offensive. And, if Iâ€™m being honest, getting people to even read a long story about a lesser-known presidential candidate can be difficult. I knew those quotes would generate buzz. I was right. Within 24 hours of going up, the story was being covered on every major news network. *Good Morning America* asked me on. â€˜You better keep him away from my husband!â€™ Feinstein said, when *Politico* asked for her response. Schweitzer quickly apologized and then he disappeared. Once a regular on MSNBC, he stopped showing up on television. No major figure ever spoke of him as a serious presidential contender again â€” including Schweitzer himself.â€? 

30.

And selective in its cruelty.

“If you really have an ill feeling about a person, you can find information — legitimate, factual information — to turn that person into anything you want. It’s the selectivity of the information. The selectivity of the *evidence*. You can go into a country under a dictatorship and say, ‘Flowering freedom is emerging here.’ It’s what you’re looking for, who you talk to. You choose people to be reflective, and to be in concert, with what you think. And you can get any kind of story, you can ruin anybody’s career. People believe the first time they read something, and it makes an impression on them, and you can’t alter it, no matter how much pleading you do to get it right, you’re lost.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/gay-talese-problem-with-media.html)\]

31.

And things can get very personal.

“I don’t want to reopen a wound. But I think that very early in my career, I did a profile of someone who was — somebody I kind of thought of as a friend, who thought of me as a friend, who really opened his life to me, and the piece was fair and straightforward and reported, but he was totally unprepared for it. There’s the great Janet Malcolm stuff on this, but there’s this element of journalism that is fundamentally about betrayal. But I think there’s also an obligation to be straightforward when you can, and to not surprise people. It’s a complicated, messy business. It’s not like there are simple rules. You want to be doing stories that somebody doesn’t want written. If everybody’s psyched that that story’s published, then it’s kind of like, ‘Well, why did we do that?’ The question of what’s gossip and what’s news is often whether you like a story. The CIA was really mad at us when we reported that a controversial senior official had married somebody who talks about the CIA on TV all the time — kind of in secret. Is that news or is that gossip? They definitely thought it was gossip. But I think most of the stories we break are pretty straightforward news. And I don’t think there are ever diminishing returns for new information. I think gossip is often used as a word to deride a story you don’t like.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/ben-smith-problem-with-media.html)\]

“I recognize it, when I’m writing the story, even though I’ve got all the official sources, I am fucking someone’s life. I had a guy that told me in Starbucks that I had ruined his life. He owned four nursing homes where he did really not-good things. The reports for all four homes, they were pretty horrific. Those were people that were vulnerable, and they were taken advantage of by this guy. They got shut down, and I reported it. I saw him months later, and I said, ‘Hi, Aaron, how are you?’ And he goes, ‘Sheila,’ in this big tall voice, ‘You ruined my life.’ Right in Starbucks.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/sheila-hagar-problem-with-media.html)\]

32.

Reporters are obsessed with gaffes.

“Where I see it on my beat is with something trivial, like Justice Scalia asking a question in the affirmative action case that was clearly artless and certainly not well phrased, but wasn’t him saying black people are stupid — and then that’s the thing that gets tweeted out. That becomes the narrative of that case. I’ve spent 17 years trying to get people to care about the court, and I’m certainly a perpetrator of the ‘Deploy humor! Deploy metaphors!’ But if all you focused on in the affirmative action case was that one Scalia question, which was not characterized accurately and which tells the story of how awful Scalia is, then you’ve missed both what the case is about and what the court is doing. Whatever this case was about, it wasn’t about ‘African-Americans are stupid.’ Put the blame on Scalia for saying it in the most horrible fashion, probably on purpose, but also put the blame on that tendency to Tweet fluff.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/dahlia-lithwick-problem-with-media.html)\]

33.

Can be horrible to women, even when they are trying to be kind.

â€œFor years Iâ€™ve pondered Hillary Clintonâ€™s press coverage. When she is in a job, she is more likely to be written about as competent, admired, warm and well-liked, despite her brutal baptism in the political press of the 1990s. As soon as she launches a campaign, the coverage curdles and becomes cold â€” often in ways that lack big billboarded examples of sexism, since people now mostly know better than to compare her to Glenn Close in *Fatal Attraction*.

This silly season has provided a moment of clarity on this point, though not in reference to Clinton, but to Elizabeth Warren. Warren is, as a senator, a hero to many on the left, now widely admired for the righteousness of her opinions and ferocity of her speech on behalf of America’s economically struggling. It wasn’t always this way. When she ran for her Senate seat, she was described in ways that might be familiar to Clinton critics: as ‘[uninspiring](https://newrepublic.com/article/105715/boston-common),’ as ‘[Stiff. Whiney. Preachy. A know-it-all. Annoying](http://www.telegram.com/article/20120916/COLUMN01/109169735),’ and, by one [ABC commentator](http://www.nameitchangeit.org/blog/entry/media-use-coded-words-for-elizabeth-warren-strident-off-putting-shrieking), as ‘off-putting’ because of ‘her tone and the way she is.’

But that was so long ago! The press loves Elizabeth Warren now! When Clinton met with her before declaring her candidacy, the New York *Times* ran a piece describing Warren as an â€œincreasingly influential senator,â€? whose ring Clinton pretty much had to kiss. When Joe Biden met with Warren, the *Times* described her as â€œthe dream presidential candidate of many progressives,â€? a senator who â€œhas helped set the agenda for progressive policyâ€? and â€œbecome a favorite of the left for her fiery rhetoric.â€?

And then Elizabeth Warren endorsed Hillary Clinton, and, more critically, instantly became an imagined contender for the job of her vice-president. This is how the New York *Times* covered Warrenâ€™s entrance into the fray, if not as a candidate herself, as a woman making a bold play for political leverage: â€œMrs. Warren cuts an imperious swath through the Capitol, striding down hallways, her jewel-toned jackets swaying behind her, refusing to speak to or even make eye contact with reporters. Small talk with elevator operators and other staff? Not her style.â€? Warren, according to the Times, is â€œnever short on confidence.â€?

There it was. Not one sweeping wound — look, nobodyÂ *called*Â her a cold bitch! — but instead denigration by a thousand tiny paper cuts. This, and not the big, bold vulgarities we rely on to prove our points about how badly the media treats the women it covers, is the grinding, insidious reality. The framing of women who dare announce their competence, theirÂ *superiority*Â to others — which by definition is the job of women who are candidates or might want to become them — as unpleasant, unreachable, remote, overconfident, chilly, all through the use of tiny code words and descriptors: their jewel-tones, their disregard for elevator operators, the brisk pace at which they walk hallways.”

34.

They can also be lascivious, when they arenâ€™t being outright lewd.

“The writer will paint this picture of the celebrity from their viewpoint, which is obviously the male gaze, and it’s often hard to break out of.

Sometimes it’s a conscious decision to have a man write the profile because the editors feel like they’ll get something different out of the story. Or they’ll just choose a male writer because they’re thinking of their audience, which is mostly male. And there’s this aesthetic that presumably has to be upheld: where women *are* objects of desire, so that’s how they end up getting written about. There’s this obsessive focus on physical appearance, sometimes to gross effect. Sometimes it’s fairy-tale-ish. There’s often this idea that the woman can’t possibly be real, which is an issue. There’s clearly a bias because they wouldn’t take that same approach with male figures — or, traditionally they haven’t. The female subject is presented through this glazy eye, or lens, that obscures their work.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/clover-hope-problem-with-media.html)\]

â€œThe [*Vanity Fair* profile of Margot Robbie](http://nymag.com/thecut/2016/07/margot-robbie-Vanity-fair-profile-bad.html) would have passed without notice only a few years ago. Now, the reaction to the way Robbie was portrayed is the story itself. There is certainly a greater awareness of sexism in the media. Itâ€™s hard to tell how much of that is due to general societal progress, to generational change, or to the internet, but thereâ€™s no question that the internet has allowed for a more robust conversation, and thereâ€™s an especially vibrant online conversation about sexism and other inequalities. Until very recently, a published article was the entire conversation â€” beginning, middle and end. Now it can be the jumping off point for something much larger and richer. And people are paying attention. Casting directors, journalists, photographers, media executives â€” theyâ€™re all thinking about these issues now in ways they wouldnâ€™t have a decade ago.â€? 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/arianna-huffington-problem-with-media.html)\]

35.

And ruthless in their pursuit of â€œthe get.â€?

“Last December, two days after the terrorist shooting in San Bernardino that left 14 people dead and 22 injured, cable-news viewers witnessed a strange spectacle: a scrum of reporters and TV-news crews swarming the home of the suspected shooters, on live television, as though it were the journalistic equivalent of *Supermarket Sweep.*

Watching reporters rush the townhouse — some racing up the stairs, others doing live coverage as their colleagues rifled through family keepsakes and private documents — set Twitter on fire. Everyone, from right-wingers alarmed about tampering with an active crime scene to lefties angry over the seemingly transparent violation of suspects’ basic rights, was united in their condemnation of the media horde — including several members of the media who weren’t present but were watching the silliness unfold from their offices.

The absurdities mounted. One woman, later identified as a neighbor, was allowed to walk into the scene with her dog. MSNBC showed photos and a driver’s license, without omitting the identifying details, live on the air. But nothing embodied the scene quite like the photo of a man brandishing what looked like an orange Popsicle live on CNN. “Tfw you have no journalistic integrity but you also really want a popsicle,” one Twitter user wrote as a caption to the photo. Another wrote: “\[breathless, sweating\] ‘A Moslem popsicle, Henry. Look at it carefully. Tastes like terrorism.’ ”

The reporter was Toby Harnden, an Orwell Prize–winning British journalist for the Sunday *Times,* and he was actually holding a screwdriver, not a Popsicle, which he had used to help the landlord open the door on the boarded-up house, unleashing the flood of reporters, including Harnden himself. His assessment of his motivations at the time is revealing: ‘It’s just like a potential gold mine,’ he said recently. ‘You don’t know what’s in there. That scene should have been roped off for a week, but once you’re given the opportunity to go in, that’s what you’re going to do because it’s your job, right?’

‘It was like this dam was breaking,’ he said of the rush inside. ‘It was a very exhilarating moment. So now you’re in this place and your mind is racing, all of your senses are working in overtime because you’re like, okay, I’m in here, I’ve got all these choices — it’s kind of a race to get to stuff before other people get to it. Where do I go? Do I go to the sitting room or the kitchen?’ Harnden ended up in a bedroom and found photos, business cards, divorce papers, credit cards, coins from Saudi Arabia and Jordan … which he photographed to make sense of later. There wasn’t time to read anything in the moment. ‘There was a herd instinct. There was some guy who had been at the front of the scrum who was being all friendly, there was a lot of banter and stuff — and when we were in there I was rifling through the bedroom drawers. And he just looked at me and was like, ‘Oh, Toby’ — this real kind of sad, disapproving tone. I was just like, ‘They’re dead!’ ”Â 

36.

Then, typically, they just move on.

“What happened in those Seattle school cases where they did away with the busing? Did the cities resegregate? Did they not resegregate? We are so bad, with very few exceptions, of doing the ‘Five years later, what was the impact of those affirmative-action cases?’ It goes to the basic attention-deficit disorder. But there is an amazing amount of work to be done on ‘How did the Supreme Court decision five years ago change everything? How did *Citizens United* change everything?’ And we almost never do that. There’s no reason that I can’t sit down and do a piece about ‘What did *Citizens United* change? What did it not change? What about the super-pacs? Where did the dark money go?’ Those are Supreme Court stories too.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/dahlia-lithwick-problem-with-media.html)\]

“We used to do all kinds of special projects, and we won awards. We got lots of glory in our little community — and beyond — for that kind of coverage. And we just don’t get to do that anymore, because we’ve all got a lot of busy work: advances, press releases, briefs. There used to be other people who could do that — like a copy editor might do those things. But the copy desk has gotten whittled down. I’m beginning work on a longer series, on parents who lost their children to drugs, but I’m going to have to just work on it piecemeal, as I can, instead of diving in and staying under, which I think serves that kind of piece better, because when you constantly have to climb out of the pool, you have to go back and figure out where you’ve already swam and what temperature the water is again. Things that occur to you while you’re writing, that you didn’t somehow message yourself about, are lost often. I think people with that kind of story to tell are less hopeful that they can get something done by the paper, so then I think they seek other venues — a long Facebook post. And it does damage the public’s institutional history.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/sheila-hagar-problem-with-media.html)\]

“The mainstream media suffers from ADD — they cover a story and then they abandon it. The web has shifted us to OCD — we cover stories obsessively until there is an impact, until something happens. At HuffPost we did this with the foreclosure crisis — our coverage was constant because the crisis was ongoing. We’re doing it now with our coverage of Donald Trump’s campaign, including challenging the media to do more of this.Â 

There are different ways to define quality, but we define it by adding value to people’s lives. If you define quality only as 10,000-word investigative reports, sure, you’re going to be disappointed — but again, there was never a media world in which things were that way.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/arianna-huffington-problem-with-media.html)\]

37.

And then thereâ€™s the problem of â€œobjectivity.â€?

“Court reporters will say, ‘Ugh, I can’t believe this is even in court,’ and then they write a piece that’s like, ‘In a spirited debate, both sides —Â *’Â *”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/dahlia-lithwick-problem-with-media.html)\]

“There’s this relatively new principle of journalism that journalists are supposed to be neutral and free of opinion. What that means is that journalists are increasingly discouraged from ever doing anything other than saying, ‘Here’s what one side says, and here’s what the other side says.’ That shows you’re objective. When Harry Reid says one thing and Mitch McConnell says the other, then you get to report that, it’s easy. When you don’t have that, when you have to go searching for it, or when there’s not this clean conflict, it’s difficult to pretend that you’re neutral.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/glenn-greenwald-problem-with-media.html)\]

“I treat he-said-she-said reporting as a problem, but it’s also a solution to a problem. A lot of the problems I see in press coverage happen because the journalists involved don’t have time to do a better job, or they don’t have enough knowledge, or they’re under pressure that prevents a more serious story from emerging. When a new study comes out, and the hospital association says, ‘Costs are decreasing,’ and the consumer’s group says, ‘Actually that’s not true, costs are continuing to go up,’ and you have to write a story by deadline, then he-said-she-said makes it writable. Then you don’t have to know who’s right. So a lot of times repetitive narratives, or lazy narratives, or devices like he-said-she-said, are substitutes for real knowledge.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/jay-rosen-problem-with-the-media.html)\]

38.

And â€œbias,â€? of course.

“Let me put it this way: Mitt Romney’s not a candidate who fits the elite media’s ideal of what a president should be — obviously there’s a preference for Obama over Romney. However, Romney’s not someone who fits the grassroots conservative ideal of who a president should be either. And so you get someone like Romney who does prevail in the Republican primaries and goes into the general election and there’s an internal psychological problem of identity there, because he can’t quite figure out what media profile he wants to present, what his public image is.Â 

In a weird way, I think the media sets that up by the way it presents and reflects the images of candidates. It winds up being very ironic, because Mitt Romney is someone who’s basically pretty centrist as far as the Republican Party goes, certainly compared to a lot of other possibilities. And yet he is presented by much of the media as being a kind of hard-right guy during the general election. And then he sometimes tries to almost do the Trump thing, tries to take advantage of that, but winds up not being able to because, of course, that’s not who he is. So I think there is a complex psychological effect here that does harm Republicans in general elections. Â 

Now certainly, on the Democratic side, sometimes there also is a tendency to be fighting against stereotypes that the media may have promoted: examples being that Democrats are weak on foreign policy, or pacifistic, and also soft on crime. But you see less of that stereotyping. It’s less of a problem for Democrats than it is for Republicans, who automatically, I think, are presented by the media as heartless and cruel and unsympathetic.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/daniel-mccarthy-problem-with-media.html)\]

“In a pre-Fox universe, news was a neutral blob. But once Fox sets up on the right, everything to its left is The Left — it’s no longer neutral. By every definition, if I’m not Fox, I’m Left. And what’s most notable about this to me is how small Fox’s audience is — their ratings are ridiculously small, in terms of the voting population of the country. But they are definitional. They are the ones who have defined the ideological shape of the American news media.

And if the *Times* ran a picture of a smiling George Bush on the front page in the summer of 2004, I would hear from readers who were outraged that this was evidence that the *Times* was favoring Bush. And I would ask such people, did you see yesterdayâ€™s front page that had a smiling picture of John Kerry on it? No, I didnâ€™t notice that. Well, they didnâ€™t notice that because that conformed to their view of the world â€” that was the world as they expected and as they want it to be. If Iâ€™m a Kerry voter and I see a picture of Kerry, it doesnâ€™t make me trust the media more. That's like saying, I trust air. Whereas that which does not conform to their view of the world â€” a smiling George Bush â€” *that* they notice.â€? 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/daniel-okrent-problem-with-media.html)\]

“I think lots of people had kind of an implied reader, who was like a guy in his mid-40s wearing khakis and barbecuing. There’s lots of mainstream media that’s like, ‘We’re neutral,’ but what they really mean is, ‘We write stories for that guy.’ ”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/alexis-madrigal-problem-with-media.html)\]

“We all know about the snarky coverage of conservatism, right? That’s not the biggest problem. The biggest problem is the overly kind coverage of conservatism. I can think of big magazine stories about megachurches that I knew that remade them over in the image of a Republican voter. I know that church. That’s a spiritual war church. I know that they do tons of exorcisms every week. I know that’s a deliverance ministry. Somewhere, someone made the assumption, ‘I don’t want to make them look stupid, so I won’t put that in.’ That that’s being respectful. But you respect them by not thinking that they want to be seen like you. Take a megachurch that’s deeply invested in deliverance ministries and has major political power in its region. It can swing state elections. If you think, ‘I don’t want to impose a story that those guys are insane, so I’m going to bend over to the opposite direction, so that I can be fair.’ Well, now you’ve erased the possibility that what you think of as wacky and serious coexist, that the deliverance ministry and the strategic political thinking coexist, and always have. And that’s sort of dangerous, right?Â  That stuff is going to simmer, simmer, simmer, simmer, because you can’t see it. You’re only dealing with conservatives who seem serious. You wouldn’t want to look at those conservatives who support Trump, because you feel like that would be disrespectful to conservatism. But it hurts on every level. It hurts us in terms of dealing with people.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/jeff-sharlet-problem-with-the-media.html)\]

39.

Plus general media ignorance.

“If they’re covering business, they don’t know enough about business. If they’re covering technology, they don’t know enough about the tech. I’m taking coding courses now. I don’t know how to code, I should know how to code, if I’m covering technology, shouldn’t I?”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/kara-swisher-problem-with-media.html)\]

“As a coffee drinker, I’ve suffered whiplash. I’ve seen surveys that say, ‘Coffee is terrible for you,’ or ‘No, actually it’s good, it will extend your life.’ ‘No! It will kill you.’ With some things, there is no final, established truth, and it’s not reasonable to expect it. But one of things that frustrates me is when a reporter says, ‘The situation is complex.’ A lot of times, what that means is, ‘I don’t really understand it well enough to explain it.’ ”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/margaret-sullivan-problem-with-media.html)\]

“The whole ‘We only have 90 seconds or two minutes, and we’re not a medical journal,’ all of which just rang very hollow with me, because I got my start in television news. I know that you can cover the same stories with a gee-whiz attitude, or leading with your uncertainties and your limitations. If you want to make the judgment that people don’t want to be hit up-front with uncertainties and limitations, that people want to be spoon-fed black-and-white and certainties, even when it doesn’t exist, so be it, but I think that you’re making a terrible and a harmful judgment.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/gary-schwitzer-problem-with-media.html)\]

“The most dreaded words that can appear in any news article are, ‘A recent study says.’ I mean, stop reading! Because there’s another study that says the opposite. The news media get manipulated by the science Establishment, the way that there’s something in the *New England Journal of Medicine,* the *Times* gets it first, and if the *Times* gets it first, they’re going to play it up big — that’s part of the deal. Those kinds of deals go on throughout journalism, as you know, but it’s particularly bad in science journalism, because a study is not news but gets treated as news. The other most dreaded words are “Analysts say,” because “Analysts say” means “I say,” but you can’t say that.Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/daniel-okrent-problem-with-media.html)\]

40.

And the way media is consumed helps all-out charlatans flourish, too.

“I just wanted to see what people were willing to believe. I started off initially targeting the conservative groups, because they seemed to really spread around completely fictional things with ease. One of the biggest stories we’ve had is President Obama funding a Muslim museum during the government shutdown a few years back. Boy, that one really went viral. Fox News picked it up and ran with it on air, *Fox & Friends.* Then they had to issue a retraction, and then a lot of the late-night television hosts picked up on it. So we got a lot of coverage. I personally wrote one that was about residents of Colorado using welfare, basically EBT cards, to buy marijuana. That one ended up resulting in actual legislation being introduced there to prevent that from happening. People really bite on this. It’s fiction — there’s hints of truth, of course, but you can make up anything you want, really.

I get a lot of criticism — I don’t consider myself a journalist, so I don’t care about the ethics of proper journalism. It’s more of an entertainment thing. I don’t lose a lot of sleep about it. But I do go back and forth about, is this the right thing to do? When it comes down to it, there’s shades of truth in all the news, and we kind of initiate people into what it’s like to be completely duped. Where somebody’s going to share a story and then someone’s going to be like, ‘You dumb-ass, that was a fake news site.’ That makes them — the next time they’re going to share something that either they didn’t fully understand or didn’t read — hopefully think twice and become a bit more critical about the content they’re consuming and what they’re sharing.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/allen-montgomery-problem-with-media.html)\]

41.

(Not that these complaints are unprecedented.)

**1830s**: Elites accuse the press of sensationalism when advertising revenue comes to dominate the newspaper business.

**1860s**: Hungry for dramatic battle scenes from the Civil War, newspapers often report rumors or make up stories entirely; “He lies like a newspaper” becomes a common insult.

**1897**: The new muckracking, partisan “yellow journalism” is [](https://books.google.com/books?id=-_kWbKnrx8AC&lpg=PA6&ots=NFZcHN-pFf&dq=yellow%20journalism%20vice&pg=PA6) [accused of](https://books.google.com/books?id=-_kWbKnrx8AC&lpg=PA6&ots=NFZcHN-pFf&dq=yellow%20journalism%20vice&pg=PA6) “corrupting the young and debauching the old, championing vice and lewdness, and defying respectability and decency.”

**1919**: Upton Sinclair publishes *The Brass Check*, an investigation of newspaper corruption that accuses newspapers and magazines of being in the pocket of their business owners and the political elite and ignoring coverage of socialist causes.

**1968**: After the disastrous Democratic National Convention in Chicago, the Democratic-majority Congress holds hearings to determine whether TV stations had incited violence. “The 1968 Democratic National Convention in Chicago was a key turning point in media trust," says media historian Thomas Mascaro.Â 

**1969**: The Nixon White House began using the term “the media,” rather than “the press,” because it sounds more unpleasant and conspiratorial. Nixon accuses the media of [](http://www.nytimes.com/roomfordebate/2015/04/29/lessons-40-years-after-the-fall-of-saigon/public-learned-less-after-media-was-blamed-for-failure-in-vietnam) [undermining the war in Vietnam](http://www.nytimes.com/roomfordebate/2015/04/29/lessons-40-years-after-the-fall-of-saigon/public-learned-less-after-media-was-blamed-for-failure-in-vietnam), declaring “our worst enemy seems to be the press,” while Vice President Spiro Agnew derides “the networks’ endless pursuit of controversy” as harmful to democracy.

**1971**: After journalist Seymour Hersh uncovers the My Lai massacre, the Army court martials and convicts Lieutenant William Calley, the only soldier to be held responsible. Believing Calley is a scapegoat, the public turns on the press for their harsh coverage. A *Time* magazine poll finds 67 percent of respondents believe that the media should not have reported statements from soldiers involved prior to a trial.

**1994**: A Gallup poll finds only 20 percent of respondents think that the ethical standards of journalists are high or very high. Beginning that year, the televised trial of O.J. Simpson trial is a ratings boon, even while critics accuse the media of sensationalism.

**1998**: A CNN poll finds that 72 percent of respondents think there was too much media coverage of the Monica Lewinsky scandal, and 55 percent think the media acted irresponsibly.

**2004**: After many of the claims used to justify invading Iraq turn out to be false, news outlets, particularly the New York Times, are accused of selling the nation on the Iraq war by uncritically parroting White House officials.

**2013**: Only 26 percent of Americans think the press [](http://www.people-press.org/2009/09/13/press-accuracy-rating-hits-two-decade-low/) [gets the facts straight most of the time](http://www.people-press.org/2009/09/13/press-accuracy-rating-hits-two-decade-low/); in 1985, 55 percent did.

42.

The mediaâ€™s lost power, and coziness with its subjects, serves the celebrity industrial complex â€¦

“One of the things that I will feel least good about, the most often, is the knowledge that some irreducible part of my job, as it often plays out, is to help rich people, who have partnered with rich companies, sell shit. When I write about the dancers and the fine-art photographers, that’s less so. But when I’m writing about Chris Pine, and we’re going to mini-golf or some other contrivance, the reason we’re doing that is because there’s a new *Star Trek* movie coming out. The flip side is that if you’re lucky, and you’re astute, and you keep that bigger context in mind, as a writer, you can treat that promotional function as the kind of precondition. It’s the foot in the door for good, surprising, revealing writing that adds up to more than just a marketing transaction. But often the opening terms are, ‘I’ve got to sell this thing.’ And that’s often why you’ll get a certain recalcitrance from publicists: They’re thinking about your interview as part of a kind of marketing puzzle or something. And you can understand — they’ve got to be efficient with it.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/jonah-weiner-problem-with-the-media.html)\]

43.

â€¦ Political operations â€¦

“I think there’s a trap that the media fall into — and I think the New York *Times* is particularly guilty of this — which is being beholden to State Department narratives. The State Department is not a neutral party. The State Department is a political actor, in relationship to Russia and in relationship to Ukraine. With foreign coverage, reporters often default to thinking of their government as the sort of ultimate authority.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/masha-gessen-problem-with-media.html)\]

44.

â€¦ Big business â€¦

“There’s almost no sustained, serious, skeptical coverage about the way that companies operate and are managed and are funded and come to market. And that’s a failure. I think it’s because of two things. One is that there’s been this rise of Reuters and Bloomberg, which have had a very uneasy relationship with investigative journalism. Most of what they do is beat journalism. The highest cost to a beat reporter is being scooped on a really big piece of news: Reuters cannot afford to have its beat reporter be shut out from a major corporation, because Bloomberg will kick its ass. It’s a situation that is ripe for journalistic capture. Meanwhile, the business press that used to exist 10, 15, 20 years ago, offering sustained, in-depth skeptical business coverage, has disappeared. *Fortune* has almost entirely disappeared. *Barron’s* used to do more sophisticated, skeptical coverage. *Forbes,* 20 years ago, used to be a very good investigative publication that took on smaller companies and their accounting and had arcane sophisticated financial coverage. They do almost none of that. Because of the internet, none of them have a business model. Meanwhile, you have the *Wall Street Journal* under Murdoch having shifted to aspiring to become a competitor of the New York *Times* as a general-interest newspaper. And what has suffered is the corporate coverage.”

45.

â€¦ And start-ups too.

“One of the good things about tech is there’s all these really cool products, there’s cool things to write about. The iPhone is fucking cool, it changed the world. It’s really interesting to write about the topics that are obviously changing our society. I don’t think we’re flacking for it. It’s not like you’re covering some dumb, I don’t know, Kellogg’sÂ comes out with a new Pop-Tart. It’s not that. Elon Musk is making spacecraft to go to Mars. That’s fucking interesting. I mean, why not write about that? You don’t want to be the one to — you can just see someone sitting on the sidelines at Kitty Hawk going, ‘I don’t think this one’s going to fly. And if it does, the wings cost too much.’ ”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/kara-swisher-problem-with-media.html)\]

46.

Who owns things is a major problem, too, whether itâ€™s a giant corporation â€¦

“I think one of the most underappreciated things when people talk about journalism is the cultural change that happened when corporations, huge corporations started buying media outlets, where media divisions were just one of their many divisions. They sell insurance, they sell weapons, they sell all kinds of products and they also have a media division. Before that happened, journalism was very muckraking and a lot of the time it really was ideological. It was very crusading, vibrant, controversial, it alienated a lot of people and attracted a lot of support from other people. And then once corporations started — Westinghouse and CBS, now Disney and ABC, and all of that — journalists started becoming corporate employees instead of journalists. When you work at a corporation, being controversial or offending and alienating people is the worst possible thing you can do. You’re supposed to kind of please the power structure that exists. If you’re a corporation that has a lot of dealings with the government, the last thing you want is your media division alienating government officials. If you talk to journalists who are at these organizations, they’ll say, ‘Look, I’ve never once been told what to say.’ And they’re totally telling the truth. But that doesn’t really say much, because as adults, one of the things we’re all really good at is coming to understand what our environment rewards. If you look at what are probably the two greatest failures of the American elite class in the past, say, 15 years, it would be the invasion of Iraq based on utterly erroneous pretenses that led to disaster on every last level; and the other would be the 2008 financial crisis that wasn’t foreseen and our economic geniuses failed to control, that led to incredible suffering. And the media played this critical role in both, because they’re the ones who failed to investigate any of these things and continue to hold up these people as authority figures, and so faith in every institution, every elite institution has been eroded, including and maybe especially the media, and once you destroy faith in elite institutions, people become very vulnerable to appeals that say, ‘I am outside of this structure, and I want to wage war on it and attack it, and I’m condemning it as corrupt.’ That’s true of both Sanders and Trump, and I do think that explains a lot of their popularity.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/glenn-greenwald-problem-with-media.html)\]

47.

Or an unsavory individual.

“Sheldon Adelson doesn’t need to call down to the paper and say, ‘I don’t want this in the story.’ It’s already known he doesn’t want to see it, so it’s not there. More than seventy percent of the population of the state is located in Clark County, where the *Review-Journal* is the single largest news organization. Adelson is reportedly hoping to buy the Reno *Gazette-Journal,* which is a smaller paper, but it covers the only other large population base in the whole state. If one person controls both newspapers, he controls the message to roughly 97 percent of the population of the state. If he does that, then he controls editorial endorsements of people running for public office, including the legislature, where he has on more than one occasion gone to either ask for changes in tax policy or changes in various laws, including the state’s anti-SLAPP lawsuit protection law. His people have lobbied to have that removed because he sues people who libel him. Or he thinks libel him.”Â 

â€œWe always knew Jared Kushner, the publisher, was Trumpâ€™s son-in-law. Jared really was a very hands-off owner. I literally had one conversation with Jared Kushner in my life. Ken Kurson, the editor-in-chief, was certainly a hands-on editor, but he was someone who at least would give lip service to the independence of the newspaper. And I still think my coverage and my work there at the *Observer* was not at all influenced by Trump, in terms of there being no orders from on top to write about Trump in a certain way necessarily. But *New York*â€™s Gabe Sherman broke the story that we all suspected, [that Ken had assisted with Trumpâ€™s AIPAC speech](http://nymag.com/daily/intelligencer/2016/04/inside-the-donald-trump-presidential-campaign.html), and for me, that was a huge turning point and a time when I really thought, like, *This is deeply unethical, and I should not be here anymore.* I mean, look, had it been Bernie Sandersâ€™s speech or Hillary Clintonâ€™s speech, it would have been deeply problematic as well. That AIPAC moment coupled with the endorsement about a week later or so was the final straw for me. And, I mean, the endorsement was so catastrophically bad in a lot of ways. Not in that a paper doesnâ€™t have a right to endorse, or a right to endorse Trump, but it really made no sense for the New York *Observer* to be endorsing, given how close their ties were to Trump, and it was just something that was immediately noted by anyone with a half a brain. This was the moment I said, â€˜Look, I have to go.â€™ And it was really more of an instinctual move as an intellectual move. I didnâ€™t plot this out. I donâ€™t have, like, the PR savvy to time my exit to such a moment. It worked out well for me, but when the endorsement came down, I was just sick of everything. I decided it was time to do something else with my life, with journalism, just not at the New York *Observer.*â€? 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/ross-barkan-problem-with-media.html)\]

48.

Social media rules everything now.

“The power of Facebook to adjudicate what is news and what is not is extraordinary and, I think, unprecedented in the history of modern media. William Randolph Hearst, Henry Luce, Walter Cronkite, the *TimesÂ *— no media titan or institution has ever had this kind of reach and power in America. There has to be more scrutiny and more transparency. Even if Facebook is behaving correctly now, as it may well be, that’s no guarantee for the future. What’s also needed is the antidote of stronger competition: What will most keep Facebook honest is the emergence of a wily and inventive upstart occupying the same space in our (social-) media culture. Here’s hoping.” Â 

“Anytime anybody has that stunningly dominant role in the distribution of news, we should be wary and skeptical going in. On the other hand, you also have to go where your readers are. First off, that’s the economics of the business, but secondly, I don’t have impact if I’m not read. And we’re read now — we, the press — 20 times more widely than we were before, which people sometimes have a tendency to forget.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/dean-baquet-problem-with-media.html)\]

“Every media company is trying to figure out ways to defend against the algorithms, and also play along with the algorithms at the same time. Facebook seems to be changing their algorithm every few months or so. Once in a while they’ll announce that they’re doing it. Sometimes they do not. Sometimes you just notice it with an effect on your numbers. So it can really really surprise you. I don’t know any growth-oriented publisher who isn’t to some extent publishing, programming around the Facebook algorithm. If it’s a more sophisticated publisher, they’re understanding that this is a topic that may not do well on Facebook but may do well elsewhere, and try to find an audience for it elsewhere, and may not just cut out those stories, but it definitely affects the stories being done, there’s no question about that. At least it affects the volume of stories being done. Most publishers have editorial integrity and are still going to be doing the stories that they considered to be important, but they’ll just do more of the stories that are important and get a larger audience and fewer of the stories that will get a smaller audience.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/michael-wertheim-problem-with-media.html)\]

â€œYou have to see the relationship of Facebook to news organizations as similar to its relationship with Zynga, the makers of Farmville. Zynga was very, very successful because Facebook drove so many people to it. But then people got sick of seeing this Farmville shit, and Facebook cut them off, and thatâ€™s exactly what Facebook will do when outrage or whatever starts becoming an issue. And they did this, in fact. Last March, Facebook tuned its algorithms a bit to stop sharing as many news stories and to share more status updates. This is partly why people started releasing content for Facebook, which is a deal with the devil if ever there was one, and itâ€™s also why revenues have been dropping. Facebook is effectively exploiting content providers, who have become increasingly dependent on Facebook. So the desperation set in, especially in 2014, 2015, you really started seeing a race to the bottom, where people were generating a huge amount of outrageous material. You saw this particularly with Op-Eds. â€˜I think that Bernie Sanders supporters are Gamergaters,â€™ or â€˜I think that if you eat meat you are racistâ€™ or â€˜I think that if you watch *The Walking Dead* you are as bad as Dylann Roof.â€™ â€?

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/david-auerbach-problem-with-media.html)\]

49.

And it does enclose everybody in customized-news silos.

“In the atomization of media, audiences find the one that’s most gratifying to them, the one that tells them what they want to hear rather than what they don’t want to hear.

Now, I’m a Twitter addict. I probably check my Twitter feed ten times a day. And I think I’m getting this wide range of opinions from different perspectives — I can add it all up and come to my own conclusion. But I’ve built my own news organization by who I’ve chosen to follow on Twitter. I don’t want to pat myself on the back, but you are what you choose to read. That applies to everyone on virtually any subject. If you read broadly, and choose the best of different viewpoints, I think you’re getting very good journalism right now. If you’re reading narrowly, you’re not.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/daniel-okrent-problem-with-media.html)\]

“We want to watch the news we like. I understand that because when the Yankees are winning a game, I will watch the whole game, I will watch Joe Girardi after the game talking about how great they were, I will watch all of the 11 o’clock news reports on the game, I will read the Post, the Daily News, and the Times the next day about how the Yankees won. If the Yankees lose, I turn it off in the middle of the game, I watch nothing, I read nothing, I do not want to hear about it. Now that’s okay, I think, as a baseball fan, not okay as a citizen.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/steven-brill-problem-with-media.html)\]

50.

(Even though, often, those silos are at war with one another.)

“The internet’s greatest social achievement has been in helping people from invisible minorities to find each other, define their identities, and get the rest of society to acknowledge their existence. As a gay guy in an interracial relationship, I’ve seen that firsthand. The internet is a liberating force; I discovered myself on the medium. But it’s these new tribes that have also stirred up conflict. The social-justice movement among gamers also helped stimulate the Gamergate reaction and gave new energy to the alt-right online movement, for instance. It’s liberating to find one’s tribe — but the tribes are often defined in opposition to each other. And now we’re in a state of tribal warfare, playing out on Facebook, Twitter, and other internet battlegrounds. It looks like Somalia, a country of clans, in a war of all against all. And right now it’s hard to see that freedom and conflict go hand in hand.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/nick-denton-problem-with-media.html)\]

51.

And yet it has also reinvigorated things.

“Even though I am a product of ‘legacy media’ — God how I hate that patronizing term! — there’s much I like about the revolution that has transformed the business. The accessibility of so many news sources and voices — Â whether sober or crazy, professional or amateur, whether delivered in video or prose or 140 characters — is a boon to both finding and assessing news as it happens throughout the world. Social media have democratized and fractionalized the dissemination of all kinds of information. This new matrix does often require that you serve as your own editor and police the objectivity, indeed the legitimacy, of the countless sources that flood over the transom. But I believe that news consumers who care about ferreting out the truth will inevitably contrast and compare — they’ll shop for the most reliable sources. Those consumers who don’t have the time or inclination to take that trouble will do what they’ve always done — cede the authority to a single arbiter they trust (rightly or wrongly), whether it be NPR or Fox News or their news feed on Facebook.”Â 

“I grew up in New Orleans in a working-class family, and I had access to two newspapers. That same kid growing up in New Orleans right now, assuming he has access to a computer, can read the *Guardian* for free, can read newspapers and news reports in many languages, often for free, can read Facebook, can look at video, has access to the world, can see art that’s in the Museum of Modern Art on his phone, this vivid reproduction, for free. Put aside — the crisis we’re in is a crisis about the institutions like the New York *Times* and the Washington *Post* that deliver news. But in terms of the quality of journalism, man, I came in last week to help run coverage of what happened in Texas, and I could call up the editor of the video unit and ask for a video. That is not a world I grew up in. That is stunningly better. It’s just better. Journalism is better than it ever was.”Â 

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/dean-baquet-problem-with-media.html)\]

52.

Particularly by diversifying things.

“In the early days of the Huffington Post, we heard plenty of pining for a mythical media past when everything was somehow better. But in many key ways it wasn’t better. The internet has democratized media in unprecedented ways. It’s dramatically lowered the barrier to participation, and as a result it’s allowed a host of new voices into the global conversation. There are an incredible number of successful journalists and media stars who started out just blogging, posting videos on YouTube or tweeting. Just a few years ago, these voices would have gone unheard. It’s hard to imagine the Black Lives Matter movement without the power of the internet to both bear witness to the consequences of an unequal justice system (usually in the form of videos) and to empower people to combine their voices and demand change. So it’s not just about giving new voices the power to go around the old media gatekeepers, but increasing the total power of the media to do its job as the fourth estate and hold government accountable.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/arianna-huffington-problem-with-media.html)\]

“The Internet removes some of the power from white, traditional media and gives a little bit of that power base to communities of color that seek to make sure their stories and their truths are reflected. So when you talk about the Internet as a bad thing, it’s inherently a strike against media diversity. Which is inherently a strike, I think, against the purpose of journalism in the modern era. So if people have such a problem with the Internet, I suggest they get used to it and learn how to adjust. Because it’s not going anywhere.”

\[[Read the Interview Transcript](http://nymag.com/daily/intelligencer/2016/07/jamil-smith-problem-with-media.html)\]

53.

But also by giving people what they want.

“Last fall, a Pew survey asked Americans if they felt their political side has been winning or losing more. Seventy-nine percent of those who lean toward the Republican Party replied that they feel their side is mostly losing — a predictable response, seven years into a Democratic presidency. But 52 percent of those who lean to the Democrats reported that they feel their side is losing. In a two-party system, it is obviously not possible for both parties to be losing a zero-sum contest for power. But this is how we experience divided control — to the red team, ‘losing’ means Barack Obama in the White House, and to the blue team, it means Mitch McConnell and Paul Ryan running the Congress. And it is also how we experience the news media, which is both the battlefield and (in part) the combatants in a never-ending partisan war, one that feels mostly to both sides like miserable defeat.

The increasingly balkanized media may allow us to seek out only the news that reflects our side’s values, but separation does not mean ignorance. A central purpose of the conservative media was to mock and scold the mainstream media for its alleged liberalism. And the new liberal media, from MSNBC commentators to the scattered Jon Stewart empire of comedic rantsplainers, performs an identical function. Everybody is aware of the other side’s beliefs and its media apparatus — at least the most outrageous or mockable elements of them. Making America aware of the awfulness of the news media is a major part of the news media’s job. Not many people are going to feel happy about the media in a world where the media refracts our mutual antipathy. We hate the media mainly because we hate each other.”

*Interviews by Nick Tabor and Jeff Wise.*

*A version of this *article appears in the July 25, 2016 issue of** New York **Magazine.**
