---
title: "Qui veut la peau des bisounours?"
url: http://hacking-social.com/2014/01/15/qui-veut-la-peau-des-bisounours/
keywords: nest,monde,bisounours,violence,cest,faire,brutalisme,quil,veut,peau,brutaliste,travail
---
> « Le terme \[bisounours\] est passé dans le langage courant pour désigner un individu aux idées exagérément naïves ou candides. De même, l’expression « pays des bisounours », utilisée de manière négative, désigne toute situation caractérisée par une exagération de bons sentiments (exemple d’emploi : « on n’est pas au pays des bisounours ! »). En politique, bisounours s’emploie surtout dans la forme négative. « Ce n’est pas les bisounours » signifie « c’est tout de même sérieux » ou « cela ne relève pas du monde merveilleux de l’enfance où tout est gratuit » [Wikipedia](http://fr.wikipedia.org/wiki/Bisounours)

**Lire cet article en PDF :[qui veut la peau des bisounours](http://hacking-social.com/wp-content/uploads/2014/01/qui-veut-la-peau-des-bisounours.pdf)**

** Connaissez-vous les Bisounours ?** Vous savez, ces petites créatures multicolores plus douces que de la guimauve ? Eh bien sachez que ces petits ours incarnent le grand fléau de notre société. Oui vraiment. Pour preuve, c’en est devenu une véritable insulte. Se faire traiter de « connard », de « salaud », d’« enflure », pourquoi pas, ce sont des insultes tout à fait supportables, constituant même une preuve concrète de notre force, de notre puissance. Mais se faire incendier de « bisounours », non! Ce serait tomber bien bas! Quelle honte!

« Bisounours », l’étiquette que l’on accole à la racaille de notre siècle : ceux qui ont de bonnes intentions, ceux qui se refusent à la banalité de la violence et à l’indifférence généralisée, ceux qui cherchent à faire bouger les lignes pacifiquement en respect des opinions contraires, ceux qui se refusent à écraser autrui… Si vous êtes un tant soit peu attentif à votre environnement social, ou à ce que diffusent les médias, vous remarquerez qu’il n’y a plus aucune place à ces « faiblesses » que sont l’altruisme et l’empathie.

**Altruisme, empathie… Comment !** **Cela existe encore ?** J’en ai bien peur. Mais ne vous en faites pas, tout est fait pour éradiquer ce fléau.

Tout ceci est certes ironique de notre part, mais cela est loin d’être le cas dans la bouche d’une majorité d’individus. La société actuelle nous formate à donner des coups, et à en recevoir; à accepter la brutalité comme moyen de parvenir à ses fins; à vanter les violences symboliques; à récompenser les dominateurs; à rabaisser les bien intentionnés… Cela n’a rien de récent, cependant cette sourde violence est devenue bien vicieuse et davantage pernicieuse. Pire, elle est banalisée au point que les minoritaires bien intentionnés sont désormais de véritables marginaux à qui l’on rétorquera devant leur embarras : « si tu n’es pas content, va voir ailleurs », « de toute façon y’a pire ailleurs », « le monde réel, ce n’est pas le pays des bisounours ».

### **Pourquoi les Bisounours ont mauvaise presse? Pourquoi les bonnes actions et les bonnes intentions donnent l’impression d’être criminalisés? Ceux qu’on nomme Bisounours sont-ils des idéalistes naïfs, bien loin des réalités?**

À ces quelques questions, il faut en ajouter une autre, et ce sera d’ailleurs le premier point que nous devrons développer: pour qui les bisounours sont-ils un problème? De toute évidence, il semble qu’il y ait derrière cette insulte une certaine mentalité, une certaine conception des individus et de la société qui condamne les bien intentionnés. Cette conception qui constitue le nouveau paradigme dominant, nous le nommerons  « brutalisme »[\[1\]](#_ftn1).

Le Bisounours est une espèce en voie de disparition, forcée à l’exil par ce paradigme dominant, le brutalisme étant l’émanation rationnelle et sophistique de la banalisation de la brutalité dans les rapports humains, plus particulièrement dans la sphère du travail.

**Le brutalisme : banalité de la brutalité au quotidien**

***Qu’est-ce qu’une « brute » dans le contexte du brutalisme?***

Par « brute », on entend un individu sans finesse, violent, grossier. Ce que nous nommons brutalisme est une nouvelle forme de brutalité faisant figure de paradigme dans nos sociétés. Le brutalisme comme mode d’être accentue la violence, l’emportement, la quête de domination, l’écrasement d’autrui.

Par violence, nous n’entendons pas uniquement cette violence directe qui transite par les muscles et les armes, mais une violence plus lourde, plus sourde, plus sournoise, difficile à identifier. Il s’agit des violences dans les rapports humains, ou de la violence symbolique en général.

Par manque de finesse, nous n’entendons pas un manque d’intelligence. Bien au contraire, nous verrons que le brutalisme n’est pas en déficit de raison, mais bien au contraire, il est en excès de raison. Le brutalisme prend racine sur une ultra-rationalisation dans tous les domaines: politique, travail, santé, éducation, culture… Par manque de finesse et par grossièreté, nous entendons une difficulté à la nuance et à l’acceptation de la différence, mais aussi une inclination à enfoncer les portes, à ne se donner aucune limite (ou en tout cas aucune limite morale).

***Les quatre principales caractéristiques du brutalisme***

– **La guerre perpétuelle:** le monde est un vaste champ de bataille. Ses guerres se font par des stratégies de communication et par diverses manipulations sur les individus et les groupes. Le brutalisme est une nouvelle forme d’un machiavélisme vulgaire et difforme dans toutes les sphères de la société, ce qui a pour conséquence des violences concrètes ou symboliques.

– **La société du spectacle:** exister consiste à se faire voir et entendre avec force, mais tout est dans l’apparence et dans les jeux des représentations. Il s’agit de faire du bruit, faire du buzz qu’importe le fond.

– **La réussite:** la seule visée est la réussite, qui n’est rien de moins qu’une course au privilège et qui repose sur la chimère de la croissance infinie. La réussite est une victoire sur l’ennemi, une prise de pouvoir ou de territoire envers et contre tous.

– **L’ambition**: la valeur pour viser cette réussite est l’ambition, l’ambition est devenue la plus haute qualité de notre société. L’ambition n’est rien d’autre que la témérité du soldat à la conquête d’un territoire qu’on lui ordonne implicitement d’envahir.

***Achille ou Ulysse?***

Il y a deux grandes figures à ce brutalisme, deux grandes figures que nous empruntons à l’épopée homérique:

– **Le brutalisme d’Achille**: c’est la force, la violence à l’état brut. Le combat se fait par le muscle. On pourrait y retrouver certains politiques, activistes ou militants violents.

– **Le brutalisme d’Ulysse**: c’est la ruse. La violence est toujours présente, mais elle est plus insidieuse, vicieuse, invisible. L’arme n’est pas le muscle, mais les aptitudes à la manipulation et à l’éloquence (on y trouve par exemple tout ce qui touche l’organisation des groupes et des individus, dont le management anglo-saxon).

De ces deux types de brutalisme, le dernier est sans doute le plus important. La manipulation est devenue simple banalité dans nos sociétés actuelles. Peu s’interroge par exemple de trouver une littérature chaque jour plus abondante sur l’apprentissage des techniques de manipulation dans les entreprises ou dans la société pour parvenir à la « réussite ».

« Se faire manipuler » n’inquiète plus personne. Quoi de plus normal! C’en est même devenu une compétence incontournable pour évoluer dans la société.

**Comment identifier les brutalistes?**

[![](https://www.hacking-social.com/wp-content/uploads/2014/01/url-1.jpg)](https://www.hacking-social.com/wp-content/uploads/2014/01/url-1.jpg)

On peut citer différentes qualités que l’on rencontre souvent chez les brutalistes. Cette liste de caractéristique est exhaustive, et nous ne disons pas qu’un brutaliste correspond à tous ces paramètres sans exception (tout dépend notamment de la vie qu’il mène en société ainsi que ses convictions, les caractéristiques ne seront pas identiques entre un PDG et un activiste par exemple). Il n’empêche que le brutaliste pourrait lui-même se reconnaître sur plusieurs de ces points.

-   Le brutaliste considère que la gentillesse, sous toutes ses formes, est une faiblesse, qu’elle n’est pas « efficace ».
-   Le brutaliste considère que la courtoisie ne vaut que pour ses égaux ou ses supérieurs, à des fins utilitaires ou pour l’apparence (les politiques par exemple sont doués pour la courtoisie d’apparence).
-   Le brutaliste considère que ceux qui ne pensent pas comme lui sont des ennemis, ou une menace potentielle.
-   Le brutaliste considère le stress généralement comme une bonne chose, une nécessité parfois, notamment dans le monde du travail. Il parle souvent de « bon stress ». Dans le monde du travail, quelqu’un qui semble « tranquille », jamais « stressé » est pour ses raisons considéré comme un feignant, un mauvais bosseur.
-   Le brutaliste évolue par concurrence, compétition et comparaison. Il évalue sa réussite en regardant l’échec des autres. Ainsi, la dictature du « chiffre » encourage le brutalisme tout en étant sa conséquence.
-   Le brutaliste use souvent d’un vocabulaire misogyne et homophobe: « on n’est pas des PD », « on n’est pas des femmelettes », « tu n’as pas de couilles »… La virilité est de mise, même pour les femmes.
-   Le brutaliste use souvent d’un vocabulaire belliciste: « killer », « anéantir », « cible », « opération », « opérationnel », « attaquer », « lutter », « serrer les rangs », « allié », « adversaire », « putsch », « ennemi », « hostile »… mais aussi d’un vocabulaire de la drogue: « doper », « accro », « shooter », « came »…
-   Le brutaliste est en quête de contrôle de lui-même, mais surtout des autres.
-   Le brutaliste est partisan de « la fin justifie les moyens ».
-   Le brutaliste considère généralement que la colère, la haine, le mépris, la peur, le stress, l’angoisse sont des forces sur lesquelles on peut prendre élan (en politique, cela se traduit par le populisme).
-   Le brutaliste use toujours des mêmes arguments pour contrer une idée ou un adversaire qui lui déplaît: « has been », « ça ne sert à rien », « c’est dépassé », « on ne vit pas dans le monde des bisounours », « ici c’est le monde réel », « il y a pire ailleurs », « tu réfléchis trop »… Il se contente de petites phrases, ou de quelques mots cinglants, car il ne veut pas mener de véritables débats, encore moins de réflexions (non pas qu’il n’en soit pas capable, mais cela lui prendrait top de temps, il n’en voit pas l’intérêt: réfléchir ne sert à rien, si ce n’est se compliquer la vie). Il vit dans un monde en guerre perpétuelle, alors il tire à vue, qu’importe la façon pourvu que ce soit efficace et que ça ne lui porte pas atteinte à long terme.
-   Le brutaliste est en général misanthrope, il peut cependant se prêter à de nobles causes libertaires et humanitaires; cela revient bien souvent à se donner une bonne image, ou à se trouver une bonne excuse pour justifier sa violence et son mépris.
-   Le brutaliste vise la réussite (souvent en quête de promotion), ce qui passe généralement par la sphère professionnelle et par la volonté de se conformer à un modèle matériel et familial parfait (marié ou en couple, avec enfants, propriétaire d’une maison, au moins deux voitures, des voyages dans d’autres pays au moins une fois par an, le dernier portable à la mode…).  Il considère une vie réussie selon les biens en sa possession ou les pouvoirs qu’il a obtenus sur les autres.
-   Le brutaliste est en quelque sorte fataliste, il considère bien souvent que le monde est comme il est, qu’on ne peut rien faire contre ça. Ce qui est un grand paradoxe de sa part, car il se considère lui-même comme absolument libre et comme ayant un pouvoir sur son environnement.
-   Les brutalistes ont souvent du mépris pour les gens engagés dans des causes. Quant aux brutalistes eux-mêmes engagés dans des causes, les plus nobles soient-elles (car oui, malheureusement il y en a, et ils sont souvent d’ailleurs responsables des dérives dans leur mouvement), ils ont du mépris pour ceux qui ne sont pas engagés comme eux. Le schéma étant le suivant: « tu n’as pas les mêmes convictions que moi, alors tu es l’ennemi ». En cela, le brutaliste qui se bat pour sa réussite professionnelle et le brutaliste qui se bat pour une cause obéissent à la même mécanique quant aux actions et aux comportements.
-   Lorsque le brutaliste cherche à faire bouger les choses, il n’envisage de le faire qu’en tapant du poing, qu’en écrasant ceux qu’il considère comme des adversaires. Il considère qu’on ne peut changer les choses sans conflits, sans dommages collatéraux. Ce n’est pas faire bouger les choses pour le meilleur qu’il veut, ce qu’il souhaite c’est créer le conflit ou s’intégrer dans le conflit. Les brutalistes ne se battent pas pour des objectifs, même s’ils le présentent ainsi (allant jusqu’à se mentir à eux-mêmes): ils aiment se battre, voilà tout. La société est pour eux un vaste champ de bataille. Ils ne peuvent concevoir d’autres façons de fonctionner.
-   Il n’y a pas de structures sociales particulières qui correspondent aux brutalistes, ni d’âges, de sexes, de convictions politiques ou religieuses. On trouve des brutalistes de droite comme de gauche, athée ou croyant, conservateur ou révolutionnaire, travailleur ou chômeur, riche ou pauvre… Le brutalisme est un état d’esprit global, consécutif de diverses influences et paramètres (que nous verrons plus tard).
-   En général, le brutaliste a une aversion maladive pour certaines personnalités, comme Stephan Hessel par exemple qu’il considère souvent comme un Bisounours ou un oui-oui (exemple d’Eric Zemmour)

&nbsp;

-   Le brutaliste aime à dire que les bons sentiments le font vomir.
-   Les brutalistes donneraient sans hésitation raison à Hobbes « l’homme est un loup pour l’homme », et ils s’appliquent avec beaucoup de zèle à transformer les groupes humains en de véritables meutes. Soit ils visent à devenir le loup alpha, soit ils cherchent à ne pas se faire dévorer par le loup alpha en pactisant explicitement ou implicitement avec lui. Et parce que les groupes humains deviennent des meutes, le souci d’appartenance à un groupe identifié est d’autant plus important: en guerre, il faut choisir son camp.

**Les arguments brutalistes**

Le brutalisme fonctionne selon des arguments non valides que nous formulons sous la forme de syllogisme (=argument en trois temps).

Voici donc quelques exemples de la logique déviante du brutaliste.

**▪ L’argument  du clan**

« –*Les gens qui ne sont pas et ne pensent pas comme nous sont nos ennemis[![](https://www.hacking-social.com/wp-content/uploads/2014/01/la-meute-qui-est-entrain-de-manger-le-proit-1.jpg)](https://www.hacking-social.com/wp-content/uploads/2014/01/la-meute-qui-est-entrain-de-manger-le-proit-1.jpg)*

*-Or, nous devons combattre violemment nos ennemis*

*-Donc ceux qui ne sont pas et ne pensent pas comme nous, nous devons les  combattre violemment »*

On touche là un point très sensible, car c’est un argument qui fonctionne très bien auprès des activistes et des militants (quelle que soit la cause): au nom d’une cause, on peut casser, détruire, nuire, violenter, insulter, écraser… Ce type de logique a pour conséquence de créer des clans, ce qui favorise l’effet de meute, le souci d’appartenance et le rejet de l’extérieur ou de la différence.

**▪ L’argument de la loi du plus fort**

« –*Pour réussir, il faut être le plus fort[![](https://www.hacking-social.com/wp-content/uploads/2014/01/95052-muskelshirt-muscle-chest-shirt-1.jpg)](https://www.hacking-social.com/wp-content/uploads/2014/01/95052-muskelshirt-muscle-chest-shirt-1.jpg)*

*-Or, le plus fort c’est celui qui mange les plus faibles*

*-Donc, pour réussir, nous devons manger les plus faibles. »*

C’est une logique vieille comme le monde, si ce n’est qu’elle prend une tout autre forme aujourd’hui. Manger les plus faibles ne consiste plus à les violenter physiquement ou à les tuer; manger les plus faibles consiste à les écraser, à les dominer symboliquement, à les détruire psychologiquement. La souffrance au travail par exemple n’est généralement pas accidentelle, les travailleurs souffrants se font véritablement manger par ceux qui veulent réussir.

***▪ L’argument de la réussite (ou pour le dire autrement: la compétitivité)***

*« -La réussite des uns suppose l’échec des autres[![](https://www.hacking-social.com/wp-content/uploads/2014/01/ecrasement-1.jpg)](https://www.hacking-social.com/wp-content/uploads/2014/01/ecrasement-1.jpg)*

*-Or je veux réussir*

*-Donc, pour réussir, je dois précipiter l’échec des autres »*

C’est une conception plus répandue qu’on le pense. Encore une fois, il suffit d’observer le monde du travail. C’est la base même du capitalisme libéral: une entreprise en échec favorisera l’entreprise concurrente, et inversement, une entreprise qui réussit fera de l’ombre à l’entreprise concurrente. On appelle cela la compétitivité, et à notre époque la compétitivité, c’est « chouette »! On veut que tout soit compétitif jusqu’aux hôpitaux, aux écoles, aux universités, aux différents bureaux Pôle emploi, aux infractions, aux compétences artistiques… Le monde est devenu un vaste champ de compétition, du tous contre tous, sans limites ni modérations.

Quand on entend parler « compétitivité » à longueur de journée, pas étonnant qu’on en vienne à des pratiques qui visent à écraser l’autre au nom de sa propre réussite (voir l’argument précédent).

***▪ L’argument du buzz***

*« -L’efficacité et l’influence ne se mesurent qu’à l’attention engendrée (=le buzz)[![](https://www.hacking-social.com/wp-content/uploads/2014/01/bruta-1.jpg)](https://www.hacking-social.com/wp-content/uploads/2014/01/bruta-1.jpg)*

*-Or, c’est la forme (l’apparence) qui fait le buzz, non le fond (le contenu)*

*-Donc, si nous voulons être influents et tendre à l’efficacité, c’est la forme qui prime et non le fond, afin de faire le plus de bruit »*

\* *

\*           * Nos sociétés, les médias en particulier, obéissent à l’attentionalisme: ce qui compte c’est de faire du bruit afin d’obtenir le plus d’attention. Ce n’est plus la qualité d’une information ou d’un produit qui compte, c’est son audience ou son impact sur les consommateurs.

D’un point de vue individuel, l’important est de se faire voir, entendre, connaître, notamment à partir des NTIC – **N**ouvelles **T**echnologies de l’**I**nformation et de la **C**ommunication – (réseaux sociaux par exemple).

Dans le monde du travail, on n’embauche plus les gens selon leurs compétences, mais à partir de leurs réseaux, de leurs connaissances (cela s’appelle du piston, mais aujourd’hui on nomme cela « réseau », cela fait moins péjoratif).

Ainsi, pour s’imposer dans la société, il faut faire du bruit, qu’importent les moyens. Compétences, professionnalisme, dialogue n’ont plus leur place, remplacés par l’influence, les réseaux et les techniques de Com.

Enfin, et on en revient à l’argument de la compétitivité, il s’agit de faire du bruit par rapport à ses concurrents ou adversaires, et faire du bruit consiste bien souvent à faire taire autrui. Quant à ceux qui veulent dialoguer et débattre, ils n’ont plus du tout leur place dans cette nouvelle société.

**L’antinomie du brutalisme**

[![](https://www.hacking-social.com/wp-content/uploads/2014/01/paradoxe_menteur-1.jpg)](https://www.hacking-social.com/wp-content/uploads/2014/01/paradoxe_menteur-1.jpg)

Une antinomie est une contradiction entre deux principes ou idées qui semblent pourtant coexister dans un même système de pensée.

L’antinomie du brutalisme est la suivante:

> –**Idée 1:** Nous n’avons aucun pouvoir sur le monde, sur notre environnement. Le monde est comme il est, on ne peut rien faire contre cela, si ce n’est avec violence. Tout changement nécessiterait donc une révolution violente, des conflits, des luttes…
>
> –**Idée 2:** L’individu est entièrement libre. Tout n’est que question de volonté et de force. Il a le pouvoir de devenir et de faire ce qu’il souhaite.

Vous comprendrez que ces deux thèses sont contradictoires. Comment l’individu peut-il être absolument libre s’il n’a aucune liberté dans la société? Comment peut-il être maître de sa « réussite » sociale tout en n’ayant aucun pouvoir sur cette même sphère sociale? Comment peut-il prétendre à une liberté absolue dans un monde qui n’autorise aucune liberté individuelle?

Pour les brutalistes, ces deux idées sont d’abord pratiques. La première idée permet au brutaliste de se désintéresser de tout ce qui ne le concerne pas ou ne l’intéresse pas. Par exemple, un brutaliste, face à ceux qui veulent changer les choses, dira: « C’est comme ça », « il y a pire ailleurs », « C’est comme ça que le monde fonctionne », « si tu n’es pas content, dégage! ». En cela, le brutaliste est le garant du monde tel qu’il est. La plupart de ces brutalistes ne s’engageront pas dans des causes sociales, humanitaires ou écologiques, sauf s’ils considèrent que ces causes sont dans leur intérêt propre, qu’ils vont y gagner (en image par exemple).

La seconde idée est le ciment de la compétition, de la concurrence, de la loi du plus fort. Le brutaliste considère que celui qui ne réussit pas ne peut s’en prendre qu’à lui-même, cela est preuve de sa faiblesse. Le brutaliste réitère les vieilles chimères religieuses, instaurant une nouvelle forme de culpabilité et de *remords du passé*, de *l’angoisse du futur*, tout en instaurant une nouvelle douleur: le *stress du présent.*

Affirmer une liberté absolue, penser que tout est possible tant qu’on y met sa force, et au contraire penser qu’on ne peut rien sur rien, voilà la double naïveté du brutalisme.

Cette antinomie conduit donc à une certaine logique absurde propre aux sociétés actuelles: nous ne sommes pas maîtres du jeu, mais on doit tout faire pour gagner le jeu.

C’est à travers cette logique que l’on trouvera deux pôles opposés qui sont deux modes du brutalisme:

– **L’indifférence** : ceux qui ne pensent qu’à leur propre réussite, et qui sont indifférents à tout le reste. Ils ne se préoccupent que très peu des autres, uniquement de leur ambition. Ils pensent qu’on ne peut changer la société, il faut s’y soumettre (***idée 1***). Mais on peut gravir les échelons pour réussir, car l’individu est absolument libre (***idée 2***), quitte à écraser l’autre. *Par exemple: les soldats du travail, cadres, politiques…*

– **Le fanatisme:** ceux qui ne pensent qu’à la réussite de leur cause, et qui sont indifférents à tout le reste. Ils ne se préoccupent pas des autres, même s’ils se présentent parfois comme des altruistes et des pacifistes. Ils pensent qu’on ne peut changer la société (***idée 1***), sauf à travers la violence ou l’éclat des actions individuelles, car l’individu est absolument libre *(**idée 2***). *Par exemple: certains syndicalistes, religieux, certains activistes et révolutionnaires…*

Pas plus que l’indifférent ne remettra pas en cause son ambition et les moyens qu’il met en œuvre, le fanatique ne remettra pas en cause ses ambitions et les moyens qu’il met en œuvre au nom de sa cause.

Dans les deux cas, il y a violence dans la mise en place des projets ou objectifs, ainsi qu’un rejet de la différence. L’indifférent est dans le déni des autres, le fanatique est dans le rejet de ceux qui ne pensent pas comme lui.

L’indifférence et le fanatisme brutaliste sont deux pôles opposés, mais ils reposent sur une même antinomie: *nous n’avons aucun pouvoir*/*nous sommes absolument libre*. En cela, fanatisme et indifférence sont deux faces d’une même pièce.

Le mal-être et les actions du brutaliste reposent sur la confrontation de deux positions inconciliables.

Bien sûr, il faudrait nuancer, il y a différents degrés entre ces deux pôles, mais ces degrés s’organisent autour de ces deux idées paradoxales.

Il y a une origine à cette antinomie. Pour comprendre cela, il nous faut désormais proposer quelques pistes sur l’origine du brutalisme.

**Pourquoi cette montée progressive du brutalisme?**

La question qu’il faut désormais poser est « pourquoi »? Pourquoi ce brutalisme dominant contre des bisounours agonisants? Depuis quand est-ce devenu un mal de vouloir bien faire les choses, d’éviter le conflit violent, de respecter son interlocuteur?

Cela mériterait une très large réflexion, mais voici quelques arguments que nous pourrions avancer.

**Le brutalisme: émanation de l’idéologie du travail**

[![](https://www.hacking-social.com/wp-content/uploads/2014/01/ambition-1.jpg)](https://www.hacking-social.com/wp-content/uploads/2014/01/ambition-1.jpg)

> « La société est présentée sur le modèle des sports de combat, avec vocabulaire et images guerrières. Celles et ceux qui ne sont ni gagnants, ni gagneurs se trouvent rejetés vers les marges de la société dont ils n’ont rien à attendre. La violence de celle-ci suscite des contre-violences, des désaffections, des nostalgies agressivement régressives ou réactionnaires »
>
> **André Gorz**, *Pourquoi la société salariale a besoin de nouveaux valets?*

\* *

Nous ne jurons que par le travail. La société s’organise autour du travail. Les individus construisent leur vie selon leur travail ou leur projet professionnel. L’idéologie du travail repose sur l’idée que le travail c’est la vie. Comme si la vie se « méritait », l’expression « gagner sa vie » est signifiante à plus d’un titre. Tout autant que l’expression « entrer dans la vie active »: les actifs étant ceux qui travaillent. Comment devrions-nous nommer les sans-emplois? Des passifs?

La course à l’emploi, la course à la promotion, à la meilleure retraite, aux meilleurs avantages, font de la compétition ou de la concurrence les principaux modes d’être de ce monde dominé par l’idéologie du travail. Il suffit de penser à ces repas de famille interminables où chacun vante son propre nombril sur ses dernières péripéties au travail, faisant la promotion d’une vie qu’il juge meilleure que celle des autres sous le prétexte qu’il « gagne plus » ou qu’il a plus d’avantages.

Ceux qui ont un emploi feront tout pour le préserver ou évoluer vers les plus hauts sommets. Ceux qui n’ont pas d’emploi seront plongés dans la honte et l’angoisse, transformant la quête de l’emploi comme la plus importante des quêtes de leur existence.

Aujourd’hui, la plupart des individus n’envisagent plus de donner un sens à leur vie qu’à travers leur vie professionnelle. Et parce que le monde professionnel repose sur la compétition des uns et des autres, cela autorise diverses violences symboliques ou réelles. De plus, pour résister soi-même aux coups, il est nécessaire de se forger une « carapace », cuir épais qui ne laisse plus passer une once de sensibilité. Et les insensibles peuvent à leur tour donner des coups, c’est un cercle vicieux.

Le travail aujourd’hui ne se définit ni par l’effort ni par la production, mais par le salaire. Ceux qui travaillent dur dans des projets non professionnels (entendez par là ceux qui ne touchent pas de salaire, comme les bénévoles) ne seront pas considérés comme des travailleurs. Pire, un individu qui use son temps à aider autrui, à lui rendre service, à l’accompagner bénévolement, à participer à des projets non lucratifs, ne sera pas considéré comme actif si celui-ci est sans emploi. Il n’aura peu, voire aucune reconnaissance, quant à sa contribution à la société, alors que l’impact de ses actions est bien plus important qu’un simple employé de bureau.

Travailler, c’est aussi rentrer dans les clous, rentrer dans le moule. Et pour rentrer dans les clous, il faut se faire violence, ce qui se traduit souvent par faire violence sur les autres.

**Le brutalisme ou le règne de la logique à outrance et d’un rationalisme sans limites**

Il faut mettre en évidence deux attitudes propres à notre société:

-   La rationalisation de toutes les activités humaines,
-   La rationalité excessive comme mode de pensée.

Commençons par le premier point.

Aujourd’hui, tout se mesure. Rien de tel qu’un sondage pour déterminer l’opinion ou l’attitude d’un groupe. Tout devient nombre, quantifiable à l’excès. Le qualitatif est lui-même dépendant du quantitatif.

L’un des critères semble-t-il indubitable est celui du chiffre: chiffre en terme de temps de travail, en terme de production, en terme de vente, en terme de client ou de patient, en terme de bénéfice, en terme de chômage, en terme de prévision… Le pourquoi et le comment ne se posent qu’après la plus importante des questions à laquelle aucune autre ne peut se substituer: combien?

Le chiffre appliqué à toute activité humaine déshumanise l’activité elle-même en la rendant abstraite. Il en est de même quand le chiffre se substitue complètement aux faits comme le chômage, l’insécurité, le bonheur. C’est une nouvelle forme d’aliénation, non plus seulement de la sphère professionnelle, mais de l’ensemble de la société.

Quelles en sont les conséquences? La course à la rentabilité, la légitimation de la concurrence et de la compétition, l’accélération des rythmes de vie, la banalisation de l’évaluation constante, le stress, la perte de repères, la surenchère d’effort et l’absence véritable de la reconnaissance de ses efforts (si ce n’est en terme de courbe)… Tout cela est propice au stress, à la brutalité, à l’instauration de rapports violents, au conflit.

L’autre attitude, qui complète la précédente (qui en est même à l’origine et qui s’en nourrit en retour) est la rationalité excessive: tout doit se justifier dans la visée d’un but précis pour la plus grande efficacité. Tout le reste doit être éjecté sans sommation. C’est ainsi que la « fin justifie les moyens », que les questions éthiques sont mises au placard. Là-dessus repose l’économisme, où rien n’est gratuit: tout est pensé, quadrillé, de l’activité jusqu’au comportement, voir jusqu’à la tenue de travail (pour exemple, vous pouvez découvrir cet épisode d’Horizon qui traite du sujet, ci-dessous).

C’est dans cette optique que s’instaure le management anglo-saxon. Le naturel, la sincérité n’ont plus leurs places. L’individu doit devenir un fin calculateur, sans quoi il se fera dévorer par les plus forts. Il ne s’agit pas d’une loi de la jungle, dont le paradigme est l’animal, mais plutôt d’une loi déshumanisée, dont le paradigme est… la robotique. Car là est finalement la destination fatale de la rationalisation excessive : la transformation de l’humain en machine, programmé pour la plus grande efficacité. Et les machines qui ne fonctionnent pas iront directement à la casse (cela s’appelle « pôle emploi »).

C’est sur ce socle que repose le brutalisme contemporain.

Expérimentation, créativité, sensibilité, spontanéité, sincérité, repos, quiétude… Tout cela n’a plus sa place dans un cadre rationnel.

**Le brutalisme, conséquence d’une société de spectacle**

\*            * Selon Guy Debord, « le spectacle n’est pas un ensemble d’images, mais un rapport social entre des personnes, médiatisées par des images » (dans *La Société du spectacle*). La violence se fait désormais par l’image et le paraître, violence symbolique qui organise les rapports sociaux. Le dialogue devient communication, l’être devient l’avoir, l’apparaître devient paraître.

Les individus ne vivent plus qu’à travers des images qui les formatent, obéissant à des archétypes préfabriqués imposés par les médias (eux-mêmes vecteur du paradigme dominant). Là encore, l’aliénation est constante, les individus souffrent de ne plus se reconnaitre eux-mêmes. L’image préfabriquée décale le réel, alourdit les rapports, empêche toute sincérité. Être est devenu un faire-semblant.

De plus, les médias et les réseaux sociaux ne nous rapprochent pas, ils nous éloignent les uns des autres, car tout devient spectacle, représentation. Nous devenons spectateurs de notre propre vie.

Les médias actuels (quels qu’ils soient) ne cessent de nous proposer un catalogue de modèle de vie, sans cesse renouvelé. Les existences ne sont plus que des prêts-à-porter. Les individus choisissent des existences préfabriquées, aussi solides et originales qu’un meuble Ikea. Et si nous meublons aussi facilement notre vie selon ces modèles préfabriqués et aseptisés, c’est bien pour combler un vide.

**Le brutalisme ou le déni d’une crise existentielle**

[![](https://www.hacking-social.com/wp-content/uploads/2014/01/radeau-1.jpg)](https://www.hacking-social.com/wp-content/uploads/2014/01/radeau-1.jpg)

Depuis le XVIII-XIXème siècle, les idoles ne cessent de tomber: religions, figures d’autorité…. Cela est une bonne chose, si ce n’est qu’au lieu de construire sur des bases saines, nous avons préféré combler les vides avec tout et n’importe quoi sous la mouvance d’angoisses existentielles.

Car c’est peut-être là la principale définition du brutalisme: **le brutaliste a peur du vide**. Il est un boulimique existentiel, qui croit combler des vides et des angoisses en consommant ou en débattant sans cesse.

L’indifférent qui ne pense qu’à sa réussite personnelle, ou le fanatique qui se réfugie aveuglément dans des causes qu’il ne médite pas ont ceci en commun: ils se débattent pour maintenir la tête hors de l’eau.

Face à la peur du vide ou la peur de sombrer en eaux profondes, il y a plusieurs attitudes:

-   Se maintenir à la première planche. Ces gens-là trouvent une bouée de fortune à laquelle s’accrocher, et ils n’en démordent pas. C’est ainsi que certains vouent leur existence à leur travail, le travail étant leur planche de Salut.
-   Nager dans le vide (c’est là qu’on retrouve davantage les brutalistes). Quand on ne maîtrise plus rien, quand on est soumis à l’inévitable, on préfère bien souvent se débattre. Se débattre consiste à agir sans cesse et sans fin, faire du bruit, crier le plus fort.

Nous sommes en effet comme des naufragés en eaux troubles et les brutalistes épuisent toutes leurs forces à bouger chaque muscle dans tous les sens afin d’éviter la noyade.

Les brutalistes se débattent sans cesse dans un monde qui leur échappe ou les dépasse, ou ils tentent de se raccrocher à la première planche.

S’ils veulent sans cesse faire du bruit, c’est pour crier leur mal-être, un dernier cri agonisant avant une fin inévitable. Leur volonté n’est pas celle d’un constructeur, mais plutôt celle d’un kamikaze. Le brutaliste est un condamné à la peine capitale qui dans le couloir de la mort croit qu’il a le droit de tout s’accorder comme dernière volonté. De là découle une situation d’urgence: le brutaliste est un homme pressé. Il faut éviter ce qui prend trop de temps, ne pas hésiter à user de raccourcis. Les techniques élaborés depuis un demi-siècle répondent à cette urgence, et nous ne parlons pas de l’évolution des transports, mais des différents dispositifs mis en œuvre, que ce soit dans le monde du travail, dans les usages de la communication, ou de la vie de tous les jours.

Le brutaliste n’a pas forcément peur de la mort, il a surtout peur de l’oubli, il a peur qu’on l’oublie. Il mène alors une vie à triple vitesse, dont le but est de laisser sa marque, son empreinte, ou au moins de se donner l’impression que sa vie a eu une certaine valeur, valeur qui se mesure à la richesse, aux privilèges ou aux actions accumulées.

**Le brutalisme par naïveté, manque de créativité et d’enthousiasme**

Les tenants du brutalisme, ceux qui considèrent que la révolte passe par la violence, que la liberté des uns ne peut qu’empiéter sur celle des autres, que la seule façon de ne pas se faire manger, c’est de bouffer l’autre, que les bonnes intentions sont vouées à l’échec, que la politesse et la courtoisie sont dépassées (sauf s’ils sont utiles)… ceux-là font preuve d’une grande naïveté, se laissant enchaîner par leurs inclinations les plus viles et les plus égocentrées. Ils ont la naïveté de voir le monde comme une jungle ou comme une grande machinerie, ils ont la naïveté d’imaginer que la liberté est absolue, synonyme de « je fais ce que je veux », et qu’au nom de cette prétendue liberté, on peut porter atteinte à celle des autres. Ils visent à dominer un environnement qu’ils ne maitrisent pas, ou tout de moins visent à ne pas se laisser détruire par cet environnement dont ils ne sont pas maîtres. Les brutalistes alternent entre coup d’éclat spectaculaire et indifférence générale. Ce sont leurs deux principaux modes d’être. Ils n’entrevoient pas d’autres possibilités ou formes d’actions.

Dans l’urgence de l’existence qu’ils mènent, il n’y a plus de place pour la créativité, l’expérimentation, la réflexion. Tout ce qui échappe aux « modèles » prédéfinis est à proscrire. Ce qui n’est pas ambitieux, ce qui ne vise la réussite, ce qui n’est pas lutte ou spectaculaire,  n’intéresse pas le brutaliste.

** **

**Quelques exemples du brutalisme dans divers contextes**

** **

**→ le brutalisme dans les commentaires des articles sur le net :**

Ils sont extrêmement nombreux, très faciles à repérer :

<http://reseauinternational.net/2013/12/19/liste-des-arnaques-repertoriees-sur-facebook-comment-y-echapper/>  ; l’article décrit les arnaques afin de prémunir les utilisateurs Facebook. Voici un des commentaires en bas de l’article  :

> **« carla ghiglieri** [19 décembre 2013 à 15:38](http://reseauinternational.net/2013/12/19/liste-des-arnaques-repertoriees-sur-facebook-comment-y-echapper/comment-page-1/#comment-9965) Si on n’a pas encore compris que Facebook c’est l’ennemi, on merite tout ça et pire. »

Pour le brutaliste, si on fait un mauvais choix, on mérite d’être la victime d’arnaques. Donc, l’article informant desdites arnaques n’aurait aucune utilité. Autrement dit, et pour poursuivre cette logique brutaliste, si vous prenez une route déconseillée par vos proches, vous méritez d’avoir un grave accident…

-   [](http://blogs.rue89.nouvelobs.com/ne-le-dis-a-personne/2013/12/05/le-secret-de-famille-de-justine-quand-jai-su-quil-netait-pas-mon-pere-jai-saute-de-joie-231779)

    http://blogs.rue89.nouvelobs.com/ne-le-dis-a-personne/2013/12/05/le-secret-de-famille-de-justine-quand-jai-su-quil-netait-pas-mon-pere-jai-saute-de-joie-231779

    ; l’article décrit une histoire de famille où la fille découvre que son père n’est pas son père biologique et s’en voit rassurée car c’est loin d’être le père idéal :

> [**Géraaaaard**](http://riverains.rue89.nouvelobs.com/geraaaaard) :Quel bordel dans certaines familles. On s’étonne que des gamins soient à côté de la plaque.
>
> [**Benoît Brisefer**](http://riverains.rue89.nouvelobs.com/benoit-brisefer) répond à [*Géraaaaard*](http://riverains.rue89.nouvelobs.com/geraaaaard) : Encore un adolescent qui croit à la famille idéale. Misère des bisounours…

Donc, selon le brutaliste, il faut abandonner l’idée que ce ne soit pas le gros bordel dans certaines familles (la famille idéale est selon lui la famille banale, sans trop d’histoire). Le brutaliste est extrêmement fataliste au point d’en rejeter l’idée même de famille « normale » : pour lui tout est chaos sur le champ de bataille. Il est misérable de penser qu’il puisse exister des familles banales, il faut de l’action!

**→ le brutalisme dans les articles de journalistes :**

L’article critique un documentaire sur deux hommes adoptant des jumeaux. Le film est émouvant, tendre comme souvent bon nombre de documentaires sur la maternité/paternité, qu’elle soit hétérosexuelle ou homosexuelle. L’inverse n’existe pour ainsi dire pas, tout simplement parce il doit être assez difficile de trouver de mauvais parents attendant avec haine leur enfant prêt à être filmé en train de le secouer.

La journaliste critique abondamment le documentaire, car il est trop « tendre », or un documentaire se doit d’être réaliste, et tant de tendresse n’est pas réaliste selon son opinion.

Jugez par vous-même:

> « **Prière de s’attendrir**
>
> Le style nouveau est arrivé: c’est une forme de réalisme-socialiste adaptée au despotisme doux d’aujourd’hui. Au lieu des ouvrières au sourire éclatant sur fond de kolkhoze et de moissons blondes, on a un couple gay au sourire ému sur fond de pouponnière. Mais c’est la même imagerie naïve, le même triomphalisme porteur des lendemains qui chantent. François, Jérôme, Colleen, la mère porteuse, et tout leur entourage existent vraiment. Ils sont les modèles et les héros de ce chromo radieux. Radieux, comme le sourire du médecin. Radieux, comme le sourire du juge qui délivre les passeports.
>
> Pas de pensée dans le film, pas de pulsions non plus, pas d’instincts (à peine une larme furtive de la mère). Uniquement des affects, très doux, très bons, très tendres. Vous qui découvrirez ce documentaire, abandonnez tout esprit critique. On est prié de s’attendrir. « 

La journaliste accuse ce reportage de faire de la propagande socialiste mièvre, tronquée de sa réalité. Donc forcément un documentaire « bisounours » dans lequel on serait prié de couper tout esprit critique à cause de la tendresse mise en avant. Cette journaliste aurait-elle peur de ressentir des émotions positives devant ce documentaire ? Ne peut-on trouver quelque chose de tendre tout en faisant usage de sa raison ? L’esprit critique est entre les mains de chacun, depuis quand des images lentes (si elles avaient été subliminales, ok)  aurait le pouvoir de nous en priver ? Le bonheur et la tendresse seraient selon elle irréalistes et propagandistes (surtout chez les gays apparemment). Voilà ce qu’on aura « appris  » de cet article décrié aussi par les lecteurs du figaro.

**→ *Le brutalisme des activistes et du militantisme***

<http://www.agoravox.fr/tribune-libre/article/le-monde-des-bisounours-et-du-102494>

Article très intéressant, dont nous pourrions être en accord sur certains points. Il n’empêche que les conceptions intrinsèques sont brutalistes, l’auteur défendant la violence au nom de la liberté et des idées:

>  » Hélas ce n’est pas demain la veille que nos députés feront le coup de poing à la Chambre comme les Russes l’osent encore à la Douma pour défendre leurs idées même quand ils ne sont pas bourrés. La liberté de penser, la vrai, ce n’est pas la soupe de Florent Pagny pour échapper au fisc, c’est celle de Valmy, c’est celle des viticulteurs contre lesquels l’armée refusa de tirer au début du siècle dernier. Qui est encore capable de prendre des coups pour défendre ses idées, la veuve et l’orphelin, ou ses droits fondamentaux dont celui de faire chier ceux que l’on n’aime pas. Non, il faut désormais être consensuel, aspirer à une démocratie castratrice à la suédoise où il n’y a plus officiellement ni putes ni fessée. »

L’article se termine ainsi:

> « Il n’est pas question de prôner la guerre civile en France, mais tout simplement de fustiger la lâcheté. »

Nous pensons tout justement à l’inverse de cet article que la lâcheté et la facilité sont du côté de ceux qui cognent tout autant que ceux qui se résignent à l’indifférence.

Stephan Hessel y est cité:

> « Il faut donc acheter le petit pamphlet de Stéphane Hessel et s’indigner en bêlant. Ça ne mange pas de pain, mais toute indignation devrait déboucher sur des actes quelles que soient ses opinions et ses aspirations. »

C’est oublier que l’appel pacifique à l’indignation de Stephan Hessel a eu des effets importants dans le monde (impulsant certains mouvements) et continue encore à inspirer anciennes comme nouvelles générations. Ce que doit sans doute reprocher l’auteur à Hessel, c’est d’être un de ces bisounours qui se refuse aux révoltes violentes, ce qui est pour l’auteur de l’article un signe de « lâcheté ».

Le titre de l’article est d’ailleurs fort révélateur: « Le monde des bisounours ou le risque zéro ». Il suggère que les problèmes actuels sont dus à un excès de bisounours. Nous pensons à l’inverse que si les choses vont mal c’est justement parce que nous manquons de bisounours.

** **

**Empathie, respect, bonnes intentions et altruisme : les grandes maladies du siècle?**

**\[attention, si ces prochaines lignes vous donnent envie de vomir pour raison de bons sentiments, vous souffrez sans doute de brutalisme aigu\]**

*[![](https://www.hacking-social.com/wp-content/uploads/2014/01/lempathiedusinge-1.jpg)](https://www.hacking-social.com/wp-content/uploads/2014/01/lempathiedusinge-1.jpg) *

> ***« *** ***Bisounours.** Expression destinée à dénigrer l’angélisme supposé d’un adversaire. Utilisé par l’*[*extrême-droite*](http://www.rue89.com/tag/extreme-droite) *pour ridiculiser la droite « molle », par cette même droite face à la gauche, et par des* [*socialistes contestataires*](http://24heuresactu.com/blog/2010/10/12/securite-un-elu-ps-denonce-la-vision-bisounours-du-parti/) *en direction de leurs camarades. En 2002 et 2007, ils disaient « bobos ». »*
>
> [***Rue 89***](http://www.rue89.com/2011/04/26/des-bisounours-a-zaz-les-nouveaux-cliches-des-politiques-199201)

Les bisounours dérangent, surtout les extrêmes. Le terme « bisounours » à dessein de discréditer une idée, un discours ou une action, est diversement utilisé dans les médias, de droite comme de gauche. Mais il faut tout de même garder à l’esprit que ce terme au sens péjoratif est d’abord un abus de langage de l’extrême droite. L’extrême droite est à la pointe des raccourcis de langage, de l’anti-intellectualisme, et ce simple mot « bisounours » de son cru a eu des répercussions incroyables: aujourd’hui tout le monde l’utilise, de droite comme de gauche, que ce soit un grand patron d’une entreprise côté en bourse ou un activiste anticapitaliste. Même les militants contre l’extrémisme se sont fait une joie de reprendre ce terme, en toute ignorance de ce qu’il renvoie. Un comble. On remarque d’ailleurs que la montée des extrêmes favorise l’usage de cette expression, ce qui n’est pas un hasard.

Mais qui sont ces bisounours? Sont-ils des naïfs, des gens manquant de sérieux souffrant d’un angélisme sans limites? Des lâches qui ne veulent prendre aucun risque?

Nous voulons bien croire que ce type de portrait puisse correspondre à certains, mais en général, le bisounours n’a rien à voir avec cette caricature de mauvaise foi.

Nous avons l’habitude de côtoyer ces gens bien intentionnés que les brutalistes nomment bisounours (ou expression équivalente), et il est nécessaire de proposer un nouveau portrait de ces « bisounours » qui dérangent.

Ceux qu’on traite de bisounours ne sont pas de lâches angélistes idéalistes la tête dans les nuages; ce ne sont pas des hommes ou des femmes passifs et inactifs; ce ne sont pas des naïfs aveugles manquant « cruellement » de sérieux ou incapables de s’adapter à la réalité.

Si les « bisounours » sont malmenés, c’est parce qu’ils ne collent absolument pas au paradigme dominant qu’est le brutalisme. Ils ne répondent pas aux conventions, à l’attitude généralement admise. Ils ne visent pas la « réussite », ne sont pas mus par l’ambition, ne s’intègrent pas nécessairement dans une lutte et ne cherchent pas à produire du spectaculaire. Pire, ils font tout pour ne jamais nuire à autrui, ils évitent au possible la pensée binaire, et ils ne considèrent pas que la fin justifie les moyens. En cela, ils sont une menace pour la majorité, car ils ne rentrent pas dans le moule. De tout côté, politique, travailleur, activiste, révolutionnaire, bancaire (…) les bisounours sont rejetés.

Le bisounours s’impose des limites. Il sait que le « pouvoir faire » ne légitime pas le « faire ». Le bisounours n’a pas une vision idéalisée de l’espèce humaine. Bien au contraire, il voit très nettement le monde tel qu’il est, mais contrairement au brutaliste il ne s’arrête pas à dire « c’est comme ça ». On lui reproche tout justement de dire « ça pourrait en être autrement », et le bisounours ajouterait sans doute « et sans user de violence, qu’elle soit physique ou morale ».

Le bisounours n’emprunte pas les sentiers les plus faciles. Il ne vise pas l’efficacité. Abandonnant toute activité belliciste, il doit faire preuve de patience et d’imagination pour arriver à ses fins. Le bisounours n’est pas ce type de révolutionnaire, le fusil à la main, prenant part à une révolte de masse. Il n’est pas nostalgique des conflits révolutionnaires. Le bisounours n’est pas violent, dans les actes comme dans la parole, car il sait que cette attitude est contreconstructive, qu’elle ne fait qu’envenimer la situation. La force morale requise pour cela est sans doute plus importante que celle du plus énergique des brutalistes. La véritable force est du côté de celui qui parviendra à ne jamais frapper. Car le bisounours garde toujours à l’esprit que les bonnes intentions et les nobles causes mènent parfois aux pires dérives. Le bisounours donnerait raison à la phrase de Jean Rochefort « Nous sommes tous possiblement des monstres. (..) Il faut faire gaffe à nous ! De grandes boucheries peuvent démarrer sur un rien… ».

[![](https://www.hacking-social.com/wp-content/uploads/2014/01/bisounours-fleur-1.jpg)](https://www.hacking-social.com/wp-content/uploads/2014/01/bisounours-fleur-1.jpg)

Le bisounours est un authentique pacifiste, le dernier des pacifistes d’ailleurs. On nous présente bien souvent dans les médias des « pseudopacifistes » que l’on nomme ainsi sous prétexte qu’ils n’ont en acte aucune violence physique. Or, bien souvent, leur violence n’est pas dans leurs poings mais dans leurs bouches. Un pacifique refuse toute violence, quelles qu’elles soient. Un individu qui n’usera jamais de violence physique mais qui ne cessera de cracher son venin ou qui sera mu par la colère ou la haine n’est pas un pacifiste, c’est un belliciste.

À noter que le brutaliste fera tout pour discréditer le pacifisme incitant à la confusion entre le pacifisme et la lâcheté (voire à la collaboration). Le brutaliste, dans ses arguments bellicistes, fait parfois référence à ces pacifistes irraisonnés de l’occupation qui prônaient la collaboration au nom de la paix. Cet anti-pacifiste primaire en oublie (volontairement sans doute) tous ces pacifistes qui se sont engagés dans la résistance bien conscients des périls et de la situation exceptionnelle. C’est là la différence entre un pacifisme idéologique, qui peut conduire à la dérive, et un pacifisme concret, jamais complètement acquis qui se construit chaque jour selon les opportunités et les périls et dont le but est de faire reculer les violences quelles qu’elles soient. Ne mélangeons pas les deux.

Pour le bisounours, il est nécessaire d’améliorer autant que possible son environnement, de le faire éventuellement à plusieurs et toujours à hauteur d’homme (et non selon un idéal inaccessible). Il vise son environnement immédiat, il sait qu’il n’a aucune prise sur le monde en son entier, mais qu’il peut toutefois changer des petites choses. Il y a donc une certaine modestie dans ses actions, et c’est notamment le reproche que certain lui font: « ce que tu fais ne sert à rien! » ou « ce que tu fais c’est du détail, tu cherches la petite bête ». Mais le bisounours est patient, et il s’appuie sur un principe tout simple aux proportions concrètes: « c’est en s’occupant d’abord des petites bêtes qu’on finira par atteindre les plus grosses ».

Avoir de l’empathie, être encore en capacité d’être choqué par des violences (physiques ou verbales) qui sont devenus banalités, voilà des sentiments qui sont de plus en plus difficiles à assumer, car perçu comme une forme de faiblesse, là où réside pourtant une véritable force morale.

Disons-le sans détour pour celui qui pratique cette empathie, ce refus de la brutalité, ces modestes contributions pour améliorer les choses, celui-là qui est étiqueté de bisounours n’est rien d’autre qu’un « être humain », ni plus ni moins (espèce semble-t-il en voie de disparition). Les tenants du brutalisme n’ont que faire de la dignité humaine, ou peu, ils font tout pour s’en séparer, et n’hésitent pas à coller cette étiquette de « bisounours » à ceux et celles qui ont encore la force de résister.

Discréditer un individu en invoquant l’argument du Bisounours, inculper l’autre de « crime de bonne volonté » ou de « crime de la non-violence » (=lâche pour les brutalistes), voilà dans un premier temps ce qu’il nous faut tous refuser.

**Les révolutions à l’ancienne, c’est terminé!**

Certains considèrent que lorsque l’on veut défendre des idées, lorsque l’on veut faire bouger les lignes, cela ne peut se faire sans violence. On voit de plus en plus resurgir le vieux spectre de la violence idéologique, les prêtres de l’anti-courtoisie, ceux qui prônent le droit à la soumission ou à l’écrasement au nom de la liberté, ceux qui prétendent qu’il faut se taper dans le pif pour défendre des convictions… Bref, on ne cesse de nous rappeler que des gens sont prêts à se battre, à se taper dessus. Mais pour quoi? Ont-ils véritablement un but, une visée à long terme? Ne veulent-ils pas tout simplement se taper dessus pour se taper dessus? Le brutaliste ne défend pas des causes ou des intérêts, il attaque, les causes et les intérêts lui servant de prétexte; il n’est pas homme de conviction, ses convictions ne sont que des motifs pour frapper et écraser; il n’est pas courageux, il est téméraire et impatient, avide qu’on lui donne toute l’importance qu’il croit mériter. Ce n’est pas un « fort » comme il ne cesse de le crier, c’est une *brute* qui se cache derrière ses apparats, un individu qui a peur du vide, qui ne cesse de se débattre dans une mer agitée qui le malmène de tous côtés.

C’est avec tristesse que nous constatons que de nobles causes dérivent trop souvent, tout justement selon ce principe brutaliste absurde que l’on n’arrive pas à ses fins sans s’imposer violemment: ceux-là mêmes qui au nom de l’humanité tuent l’humanité; qui au nom de la liberté de penser crachent sur les idées des autres; qui au nom de la paix font résonner le glaive; qui au nom de l’égalité écrasent les plus faibles; qui au nom de la prospérité, appauvrissent la majorité; qui au nom de la vie, condamnent à mort; qui au nom de quiétude ne cessent d’invoquer la peur…

La lutte acharnée est toujours la mauvaise solution, et lorsque la lutte reste la dernière solution, le dernier recours (car il y a évidemment des situations extrêmes où nous sommes contraints de lutter) c’est que nous avons auparavant manqué toutes les occasions qui nous auraient permis d’éviter cette impasse.

Ce ne sont pas de soldats ou de guerriers dont nous avons besoin, mais de bâtisseurs, de constructeurs, de créateurs, de docteurs…Bref,  on a besoin de bisounours.

[![](https://www.hacking-social.com/wp-content/uploads/2014/01/1ae49da1-1.gif)](https://www.hacking-social.com/wp-content/uploads/2014/01/1ae49da1-1.gif)Lorsqu’un bisounours cherche à améliorer les choses, les révolutions qu’il entreprend sont bien différentes de celles des brutalistes.

Les formes de révolutions sont en effet à réinventer, en prenant soin d’écarter toute inclination belliqueuse. On n’améliore pas les choses avec du ressentiment, de la colère, de la haine ou du mépris. On n’améliore pas les choses avec la nostalgie du passé. On ne peut plus, dans les pays occidentaux, se borner à cette naïveté d’un autre âge où il suffirait de renverser brutalement un pouvoir en place pour que tout s’arrange.

Les révolutions du bisounours ne peuvent pas être utopiques, car la tentative d’imposer une utopie est un projet qui, au mieux, ne peut aboutir complètement, et qui, au pire, instaure sa propre négation. De plus, les utopies sont globales et relèvent d’une idéologie qui cherche à s’imposer, ce qui nous ramène au brutalisme.

Les révolutions du bisounours se font par le bas, non en brandissant la fourche, mais en usant le langage, la ruse, la créativité, l’expérimentation. Les révolutions du bisounours sont parcellaires et modestes, nul besoin de changer le monde ou un pays, c’est son environnement immédiat que l’on doit d’abord toucher: ses voisins, sa famille, ses collègues au travail; sans oublier bien entendu de magnifiques outils tels qu’Internet qui peuvent créer une proximité où il y a éloignement, ce qui ouvre des perspectives infinies. Les bisounours ne s’incorporent pas dans des mouvements de masse, ils entreprennent des actions individuelles ou en petit groupe.

Cela paraît moins spectaculaire, plus lent dans les effets et les conséquences (d’où le désintéressement des médias qui retransmettent les grandes manifestations et autres accrochages, mais qui couvre trop peu (ou mal) les véritables concrétisations à l’échelle locale, et pourtant il n’en manque pas); cela demande plus d’efforts, une réflexion soutenue, des expérimentations qui n’aboutiront pas systématiquement…. Mais nul doute qu’à long terme les efforts portent leurs fruits, le brutaliste lui-même peut le constater, s’il daigne prendre lui-même le temps de l’observer et la bonne foi de le reconnaître.

**Le bisounours ne se bat contre personne. Il ne lutte pas, il construit.**

On ne peut pas demander ou impulser un changement au nom d’un *contre,* d’un *anti*, ou d’une opposition. Une opposition est la face d’une même pièce. Comment une opposition peut-elle prétendre à une véritable alternative alors qu’elle n’est que le versant opposé de ce qu’elle « combat »? La seule alternative possible ne peut être que hors cadre, en dehors de l’arène des conflits; non par réaction, mais par construction.

Nous avons besoin de concrétisation et de propositions plus que d’oppositions, car les oppositions sont condamnées au surplace, elle ne permettent pas d’avancer.

Le bisounours n’attend pas les politiques, il cherche des solutions concrètes applicables ici et maintenant.

Si les Bisounours sont minoritaires, qu’ils sont rejetés par le plus grand nombre, alors les bisounours incarnent la subversion par excellence.

Alors, amis subversifs, assumez-vous sans tabous et sans aucune honte. Assumez-vous bisounours.

Et le hacking social dans tout ça ? Est-ce un projet de bisounours ?

Au sens où nous venons de définir ce terme: oui, mon capitaine, sans aucun doute.

**Il est commun de croire qu’il n’y a que deux voies possibles au changement:**

Laisser faire

**ou**

Combattre

** **

**Nous suivons une troisième voie:**

Expérimenter et construire

** **

**Cette troisième voie est celle du** hacker social**,**

**un** authentique bisounours**.**

 

[\[1\]](#_ftnref1) Ce que nous nommons « brutalisme » n’a aucun rapport avec le style architectural du même nom.
