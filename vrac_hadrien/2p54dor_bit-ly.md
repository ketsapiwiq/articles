---
title: ""Que voulez-vous faire des juifs ?""
url: http://bit.ly/2P54dor
keywords: nest,voulezvous,faire,juifs,lallemagne,vouée,transparaître,presse,duhamel,projet,écrivain,schneidermann
---
**Certains romanciers ne méritent pas** d'être effacés par le temps. C'est le cas de Georges Duhamel, dont l'oeuvre a désormais l'odeur des vieux livres jaunis des bibliothèques de nos parents ou grands-parents. Elle n'est pas désagréable, c'est juste l'odeur du passé. Georges Duhamel est pourtant un auteur qui savait voir l'avenir, même le plus terrible.

Je ne connaissais pas l'article d'une effroyable claivoyance qu'il a, dans «le Figaro», consacré à l'antisémitisme nazi, le 23 juin 1938 - avant la Nuit de Cristal, donc. Merci Daniel Schneidermann, qui me l'a fait découvrir dans son dernier livre, «Berlin, 1933» (dont Xavier de La Porte nous a déjà parlé [dans ce Screenshot-là](https://bibliobs.nouvelobs.com/screenshot/20180927.OBS3055/pourquoi-les-journalistes-ont-ils-sous-estime-hitler.html)). Dans cette passionnante enquête, le critique des médias d'Arrêts sur Images examine à la loupe comment la presse a traité de la montée du nazisme - une façon d'inviter les journalistes d'aujourd'hui à rester vigilants, à l'heure de la montée des nationalismes que ce soit en Europe ou outre-Atlantique.

[SCREENSHOT \#10. Pourquoi les journalistes ont-ils sous-estimé Hitler ?](https://bibliobs.nouvelobs.com/screenshot/20180927.OBS3055/pourquoi-les-journalistes-ont-ils-sous-estime-hitler.html)

L'article de Duhamel est titré «Une entreprise vouée à l'échec». L'auteur, qui à l'époque ne manque pas une occasion de dénoncer le nazisme, s'y interroge froidement sur la faisabilité du projet antisémite de l'Allemagne nazie. Il apostrophe de façon cash le régime hitlérien: «Que voulez vous faire des juifs?»:

Comme l'Allemagne moderne a toujours déclaré ses dessins avec une sorte de candeur monstrueuse dont il faut quand même lui savoir gré, je ne crois ni vain ni malséant de poser directement aux docteurs du nouveau Reich la question qui nous occupe et de leur dire: “Que voulez-vous faire des Juifs?” L'Allemagne espère-t-elle d'intimider les Juifs, et de leur donner ce qu'on appelle une correction? Je ne le crois pas. Pour le spectateur attentif et impartial, il semble bien que l'Allemagne ait entrepris l'abaissement puis l'extermination et, en définitive, l'extirpation totale de l'élément israélite.

Il conclut que la seule façon de le mener ce projet à son terme serait d'exterminer le peuple juif, non seulement en Allemagne, mais partout dans le monde. Impensable, selon lui : l'entreprise est donc «vouée à l'échec».

Si l'Allemagne veut détruire Israël sur toute l'étendue de son territoire, la faim et l'isolement ne suffiront pas. Il faut très peu de choses pour subsister et attendre. Beaucoup de Juifs périront, mais il en restera bien assez pour ensemencer et repeupler de nouveau, plus tard, la Germanie tout entière. L'Allemagne en arrivera-t-elle à des exécutions massives? Je ne parle pas des réactions que de tels actes provoqueraient dans la conscience universelle; je me contente de dire que ce serait une œuvre très difficile. \[…\] D'ailleurs, le problème est plus complexe. Pour anéantir l'élément juif à l'intérieur de ses frontières, l'Allemagne devrait le supprimer aussi dans toutes les nations du globe.

[SCREENSHOT \#21. Le sucre est-il plus fort que la cocaïne ?](https://bibliobs.nouvelobs.com/screenshot/20181019.OBS4200/le-sucre-est-il-plus-fort-que-la-cocaine.html)

Dans la page suivante, Schneidermann s'arrête sur ce passage:

Je m'abstiendrai, car ce n'est vraiment pas nécessaire, de juger l'opération du point de vue moral, ni de laisser transparaître l'horreur qu'elle ne peut manquer d'inspirer à des hommes dignes de ce nom.

![](https://media.nouvelobs.com/ext/uri/ureferentiel.nouvelobs.com/file/16659793.jpg)

(Extrait de “Berlin, 1933 - La Presse internationale face à Hitler”,  
de Daniel Schneidermann, Le Seuil)

Duhamel se borne, constate Schneidermann, à cette «étrange apostrophe» sur le thème: «ça ne marchera pas». Pourquoi cette retenue? Le journaliste n'a pas la réponse. Mais il «pressent» que Duhamel, écrivain bourgeois et insoupçonnable de penchants communistes, se prémunit contre «les attaques des Zemmour de l'époque, contre sa supposée bien-pensance, son politiquement correct.» 

[SCREENSHOT \#22. Qu’est-ce qui pervertit la jeunesse ?](https://bibliobs.nouvelobs.com/screenshot/20181029.OBS4610/qu-est-ce-qui-pervertit-la-jeunesse.html)

Cette interprétation un brin anachronique ne me convainc pas. Quoi que feigne d'en dire Duhamel, il laisse bel et bien «transparaître» dans son texte, par le truchement d'une habile prétérition, l'horreur que lui inspire «l'opération». Il a simplement choisi de ne pas centrer son article sur son propre dégoût, afin de lui donner plus de force. En bon écrivain, il sait que rien n'est plus glaçant que la présentation clinique des faits ou de raisonnements. Pour dénoncer une monstruosité, l'indignation est rarement le procédé le plus efficace, jamais le plus violent.

**Pascal Riché**

**Retrouvez [ici tous nos Screenshots](http://bibliobs.nouvelobs.com/screenshot/).**
