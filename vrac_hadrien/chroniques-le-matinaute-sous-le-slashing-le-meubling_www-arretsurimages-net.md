---
title: "Sous le slashing, le meubling - Par Daniel Schneidermann"
url: https://www.arretsurimages.net/chroniques/le-matinaute/sous-le-slashing-le-meubling
keywords: soif,meubling,révélions,sagit,étude,cest,planning,daniel,temps,slashing,presse,lci,schneidermann
---
Vous connaissez le slashing ? Sinon, vous allez connaitre.  C'est le fait de cumuler plusieurs boulots. Cette pratique "séduit de plus en plus", [c'est LCI qui le dit](https://www.lci.fr/emploi/le-slashing-seduit-de-plus-en-plus-un-francais-sur-trois-pret-a-cumuler-deux-emplois-etude-opinionway-horoquartz-2111479.html), photo à l'appui : il s'agit de "casser une routine, ou de naviguer dans des mondes différents". Bon. En réalité, c'est le quotidien sordide de salariés contraints de cumuler deux ou plusieurs jobs pour boucler les fins de mois. Pourquoi cette enquête sur le slashing ? Parce que LCI s'est laissé fourguer un sondage bidon commandé par un marchand de "solutions de gestion des temps et planning", nommé Horoquartz, [comme l'a relevé](https://twitter.com/peultier/status/1090294280999047168) mon excellent confrère de Libération, Frantz Durupt . 

C'est quoi, une "solution de gestion du temps" ? Il s'agit, si j'ai bien compris, dans les entreprises, de mieux planifier le travail des chargés de planning (ou si vous préférez, de passer moins de temps à gérer le temps des autres). [Mais je vous laisse découvrir](https://www.horoquartz.fr/gestion-des-temps/gestion-des-temps/38-gestion-des-temps.html). Vous m'expliquerez.

Je ne vais pas parler ici de cette nième tentative de maquiller la précarité et les "bullshit jobs" sous les couleurs flashy du fun,  mais de ces "enquêtes" sociétales aux origines opaques et aux objectifs sournois, qui fleurissent dans la presse en toute saison. Car celle-ci m'en rappelle d'autres. Vous aviez entendu parler de cette étude sur l'absentéisme, révélant qu'un salarié du privé est absent 17 jours par an ? En fait, elle était commandée par un cabinet conseil vendant des formations en "management de l'absentéïsme" [(nous vous le révélions ici)](https://www.arretsurimages.net/articles/absenteisme-letude-tres-orientee-qui-a-fascine-les-medias).  

Et cette prolifération d'études hautement scientifiques sur l'hydratation, dans les pages santé de la presse, nous persuadant que nous avons toujours soif, même quand nous n'avons pas soif ? Elles provenaient d'un cabinet créé et financé par...Coca Cola [(nous le révélions ici)](https://www.arretsurimages.net/articles/comment-coca-abreuve-les-etudes-sur-lhydratation).  

Et, plus ancien encore, en 2012 (et peut-être mon préféré), ce chiffre terrifiant de 200 000 Français qui seraient victimes chaque année d'une "usurpation d'identité" ? Là, la source était une étude du CREDOC financée par...vous ne devinerez jamais, un vendeur de broyeuses de documents. [Tout était raconté ici](https://www.arretsurimages.net/articles/derriere-la-psychose-des-usurpations-didentite-un-fabricant-de-broyeurs).  

La récurrence révèle assez que ce procédé "fait système", comme on dit : il existe un circuit, balisé, éprouvé, de recyclage de sondages biaisés en reportages "sociétaux", idéaux pour meubler le temps d'antenne, ou les pages des magazines, entre deux (vraies) pubs. La fake news est à la mode. Mais il ne faudrait pas oublier sa grande soeur, la junk news, providence de la presse paresseuse en mal de meubling. 

  

  
