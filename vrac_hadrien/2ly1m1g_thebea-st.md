---
title: "Inside Bannon’s Plan to Hijack Europe for the Far-Right"
url: https://thebea.st/2Ly1m1G
keywords: bannons,parties,inside,trump,rightwing,week,hijack,plan,national,farright,european,populist,bannon,movements,europe
---
LONDON—[Steve Bannon](https://www.thedailybeast.com/you-are-either-with-trump-or-you-are-against-him-says-bannon-as-putin-mayhem-tests-presidents-grip-on-gop?ref=home) plans to go toe-to-toe with [George Soros](https://www.thedailybeast.com/keyword/george-soros.html) and spark a right-wing revolution in Europe.

Trump’s former White House chief advisor told The Daily Beast that he is setting up a foundation in Europe called The Movement which he hopes will lead a right-wing populist revolt across the continent starting with the European Parliament elections next spring.

The non-profit will be a central source of polling, advice on messaging, data targeting, and think-tank research for a ragtag band of right-wingers who are surging all over Europe, in many cases without professional political structures or significant budgets.

Bannon’s ambition is for his organization ultimately to rival the impact of Soros’s Open Society, which has given away [$32 billion to largely liberal causes since it was established in 1984](https://www.opensocietyfoundations.org/explainers/open-society-foundations-and-george-soros).

Over the past year, Bannon has held talks with right-wing groups across the continent from Nigel Farage and members of Marine Le Pen’s Front National (recently renamed Rassemblement National) in the West, to Hungary’s Viktor Orban and the Polish populists in the East.

He envisions a right-wing “supergroup” within the European Parliament that could attract as many as a third of the lawmakers after next May’s Europe-wide elections. A united populist bloc of that size would have the ability to seriously disrupt parliamentary proceedings, potentially granting Bannon huge power within the populist movement.

After being [forced out of the White House](https://www.thedailybeast.com/trump-smites-bannon-hes-lost-his-mind) following internal wranglings that would later surface in the book Fire and Fury , Bannon is now reveling in the opportunity to plot his new European empire. “I'd rather reign in hell, than serve in heaven,” he said, paraphrasing John Milton’s Satan in Paradise Lost .

The Movement’s headquarters are expected to be located in Brussels, Belgium, where they will start hiring staff in coming months. It is expected that there will be fewer than 10 full-time staff ahead of the 2019 elections, with a polling expert, a communications person, an office manager and a researcher among the positions. The plan is to ramp that up to more like 25 people post-2019 if the project has been a success.

Bannon plans to spend 50 percent of his time in Europe—mostly in the field rather than the Brussels office—once the midterm elections in the U.S. are over in November.

The operation is also supposed to serve as a link between Europe’s right-wing movements and the pro-Trump Freedom Caucus in the U.S. This week Paul Gosar (R-AZ) was its envoy to Bannon’s operation in London.

Bannon and Raheem Kassam, a former Farage staffer and Breitbart editor, set up shop in a five-star Mayfair hotel for a week while Donald Trump was visiting Europe. Between TV appearances as Trump surrogates, they hosted a raft of Europe’s leading right-wingers at the hotel.

“It was so successful that we're going to start staffing up,” said Bannon. “Everybody agrees that next May is hugely important, that this is the real first continent-wide face-off between populism and the [party of Davos](https://www.thedailybeast.com/heres-how-and-why-trumps-going-to-blow-up-the-foundations-of-davos). This will be an enormously important moment for Europe.”

Having seen the shock right-wing victory with the Brexit referendum and Matteo Salvini’s electoral success in Italy, which were achieved on relatively tight budgets, Bannon sees the opportunity to boost radically disparate nationalist parties by deploying a well-financed centralized operation intended to blow local opponents out of the water.

Up until now insurgent populist groups across Europe have often suffered from similar problems: lack of expertise and finances. [Le Pen’s party was kept afloat by Russian loans back in 2014](https://www.thedailybeast.com/russias-putin-picks-le-pen-to-rule-france), when French banks refused to extend lines of credit for the Front National. Le Pen was back in Moscow shaking Putin’s hand before last year’s French elections, [which the NSA  subsequently revealed had been hacked by the Russians.](https://www.wired.com/2017/05/nsa-director-confirms-russia-hacked-french-election-infrastructure/)

The Movement plans to research and write detailed policy proposals that can be used by like-minded parties; commission pan-European or targeted polling; and share expertise in election war room methodology such as message discipline, data-led voter targeting and field operations. Depending on electoral law in individual countries, the foundation may be able to take part in some campaigns directly while bolstering other populist groups indirectly.

“I didn't get the idea until Marine Le Pen invited me to speak at Lille at the Front National,” recalled Bannon. “I said, ‘What do you want me say?’”

The response came back: “All you have to say is, ‘We're not alone.’”

Bannon was stunned to discover that the nationalist movements in Europe were not pooling skills and sharing ideas with populist parties in neighboring countries—let alone on a global scale.

Bannon said the Front National recognized that he was “the guy that goes round and understands us as a collective.”

Up on stage he told the crowd: “You fight for your country and they call you racist. But the days when those kind of insults work is over. The establishment media are the dogs of the system. Every day, we become stronger and they become weaker. Let them call you racists, xenophobes or whatever else, wear these like a medal.”

The former Trump campaign manager believes the fuse for the global populist revolt—now led from Washington, D.C. by his former boss—was lit 10 years ago during the financial crisis and President Barack Obama’s bailout of the broken financial sector. With income inequality growing, Bannon first championed Sarah Palin and then Donald Trump as vanquishers of the establishment elite who were capable of turning traditional politics on its head.

His next populist heroes can be found in Europe.

He sees Angela Merkel, the German chancellor, as the perfect foil to help accelerate that dynamic in Europe.

Noting Trump’s controversial decision to call out Merkel over her gas pipeline deal with Russia last week, Bannon said: “This is the lie of Angela Merkel. She’s a complete and total phony. The elites say Trump is disruptive but she’s sold out control to Russia for cheaper energy prices.”

He describes Merkel and Emmanuel Macron, the French president who crushed Le Pen in a runoff election last year but has since flagged in the polls, as vulnerable figureheads of establishment Europe. With Britain voting to quit the E.U., Merkel and Macron’s vision of a united continent will be put to the test at next year’s elections.

Bannon is convinced that the coming years will see a drastic break from decades of European integration. “Right-wing populist nationalism is what will happen. That’s what will govern,” he told The Daily Beast. “You're going to have individual nation states with their own identities, their own borders.”

The grassroots movements are already in place waiting for someone to maximize their potential. “It will be instantaneous—as soon as we flip the switch,” he said.

The sight of Brexit virtually upending the entire European Union with a campaign spending cap of £7 million ($9 million) was a great inspiration. “When they told me the spending cap was £7 million, I go, ‘You mean £70 million? What the fuck?!’ £7 million doesn’t buy anything. It doesn’t buy you Facebook data, it doesn’t buy you ads, it doesn’t do anything.”

“Dude! You just took the fifth largest economy in the world out of the EU for £7 million!”

This week, British officials ruled that the [Brexit campaign had not stuck to the legal limit](https://www.thedailybeast.com/its-official-the-brexit-campaign-cheated-its-way-to-victory?ref=author)—overspending by more than $600,000. There were also unofficial campaigns which spent additional millions arguing that Britain should leave the E.U.

Nonetheless, Britain’s GDP is around $2.6 trillion and leaked government figures estimate that [Brexit could wipe 10 percent off that figure](https://www.parliament.uk/documents/commons-committees/Exiting-the-European-Union/17-19/Cross-Whitehall-briefing/EU-Exit-Analysis-Cross-Whitehall-Briefing.pdf), meaning the impact of the democratic decision vastly dwarfs the scale of the investment by the campaign.

“The first thing they teach you at Harvard Business School is operating leverage,” said Bannon. With his expertise, contacts and financial backing, he is convinced that he can have an outsized impact all across Europe.

Bannon went to Italy to observe the campaign earlier this year as populist parties surged in the polls despite their tiny operations. “Look at Five Star and the Northern League,” he said. “They used their own credit cards. They took control of the seventh largest economy in the world—on their credit cards! It's insane.”

The two anti-establishment parties reached a coalition agreement that made Matteo Salvini deputy prime minister and put him in charge of the interior ministry two months ago. He has since shut Italy’s ports to NGO ships carrying rescued migrants and [called for a census of the Roma](https://www.independent.co.uk/news/world/europe/italy-matteo-salvini-video-immigration-mass-cleansing-roma-travellers-far-right-league-party-a8409506.html) community that may lead to mass deportations. Last year, he called for a radical crackdown on immigrants. “We need a mass cleansing, street by street, piazza by piazza, neighborhood by neighborhood,” he said.

Bannon sees Salvini as a model for his future Movement partners to follow. “Italy is the beating heart of modern politics,” he said. “If it works there it can work everywhere.”

He admitted that the scale of his right-wing coalition could be limited by the extreme positions of some of The Movement’s potential partners. “Some people may opt out because they think some of the guys may be too immigrant focused,” he conceded.

“We're not looking to include any ethno-nationalist parties in this although guys like the Sweden Democrats or the True Finns are perfect casting.”

Kent Ekeroth of the Sweden Democrats was one of those who met Bannon in Central London in the last week. The party, which had its roots in the Neo-Nazi and white supremacist movements of the 1980s, has [shot up to almost 20 percent in recent polls after adopting a more conventionally populist, anti-immigration message](https://www.reuters.com/article/us-sweden-politics-poll/support-for-anti-immigration-sweden-democrats-surges-ahead-of-september-election-idUSKCN1J117P).

Jérôme Rivière of Marine Le Pen’s Front National (Rassemblement National since June) also made the pilgrimage to London’s Mayfair, as did Mischaël Modrikamen of the People’s Party of Belgium, Nigel Farage of UKIP and Filip Dewinter of Vlaams Belang, a Flemish nationalist party formed in 2004 when its predecessor was found to be [in breach of a Belgian law on racism and xenophobia.](https://www.tandfonline.com/doi/abs/10.1080/01402380500085681?journalCode=fwep20)

Bannon said Farage and Le Pen would take the lead in figuring out the logistics of creating a new European parliamentary grouping that could be home to all of these parties and more.

Gosar, the Republican congressman, also stopped by Bannon’s London hotel. He was in Britain to attend a rally for the street protester and alt-right provocateur [Tommy Robinson](https://www.thedailybeast.com/far-right-media-falsely-claim-britain-sent-anti-muslim-activist-to-muslim-prison), who was recently jailed for contempt of court for breaching reporting restrictions on a trial. During his trip, Gosar accused the British government of jailing Robinson as part of a cover up of rape [perpetrated by “disgusting and depraved individuals”](https://www.independent.co.uk/news/uk/home-news/free-tommy-robinson-rally-arizona-congressman-paul-gosar-racism-islamophobia-a8454571.html) from Muslim immigrant communities, which he described as a “scourge.”

Bannon’s ambition is no less than to take a stranglehold on Europe in the same way that he believes Soros has been able to dominate proceedings in recent decades.  

“Soros is brilliant,” he said. “He's evil but he's brilliant.”

Bannon wants to fulfil that role on the right and he is not ashamed to assert his objectives. “I'm about winning. I'm about power,” he said. “I want to win and then I want to effectuate change.”

He is not afraid of being caricatured in the way that Soros has been vilified by the right. He compared it to the fallout from the Cambridge Analytica scandal. “Look at Chris Wylie \[the Cambridge Analytica whistleblower\]. He is saying ‘Bannon made psychological weapons.’ He's literally made me the most brilliant evil genius. I'm a Bond villain. I kind of dig it.”

Kassam, who worked closely with Bannon at Breitbart and followed him out the door of the populist news site, said The Movement was shaping up as a force that would subsume national politics.

“Forget your Merkels,” said Kassam. “Soros and Bannon are going to be the two biggest players in European politics for years to come.”
